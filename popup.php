<!doctype html>
<html lang="en-US" prefix="og: http://ogp.me/ns#" class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8,IE=9,IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://www.tg4.ie/wp-content/themes/tg4-starter/assets/css/main-min.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>
            $('#PromoVideo').on('loadstart', function (event) {
                $(this).addClass('bkg');
                $(this).attr("poster", "http://iphonewrd.com/img/loading.gif");
            });
            $('#PromoVideo').on('canplay', function (event) {
                $(this).removeClass('bkg');
                $(this).removeAttr("poster");
            });
        </script>
        <style>
            body {
                margin: 0;
            }
            
            .videoContent {
                padding: 10px;
                text-align: center;
            }

            video.bkg {
                background: black
            }
        </style>
        <title>Newsletter | TG4 | Súil Eile</title>
    </head>
    <body>
        <div class="header-wrapper">
            <div class="top-bar">
                <span class="logo-home"><img src="http://s3.amazonaws.com/tg4-docs/tg4-redesign-2015/wp-content/uploads/2015/10/TG4.png" alt="TG4 Logo" class="logo"></span>
            </div>
        </div>
        <div class="section-header">
            <h1 class="section-title">Nuachtlitir TG4</h1>
        </div>
        <div class="videoContent"><br />
            <?php
            if(isset($_GET['vid'])) {
                $videoID = $_GET['vid'];
            } else {
                $videoID = "https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2017/04/Turas-Bóthair.mp4";
            }

            if ($videoID != '') {
            ?>
                <!-- Google Analytics Tracking Code -->
                <script>
                    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
                    ga('create', 'UA-4024457-9', 'auto');
                    ga('send', 'pageview');
                </script>
            
                <video id="PromoVideo" width="40%" preload="auto" controls>
                    <source src="<?php echo $videoID; ?>" type="video/mp4">
                </video>
            <?php } ?>
        </div>
        <div align="center"><a href="javascript: window.close()"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/close-win-1.png" alt="Dún | Close Window">&nbsp;&nbsp;&nbsp;Dún | Close</a></div>
    </body>
</html>