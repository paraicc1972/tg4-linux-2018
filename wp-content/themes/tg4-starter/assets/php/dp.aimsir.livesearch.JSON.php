<?php
$UseDB = isset($_GET['ldb']) ? 'LIVE': 'TEST';

if ($UseDB === 'LIVE') {
	header('Content-Type: application/json; charset=latin1');
	$mysqli = new mysqli("tg4-linux-cluster1-cluster.cluster-ro-c1hcdfsgywwv.eu-west-1.rds.amazonaws.com", "root", "Ga1wayman", "tg4"); // LIVE: www.tg4.ie
} else {
	header('Content-Type: application/json');
    $mysqli = new mysqli("77.75.98.42", "root", "ga1way", "tg4"); // LIVE: old.tg4.ie
}

/* check connection */
if ($mysqli->connect_errno) {
    //printf("Connect failed: %s\n", $mysqli->connect_error);
    //header('Content-Type: application/json');
    echo 'null: DB';
    exit();
} else {
    //echo 'connected...'; 
}

$YAMS_ID = isset($_GET['l']) ? $_GET['l']: 'ie';
$c_name =  isset($_GET['c']) ? $_GET['c']: '';
$a_name =  isset($_GET['a']) ? $_GET['a']: null;

if ($c_name == ''){ // No City search term passed, stop processing
    //header('Content-Type: application/json');
    echo 'null: City Name';
    exit();
} elseif ($UseDB === 'LIVE') {
	$c_name = utf8_decode($c_name);
}

// Prepare the sql for the Stored Procedure call:
//$thesql = "CALL dp_searchDataPoints('" . $YAMS_ID . "','" . str_replace("'", "''", $c_name) . "',";
$thesql = "CALL dp_searchDataPointsV3('" . $YAMS_ID . "','" . str_replace("'", "''", $c_name) . "',";
if ($a_name == '' || $a_name == null){
    $a_name = null; // When we don't want the Stored Procedure to search on 'area' (county OR country) we pass a variable as 'null'
    $thesql .= "null";
    
} else {
	if ($UseDB === 'LIVE') {
		$a_name = utf8_decode($a_name);
	}
    $thesql .= "'" . str_replace("'", "''", $a_name) . "'";
}
$thesql .= ")";

//echo "<p>" . $thesql . "</p>";

/* Select queries return a resultset */
if ($result = $mysqli->query($thesql)) {
    //printf("Select returned %d rows.\n", $result->num_rows);
    
    if ($result->num_rows > 0) {
        $counter = 0;
        //header('Content-Type: application/json');
        echo '[';
        /* fetch associative array */
        while ($row = $result->fetch_assoc()) {
            if ($counter > 0){ // Add 'delimiter to JSON output
                echo ',';
            }
            $counter ++;
            /*
             * 
            echo '[' . $row["id"] . ', "' . str_replace('"', '\"', $row["c_name"]) . '", "' 
                    . str_replace('"', '\"', $row["c_name_html"]) . '", "'  
                    . str_replace('"', '\"', $row["a_name"]) . '", "'  
                    . str_replace('"', '\"', $row["a_name_html"]) . '"]' ;
             */
            echo '[' . $row["id"] . ', "' . str_replace('"', '\"', $row["c_name_html"]) . '", "' 
                    . str_replace('"', '\"', $row["a_name_html"]) . '"]' ;
            
            //printf ("%s, %s</br>", $row["c_name"], $row["a_name"]);
            
        }
        echo ']';
    } else {
        echo 'null: Query Result';
    }
    /* free result set */
    $result->free();
    
    /* close connection */
    $mysqli->close();
} else {
    /* close connection */
    $mysqli->close();
    echo 'null: Query Connection';
    exit();
}
?>