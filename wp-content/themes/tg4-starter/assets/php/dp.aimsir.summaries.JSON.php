<?php

$UseDB = isset($_GET['ldb']) ? 'LIVE': 'TEST';

if ($UseDB === 'LIVE') {
    $mysqli = new mysqli("tg4-linux-cluster1-cluster.cluster-ro-c1hcdfsgywwv.eu-west-1.rds.amazonaws.com", "root", "Ga1wayman", "tg4"); // LIVE: www.tg4.ie
    //$mysqli = new mysqli("localhost", "root", "Ga1wayman", "tg4"); // LIVE: www.tg4.ie
} else {
    $mysqli = new mysqli("77.75.98.42", "root", "ga1way", "tg4"); // LIVE: old.tg4.ie
}

if ($mysqli->connect_errno) { // test DB connection
    echo 'null: DB';
    exit(); // quit, connection failed
}

/* For a JSON call:
 *  - the id of the matched 'city' will be passed
 *  - DB query then made to get actual 'datapoint' cityname
 */
$searchID = isset($_GET['id']) ? $_GET['id']: '';
if ($searchID != "" && is_numeric($searchID)) { 
    $searchID = (float)$searchID;
} else {
    echo 'null: SearchID';
    exit(); //quit, non-numeric [id] passed
}

/* Get relevant datapoint name */
if ($result = $mysqli->query('SELECT datapoint_name FROM dp_qrygetdatapointname WHERE city_id=' . $searchID)) {    
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $city = $row['datapoint_name']; // Main variables for the Metra XML call below
    } else {
        /* close connection */
        $mysqli->close();
        echo 'null: datapoint_name';
        exit(); //quit, no datapoint found
    }
 } else {
    /* close connection */
    $mysqli->close();
    echo 'null: DB-datapoint_name';
    exit(); //quit, problem with DB query
}
/* close connection */
$mysqli->close();

/* Other variables for the Metra XML call */
$xmlURL = 'http://77.75.98.42:8080/WeatherDataServlet-1.0/weatherdata'; // URL source of servlet for XML call
$countDays = 4;
$afterDate = date("Y-m-d"); // start date value for XML call
$beforeDate = date("Y-m-d", strtotime("+" . $countDays . " day", strtotime($afterDate))); // end date value for XML call
// Limit XML doc to only datatypes required
$fields = 'maxTemperature,minTemperature,expectedWeatherIcon,periodStart,periodEnd';


/* Make the Metra XML call 
 *  - we have also limited this to 'periods' of 24hrs
 *  - This will return summary data for Irish cities/areas where the <periodStart> & <periodEnd> finish with '23:00:00'
 *  - It will also return correct summaries for the likes of 'Los Angeles'  where the <periodStart> & <periodEnd> finish with '07:00:00'
 *  - the '@' in [@simplexml_...] tells PHP to suppress error warnings, for example an I/O error, file not found etc.
 *			- If the call fails [$xml] will hold a value of [false]
 */
$xml = @simplexml_load_file($xmlURL . "?city=" . urlencode($city) . "&minperiod=24&maxperiod=24&after=" . $afterDate . "%2000:00:00&before=" . $beforeDate . "%2000:00:00&datatypes=" . $fields);
if (!$xml){
    echo 'null: Load XML';
    exit(); //quit, problem with accessing Metra XML data
}
//print_r($xml); //uncomment this line to check XML (loaded into simplexml object form) returned

/* Apply filter for the 'daily summaries'
 * - Omit the <SiteInfo> node that is always returned
 * - Only want to deal with <forecast> nodes that have children nodes of: <expectedWeatherIcon>, <maxTemperature> & <minTemperature>
 *      - this is a double check, the 3 child nodes should always exist for the parent, as the XML is currently formatted on 27/08/13
 */

// limit nodes to be parsed to [Forecast] nodes that contain child nodes [expectedWeatherIcon], [maxTemperature] & [minTemperature]
// - this is a double check, the 3 child nodes should always exist for the parent, as the XML is currently formatted on 27/08/13
$summaries = $xml->xpath("/WeatherData/Site/Forecasts/Forecast[Values/expectedWeatherIcon and Values/maxTemperature and Values/minTemperature]");
//print_r($summaries); //uncomment this line to check data after filter applied

if(empty($summaries)) { // check that there are daily summaries. Applying an xpath filter that returns no nodes results in a simplexml object with an empty array.
    echo 'null: Summaries';
    exit(); // no data, stop processing
} else {
    /* Prepare JSON output */
    header('Content-Type: application/json');
    echo '{"days":['; //Open JSON object notation
    
    $chkCountDays = 1; // counter to ensure we only return required number of 'daily summaries' based on value in [$countDays] set above
    foreach($summaries as $daySummary) {
        // get date value from the node we are parsing
        $date = ($daySummary->periodEnd);
        $date = substr($date, 0, strpos($date, ' '));
        echo '{"date":"' . $date. '"';
        echo ',"icon":"' . ($daySummary->Values->expectedWeatherIcon). '"';
        echo ',"mxTmp":' . ($daySummary->Values->maxTemperature);
        echo ',"mnTmp":' . ($daySummary->Values->minTemperature) . '}';
        
        $chkCountDays ++;
        if ($chkCountDays > $countDays) {
            break;
        } elseif ($chkCountDays >= 2) { // [$chkCountDays] will have been incremented to '2' on 1st iteration, only add JSON comma seperator on subsequent passes
            echo ',';
        }
    }
    echo ']}'; //Close JSON object notation
}
?>
