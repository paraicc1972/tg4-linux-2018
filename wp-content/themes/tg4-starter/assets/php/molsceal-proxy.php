<?php
ini_set('display_errors',0);
//error_reporting(E_ALL|E_STRICT);

/**
 * bcls-proxy.php - proxy for Brightcove RESTful APIs
 * gets an access token, makes the request, and returns the response
 * Accessing:
 *     URL: https://solutions.brightcove.com/bcls/bcls-proxy/bcsl-proxy.php
 *         (note you should *always* access the proxy via HTTPS)
 *     Method: POST
 *
 * @post {string} url - the URL for the API request
 * @post {string} [requestType=GET] - HTTP method for the request
 * @post {string} [requestBody=null] - JSON data to be sent with write requests
 * @post {string} client_id - OAuth2 client id with sufficient permissions for the request
 * @post {string} client_secret - OAuth2 client secret with sufficient permissions for the request
 *
 * @returns {string} $response - JSON response received from the API
 */

// CORS enablement
header("Access-Control-Allow-Origin: *");

// set up request for access token
$data = array();

$client_id     = '89894907-bd7d-477a-ba60-57a90b6fb45c';
$client_secret = '-kxRqGoIpJcsRs1SUF-s8ENoCLtYXF5o3OIuO5EazLgrbulzcMoBaIL4daLBTtAe_cC3ZBx5V9EvBJKY4KO5ag';
$auth_string   = "{$client_id}:{$client_secret}";
$request       = "https://oauth.brightcove.com/v3/access_token?grant_type=client_credentials";
$ch            = curl_init($request);
curl_setopt_array($ch, array(
        CURLOPT_POST           => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_SSL_VERIFYPEER => FALSE,
        CURLOPT_USERPWD        => $auth_string,
        CURLOPT_HTTPHEADER     => array('Content-type: application/x-www-form-urlencoded'),
        CURLOPT_POSTFIELDS => $data
    ));
$response = curl_exec($ch);
// Check for errors
if ($response === FALSE) {
    die(curl_error($ch));
}
curl_close($ch);

// Decode the response
$responseData = json_decode($response, TRUE);
$access_token = $responseData["access_token"];
$expires_in = $responseData["token_type"];
//var_dump($responseData);
//var_dump($access_token);
print_r($access_token);
?>