var wIconsFolder = "https://d1og0s8nlbd0hm.cloudfront.net/images/Aimsir/weather-icons/";
var wPrevSrch = ''; //used mainly to ignore spacebar being pressed: 'lon ' won't fire JSON call after last one of 'lon'.
var wSrchCity = ''; // will hold value of last searched city/area
var wSrchRegion = ''; // will hold value of last searched region
var wURL = ''; //the base URL to be passed to Google Analytics, this will be contained in a hidden field in the Search Form
var wTab_Conn = ''; //used to check if a hidden field has been passed to the HTML to flag the use of the LIVE MySQL Datapoints DB
var wArrIcons = new Array();//[wArrIcons] array generated from ModX to maintain one list of values & language strings 
wArrIcons['Cloud'] = ['Cloud-1.png','Cloud','Scamallach'];
wArrIcons['Drizzle'] = ['Drizzle-1.png','Drizzle','Ceobhr&aacute;n'];
wArrIcons['FewShowers'] = ['FewShowers-1.png','Few Showers','Be&aacute;gan muir na b&aacute;ist&iacute;'];
wArrIcons['Fine'] = ['Fine-1.png','Fine','Grianmhar'];
wArrIcons['Fog'] = ['Fog-1.png','Fog','Ceo'];
wArrIcons['Hail'] = ['Hail-1.png','Hail','Cloichshneachta'];
wArrIcons['PartCloudy'] = ['PartCloudy-1.png','Part Cloudy','Cuid Scamallach'];
wArrIcons['Rain'] = ['Rain-1.png','Rain','B&aacute;isteach'];
wArrIcons['Showers'] = ['Showers-1.png','Showers','Muir na B&aacute;ist&iacute;'];
wArrIcons['Snow'] = ['Snow-1.png','Snow','Sneachta'];
wArrIcons['Thunder'] = ['Thunder-1.png','Thunder','Toirneach'];
wArrIcons['Wind'] = ['Wind-1.png','Wind','Gaoth'];

/* Preload all Weater Icon images & the loading images immediately*/
var wArrLoadIcons = [];
for(var loopIdx in wArrIcons){
    wArrLoadIcons[loopIdx] = new Image();
    wArrLoadIcons[loopIdx].src = wIconsFolder + wArrIcons[loopIdx][0];
};
var wLoadingImg1 = new Image();
wLoadingImg1.src = wIconsFolder + 'loader40x40C_838383.gif'; // referenced in [aimsir.css]
var wLoadingImg2 = new Image();
wLoadingImg2.src = wIconsFolder + 'loader40x40C_383838.gif'; // referenced in [aimsir.css]

var wSuccCity = ''; // the last successfully loaded city (block of text) - used for roll back
var wPrevCity = ''; //the last shown city (block of text), may be in middle of loading details
// the variable [wNewCity] (block of text), the current city selected to be loaded,  will be passed to [fct_wDtls_getData()] when that div is clicked in the search results

var wJSON_Dtls; // variable to store the JSON function call for 'weather data' so that it can be cancelled if a subsequent call is made
var wJSON_Srch; // variable to store the JSON function call for 'cities' so that it can be cancelled if a subsequent call is made

var wSrchPrompt = {"en":"Enter a city/area name.","ie":"Cuir isteach ainm do cheantar."};
var wNoResults = {"en":"No matches.","ie":"Gan tortha."};
var wSearching = {"en":"Searching....","ie":"Cuardach...."};
var wSrchDtlsTag = {"en":"View search details.","ie":"Breathnaigh ar treoir cuardach"};
var wNoIconTxt = {"en":"Not available","ie":"N&iacute;l eolas ar f&aacute;"};

/*
jQuery(document).ready(function($){
    wSumm_init();
});
*/

function wSumm_init(){    
    //var $ = jQuery.noConflict();
    /*
     * Set variable pointers to commonly referenced jQuery elements/collections
     * - will not have to parse the DOM numerous times when referencing these elements/collections numerous times
     * - will also save on file-size of this script as fewer CHARS (each CHAR = 8bytes) will be used to reference elements/collections
     * - by not using the [var] keyword, all these variables have global scope.
     *      - document needs to be ready before we can bind these variables to their related DOM elements using jQuery
     *      - no need to declare these variables outside the jQuery [ready] function, so we avoid al those extra lines of code
     *      - all functions here are called/used only when the DOM is 'ready' to be manipulated
     *          - so all variables will have been initialised before they are referenced in any function below.
     */
    $wCity = $('h1.weather-summary'); // get a handle on 'city/area' name [h1] tag
    wSuccCity = $wCity.html(); // save the initially loaded/success city
    $wDays = $('.weather-day'); // get a handle on the 'weather days' collection of divs
    $wDay = new Array(); // get a handle on each weather day div
    $wDays.each(function(index) {
        $wDay[index] = $(this);
    });
    $wAllSum = $('.day-summary',$wDays); // get a handle on all weather day summary (icon) divs
    $wAllMx = $('.temp-high',$wDays); // get a handle on all weather day max temp divs
    $wAllMn = $('.temp-low',$wDays); // get a handle on all weather day min temp divs
    $wSrchList = $('#wSrchList'); // get a handle on the div for the city search results/messages
    $wSrchDtls = $('#wSrchDtls'); // get a handle on the div for the search instructions
    $wSrchTxt = $('#wSrchTxt'); // get a handle on the form textbox for the city search text
    wLang = $('#wLang').attr('value'); // get the language string from the hidden form textbox ('ie' or 'en', this is a constant)
    wURL = $('#wURL').attr('value'); // get the base URL to be used for Google Analytics
    //if ($('#ldb').attr('value')  !== undefined){
        wTab_Conn = "&ldb"
    //}
    
    // add event listeners to the 'search' textbox
    $wSrchTxt.keyup(function(){
       fct_wSrch_getData($.trim($wSrchTxt.val())); // get value of search text and remove any trailing/leading spaces, then pass to function to do the actual JSON search
    }).focus(function(){
        fct_wSrch_CheckShowResults();
        $wSrchDtls.hide();
    }).click(function(){
        fct_wSrch_CheckShowResults();
        $wSrchDtls.hide();
    }).mouseleave(function() {
        fct_wSrch_StartHideTimer();
    }).mouseenter(function() {
        fct_wSrch_CancelHideTimer();
        fct_wSrch_ChkShowDefault();
    });
    
    // add event listeners to the 'results' div
    $wSrchList.mouseleave(function() {
        fct_wSrch_StartHideTimer();
    }).mouseenter(function() {
        fct_wSrch_CancelHideTimer();
    });
    
    // add event listeners to the 'instructions' div
    $wSrchDtls.mouseleave(function() {
        fct_wSrch_StartHideTimer();
    }).mouseenter(function() {
        fct_wSrch_CancelHideTimer();
    });
    
    // add a 'click' event listener to hide 'results' if any other part of the page is clicked
    $('body').on('click.hide_wSrchLists', function(e) {
        if ($wSrchList.css('display') !== 'none'){
            var $clickedElem = jQuery(e.target);
            var chkID = $clickedElem.attr('id'); // used to check if anything other than the seach box is clicked, then hide search results
            /*  The functionality to handle clicking on a result has been moved to [fct_wSrch_ApplyResultActions()]
            *   - previously was using [var wC_ID = $clickedElem.attr('wc_id');] & [$clickedElem.text()] here but there was an intermittent problem:
            *       - Most times it worked fine but sometimes it was giving a [wC_ID] value of 'undefined' and [.text()] was giving a partial string ?????
            */
            if(chkID === 'wSrchDtlsIcon'){ // if user clicks on the 'instructions icon show/hide it
                $wSrchDtls.toggle();
            }else if(!(chkID === 'wSrchTxt'  || chkID === 'wSrchList' || chkID === 'wSrchDtls')){ // a list of the only things on page that can be clicked AND not have search divs close
                $wSrchList.hide();
                $wSrchDtls.hide();
            };
        };
    });
    
    //Check in case someone started typing a search before the document was ready
    fct_wSrch_getData($.trim($wSrchTxt.val()));
};

function fct_wSrch_getData(val){
    val = jQuery.trim(val);
    var idxComma = val.indexOf(',');
    if (idxComma === val.length-1){
        val = jQuery.trim(val.substring(0,val.length-1));
    };
    if (val === wPrevSrch) return; //no animation if: space bar is pressed, until another charachter is entered OR on initial check when DOM is ready and nothing typed into box
    
    wPrevSrch = val;
    fct_wSrch_ShowNewResults(wSearching[wLang]);
    // cancel any previouse request
    if (wJSON_Srch) {
        wJSON_Srch.abort();
        wJSON_Srch = null;
    };
    if (!fct_wSrch_ChkStr(val)){ // if no valid text entered
        fct_wSrch_ShowMsg(wSrchPrompt[wLang]);
    }else{
        
        //check if a city/area AND a region search made
        if (wSrchRegion !== ''){
            var qryRegion = '&a=' + encodeURI(wSrchRegion.substring(0,29)); // only pass upto 30 charachters
        }else{
            var qryRegion = '';
        };
        // make JSON call/request
        wJSON_Srch = jQuery.getJSON('http://77.75.98.42/assets/snippets/tg4/dp.aimsir.livesearch.JSON.php?l=' + wLang + '&c=' + encodeURI(wSrchCity.substring(0,29)) + qryRegion + wTab_Conn, //LIVE
        //wJSON_Srch = jQuery.getJSON('http://77.75.98.42/assets/snippets/tg4/dp.aimsir.livesearch.JSON.php?l=' + wLang + '&c=' + encodeURI(wSrchCity.substring(0,29)) + qryRegion + wTab_Conn, //LIVE
        //wJSON_Srch = jQuery.getJSON('http://localhost/search/searchdatapoints.php?l=' + wLang + '&c=' + wSrchCity.substring(0,29) + qryRegion,
            function(data){
               if (data !== null){
                    var out = '';
                    //var lenSrch = wPrevSrch.length; // as any subsequent JSON call cancells the previous one. the global variable [wPrevSrch] will hold the same value as the [val] that was passed to this call
                    var lenSrchC = wSrchCity.length;
                    var lenSrchR = wSrchRegion.length;
                    for(var ind=0; ind < data.length; ind++){
                        var city = data[ind][1];
                        var region = data[ind][2];
                        
                        if(city.substring(0, lenSrchC).toLowerCase() === wSrchCity.toLowerCase()){
                            city = '<b>' + city.substring(0, lenSrchC) + '</b>' + city.substring(lenSrchC, city.length);
                        };
                        if (wSrchRegion === ''){
                            if(region.substring(0, lenSrchC).toLowerCase() === wSrchCity.toLowerCase()){
                                region = '<b>' + region.substring(0, lenSrchC) + '</b>' + region.substring(lenSrchC, region.length);
                            };
                        }else{
                            if(region.substring(0, lenSrchR).toLowerCase() === wSrchRegion.toLowerCase()){
                                region = '<b>' + region.substring(0, lenSrchR) + '</b>' + region.substring(lenSrchR, region.length);
                            };
                        };
                        out += '<div wC_id = "' + data[ind][0] + '">' + city + ', ' +  region + '</div>';
                    };
                    fct_wSrch_ShowNewResults(out);
                }else{
                    fct_wSrch_ShowMsg(wNoResults[wLang]); // show no results message
                };
        });
    };
};

function fct_wSrch_ChkStr(txt){
    txt = jQuery.trim(txt);
    var idxComma = txt.indexOf(',');
    if (idxComma=== -1){ // area/city search
        wSrchCity = txt;
        wSrchRegion = '';
    }else{ // area/city AND region search
        wSrchCity = jQuery.trim(txt.substring(0,idxComma));
        wSrchRegion = jQuery.trim(txt.substring(idxComma+1,txt.length));
    };
    if (wSrchCity === '' && wSrchRegion === ''){
        return false;
    }else if (wSrchCity === '' ){
        wSrchCity = wSrchRegion;
        wSrchRegion = '';
    };
    return true;
};
    
function fct_wSrch_ShowNewResults(txt){
        $wSrchList.hide();
        $wSrchList.html(txt);
        fct_wSrch_ApplyResultActions(); // call function to bind mouseover styles and onclick action to newly created HTML elements (list of cities), elements must exist before you can bind/appy actions/styles to them.
        $wSrchList.show();
};

function fct_wSrch_ShowMsg(txt){
    var info = '<div id="wSrchDtlsIcon" title="' + wSrchDtlsTag[wLang] + '">?</div>';
    fct_wSrch_ShowNewResults(info + txt);
};

function fct_wSrch_ChkShowDefault(){
    var val = jQuery.trim($wSrchTxt.val());
    if (val === ''){
        fct_wSrch_ShowMsg(wSrchPrompt[wLang]);
    };
};  

function fct_wSrch_ApplyResultActions(){
    jQuery('div',$wSrchList).each(function(){
        jQuery(this).hover(function(){
            jQuery(this).toggleClass("dp_mouseover");
        }).click(function(){
            var wC_ID = jQuery(this).attr('wc_id'); // gets the City_ID from search list item clicked (Only divs in the search list have a [c_id] attribute)
            if(wC_ID !== undefined){ // [wC_ID] should always be valid
                fct_wDtls_getData(wC_ID, jQuery(this).text()); // pass the city_id & city_text to the function responsible for displaying the weather details. Using [.text()] instead of [.html()] to have all '<b>' tags removed.
            };
        });
    });
};

function fct_wSrch_CheckShowResults(){
    if ($wSrchList.html() !== '' && $wSrchList.css('display') === 'none'){
        $wSrchList.show();
    };
};

// If user moves the mouse away from search/results (maybe scrolls the page), hide results
var dpSrch_HideTimer = null;
function fct_wSrch_StartHideTimer(){
    dpSrch_HideTimer = setTimeout(function(){
        $wSrchList.hide();
        $wSrchDtls.hide();
    },1500);
};

function fct_wSrch_CancelHideTimer(){
    clearTimeout(dpSrch_HideTimer);
};

function fct_wDtls_getData(cityID, wNewCity){
    /*
     * Assign [$] for reference to [jQuery] object (this alias may nave been renamed in other scripts running on this page)
     * Scope limited to this function
     * Saves 5 X CHARS (each CHAR = 8bytes) for every use
     */
    //var $ = jQuery.noConflict();
    
    // hide the search results
    $wSrchList.hide();
    
    wPrevCity = $wCity.html(); // the current 'area/city' name shown
    
    //avoid any duplicate request, user clicked on same city in results more than once
    if(wNewCity === wPrevCity){
        return; // quit this function here
    };
    
    /*
     * Cancel any previouse request (user clicks another city in results before previous one has loaded)
     * - [wJSON_Dtls] stores an object reference, you can see this by typing this variable name into a browser console window and clicking enter 
     */ 
    if (wJSON_Dtls) {
        wJSON_Dtls.abort();
        wJSON_Dtls = null;
    };
    
    /*
     * If no 'loading' images are currently being shown when this function runs:
     * - the city name shown was successfully loaded
     * - we will use this city name (stored in [wSuccCity]) in case of error/rollback
     * - hide icons, min & max temps
     * - show loading images in each weather day div
     * If 'loading' images are currently being shown:
     * - we've already set a value for [wSuccCity] in a previous call to this function
     * - icons, min & max temps already hidden
     */
    if ($('.dploading', $wDay[0]).html() === null || jQuery('.dploading', $wDay[0]).html() === undefined){
        wSuccCity = wPrevCity;
        $wAllSum.hide();
        $wAllMx.hide();
        $wAllMn.hide();
        $wDays.append('<div class="dploading"></div>');
    };
    
    $wCity.html(wNewCity); // show the city currently being loaded
    
    fct_wDtls_SetMainTitles(wPrevCity,wNewCity); // update main title tags
    
    //update Google analytics here
    //if (typeof _gaq.push === 'function' && wURL !== "") { // check the Google function exists within the page
    //    var trackPageURL = wURL + 'id=' + cityID + '&city=' + escape(wNewCity);
    //    _gaq.push(['_trackPageview', trackPageURL]);
    //}
    
    //make JSON call to get contents          
    wJSON_Dtls = $.getJSON('http://77.75.98.42/assets/snippets/tg4/dp.aimsir.summaries.JSON.php?id=' + cityID  + wTab_Conn, //LIVE
    /* wJSON_Dtls = $.getJSON('/tg4-redesign-2015/wp-content/themes/tg4-starter/assets/php/dp.aimsir.summaries.JSON.php?id=' + cityID  + wTab_Conn, //TESTING
    wJSON_Dtls = $.getJSON('/wp-content/themes/tg4-starter/assets/php/dp.aimsir.summaries.JSON.php?id=' + cityID  + wTab_Conn,
    wJSON_Dtls = $.getJSON('/tg4/Metra/metraJSON3.php?id=' + cityID, */
        function(data){
            if (data!==null){ // successfully returned summary value for days
                var intIconText = (wLang === 'en')?1:2; //used to access the correct language string in array [wArrIcons]
                var indexLen = data.days.length;
                //NOTE: the number of day objects returned MUST match the count of weather day divs displayed in the page
                for(var index = 0; index < indexLen; index++){
                    // update details for that day
                    $('.temp-high', $wDay[index]).html('<span class="highTempLabel">High</span><br />' + data.days[index].mxTmp + '&deg;C');
                    $('.temp-low', $wDay[index]).html('<span class="lowTempLabel">Low</span><br />' + data.days[index].mnTmp + '&deg;C');
                    fct_wDtls_SetDtlTitles(index, '.temp-high', data.days[index].mxTmp + '&deg; celcius');// update 'title' for max temp    
                    fct_wDtls_SetDtlTitles(index, '.temp-low', data.days[index].mnTmp + '&deg; celcius');// update 'title' for min temp  
                    
                    // make sure that the 'expectedWeatherIcon returned' exists
                    if(wArrIcons[data.days[index].icon] !== undefined){
                        fct_wDtls_SetDtlTitles(index, '.day-summary', wArrIcons[data.days[index].icon][intIconText]);// update 'title' for icon
                        $('.day-summary img', $wDay[index]).attr('src', wIconsFolder + wArrIcons[data.days[index].icon][0]);
                    }else{
                        fct_wDtls_SetDtlTitles(index, '.day-summary', wNoIconTxt[wLang]);// update 'title' for icon to use 'no info..'
                        $('.day-summary img', $wDay[index]).attr('src', '');
                    }
                };
                //Set cookie to show the last searched city/area when user returns to the site
                //  - this relies on [jquery.cookie.js]
                if ($.isFunction($.cookie)){
                    $.cookie('dpCityID', cityID, { expires: 365, path: '/' });
                };
                
                //update google analytics after a successful JSON call/page update
                //if (typeof _gaq.push == 'function') { // check the Google function exists within the page
                        //var trackPageURL = $('#content_carousel > ul li a').eq(index).attr('href') + "&tab=" + index;
                       // _gaq.push(['_trackPageview', trackPageURL]);
                //};
            }else{ // no summary day data, rollback to last successfully shown 'city/area'
                $wCity.html(wSuccCity);
                fct_wDtls_SetMainTitles(wNewCity,wSuccCity);
            };
             //hide loading image
            $('.dploading', $wDays).remove();
            //show updated details
            $wAllSum.show();
            $wAllMx.show();
            $wAllMn.show();
    });
};

/* NOTES on updating 'title' tag attributes using jQuery:
* - It does some form of 'escaping' on charachters, HTML entity references not displayed/rendered correctly.
* - The following: $('#someElemID).attr('title', '10 &deg; celcius');
*       - Would show the ampersand etc and not the 'degree' charachter.
* - JQuery work-around: $('#someElemID).attr('title', jQuery('<div/>').html('10 &deg; celcius').text());
*       - Use jQuery to create a temporary <div> element 
*       - Use the jQuery [.html()] function to set the HTML content of this div to your text (that includes any HTML entity references)
*       - Then use the jQuery [.text()] function to get a handle back onto your text and pass this as the new value of
*           your 'title' tag attribute.
*/

function fct_wDtls_SetMainTitles(oldTxt,newTxt){
    $wCity.attr('title', $wCity.attr('title').replace(oldTxt,newTxt)); // update the <h1> title tag/attribute of the city
    var indexLen = $wDay.length;
    for(var index = 0; index < indexLen; index++){
        $wDay[index].attr('title', jQuery('<div/>').html($wDay[index].attr('title').replace(oldTxt,newTxt)).text()); // update the <h1> title tag/attribute of the city        
    };
};

function fct_wDtls_SetDtlTitles(index, elem, txt){
    theTitle = jQuery(elem, $wDay[index]).attr('title');
    theTitle = theTitle.substring(0,theTitle.indexOf(":")+2) +  txt;
    jQuery(elem, $wDay[index]).attr('title', jQuery('<div/>').html(theTitle).text());
};