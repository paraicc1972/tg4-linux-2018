/*
 * IMPORTANT NOTEs on passed vars (declared and initialised in the containing webpage): 
 *  - [wMap_data] cantains data to be used to creata an array of [wmap_tab] objects.
 *      - these objects will be stored in [theTabs] below
 *      - the count of these objects MUST MATCH the number of 'slider' dom elements in the calling webpage
 *  - [wmStartTab] is the initial tab to be shown.
 *      - this must be a valid index reference for the array stored in [theTabs]
 *  - The value of [wmCacheBuster] is appended to the [GET] request string for images. 
 *      This is generated on server and passed in calling webpage, ensures latest version of 
 *      weather images are always displayed, only loaded from local cache when appropriate.
 *      This will be adequately unique, formatted based on timestamp as: 'yyyy-mm-ddhhmm'
 *  - [wmThisURL] This will be set in calling page, including the '?' for querystring, and used 
 *      to update Google Analytics when 'tabs' are clicked
 */ 
 
var theTabs = []; //An array of [wmap_tab] objects that control the layout/functionality this will be loaded from data in [wMap_data] in the calling webpage
var wmTab_timeslot = []; // an array holding the timeslots text ('Early Morning','Morning',etc) this will be loaded from data in [wMap_time] in the calling webpage
var wmTab_day = []; // an array holding the timeslots text ('Early Morning','Morning',etc) this will be loaded from data in [wMap_time] in the calling webpage
var wmTab_imgPrefix = []; // an array holding the image prefixes text ('CloudAndRain-','TempAndWind-',etc) this will be loaded from data in [wMap_imgPre] in the calling webpage
var wmTab_imgType = [];// an array holding the image types text ('jpg',etc) this will be loaded from data in [wMap_imgType] in the calling webpage
var wmImgRetryMax = 5; // no of times to retry preloading of image
var wmImgRetryMilli = 400; // milliseconds between retying to preload an image
var wmImgFolder = 'https://d2dlsfk5bqexp7.cloudfront.net/images/'; // this is the filepath to the weather images folder

var wmSliderPointer = new Image();
wmSliderPointer.src = "https://d1og0s8nlbd0hm.cloudfront.net/images/Aimsir/wMap/Slider.png";
var wmPrevImg = new Image();
wmPrevImg.src = "https://d1og0s8nlbd0hm.cloudfront.net/images/Aimsir/wMap/Over_Back_Arrows-left.png";
var wmNextImg = new Image();
wmNextImg.src = "https://d1og0s8nlbd0hm.cloudfront.net/images/Aimsir/wMap/Over_Back_Arrows-right.png";
var wmSmallLoader1 = new Image();
wmSmallLoader1.src = "https://d1og0s8nlbd0hm.cloudfront.net/images/Aimsir/wMap/loader15x15C_383838.gif";
var wmSmallLoader2 = new Image();
wmSmallLoader2.src = "https://d1og0s8nlbd0hm.cloudfront.net/images/Aimsir/wMap/loader15x15C_838383.gif";
var wmBigLoader = new Image();
wmBigLoader.src = "https://d1og0s8nlbd0hm.cloudfront.net/images/Aimsir/wMap/loader80x80C_383838.gif";


var wmCurrentTab = -1; // Used for 'slider' functions. Saves us having to constantly check the DOM to see what 'tab' is active

// Constructor for 'tabs' objects. All Array variables passed should have the same number of elements.
function wmap_tab(tabname, tabSteps, tabLabels, tabImages, tabImageLabels, startPosition){
    // Public properties/variables
    this.name = tabname; // The name of the tab, should correspond to whats displayed on screen
    this.steps = tabSteps; // Array to define where you can 'step' to, 'ticks' are also shown here
    this.labels = tabLabels; // Array to store the labels to be shown with 'steps', a blank label uses a smaller 'tick' style
    this.imagesSrc = tabImages; // Array of strings to store the source/path of images for the slider
    this.imageLabels = tabImageLabels; // Array to store text to be overlaid on images as 'slider' is moved.
    this.imageLoadingCount = 0; // This is incremented by 1 as each image gets preloaded, used to check/set the [startImageLoaded] flag to [true]
    this.isActive = false;
    if(!isNaN(startPosition)){ // Check a valid number has been passed for the start image/step for that tab/slider
        this.startPosition = startPosition;
    }else{
        this.startPosition = 0;
    }
    this.imagesPreload = []; // Array to store preloaded images. Uses [...new Image();]
    this.imagesPreloadErrCnt = []; // Array to store how many times we've tried to reload an image that could not be found
    this.imagesPreloadRetry = []; // Array used to try and store reloaded images, when an error occurs. If successful, IMG moved to [imagesPreload]
    this.imageAjax = [];
    this.sliderMaxValue = tabSteps[tabSteps.length-1];
    this.sliderActiveSteps = [];
};

jQuery(document).ready(function($) {
    //wMap_init();
});
function wMap_init(){
    if(!isNaN(wmStartTab)){ // should always be set in calling webpage
        wmStartTab = 0;
    };
    // FIRST THING TO DO: 
    try{
        /* load data from [wMap_data] in the calling webpage into [theTabs] */
        var cntWmap = wMap_data.length;
        for (loopIdx = 0; loopIdx <cntWmap ;loopIdx++){
            // Create the 'tab' objects
            theTabs[loopIdx] = new wmap_tab(
                    wMap_data[loopIdx][0] // tabname
                    ,wMap_data[loopIdx][1] // tabSteps
                    ,wMap_data[loopIdx][2] // tabLabels
                    ,wMap_data[loopIdx][3] // tabImages
                    ,wMap_data[loopIdx][4] // tabImageLabels
                    ,wMap_data[loopIdx][5] // startPosition
                );
        }
        /* load data from [] in the calling webpage into [] */
        if (typeof wMap_time === "object"){
            wmTab_timeslot = wMap_time;
        }else{
            return;
        }
        /* load data from [] in the calling webpage into [] */
        if (typeof wMap_day === "object"){
            wmTab_day = wMap_day;
        }else{
            return;
        }
        /* load data from [] in the calling webpage into [] */
        if (typeof wMap_imgPre === "object"){
           wmTab_imgPrefix = wMap_imgPre;
        }else{
            return;
        }
        /* load data from [] in the calling webpage into [] */
        if (typeof wMap_imgType === "object"){
            wmTab_imgType = wMap_imgType;
        }else{
            return;
        }
    }catch(err){
        //Handle errors here
        return; // should never be an error, if there is then stop this script in its' tracks!!
    }
   
    //var $ = jQuery.noConflict();
    
    wmCurrentTab = wmStartTab;
    
    // 1) attach onclick events to the tabs
    $("#wmap_header ul li").click(function(){
        var tabIdx = $("#wmap_header ul li").index(this);// get a handle on 'tab' just clicked
        fct_wMap_CancelPreload(wmCurrentTab); // cancel any image preloading on the previous tab
        
        if (tabIdx === wmCurrentTab){ //if clicking on a tab that has alreay been selected
            if(fct_wMap_ChkTabPreloaded(wmCurrentTab) === false) fct_wMap_RunTabPreload(wmCurrentTab);
            return false; 
        }; 
        
        //update Google analytics here
        //if (typeof _gaq.push === 'function' && wmThisURL !== "") { // check the Google function exists within the page
        //        var trackPageURL = wmThisURL + 'TabClick=' + escape(theTabs[tabIdx].name);
        //        _gaq.push(['_trackPageview', trackPageURL]);
        //}
            
        fct_wMap_HideSlider(wmCurrentTab);    
        $("#wmap_header ul li.selected").removeClass("selected"); // flag previous tab as unselected
        fct_wMap_CancelPreload(wmCurrentTab); // cancel any image preloading on the previous tab
        
        /* look to match position on slider when moving between 'tabs'*/
        if(theTabs[wmCurrentTab].isActive === true){
            var tabValue = $('#wmap-slider' + wmCurrentTab).labeledslider('option', 'value');
            var matchText = theTabs[wmCurrentTab].imageLabels[tabValue]; // base matches on image text label as there may be different slider layouts between 'tabs'
            var loopCnt = theTabs[tabIdx].imageLabels.length;
            for (var loopIdx = 0; loopIdx < loopCnt; loopIdx++){
                if (theTabs[tabIdx].imageLabels[loopIdx] === matchText){ // found a matching timeslot on 'tab' to be moved too
                    if(theTabs[tabIdx].isActive === true){
                        if (typeof theTabs[tabIdx].imagesPreload[loopIdx] === "object"){ // the 'new 'tab' has successfully loaded the image for the matching slot
                            $('#wmap-slider' + tabIdx).labeledslider('option', 'value', loopIdx);
                        };
                    }else{
                        theTabs[tabIdx].startPosition = loopIdx; // before running the preloader for the tab clicked, set its' start position to the matching slot
                    };
                    break;
                };
            };
        };
        wmCurrentTab = tabIdx; // update reference to this, the currently selected 'tab'
        
        $(this).addClass("selected");// flag this tab as selected
        fct_wMap_ActivateTab(wmCurrentTab); // activate the tab that has just been clicked on
        
        return false; //stop the href action firing when tab is clicked
    });
    
    // 2) turn off loading icon on 'unselected' tabs as we are now ready to do stuff
    $('#wmap_header > ul > li > div.wmap-ldr-sml').each(function(idx){
	if(idx !== wmStartTab){
            $(this).hide();
        };
    });
    
    /* 3) this controls the mouseover/hover colour on the nav buttons
    * NOTE: this has to be within a 'document ready' function to correctly bind the event to the buttons
    */
    $('.wmap-button').hover(function(){$(this).toggleClass('wmap-button-hover');});
    
    // 4) this controls the 'next' button on the slider
    $("#wmap-next").click(function(){
        var slider = $('#wmap-slider' + wmCurrentTab);
        var value = slider.labeledslider("value");
        var index = getArrayIndex(value, theTabs[wmCurrentTab].sliderActiveSteps);
        if (index < (theTabs[wmCurrentTab].sliderActiveSteps.length - 1)){
            loadImg(theTabs[wmCurrentTab].sliderActiveSteps[index+1]);
            slider.labeledslider('option', 'value', theTabs[wmCurrentTab].sliderActiveSteps[index+1]);
        };
        return false;
    });

    // 5) this controls the 'previous' button on the slider
    $("#wmap-prev").click(function(){
        var slider = $('#wmap-slider' + wmCurrentTab);
        var value = slider.labeledslider("value");
        var index = getArrayIndex(value, theTabs[wmCurrentTab].sliderActiveSteps);
        if (index > 0){
            loadImg(theTabs[wmCurrentTab].sliderActiveSteps[index-1]);
            slider.labeledslider('option', 'value', theTabs[wmCurrentTab].sliderActiveSteps[index-1]);
        };
        return false;
    });
    
    // 6) Start preloading images for start/default tab.
    fct_wMap_ActivateTab(wmCurrentTab);
};

function fct_wMap_ActivateTab(tabIdx){
    /*
     * The next [if..] block is needed in case this 'tab' was partially preloaded and the user moved to another tab.
     * The user might then click back from an activated tab => the [.startPosition] for this 'tab' may have been altered 
     * and the 'activation' sequence in [fct_wMap_PreloadImgFinished] might never fire.
     */
    if(theTabs[tabIdx].isActive === false  && theTabs[tabIdx].startPosition !== -1  && fct_wMap_ChkImgPreloaded(tabIdx,theTabs[tabIdx].startPosition)){
        theTabs[tabIdx].isActive = true;
        loadImg(theTabs[tabIdx].steps[theTabs[tabIdx].startPosition]);
        fct_wMap_ShowSlider(tabIdx);
    }
    if(theTabs[tabIdx].isActive === true){
        var tabValue = jQuery('#wmap-slider' + tabIdx).labeledslider('option', 'value');
        loadImg(tabValue);
        fct_wMap_ShowSlider(tabIdx); // tab was previously activated so show it
    }else{
        fct_wMap_HideSlider(tabIdx);
    }; 
    if (!fct_wMap_ChkTabPreloaded(tabIdx)) fct_wMap_RunTabPreload(tabIdx); // check if preloading images is done/needs doing
};

function fct_wMap_CancelPreload(tabIdx){
    fct_wMap_TabLoadingIcon(tabIdx,false); // hide 'tab' loading icon
    var imgCnt = theTabs[tabIdx].imagesSrc.length;
    for(var loopIdx = 0; loopIdx < imgCnt; loopIdx++){
        if (typeof theTabs[tabIdx].imageAjax[loopIdx] === "object" && theTabs[tabIdx].imageAjax[loopIdx] !== null) theTabs[tabIdx].imageAjax[loopIdx].abort(); // cancel image preloader AJAX call
        clearTimeout(theTabs[tabIdx].imagesPreloadRetry[loopIdx]); // cancel any timers to retry preloading of image
        theTabs[tabIdx].imagesPreloadErrCnt = []; // erase any count of previous retry fails
    };
};

function fct_wMap_RunTabPreload(tabIdx){
    jQuery('#wmap-noinfo').hide(); //ensure 'no info..' message hidden, only shown when job here done and no images
    if (theTabs[tabIdx].isActive === false) jQuery('#wmap-loader').show();
    /*
     * On the 1st run of this function for a tab [imageLoadingCount] will be '0'
     * subsequent runs will only try to reload any failed/untried images
     * so set [.imageLoadingCount] to the successful count so that when it finishes
     * trying to load an image and the [.imageLoadingCount] is incremented in [fct_wMap_PreloadImgFinished]
     * we can detect when its job has been fully done
     */
    theTabs[tabIdx].imageLoadingCount = theTabs[tabIdx].sliderActiveSteps.length; 
    
    fct_wMap_TabLoadingIcon(tabIdx,true); //show loading icon
    /* Loop and queue images for preloading */
    var imgCnt = theTabs[tabIdx].imagesSrc.length;
    var startTabImage = theTabs[tabIdx].startPosition;
    // 1) preload the start/default image
    if (!fct_wMap_ChkImgPreloaded(tabIdx,startTabImage)) fct_wMap_PreloadImg(tabIdx, startTabImage); // start/default image not preloaded, do it now
    // 2) preload all images following the start/default image    
    for(var loopIdx = (startTabImage+1); loopIdx < imgCnt; loopIdx++){
        if (fct_wMap_ChkImgPreloaded(tabIdx,loopIdx)) continue; // this image loaded, skip rest of loop iteration & move to next image
        fct_wMap_PreloadImg(tabIdx, loopIdx);
    };
    // 3) preload all images prior to the start/default image, in reverse order 
    for(var loopIdx = (startTabImage-1); loopIdx >= 0; loopIdx--){
        if (fct_wMap_ChkImgPreloaded(tabIdx,loopIdx)) continue; // this image loaded, skip rest of loop iteration & move to next image
        fct_wMap_PreloadImg(tabIdx, loopIdx);
    };
};

function fct_wMap_PreloadImg(tabIdx, imgIdx){
    /* 'unpack' the image name & filepath */
    var imgNameArr = theTabs[tabIdx].imagesSrc[imgIdx].split(".");
    var imgPath = wmImgFolder // add folder path
            + wmTab_imgPrefix[imgNameArr[0]] // add image prefix
            + imgNameArr[1] // add image suffix 
            + '.' + wmTab_imgType[imgNameArr[2]] // // add image filetype 
            + '?' + wmCacheBuster;
    /* make the AJAX call to attempt image preloading */
    theTabs[tabIdx].imageAjax[imgIdx] = jQuery.ajax({
        type: "get", // use type of "HEAD" to ensure it works in IE7 - IE9
        url: imgPath, 
        success: (function(){
            return function(e){
                theTabs[tabIdx].imagesPreload[imgIdx] = new Image();
                theTabs[tabIdx].imagesPreload[imgIdx].src = imgPath; 
                
                // add a reference for this successfull preload to the [sliderActiveSteps] array.
                theTabs[tabIdx].sliderActiveSteps.push(theTabs[tabIdx].steps[imgIdx]); 
                theTabs[tabIdx].sliderActiveSteps.sort(function(a,b){return a-b}); //sort the active steps numerically
                
                fct_wMap_PreloadImgFinished(tabIdx, imgIdx); // call to increment 'loaded images' counter for this 'tab' & to check if 'loading' icon needs to be hidden
            };
        })(tabIdx,imgIdx),
        error: (function(tabIdx,imgIdx){
            return function(e){
                if(e.status === 404){ // file not found, check if another attempt at preloading should be done
                    fct_wMap_RetryPreloadImg(tabIdx, imgIdx);
                };
            };
        })(tabIdx,imgIdx)
    });
}

function fct_wMap_RetryPreloadImg(tabIdx, imgIdx){
    if(theTabs[tabIdx].imagesPreloadErrCnt[imgIdx] === undefined){ //this function has been called after the first fail, so the array cell has yet to be initialised
        theTabs[tabIdx].imagesPreloadErrCnt[imgIdx] = 1;
    }else{ // increment fail counter
        theTabs[tabIdx].imagesPreloadErrCnt[imgIdx] ++;
    };
    if(theTabs[tabIdx].imagesPreloadErrCnt[imgIdx] < wmImgRetryMax){ // set 'timer' to call [fct_wMap_PreloadImg()] to attempt another image preload
        theTabs[tabIdx].imagesPreloadRetry[imgIdx] = setTimeout(function(){
            fct_wMap_PreloadImg(tabIdx, imgIdx);
        },wmImgRetryMilli);
    }else{ // no more attempts to preload this image. If user clicks on tab again, further checks/attempts will be made
        //if this failed image was the default the set [theTabs[tabIdx].startPosition] to '-1' to allow the [slider] to be activated on any successfull preload
        if(theTabs[tabIdx].startPosition === imgIdx){
            theTabs[tabIdx].startPosition = -1;
        };
        fct_wMap_PreloadImgFinished(tabIdx, imgIdx); // call to increment 'loaded images' counter for this 'tab' & to check if 'loading' icon needs to be hidden
    };
};

function fct_wMap_PreloadImgFinished(tabIdx, imgIdx){
    theTabs[tabIdx].imageLoadingCount ++; // increment the preloaded counter
    theTabs[tabIdx].imageAjax[imgIdx] = null; // destroy reference to AJAX call, free some memory
    
    /*
     * Add the 'step' relating to this image to the 'active steps' array for this tab
     *  - this could be moved to the AJAX success function to ignore failed images entirely
     */ 
    
    //theTabs[tabIdx].sliderActiveSteps.push(theTabs[tabIdx].steps[imgIdx]); 
    //theTabs[tabIdx].sliderActiveSteps.sort(); 
    
    if (theTabs[tabIdx].imageLoadingCount === theTabs[tabIdx].imagesSrc.length) { // have now tried to preload all images
        fct_wMap_TabLoadingIcon(tabIdx,false); // if all images preloaded (or all retries completed) hide tab loading icon
        if(theTabs[tabIdx].sliderActiveSteps.length === 0){ // then no images were successfully preloaded
            // show total failure, 'available soon...', text
            jQuery('#wmap-loader').hide();
            jQuery('#wmap-noinfo').show();
            theTabs[tabIdx].startPosition = 0;
            return;
        };
    };
    if ((theTabs[tabIdx].startPosition === imgIdx  && fct_wMap_ChkImgPreloaded(tabIdx,imgIdx)) 
            || (theTabs[tabIdx].startPosition === -1 && theTabs[tabIdx].sliderActiveSteps.length > 0) ){
        
        if (theTabs[tabIdx].startPosition === -1) theTabs[tabIdx].startPosition = theTabs[tabIdx].sliderActiveSteps[0];
        // the default/start image has been successfully preloaded, show the slider          
        theTabs[tabIdx].isActive = true;
        loadImg(theTabs[tabIdx].steps[theTabs[tabIdx].startPosition]);
        fct_wMap_ShowSlider(tabIdx);
    };
    if(theTabs[tabIdx].sliderActiveSteps.length > 1 && theTabs[tabIdx].isActive){ // show prev/next button if tab has been activated and there is more than 1 valid timeslot
        jQuery('.wmap-button').show();
    };
    if (theTabs[tabIdx].isActive === true && fct_wMap_ChkImgPreloaded(tabIdx,imgIdx)){ // if the image was successfully preloaded, update the 'slider' css
        jQuery('#wmap-slider-box' + tabIdx + ' .ui-slider-labels .ui-slider-label-ticks > span').eq(theTabs[tabIdx].steps[imgIdx]).removeClass('inactive'); // Using CSS, show this timeslot as active
        fct_wMap_redrawTickCap(tabIdx, imgIdx); // update the timeslot 'caps', to match the main slider bar
    };
};

function fct_wMap_ChkTabPreloaded(tabIdx){
    if (theTabs[tabIdx].sliderActiveSteps.length === theTabs[tabIdx].imagesSrc.length){ // if all images successfully preloaded
        return true;
    }else{
        return false;
    };
};

function fct_wMap_ChkImgPreloaded(tabIdx,imgIdx){
    if (typeof theTabs[tabIdx].imagesPreload[imgIdx] === "object"){
        return true;
    }else{
        return false;
    };
};

function fct_wMap_TabLoadingIcon(tabIdx,boolAction){ // boolAction will have a value: true/false
    if (boolAction){
        jQuery('#wmap_header > ul > li > div.wmap-ldr-sml').eq(tabIdx).show();
    }else{
        jQuery('#wmap_header > ul > li > div.wmap-ldr-sml').eq(tabIdx).hide();
    };
};

function fct_wMap_ShowSlider(tabIdx){
    jQuery('#wmap-loader').hide();
    jQuery('#wmap-noinfo').hide();
    if(theTabs[tabIdx].sliderActiveSteps.length > 1){
        jQuery('.wmap-button').show();
    };
    jQuery('#wmap-image').show();
    jQuery('#wmap-slider-text').show();
    jQuery('#wmap-slider-box' + tabIdx).show();
    if (jQuery('#wmap-slider' + tabIdx).html() === ''){ //object/slider not initialised yet
        renderSlider(tabIdx);
    };
};

function fct_wMap_HideSlider(tabIdx){
    jQuery('#wmap-noinfo').hide();
    jQuery('#wmap-loader').show();
    jQuery('#wmap-image').hide();
    jQuery('.wmap-button').hide();
    jQuery('#wmap-slider-text').hide();
    jQuery('#wmap-slider-box' + tabIdx).hide();
};

/* 'Slider' functions after this point. */

function renderSlider(indexTab){
    //var $ = jQuery.noConflict();
    $('#wmap-slider' + indexTab).labeledslider({ 
        max: theTabs[indexTab].sliderMaxValue // Set the max value for the sliderto the maximum step value. This should allways be the last element in the 'steps' array
        , step: 1
        , tickInterval: 1
        , value: theTabs[indexTab].steps[theTabs[indexTab].startPosition]
        , tickLabels: theTabs[indexTab].labels // add the [theLabels] array as an option, for access in main .js file
        , tickSteps: theTabs[indexTab].sliderActiveSteps  // add the [sliderActiveSteps] array as an option, for access in main .js file
        , stop: function( event, ui ) {}
        , slide: function(event, ui) { // Overridden to position the pointer on the slider in relation to the defined [theSteps] array
                    var includeLeft = event.keyCode != $.ui.keyCode.RIGHT;
                    var includeRight = event.keyCode != $.ui.keyCode.LEFT;
                    $('#wmap-slider' + indexTab).labeledslider('option', 'value', findNearest(includeLeft, includeRight, ui.value));
                    return false;
                }
    });
    /*
    * Need to run the following code in order to place colored span tags at the top of each tick/label
    *  - the start & end tick/label would originally have jutted out a number of pixels to both ends of the slider bar
    *  - jQuery lets us detect the pixels exactly, regardless of the browser, so we can put a 'cap' on each tick/label
    *      with the height & color of the 'slider' bar.
    */
    var $slider = $('#wmap-slider' + indexTab + '.ui-widget-content'); // get a handle on the 'slider' bar
    var sHeight = $slider.css('border-top-width'); // get 'slider' bar height, in pixels
    var sColor = $slider.css('border-top-color'); // get 'slider' bar color
    var $spans = $('#wmap-slider-box' + indexTab + ' .ui-slider-labels > div > span'); // get a handle on all spans that may need 'capping'
    $spans.each(function(index){
        var newSpan = '<span style="left:' + $(this).position().left + 'px;width:' + $(this).css('border-left-width') + ';height:' + sHeight + ';background-color:' + sColor + ';display:inline-block;position:absolute;top:0px;"></span>';
        $(this).prepend(newSpan);
    });
};

function fct_wMap_redrawTickCap(tabIdx, imgIdx){
    var $span1 = jQuery('#wmap-slider-box' + tabIdx + ' .ui-slider-labels .ui-slider-label-ticks > span').eq(theTabs[tabIdx].steps[imgIdx]);
    var $span2 = jQuery('#wmap-slider-box' + tabIdx + ' .ui-slider-labels .ui-slider-label-ticks > span > span').eq(theTabs[tabIdx].steps[imgIdx]);
    var $slider = jQuery('#wmap-slider' + tabIdx + '.ui-widget-content'); // get a handle on the 'slider' bar
    var sHeight = $slider.css('border-top-width'); // get 'slider' bar height, in pixels
    var sColor = $slider.css('border-top-color'); // get 'slider' bar color
    $span2.attr('style','left:' + $span1.position().left + 'px;width:' + $span1.css('border-left-width') + ';height:' + sHeight + ';background-color:' + sColor + ';display:inline-block;position:absolute;top:0px;');
};

/* [findNearest] : used to position the pointer on the slider in relation to the loaded [sliderActiveSteps] array */
function findNearest(includeLeft, includeRight, value) {
    var theSteps = theTabs[wmCurrentTab].sliderActiveSteps;
    if (theSteps == null){
        return value;
    }else{
    	var nearest = null;
    	var diff = null;
    	for (var i = 0; i < theSteps.length; i++) {
            if ((includeLeft && theSteps[i] <= value) || (includeRight && theSteps[i] >= value)) {
                var newDiff = Math.abs(value - theSteps[i]);
                if (diff == null || newDiff < diff) {
                    nearest = theSteps[i];
                    diff = newDiff;
                };
            };
    	};
	loadImg(nearest);
	return nearest;
    };
};

function loadImg(step){
    //var $ = jQuery.noConflict();
    var images = theTabs[wmCurrentTab].imagesPreload;
    var theSteps = theTabs[wmCurrentTab].steps;

    if (theSteps !== null){ 
        for (var i =0; i<theSteps.length;i++){
            if (theSteps[i] === step){
                /* Check src of current image before deciding on replace, would stop any rapid flicker 
                *  as this code block can run numerous times during a slide.
                *  Placing the image change here means images will change while 'sliding' and when 'clicking'
                *       - Ammended to check text associated with current image as well:
                *       - If a new image OR new text => update
                */
                //if (images[i].src !== $('#wmap-image img').attr('src') || theTabs[wmCurrentTab].imageLabels[i] !== $('#wmap-slider-text').html()){
                if (images[i].src !== $('#wmap-image img').attr('src')){
                    $("#wmap-image" ).html(images[i]);
                    $("#wmap-slider-text" ).html(theTabs[wmCurrentTab].imageLabels[i]);
                };
                /* 'unpack' image text */
                var imgTextArr = theTabs[wmCurrentTab].imageLabels[i].split('.');
                var imgText = 	'<b>' + wmTab_day[imgTextArr[0]] + '</b><br/>' // add the 'day' text
                    + wmTab_timeslot[imgTextArr[1]] + '<br/>' // add the 'timeslot' text
                    + imgTextArr[2]; // add the min & hrs
                
                $("#wmap-tag" ).html(imgText);
                $("#wmap-slider-text" ).html(imgText);
                break;
            };
        };
    };
};

// Utility function to return the index of a value in an array
function getArrayIndex(theValue, theArray){
    var cntArray = theArray.length;
    for (var arrIndex = 0; arrIndex < cntArray; arrIndex++){
        if (theArray[arrIndex] === theValue){
            return arrIndex; 
            break; //exit loop
        };
    };
    return -1; // flag to show not found
};

/*!
 * Copyright (c) 2012 Ben Olson
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Depends:
 *  jquery.ui.core.js
 *  jquery.ui.widget.js
 *  jquery.ui.mouse.js
 *  jquery.ui.slider.js
 *
 * 11/06/13 - 01/09/13 Updated and Extended significantly by Adrian � Cual�in.
 * - Allows for different tick types/spacing and associated labelling
 * - The following resource was referenced:
 *		http://stackoverflow.com/questions/967372/jquery-slider-how-to-make-step-size-change
 */

(function( $, undefined ) {
    $.widget( "ui.labeledslider", $.ui.slider, {

      options: {
         tickInterval: 0,
         tickLabels: null,
         tickSteps: null
      },

      uiSlider: null,
      tickInterval: 0,

      _create: function( ) {

         this._detectOrientation();

         this.uiSlider =
             this.element
                .wrap( '<div class="ui-slider-wrapper ui-widget"></div>' )
                .before( '<div class="ui-slider-labels">' )
                .parent()
                .addClass( this.orientation )
                .css( 'font-size', this.element.css('font-size') );

         this._super();

         this.element.removeClass( 'ui-widget' );

         this._alignWithStep();

         if ( this.orientation == 'horizontal' ) {
            this.uiSlider
               //.width( this.element.width() );
         } else {
            this.uiSlider
               .height( this.element.height() );
         };

         this._drawLabels();
      },

      _drawLabels: function () {

         var labels = this.options.tickLabels || {},
             $lbl = this.uiSlider.children( '.ui-slider-labels' ),
             dir = this.orientation == 'horizontal' ? 'left' : 'bottom',
             min = this.options.min,
             max = this.options.max,
             inr = this.tickInterval,
             cnt = ( max - min ) / inr,
             i = 0;

         $lbl.html('');
		
         for (;i<=cnt;i++) {
            $('<div>').addClass( 'ui-slider-label-ticks' )
               .css( dir, (Math.round( i / cnt * 10000 ) / 100) + '%' )
               //.html( ( dir == 'left' ? '<span></span><br/>' : '<span></span> ' ) + ( labels[i*inr+min] ? labels[i*inr+min] : i*inr+min ) )
               .html( showLabel(i, labels, this.options.tickSteps))
               .appendTo( $lbl );
         };

      },

      _setOption: function( key, value ) {
          this._super( key, value );

          switch ( key ) {
             case 'tickInterval':
             case 'tickLabels':
             case 'min':
             case 'max':
             case 'step':

                this._alignWithStep();
                this._drawLabels();
                break;

             case 'orientation':

                this.element
                   .removeClass( 'horizontal vertical' )
                   .addClass( this.orientation );

                this._drawLabels();
                break;
          };
       },

       _alignWithStep: function () {
          if ( this.options.tickInterval < this.options.step )
            this.tickInterval = this.options.step;
          else
            this.tickInterval = this.options.tickInterval;
       },

       _destroy: function() {
          this._super();
          this.uiSlider.replaceWith( this.element );
       },

       widget: function() {
          return this.uiSlider;
       }

   });

}(jQuery));
/*
* LABEL NOTES:
* - there needs to be a value passed into [tickLabels] for each 'interval' on the slider:
*       - [null] : show nothing for that 'interval'
*       - [''] : show a default, css styled, 'tick' for that 'interval'
*       - ['some_text'] : show a larger, css styled, 'tick' for that 'interval' with the string passed displayed beneath it
 */
function showLabel(i, tickLabels, tickSteps){
    var theHTML = "";
    if (tickSteps === null || tickLabels === null){
            theHTML = '<span class="label"></span><br/>' + i;
    }else {
        var addClass = ' inactive';
        for (var stp = 0; stp < tickSteps.length; stp++){
            if (tickSteps[stp] === i){
                var addClass = '';
                break;
            };
        } ;      
       if (tickLabels[i] === null){
               theHTML = '<span></span><br/>';
       }else if (tickLabels[i] !== ""){
               theHTML = '<span class="label' + addClass + '"></span><br/>' + (tickLabels[i]);
       }else{
               theHTML = '<span class="nolabel' + addClass + '"></span><br/>' + (tickLabels[i]);
       };
    } ;
    return theHTML;
};