
var tabActiveIdx = null;
var $tabActive = null;
var $currAudio_img = null;
var $currAudio2_img = null;
var myPlayerData = null;
var fixFlash_mp4 = null;
var fixFlash_mp4_id = null;
var ignore_timeupdate = null;
// Preload common icons
var icAimsirSpeaker = new Image();
icAimsirSpeaker.src = 'https://d1og0s8nlbd0hm.cloudfront.net/images/Aimsir/focail-aimsire/Play-Speaker.png'; 
var icAimsirPlay = new Image();
icAimsirPlay.src = 'https://d1og0s8nlbd0hm.cloudfront.net/images/Aimsir/focail-aimsire/audio-play.png'; 
var icAimsirPause = new Image();
icAimsirPause.src = 'https://d1og0s8nlbd0hm.cloudfront.net/images/Aimsir/focail-aimsire/audio-pause.png'; 
var icAimsirStop = new Image();
icAimsirStop.src = 'https://d1og0s8nlbd0hm.cloudfront.net/images/Aimsir/focail-aimsire/audio-stop.png'; 

function initAimsirAudio(){
    jQuery('.focail_button').hover(function(){jQuery(this).toggleClass('focail_button_hover');});
    
    tabActiveIdx = jQuery("#focail_menu li").index(jQuery(".navHere")); // Only one tab should ever be active at a time, this sets the [index] of the 'tab' that's initially active
    $tabActive = jQuery("#focail_menu li").eq(tabActiveIdx); // this saves a jQuery reference to the 'tab' that's initially active
    
    jQuery("#focail_menu li").click(function(){
        var tabIdx = jQuery("#focail_menu li").index(this);// get a handle on the [index] of 'tab' just clicked
        
        if (tabIdx === tabActiveIdx) return; //Exit, 'tab' clicked is already active
        
        $tabActive.removeClass('navHere'); // Update CSS of previous tab to be 'inactive'
        jQuery(this).addClass('navHere'); // Update CSS of current tab to be 'active'
        
        jQuery('.focail_box').eq(tabActiveIdx).hide(); // hide the 'word box(es)' of previously active 'tab'
        jQuery('.focail_box').eq(tabIdx).show(); // now show 'word box(es)' of currently active/clicked 'tab'
        
        tabActiveIdx = tabIdx; // update the [index] reference to that of the currently active 'tab'
        $tabActive = jQuery(this); // update the jQuery reference to that of the currently active 'tab'
        
        /*
         * Update Google Analytics here
         *  - Pass the HREF value of the 'main-tab' clicked
         */ 
        //AimsirAnalytics(jQuery('a',this).attr('href'));
    });
    
    jQuery(".focail_submenu li").click(function(){
        var $subActive = jQuery('li.navHere', jQuery('.focail_box .focail_submenu ul').eq(tabActiveIdx));
        var subActiveIdx = jQuery('li', jQuery('.focail_box .focail_submenu ul').eq(tabActiveIdx)).index($subActive);
        var subIdx = jQuery('li', jQuery('.focail_box .focail_submenu ul').eq(tabActiveIdx)).index(this);
        
        if (subIdx === subActiveIdx) return; //Exit, 'subtab' clicked is already active
        
        $subActive.removeClass('navHere');
        jQuery(this).addClass('navHere');
       
        jQuery('.focail_dtls', jQuery('.focail_box').eq(tabActiveIdx)).eq(subActiveIdx).hide(); // hide all 'word boxes'
        jQuery('.focail_dtls', jQuery('.focail_box').eq(tabActiveIdx)).eq(subIdx).show(); // now show correct 'word box'
        
        /*
         * Update Google Analytics here
         *  - Pass the HREF value of the 'sub-tab' clicked
         */ 
        //AimsirAnalytics(jQuery('a',this).attr('href'));
    });
    mediaPlayer = jQuery('#Audio-MediaContainer');
    mediaPlayer.jPlayer({
        swfPath: '/assets/js/jplayer/Jplayer.swf',
        supplied: "mp3",//,
        //wmode: "window",
        //preload:"auto",
        //autoPlay: false,
       // errorAlerts:false,
        //warningAlerts:false
        ended: function() {
            jQuery('.audio_stop', $currAudio_img).hide();
            jQuery('.audio_play', $currAudio_img).show();
            $currAudio_img = null;
            mediaPlayer.jPlayer( "clearMedia");
        }
    });
    /*
        * jQuery UI ThemeRoller
        *
        * Includes code to hide GUI volume controls on mobile devices.
        * ie., Where volume controls have no effect. See noVolume option for more info.
        *
        * Includes fix for Flash solution with MP4 files.
        * ie., The timeupdates are ignored for 1000ms after changing the play-head.
        * Alternative solution would be to use the slider option: {animate:false}
        */

       myPlayer = jQuery("#jquery_jplayer_1"),
           myPlayerData,
           fixFlash_mp4, // Flag: The m4a and m4v Flash player gives some old currentTime values when changed.
           fixFlash_mp4_id, // Timeout ID used with fixFlash_mp4
           ignore_timeupdate, // Flag used with fixFlash_mp4
           options = {
               ready: function (event) {
                       // Hide the volume slider on mobile browsers. ie., They have no effect.
                       if(event.jPlayer.status.noVolume) {
                               // Add a class and then CSS rules deal with it.
                               jQuery(".jp-gui").addClass("jp-no-volume");
                       }
                       // Determine if Flash is being used and the mp4 media type is supplied. BTW, Supplying both mp3 and mp4 is pointless.
                       fixFlash_mp4 = event.jPlayer.flash.used && /m4a|m4v/.test(event.jPlayer.options.supplied);
                       // Setup the player with media.
                       //jQuery(this).jPlayer("setMedia", {
                       //    mp3: "/assets/aimsir/Tearmai_Aimsire/Tearmai-Muimhneach/anghaothaduaidh.mp3"
                       //});
               },
               timeupdate: function(event) {
                       if(!ignore_timeupdate) {
                               myControl.progress.slider("value", event.jPlayer.status.currentPercentAbsolute);
                       }
               },
               volumechange: function(event) {
                       if(event.jPlayer.options.muted) {
                               myControl.volume.slider("value", 0);
                       } else {
                               myControl.volume.slider("value", event.jPlayer.options.volume);
                       }
               },
               swfPath: "https://d1og0s8nlbd0hm.cloudfront.net/js/jplayer/Jplayer.swf",
               supplied: "mp3",
               cssSelectorAncestor: "#jp_container_1",
               wmode: "window",
               keyEnabled: true,                    
               ended: function() {
                   console.log('wahey...');
               }
           },
           myControl = {
                   progress: jQuery(options.cssSelectorAncestor + " .jp-progress-slider")
           };

	// Instance jPlayer
	myPlayer.jPlayer(options);

	// A pointer to the jPlayer data object
	myPlayerData = myPlayer.data("jPlayer");

	// Define hover states of the buttons
	jQuery('.jp-gui ul li').hover(
		function() { jQuery(this).addClass('ui-state-hover'); },
		function() { jQuery(this).removeClass('ui-state-hover'); }
	);

	// Create the progress slider control
	myControl.progress.slider({
		animate: "fast",
		max: 100,
		range: "min",
		step: 0.1,
		value : 0,
		slide: function(event, ui) {
			var sp = myPlayerData.status.seekPercent;
			if(sp > 0) {
				// Apply a fix to mp4 formats when the Flash is used.
				if(fixFlash_mp4) {
					ignore_timeupdate = true;
					clearTimeout(fixFlash_mp4_id);
					fixFlash_mp4_id = setTimeout(function() {
						ignore_timeupdate = false;
					},1000);
				}
				// Move the play-head to the value and factor in the seek percent.
				myPlayer.jPlayer("playHead", ui.value * (100 / sp));
			} else {
				// Create a timeout to reset this slider to zero.
				setTimeout(function() {
					myControl.progress.slider("value", 0);
				}, 0);
			}
		}
	});

	
    
    jQuery(".audio_img a").click(function(){
        if($currAudio2_img !== null){
            myPlayer.jPlayer("stop");
            myPlayer.jPlayer( "clearMedia");
            jQuery('.audio_stop', $currAudio2_img).hide();
            jQuery('.audio_play', $currAudio2_img).show();
            $currAudio2_img = null;
            jQuery("#jp_container_1").hide();
        } 
        if($currAudio_img !== null){
            mediaPlayer.jPlayer("stop");
            mediaPlayer.jPlayer("clearMedia");
            jQuery('.audio_stop', $currAudio_img).hide();
            jQuery('.audio_play', $currAudio_img).show();
            if ($currAudio_img === this){
                $currAudio_img = null;
                return;
            }
        }  
        $currAudio_img = this;
        
        jQuery('.audio_play', this).hide();
        jQuery('.audio_stop', this).show();
        
        mediaPlayer.jPlayer("setMedia", {
            mp3: jQuery(this).attr('href')
        });
        
        mediaPlayer.jPlayer("play");
 
    });
    jQuery(".audio_img2 a").click(function(){
        if($currAudio2_img !== null){
            myPlayer.jPlayer("stop");
            myPlayer.jPlayer( "clearMedia");
            jQuery('.audio_stop', $currAudio2_img).hide();
            jQuery('.audio_play', $currAudio2_img).show();
            $currAudio2_img = null;
            jQuery("#jp_container_1").hide();
        } 
        if($currAudio_img !== null){
            mediaPlayer.jPlayer("stop");
            mediaPlayer.jPlayer("clearMedia");
            jQuery('.audio_stop', $currAudio_img).hide();
            jQuery('.audio_play', $currAudio_img).show();
            if ($currAudio_img === this){
                $currAudio_img = null;
                return;
            }
        }  
        $currAudio_img = this;
        
        jQuery('.audio_play', this).hide();
        jQuery('.audio_stop', this).show();
        
        mediaPlayer.jPlayer("setMedia", {
            mp3: jQuery(this).attr('href')
        });
        
        mediaPlayer.jPlayer("play");
 
    });
    jQuery(".audio_img3 a").click(function(){
        if($currAudio_img !== null){
            mediaPlayer.jPlayer("stop");
            mediaPlayer.jPlayer( "clearMedia");
            jQuery('.audio_stop', $currAudio_img).hide();
            jQuery('.audio_play', $currAudio_img).show();
            $currAudio_img = null;
        }   
          
        if($currAudio2_img !== null){
            myPlayer.jPlayer("stop");
            myPlayer.jPlayer("clearMedia");
            jQuery("#jp_container_1").hide();
            jQuery('.audio_stop', $currAudio2_img).hide();
            jQuery('.audio_play', $currAudio2_img).show();
            if ($currAudio2_img === this){
                $currAudio2_img = null;
                
                return;
            }
        }  
        $currAudio2_img = this;
        
        jQuery('.audio_play', this).hide();
        jQuery('.audio_stop', this).show();
        
        myPlayer.jPlayer("setMedia", {
            mp3: jQuery(this).attr('href')
        });
        var thePlayer = jQuery("#jp_container_1").detach();
        thePlayer.insertAfter(this);
        jQuery("#jp_container_1").show();
        myPlayer.jPlayer("play");
 
    });
}

function AimsirAnalytics(theUrl){
    //update Google analytics here
    if (typeof _gaq.push === 'function' && theUrl !== "") { // check the Google function exists within the page
        var trackPageURL = theUrl;
        _gaq.push(['_trackPageview', trackPageURL]);
    };
};