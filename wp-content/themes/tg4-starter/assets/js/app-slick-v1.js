'use strict';

var tg4App=angular.module('tg4App',['slickCarousel']);
tg4App.controller('tg4Ctrl', function($scope,$http) {

    $scope.diffDate = function(date1) {
        var dateOut1 = new Date(Date.parse(date1, "yyyy-MM-dd HH:mm:ss")); // It will work if date1 is in ISO format
        var dateOut2 = new Date(); // Today's date
        var timeDiff = Math.abs(dateOut1.getTime() - dateOut2.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        return diffDays;
    };

    //* Check querystring parameters for Sort, Page Count...
    var params={};
    decodeURIComponent(location.search).replace(/[?&]+([^=&]+)=([^&]*)/gi,function(m,k,v){ params[k]=v });
    //console.log(params);

    //====================================
    // Slick 1
    //====================================
    //$http.post("http://localhost/tg4-redesign-2015/wp-content/themes/tg4-starter/assets/php/tg4-proxy.php").then(function(response) {
    $http.post("https://www.tg4.ie/wp-content/themes/tg4-starter/assets/php/tg4-proxy.php").then(function(response) {
        $scope.tAuth = response.data;
        //console.log($scope.tAuth);
        if ($scope.tAuth) {
            var headerParam = { headers: {
                "Authorization": "Bearer " + $scope.tAuth,
                "Accept": "application/json;odata=verbose"
                }
            };

            var srchTerm = encodeURI(params.series);

            // Videos by Series Title
            var endPoint1 = '/videos?q=%2Dseriestitle:"TG4-Beo"+%2Bseriestitle:"' + srchTerm + '"+%2Bplayable%3Atrue&limit=12&offset=0&sort=-schedule_starts_at';
            $http.get('https://cms.api.brightcove.com/v1/accounts/1555966122001' + endPoint1, headerParam).then(function(d) {
                $scope.pSeries = d.data;
                $scope.pCntSeries = d.data.length;
                console.log($scope.pSeries, $scope.pCntSeries);
            });

            // Featured Videos
            var endPoint2 = '/videos?q=%2Dseriestitle:"TG4-Beo"+%2Btags:"xfeature"+%2Bplayable%3Atrue&sort=-schedule_starts_at';
            $http.get('https://cms.api.brightcove.com/v1/accounts/1555966122001' + endPoint2, headerParam).then(function(d) {
                $scope.pFeature = d.data;
                $scope.pCntFeature = d.data.length;
                console.log($scope.pFeature, $scope.pCntFeature);
            });

            /*
            // Sports Clips Videos
            var endPoint3 = '/videos?q=%2Dseriestitle:"TG4-Beo"+%2Bcategory_c:"Sports Clips"+%2Bplayable%3Atrue&limit=12&offset=0&sort=-schedule_starts_at';
            $http.get('https://cms.api.brightcove.com/v1/accounts/1555966122001' + endPoint3, headerParam).then(function(d) {
                $scope.pSportsClips = d.data;
                $scope.pCntSportsClips = d.data.length;
                console.log($scope.pSportsClips, $scope.pCntSportsClips);
            });

            // Drama Videos
            var endPoint4 = '/videos?q=%2Dseriestitle:"TG4-Beo"+%2Bcategory_c:"Drama"+%2Bplayable%3Atrue&limit=12&offset=0&sort=-schedule_starts_at';
            $http.get('https://cms.api.brightcove.com/v1/accounts/1555966122001' + endPoint4, headerParam).then(function(d) {
                $scope.pDrama = d.data;
                $scope.pCntDrama = d.data.length;
                console.log($scope.pDrama, $scope.pCntDrama);
            });

            // Documentary Videos
            var endPoint5 = '/videos?q=%2Dseriestitle:"TG4-Beo"+%2Bcategory_c:"Faisneis"+%2Bplayable%3Atrue&limit=12&offset=0&sort=-schedule_starts_at';
            $http.get('https://cms.api.brightcove.com/v1/accounts/1555966122001' + endPoint5, headerParam).then(function(d) {
                $scope.pDocs = d.data;
                $scope.pCntDocs = d.data.length;
                console.log($scope.pDocs, $scope.pCntDocs);
            });
            */
        }
    });

    $scope.slickConfig1Loaded = true;
    $scope.slickConfig1 = {
        method: {},
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
        {
            breakpoint: 1100,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: false
            }
        },
        {
            breakpoint: 938,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 622,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        ]
    };
});