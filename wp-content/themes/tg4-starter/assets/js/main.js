// @codekit-prepend "vendor/modernizr-2.8.3.min.js";
// @codekit-prepend "vendor/mmenu/jquery.mmenu.min.js"; 
// @codekit-prepend "vendor/zurb/responsive-tables.js";
// @codekit-prepend "plugins.js";
// @codekit-prepend "vendor/outdatedbrowser/outdatedbrowser.js";
// @codekit-prepend "vendor/jquery.columnlist.js";
// @codekit-prepend "vendor/limit.js";
// @codekit-prepend "vendor/ssm.min.js";
// @codekit-prepend "vendor/greensock/TweenLite.min.js";
// @codekit-prepend "vendor/greensock/easing/EasePack.min.js";
// @codekit-prepend "vendor/greensock/plugins/CSSPlugin.min.js";
// @codekit-prepend "vendor/slick.min.js";
// @codekit-prepend "vendor/match-height.js";


jQuery(document).ready(function($) {

    /*  ==========================================================================
        Global Variables
        ========================================================================== */

    var throttleFreq = 100; // milliseconds of mouse move function call throttling, 100 optimal for hover trigger without null fires

    // common jquery cached objects
    var $body = $('body');
    // search bar elements
    var $searchWrapper = $('.search-wrapper');
    var $btnSearchToggle = $('.search-btn-toggle');
    var $inputSearch = $('.search-input');
    var $btnSearch = $('.search-btn');
    var $navCover = $('.nav-cover');

    /*  ==========================================================================
        Window resizing
        ========================================================================== */

    // uses limit.js for debouncing, can also use throttling.
    window.onresize = wResize.debounce(throttleFreq);

    function wResize() {
        // add any functions here for when a resize event is required.
    }

    /*  ==========================================================================
        MatchHeight
        ========================================================================== */

    if($('.tonight-slider').length) {
        $('.tonight-title').matchHeight();
    }

    if($('.prog-footer').length) {
        $('.prog-footer').matchHeight();
    }

    if($('.featv-footer').length) {
        $('.featv-footer').matchHeight();
    }

    if($('.MH-sched-item').length) {
        $('.MH-sched-item').matchHeight();
    }

    /*  ==========================================================================
        Set up MMenu for mobile **** NEEDS TO B SET BEFORE SSM ON ENTER FOR API TO REGISTER ****
        ========================================================================== */

    var $mPanel = $('.mm-panel');

    $("#main-menu").mmenu({
        // options
        //slidingSubmenus: false
    }, {
        // configuration
        clone: true // clones the menu to create a separate off canvas one to help with bespoke styling of menus.
    });

    // close the mmenu mobile nav when resizing up from mobile to desktop dimensions
    function closeMobNav() {
        var api = $("#main-menu").data("mmenu");
        api.close();
    }

    /*  ==========================================================================
        Simple State Manager: http://www.simplestatemanager.com/
        ========================================================================== */

    // use SSM to add a class for when the site crosses the mobile breakpoint incase small browser size and doesn't pick up WP isMobile
    ssm.addStates([{
        id: 'mobile',
        maxWidth: 1024,
        onEnter: function() {
            $body.removeClass('ssm-desktop');
            $body.addClass('ssm-mobile');
            resetSearchBar();
        }
    }, {
        id: 'desktop',
        minWidth: 1025,
        onEnter: function() {
            $body.removeClass('ssm-mobile');
            $body.addClass('ssm-desktop');
            closeMobNav(); // close the mmenu mobile nav when resizing up from mobile to desktop dimensions
            resetSearchBar();
        }
    }]);
    ssm.ready();

    /*  ==========================================================================
        Add device orientation classes
        ========================================================================== */

    // example: used for repositioning the window scroll position for landscale mobile devices when expanding the search bar to bring the input into view instead of having to scroll again.

    if (window.DeviceOrientationEvent) {
        function doOnOrientationChange() {
            switch (window.orientation) {
                case -90:
                case 90:
                    //alert('landscape');
                    $body.addClass('landscape');
                    $body.removeClass('portrait');
                    break;
                default:
                    //alert('portrait');
                    $body.addClass('portrait');
                    $body.removeClass('landscape');
                    break;
            }
        }
        window.addEventListener('orientationchange', doOnOrientationChange);
        doOnOrientationChange();
    }

    /*  ==========================================================================
        Drop nav multi-columns
        ========================================================================== */

    // split an unordered list into mutliple columned unordered lists. Refer to _navigation.scss

    if ($('.col-4-list').length) {
        $('.col-4-list > ul.sub-menu').columnlist({
            size: 4
        });
    }
    if ($('.col-3-list').length) {
        $('.col-3-list > ul.sub-menu').columnlist({
            size: 3
        });
    }
    if ($('.col-2-list').length) {
        $('.col-2-list > ul.sub-menu').columnlist({
            size: 2
        });
    }

    /*  ==========================================================================
        Search Bar
        ========================================================================== */

    var searchExp = false;

    $btnSearchToggle.click(function(e) {
        cancelBubbling(e);
        if (!searchExp) {
            openSearch()
        } else {
            closeSearch()
        }
    });

    // stop click on document bubbling so as not to close the search form on focus
    $inputSearch.click(function(e) {
        cancelBubbling(e);
    })

    function cancelBubbling(e) {
        if (!e) var e = window.event;
        //e.cancelBubble is supported by IE - this will kill the bubbling process.
        e.cancelBubble = true;
        e.returnValue = false;
        //e.stopPropagation works only in Firefox.
        if (e.stopPropagation) {
            e.stopPropagation();
            e.preventDefault();
        }
    }

    function openSearch() {
        searchExp = true;
        if ($body.hasClass('ssm-mobile')) {
            TweenLite.to($searchWrapper, 0.4, {
                top: 60,
                ease: Expo.easeInOut
            });
            if ($body.hasClass('landscape')) {
                $('body').scrollTop(60);
            }
        } else {
            TweenLite.to($searchWrapper, 0.4, {
                width: 330,
                ease: Expo.easeInOut
            });
        }
        $btnSearchToggle.addClass('search-exp');
        $navCover.addClass('search-open');
        $inputSearch.focus();
    }

    function closeSearch() {
        searchExp = false;
        if ($body.hasClass('ssm-mobile')) {
            TweenLite.to($searchWrapper, 0.4, {
                top: 0,
                ease: Expo.easeInOut
            });
        } else {
            $btnSearchToggle.blur();
            TweenLite.to($searchWrapper, 0.4, {
                width: 0,
                ease: Expo.easeInOut
            });
        }
        $btnSearchToggle.removeClass('search-exp');
        $navCover.removeClass('search-open');
    }

    $(document).click(function() {
        // close the search form when clicking outside the form
        closeSearch();
    });

    /*  ==========================================================================
        Reset Search Bar
        ========================================================================== */

    function resetSearchBar() { // function called by simple state manager when jumping between mobile and desktop states
        TweenLite.killTweensOf($searchWrapper);
        $searchWrapper.removeAttr("style"); // to stop the topbar from being repositioned by inline styles from Tweenlite
        $btnSearchToggle.removeClass('search-exp'); // reset the search cover button to inital state
        $navCover.removeClass('search-open');
        searchExp = false; // reset search expanded boolean
    }

    /*  ==========================================================================
        Homepage header royal slider
        ========================================================================== */

    if ($('#full-width-slider').length) {
        // jQuery.rsCSS3Easing.easeOutBack = 'cubic-bezier(0.175, 0.885, 0.320, 1.275)';
        // jQuery.rsCSS3Easing.easeOutExpo = 'cubic-bezier(0.190, 1.000, 0.220, 1.000)';
        // jQuery.rsCSS3Easing.easeOutQuint = 'cubic-bezier(0.230, 1.000, 0.320, 1.000)';
        // jQuery.rsCSS3Easing.easeOutQuart = 'cubic-bezier(0.165, 0.840, 0.440, 1.000)';

        var sliderEase = 'easeInOutSine';

        $('#full-width-slider').royalSlider({
            arrowsNav: true,
            loop: true,
            keyboardNavEnabled: true,
            controlsInside: false,
            imageScaleMode: 'fill',
            arrowsNavAutoHide: false,
            autoScaleSlider: true,
            autoScaleSliderWidth: 16, // aspect ratio
            autoScaleSliderHeight: 6.5, // aspect ratio
            controlNavigation: 'bullets',
            thumbsFitInViewport: false,
            navigateByClick: true,
            startSlideId: 0,
            autoPlay: {
                // autoplay options go gere
                enabled: true,
                pauseOnHover: true,
                delay: 5000
            },
            transitionType: 'move',
            globalCaption: false,
            slidesSpacing: 0,
            slidesOrientation: 'horizontal',
            easeInOut: sliderEase,
            allowCSS3: true,
            addActiveClass: true,
            deeplinking: {
                enabled: true,
                change: false
            },
        });
    }

    if ($('#mini-width-slider').length) {
        // jQuery.rsCSS3Easing.easeOutBack = 'cubic-bezier(0.175, 0.885, 0.320, 1.275)';
        // jQuery.rsCSS3Easing.easeOutExpo = 'cubic-bezier(0.190, 1.000, 0.220, 1.000)';
        // jQuery.rsCSS3Easing.easeOutQuint = 'cubic-bezier(0.230, 1.000, 0.320, 1.000)';
        // jQuery.rsCSS3Easing.easeOutQuart = 'cubic-bezier(0.165, 0.840, 0.440, 1.000)';

        var sliderEase = 'easeInOutSine';

        $('#mini-width-slider').royalSlider({
            arrowsNav: true,
            loop: true,
            keyboardNavEnabled: true,
            controlsInside: false,
            imageScaleMode: 'fill',
            arrowsNavAutoHide: false,
            autoScaleSlider: true,
            autoScaleSliderWidth: 16, // aspect ratio
            autoScaleSliderHeight: 9.6, // aspect ratio
            controlNavigation: 'bullets',
            thumbsFitInViewport: false,
            navigateByClick: true,
            startSlideId: 0,
            autoPlay: {
                // autoplay options go gere
                enabled: true,
                pauseOnHover: true,
                delay: 5000
            },
            transitionType: 'move',
            globalCaption: false,
            slidesSpacing: 0,
            slidesOrientation: 'horizontal',
            easeInOut: sliderEase,
            allowCSS3: true,
            addActiveClass: true,
            deeplinking: {
                enabled: true,
                change: false
            },
        });
    }

    /*  ==========================================================================
        Tonight Slick Slider
        ========================================================================== */

    if ($('.tonight-slider').length) {
        $('.tonight-slider').on('init', function(event, slick){
            $(this).removeClass('visuallyhidden');
        });
        $('.tonight-slider').slick({
            dots: false,
            infinite: false,
            speed: 300,
            swipeToSlide: true,
            centerMode: false,
            variableWidth: true
        });
    }

    /*  ==========================================================================
        Online Slick Slider
        ========================================================================== */

    if ($('.online-slider').length) {
        $('.online-slider').on('init', function(event, slick){
            $(this).removeClass('visuallyhidden');
        });
        $('.online-slider').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 1,
            //swipeToSlide: true,
            responsive: [
                {
                  breakpoint: 938,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    dots: false
                  }
                },
                {
                  breakpoint: 622,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false
                  }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    }

    /*  ==========================================================================
        Featured Video Slider
        ========================================================================== */

    if ($('.featv-slider').length) {

        $('.featv-slider').on('init', function(event, slick){
            $(this).removeClass('visuallyhidden');
        });
        $('.featv-slider').slick({
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 4,
            slidesToScroll: 4,
            arrows: false,
            //rows: 1,
            responsive: [
                {
                  breakpoint: 1100,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                  }
                },
                {
                  breakpoint: 938,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                  }
                },
                {
                  breakpoint: 622,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }
            ]
        });
        $('.featv-prev').click(function(event) {
            $('.featv-slider').slick('slickPrev');
        });
        $('.featv-next').click(function(event) {
            $('.featv-slider').slick('slickNext');
        });
        
    }

    /*  ==========================================================================
        Featured Video Slider - Player
        ========================================================================== */

    if ($('.featvp-slider').length) {

        $('.featvp-slider').on('init', function(event, slick){
            $(this).removeClass('visuallyhidden');
        });
        $('.featvp-slider').slick({
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 4,
            slidesToScroll: 4,
            autoplay: false,
            autoplaySpeed: 3000,
            arrows: false,
            //rows: 1,
            responsive: [
                {
                  breakpoint: 1100,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                  }
                },
                {
                  breakpoint: 938,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                  }
                },
                {
                  breakpoint: 622,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }
            ]
        });
        $('.featvp-prev').click(function(event) {
            $('.featvp-slider').slick('slickPrev');
        });
        $('.featvp-next').click(function(event) {
            $('.featvp-slider').slick('slickNext');
        });
        
    }

    /*  ==========================================================================
        Featured Sport Slider - Player
        ========================================================================== */

    if ($('.featsp-slider').length) {

        $('.featsp-slider').on('init', function(event, slick){
            $(this).removeClass('visuallyhidden');
        });
        $('.featsp-slider').slick({
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 4,
            slidesToScroll: 4,
            arrows: false,
            //rows: 1,
            responsive: [
                {
                  breakpoint: 1100,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                  }
                },
                {
                  breakpoint: 938,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                  }
                },
                {
                  breakpoint: 622,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }
            ]
        });
        $('.featsp-prev').click(function(event) {
            $('.featsp-slider').slick('slickPrev');
        });
        $('.featsp-next').click(function(event) {
            $('.featsp-slider').slick('slickNext');
        });
        
    }


    /*  ==========================================================================
        Featured Docs Slider - Player
        ========================================================================== */

    if ($('.featdoc-slider').length) {

        $('.featdoc-slider').on('init', function(event, slick){
            $(this).removeClass('visuallyhidden');
        });
        $('.featdoc-slider').slick({
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 4,
            slidesToScroll: 4,
            arrows: false,
            //rows: 1,
            responsive: [
                {
                  breakpoint: 1100,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                  }
                },
                {
                  breakpoint: 938,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                  }
                },
                {
                  breakpoint: 622,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }
            ]
        });
        $('.featdoc-prev').click(function(event) {
            $('.featdoc-slider').slick('slickPrev');
        });
        $('.featdoc-next').click(function(event) {
            $('.featdoc-slider').slick('slickNext');
        });
        
    }


    /*  ==========================================================================
        Highlights slider
        ========================================================================== */

    if ($('.high-slider').length) {
        $('.high-slider').on('init', function(event, slick){
            $(this).removeClass('visuallyhidden');
        });
        $('.high-slider').slick({
            infinite: true,
            dots: true,
            slidesToShow: 4,
            slidesToScroll: 4,
            arrows: true,
            //rows: 1,
            responsive: [
                {
                    breakpoint: 1600,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 1100,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 938,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }

    /*  ==========================================================================
        Schedule days
        ========================================================================== */

    if ($('.sched-slider').length) {
        $('.sched-slider').on('init', function(event, slick){
            $(this).removeClass('visuallyhidden');
        });
        $('.sched-slider').slick({
            infinite: false,
            dots: false,
            slidesToShow: 7,
            slidesToScroll: 7,
            arrows: true,
            speed: 500,
            responsive: [
                {
                    breakpoint: 1100,
                    settings: {
                        slidesToShow: 6,
                        slidesToScroll: 6
                    }
                },
                {
                    breakpoint: 900,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 5
                    }
                },
                {
                    breakpoint: 700,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4
                    }
                },
                {
                    breakpoint: 500,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 400,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }
            ]
        });
    }

    /*  ==========================================================================
        Section sub menu e.g. programes
        ========================================================================== */

    if ($('.section-submenu').length) {  
        // set up sub section selected title for submenu nav
        var $sSub = $('.section-submenu')
        var $submenuWrap = $('.section-submenu-wrap');
        var sectionPageTitle = $sSub.find('.current_page_item').find('a').text();
        var subOpen = false;
        $sSub.prepend('<div class="section-sub-page">'+sectionPageTitle+'<span></span></div>');

        $('.section-sub-page').click(function(event) {
            if(!subOpen) {
                $submenuWrap.addClass('section-sub-open');
                subOpen = true;
            }
            else {
                $submenuWrap.removeClass('section-sub-open');
                subOpen = false;
            }
        });

    }

});