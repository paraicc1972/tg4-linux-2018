<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8,IE=9,IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="dns-prefetch" href="https://www.google-analytics.com">
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
        <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png">
        
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/main-molsceal.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,700" rel="stylesheet">
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="https://d1og0s8nlbd0hm.cloudfront.net/js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
        <script src='https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js'></script>

        <script>
        // Wait for the DOM to be ready
        $(function() {
            // Initialize form validation on the search form.
            // It has the name attribute "searchform"
            $("form[name='searchform']").validate({
                // Specify validation rules
                rules: {
                    // The key name on the left side is the name attribute
                    // of an input field. Validation rules are defined on the right side
                    search: "required"
                },
                // Specify validation error messages
                messages: {
                    search: "Téarma ag teastail"
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });
        </script>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-4024457-16"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-4024457-16');
        </script>

        <?php
        wp_head();
        /* $ancestors = get_post_ancestors($post->ID);
        echo "<title>" . get_the_title($ancestors[1]) . "</title>"; */
        echo "<title>" . get_post_meta(get_the_ID(), '_yoast_wpseo_title', true) . "</title>";
        ?>
    </head>
    <body <?php body_class(); ?>>
        <a href="#maincontent" class="skip-to-main">Skip to main content</a> 
        <div class="wrapper">
            <div class="header-wrapper">
                <div class="top-bar-molsceal">
                    <div class="top-bar-wrapper">
                        <div class="region">
                            <a class="regiontoggle">Mo Réigiún</a>&nbsp;
                            <a class="regiontoggle"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/MolSceal-Region.png" alt="MolScéal: Réigiún"></a>
                        </div>
                        <div class="search">
                            <form name="searchform" method="get" id="searchform" class="search-form" action="<?php echo site_url(); ?>/toradh" autocomplete="off">
                                <input required type="search" class="search-input" name="search" id="search" value="">
                                <button type="submit" id="searchsubmit" value="Search" class="search-btn"></button>
                            </form>
                        </div>
                        <div id="regiondd">
                            <ul>
                                <li class="regLnk"><a href="<?php echo site_url(); ?>/connachta">Connachta</a></li>
                                <li class="regLnk"><a href="<?php echo site_url(); ?>/an-mhumhain">An Mhumhain</a></li>
                                <li class="regLnk"><a href="<?php echo site_url(); ?>/laighin">Laighin</a></li>
                                <li class="regLnk"><a href="<?php echo site_url(); ?>/ulaidh">Ulaidh</a></li>
                                <!-- <li class="regLnk"><a class="fa fa-times closetoggle" style="color:#000"></a></li> -->
                            </ul>
                        </div>
                    </div>
                    <div class="title-bar-molsceal"><a href="<?php echo site_url(); ?>/"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/MolSceal-Logo.png" alt="Molscéal Logo"></a></div>
                </div>
            </div>
            <main id="maincontent">