<?php /* Template Name: Schedule - Ajax */ ?>

<?php get_header(); ?>

<script>
function showDate(str) {
    if (str == "") {
        document.getElementById("sched-list-wrap").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("sched-list-wrap").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET","<?php echo site_url(); ?>/wp-content/themes/tg4-starter/includes/daily-schedule.php?date="+str+"&lang=<?php echo ICL_LANGUAGE_CODE; ?>",true);
        xmlhttp.send();
    }
}
</script>

<div class="section-header">
	<h1 class="section-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Sceidil TG4' : 'Irish TV Schedule'); ?></h1>
</div>

<?php include(locate_template('/includes/schedule-highlights.php')); ?>

<section class="section-panel-blue schedule-tonight">
    <div class="title-tab-wrap">
        <h2 class="title-tab-blue"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Inniu ar TG4' : 'Today on TG4'); ?></h2>
        <!-- Maybe this needs retitled, e.g. Coming up on TG4. Should maybe show a feed of the next 24 hours of shows. -->
    </div>
    <?php get_template_part('/includes/tonight-slider'); ?>
</section>

<section class="sched-lists" id="sched-lists">
	<h2 class="sched-lists-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Sceidil an Lae' : 'Daily Schedule'); ?></h2>
	<div class="sched-days-wrap">
		<div class="sched-days">
			<div class="sched-slider visuallyhidden">
				
				<!-- <a href="#" class="day-link day-selected">Mon<span class="sched-date">9 Mar</span></a>
				<a href="#" class="day-link">Tue<span class="sched-date">10 Mar</span></a> -->

				<?php /* Creates a menu for pages beneath the level of the current page */
				if ($_GET["date"]) {
					$sceiDate = $_GET["date"];
				} else {
					$sceiDate = date("Y-m-d");
				}

				$dateEndScei = date_create($sceiDate);
				date_add($dateEndScei,date_interval_create_from_date_string("10 days"));
				$dateEndSceiSQL = date_format($dateEndScei,"Y-m-d");

				switch (date("l", strtotime($sceiDate))) {
					case "Saturday":
						$day_title= "D&eacute; Sathairn";
						break;
					case "Sunday":
				 		$day_title= "D&eacute; Domhnaigh";
						break;
					case "Monday":
				 		$day_title= "D&eacute; Luain";
						break;
					case "Tuesday":
				 		$day_title= "D&eacute; M&aacute;irt";
						break;
					case "Wednesday":
				 		$day_title= "D&eacute; C&eacute;adaoin";
						break;
					case "Thursday":
				 		$day_title= "D&eacute;ardaoin";
						break;
					case "Friday":
				 		$day_title= "D&eacute; hAoine";
						break;
				}

				switch (date("F", strtotime($sceiDate))) {
					case "January":
				 		$month_title= "Ean&aacute;ir";
						break;
				 	case "February":
				 		$month_title= "Feabhra";
						break;
				 	case "March":
				 		$month_title= "M&aacute;rta";
						break;
				 	case "April":
				 		$month_title= "Aibre&aacute;n";
						break;
				 	case "May":
				 		$month_title= "Bealtaine";
						break;
				 	case "June":
				 		$month_title= "Meitheamh";
						break;
				 	case "July":
				 		$month_title= "I&uacute;il";
						break;
				 	case "August":
				 		$month_title= "L&uacute;nasa";
						break;
				 	case "September":
				 		$month_title= "Me&aacute;n F&oacute;mhair";
						break;
				 	case "October":
				 		$month_title= "Deireadh F&oacute;mhair";
						break;
				 	case "November":
				 		$month_title= "Samhain";
						break;
				 	case "December":
				 		$month_title= "Nollaig";
						break;
				}

				if ($dayTitles = $wpdb->get_results("SELECT DISTINCT fldDate FROM tg4_site_schedule WHERE fldDate >= '$sceiDate' AND fldDate <= '$dateEndSceiSQL' ORDER BY fldDate ASC")) {  ?>
					<?php foreach ($dayTitles as $dayTitle) { ?>
						<a style="cursor:pointer;" onclick="showDate('<?php echo $dayTitle->fldDate; ?>')" class="day-link <?php //echo ($dayTitle->fldDate == $sceiDate ? 'day-selected' : ''); ?>"><?php echo (ICL_LANGUAGE_CODE == "ga" ? schedule_daily_abbr(date("D", strtotime($dayTitle->fldDate))) : date("D", strtotime($dayTitle->fldDate))); ?><span class="sched-date"><?php echo (ICL_LANGUAGE_CODE == "ga" ? date("j ", strtotime($dayTitle->fldDate)) . schedule_daily_abbr(date("M", strtotime($dayTitle->fldDate))) : date(" j M ", strtotime($dayTitle->fldDate))); ?></span></a>
					<?php } } ?>
			</div>
		</div>
	</div>
	<div id="sched-list-wrap"><?php include(locate_template('/includes/daily-schedule-ajax.php')); ?></div>
</section>

<?php get_footer(); ?>