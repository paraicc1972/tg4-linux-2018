<?php /* Template Name: Xmas Schedule - Ajax */ ?>

<?php get_header(); ?>

<script>
function showDate(str) {
    if (str == "") {
        document.getElementById("sched-list-wrap").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("sched-list-wrap").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET","<?php echo site_url(); ?>/wp-content/themes/tg4-starter/includes/daily-schedule.php?date="+str+"&lang=<?php echo ICL_LANGUAGE_CODE; ?>",true);
        xmlhttp.send();
    }
}
</script>

<div class="section-header-xmas">
	<h1 class="section-title-xmas">
        <?php echo get_the_title($post->post_parent); ?>
	</h1>
</div>

<?php
$feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
if ($feat_image != '') { ?>
    <img src ='<?php echo $feat_image; ?>'>
<?php } ?>

<section class="prog-feat-section">
	<div class="section-panel-white">
	    <div class="prog-feat center-panel">
	    	<div class="prog-feat-wrap">
	    		<?php echo apply_filters('the_content', get_post_field('post_content', $post_id)); ?>
	    	</div>

            <?php if(have_rows('file_name') || have_rows('image_name')) { ?>
            <div class="prog-feat-ad mod-ad">
                <!-- Uploaded Files to Appear here -->
                <div class="prog-feat-file mod-file">
                <?php while(have_rows('file_name')): the_row();
                    if (get_sub_field('file_title')) { ?>
                        <div class="file-content-wrap">
                            <div class="file-btn"><a href="<?php the_sub_field('upload_file') ?>" class="mod-file" target="_blank"><div class="file-btnImg"></div></a></div>
                            <div class="file-content">
                                <a href="<?php the_sub_field('upload_file') ?>" class="mod-file" target="_blank"><?php the_sub_field('file_title'); ?></a>
                             </div>
                        </div>
                <?php } endwhile; ?>
                </div>
                <!-- Uploaded Image to Appear here -->
                <div>
                    <?php
                    while(have_rows('image_name')): the_row();
                        $sideImage = get_sub_field('upload_image');
                    ?>
                        <image src="<?php echo $sideImage['url']; ?>" style="border:1px solid #043244; margin-bottom:10px;">
                    <?php endwhile; ?>
                </div>
            </div>
            <?php } ?>
		
	    </div>
	</div>
</section>
<div id="sched-list-wrap"><?php include(locate_template('/includes/daily-schedule-xmas.php')); ?></div>

<!-- Video Anchor Tag 
<a name="video"></a>
<section class="prog-feat-section">
    <div class="section-panel-dark-3">
        <div class="title-tab-wrap">
            <h2 class="title-tab-dark-3"><?php //echo (ICL_LANGUAGE_CODE == "ga" ? 'Físeáin Faoi Thrácht' : 'Featured Video'); ?></h2>
        </div>
        <div class="prog-feat center-panel">
            <div class="prog-feat-wrap">
                <video id="PromoVideo" class="vjs-big-play-centered"
                controls preload="auto" width="100%" height="auto">
                <source src="<?php //echo get_field("promo_video"); ?>" type='video/mp4' />
                    <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
                </video>
            </div>
            <div class="prog-feat-ad mod-ad">
                <h2 class="mod-ad-title"><?php //echo (ICL_LANGUAGE_CODE == "ga" ? 'Fógraíocht' : 'Advertisement'); ?></h2>
                <div class="ad-wrapper">
                    <iframe src="<?php //echo get_template_directory_uri(); ?>/mpu-banner.htm" height="250" width="300" scrolling="no" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</section-->

<?php get_footer(); ?>