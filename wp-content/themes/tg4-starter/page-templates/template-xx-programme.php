<?php /* Template Name: XX Programme */ ?>

<?php get_header(); ?>

<div class="section-header">
	<h1 class="section-title">
		<?php
		if (get_the_title($post->post_parent) == "Programmes" || get_the_title($post->post_parent) == "Cláir" || get_the_title($post->post_parent) == "Sport" || get_the_title($post->post_parent) == "Spórt") {
			echo get_the_title($post->ID);
		} else {
			echo get_the_title($post->post_parent);
		}
		?>
	</h1>
	<div class="showing-times">
		<?php 
		$broadcast_day = get_field("broadcast_day");
		$broadcast_time = get_field("broadcast_time");
		if ($broadcast_day) { ?>
			<div class="showing-box">
				<div class="showing-day"><?php echo (ICL_LANGUAGE_CODE == "ga" ? substr($broadcast_day, 0, 4) : substr($broadcast_day, 0, 3)); ?></div>
				<div class="showing-time"><?php echo $broadcast_time; ?></div>
			</div>
		<?php }
		$repeat_day = get_field("repeat_day");
		$repeat_time = get_field("repeat_time");
		if ($repeat_day) { ?>
			<div class="showing-box">
				<div class="showing-day"><?php echo (ICL_LANGUAGE_CODE == "ga" ? substr($repeat_day, 0, 4) : substr($repeat_day, 0, 3)); ?></div>
				<div class="showing-time"><?php echo $repeat_time; ?></div>
			</div>
		<?php } ?>
	</div>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
	<div class="section-submenu-wrap">
		<ul class="section-submenu-list">
			<?php 
			//echo $post->ID . " - " . $post->post_parent;
    		if ($post->post_parent == 22890 || $post->post_parent == 22893) {
				wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=1&exclude=24634,23883');
			} elseif ($post->post_parent == 24634 || $post->post_parent == 23883) {
				if (ICL_LANGUAGE_CODE == "ga") {
					wp_list_pages('sort_column=menu_order&title_li=&child_of=22890&depth=1&exclude=24634,23883');
				}else{
					wp_list_pages('sort_column=menu_order&title_li=&child_of=22893&depth=1&exclude=24634,23883');
				}
			}
			?>

            <li>
            <select style="margin-left: 20px;" name="page-dropdown"
                onchange='document.location.href=this.options[this.selectedIndex].value;'> 
                <option value=""><?php echo esc_attr(((ICL_LANGUAGE_CODE == "ga" ? 'Na Blianta' : 'Down the Years'))); ?></option> 
                <?php
                if (ICL_LANGUAGE_CODE == "ga") {
                    $xxYearsPageID = 24634;
                } else { 
                    $xxYearsPageID = 23883;
                } 
                //$pages = get_pages();
                $pages = get_pages(array('child_of' => $xxYearsPageID, 'sort_column' => 'menu_order', 'sort_order' => 'asc', 'exclude' => '16670, 16673'));
                foreach ($pages as $page) {
                    $option = '<option value="' . get_page_link($page->ID) . '">';
                    $option .= $page->post_title;
                    $option .= '</option>';
                    echo $option;
                }
                ?>
            </select>
        	</li>
			&nbsp;
		</ul>
	</div>
</div>

<?php
$feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));

if ($feat_image != '') {
	if (get_field("centre_feature_image") == '0' || get_field("centre_feature_image") == 'no') {
		$feat_Class = "prog-head";
	} elseif (get_field("centre_feature_image") == '1' || get_field("centre_feature_image") == 'yes')  {
		$feat_Class = "prog-headcenter";
	} else {
		$feat_Class = "prog-head";
	}
?>
	<section class="<?php echo $feat_Class; ?>" style="background-image: url(<?php echo $feat_image; ?>);">
		<div class="blockContainer">
			<?php 
			if (get_field("intro_text")) { ?>
				<div class="content-block">
					<h2 class="content-subtitle"><?php echo get_field("intro_sub_title"); ?></h2>
					<p><?php echo get_field("intro_text"); ?></p>
				</div>
			<?php } ?>
		</div>
	</section>
<?php }
$categoryTag = get_field("category_tag");
$seriesTag = get_field("series_tag"); 

if (get_field("promo_video")) { ?>
	<!-- Video Anchor Tag -->
	<a name="video"></a>
	<section class="prog-feat-section">
		<div class="section-panel-dark-3">
			<div class="title-tab-wrap">
				<h2 class="title-tab-dark-3"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Físeáin Faoi Thrácht' : 'Featured Video'); ?></h2>
			</div>
			<div class="prog-feat center-panel">
				<div class="prog-feat-wrap">
					<video id="PromoVideo" class="vjs-big-play-centered"
					controls preload="auto" width="100%" height="auto">
					<source src="<?php echo get_field("promo_video"); ?>" type='video/mp4' />
						<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
					</video>
				</div>
				<div class="prog-feat-ad mod-ad">
					<h2 class="mod-ad-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Fógraíocht' : 'Advertisement'); ?></h2>
					<div class="ad-wrapper">
	                	<iframe src="<?php echo get_template_directory_uri(); ?>/mpu-banner.htm" height="250" width="300" scrolling="no" frameborder="0"></iframe>
	            	</div>
				</div>
			</div>
		</div>
	</section>
	<div class="prog-feat-intro-wrap">
		<div class="center-panel">
			<div class="prog-feat-intro">
				<div class="prog-feat-toolbar">
					<div class="prog-remind">
						<div class="prog-remind-txt"><?php echo get_field("promo_title") ?></div>
					</div>
					<div class="prog-share">
						<!-- Facebook -->
						<a href="#" onClick="window.open('http://www.facebook.com/sharer.php?u=<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>','TG4/Facebook Share','resizable,height=350,width=500'); return false;" class="facebook-share">Facebook</a>
						<noscript><a href="http://www.facebook.com/sharer.php?u=<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" target="_blank" class="facebook-share">Facebook</a></noscript>

						<!-- Twitter -->	
						<a href="#" onClick="window.open('https://twitter.com/share?text=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Féach seo...' : 'Féach seo...'); ?>&amp;url=<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>&amp;hashtags=TG4','TG4/Twitter Share','resizable,height=350,width=500'); return false;" class="twitter-share">Twitter</a>
						<noscript><a href="https://twitter.com/share?text=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Féach seo...' : 'Féach seo...'); ?>&amp;url=<?php echo 'http://'. $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>&amp;hashtags=TG4" target="_blank" class="twitter-share">Twitter</a></noscript>
						
						<!-- Email -->
						<a href="mailto:?Subject=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Suíomh TG4 - ' . site_url() . '' : 'TG4 Website - ' . site_url() . ''); ?>&amp;Body=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'A%20Chara,%0A%0AB\'fhéidir%20go%20mbeadh%20suim%20agat%20sa%20chlár%20seo%20ar%20suíomh%20idirlín%20TG4!%0A%0A' : 'A%20Chara,%0A%0AI%20saw%20this%20on%20the%20TG4%20website%20and%20thought%20you%20might%20be%20interested!%0A%0A'); ?><?php echo 'http://'. $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" class="email-share">Email</a>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php }

if (get_post_field("post_content")) { ?>
	<section class="prog-feat-section">
		<div class="section-panel-white">
		    <div class="prog-feat center-panel">
		    	<!-- POFL: 27.2.16 - Add Refresh & Timer for Social Walls -->
		    	<?php if ($post->ID == 13735 || $post->ID == 13733 || $post->ID == 13322 || $post->ID == 13211) { ?>
				    <script>
						function checklength(i) {
						    'use strict';
						    if (i < 10) {
						        i = "0" + i;
						    }
						    return i;
						}

						var minutes, seconds, count, counter, timer;
						count = 600; //seconds
						counter = setInterval(timer, 1000);

						function timer() {
						    'use strict';
						    count = count - 1;
						    minutes = checklength(Math.floor(count / 60));
						    seconds = checklength(count - minutes * 60);
						    if (count < 0) {
						        clearInterval(counter);
						        return;
						    }
						    document.getElementById("timer").innerHTML = 'Athlódáil/Refresh ' + minutes + ':' + seconds + ' ';
						    if (count === 0) {
						        location.reload();
						    }
						}
					</script>
					<span id="timer"></span>
				<?php } ?>
				<div class="prog-feat-wrap">
		    		<?php echo apply_filters('the_content', get_post_field('post_content', $post_id)); ?>
		    		<?php if (get_field("content_blue_box")) { ?>
				    	<div class="prog-feat center-panel">
				    		<div class="content-blue-box-1"><?php echo get_field("content_blue_box"); ?></div>
				    	</div>
				    <?php } ?>
				</div>
		    
				<?php if(have_rows('file_name') || have_rows('image_name')) { ?>
					<div class="prog-feat-ad mod-ad">
						<!-- Uploaded Files to Appear here -->
						<div class="prog-feat-file mod-file">
					    <?php while(have_rows('file_name')): the_row();
							if (get_sub_field('file_title')) { ?>
					    	<div class="file-content-wrap">
	                            <div class="file-btn"><a href="<?php the_sub_field('upload_file') ?>" class="mod-file" target="_blank"><div class="file-btnImg"></div></a></div>
	                            <div class="file-content">
	                                <a href="<?php the_sub_field('upload_file') ?>" class="mod-file" target="_blank"><?php the_sub_field('file_title'); ?></a>
	                             </div>
	                        </div>
					    <?php } endwhile; ?>
			        	</div>
						<!-- Uploaded Image to Appear here -->
						<div>
						    <?php
						    while(have_rows('image_name')): the_row();
						    	$sideImage = get_sub_field('upload_image');
						    ?>
		                        <image src="<?php echo $sideImage['url']; ?>" style="border:1px solid #043244; margin-bottom:10px;">
						    <?php endwhile; ?>
			        	</div>
			        </div>
				<?php } ?>
		    </div>
		</div>
	</section>
<?php } 

if (get_post_field("social_stream_tag")) { ?>
	<section class="prog-feat-section">
		<div class="section-panel-pale-2">
		    <div class="prog-feat center-panel" style="margin-top:-40px;">
				<?php echo apply_filters('the_content', get_post_field('social_stream_tag', $post_id)); ?>
		    </div>
		</div>
	</section>
<?php } ?>

<!-- Angular -->
<script src= "https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular-touch.min.js"></script>
<!-- Angular -->
<section class="section-panel-dark-2">
	<div class="title-tab-wrap">
		<h2 class="title-tab-dark-2"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Físeáin ar Fáil' : 'Available Videos'); ?></h2>
	</div>
	<?php include(locate_template('/includes/featured-videos-api.php')); ?>
</section>

<?php get_footer(); ?>