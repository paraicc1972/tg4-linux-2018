<?php /* Template Name: News */ ?>

<?php get_header(); ?>

<div class="section-header">
	<h1 class="section-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Nuacht TG4' : 'Irish News'); ?></h1>
</div>

<?php
$aid = ctype_digit($_GET['id']) ? $_GET['id'] : '';
if ($aid) { ?>
    <section class="prog-feat-section">
        <div class="section-panel-white">
            <div class="prog-feat center-panel">
                <div class="prog-feat-wrap">
                <?php
                $rawData = file_get_contents('https://api.rte.ie/rteapi/list/?category=nuacht&format=json&'.time());
                $jsonBack = json_decode($rawData, true);
                for ($i=0; $i<$jsonBack['rows']; $i++) {
                    if ($aid == $jsonBack ['documents'][$i]['id']) {
                        $textTime = substr($jsonBack ['documents'][$i]['date_modified'],11,5);
                        $textDate = substr($jsonBack ['documents'][$i]['date_modified'],0,10);
                        $textYear = substr($textDate,0,4);
                        $textMonth = substr($textDate,5,2);
                        $textDays = substr($textDate,8,2);
                        //$imgUrlPr = substr($jsonBack ['documents'][$i]['thumbnail_url'],0,29);
                        //$imgUrlEx = substr($jsonBack ['documents'][$i]['thumbnail_url'],29,4);
                        //$imgUrl = $imgUrlPr.'-642'.$imgUrlEx;
                        $imgNews = $jsonBack['documents'][$i]['thumbnail_url_large'];
                        switch ($textMonth) {
                            case '01':
                                if (ICL_LANGUAGE_CODE == "ga" ? $Month='EANÁIR' : $Month='JANUARY');
                                break;
                            case '02':
                                if (ICL_LANGUAGE_CODE == "ga" ? $Month='FEABHRA' : $Month='FEBRUARY');
                                break;
                            case '03':
                                if (ICL_LANGUAGE_CODE == "ga" ? $Month='MÁRTA' : $Month='MARCH');
                                break;
                            case '04':
                                if (ICL_LANGUAGE_CODE == "ga" ? $Month='AIBREÁN' : $Month='APRIL');
                                break;
                            case '05':
                                if (ICL_LANGUAGE_CODE == "ga" ? $Month='BEALTAINE' : $Month='MAY');
                                break;
                            case '06':
                                if (ICL_LANGUAGE_CODE == "ga" ? $Month='MEITHEAMH' : $Month='JUNE');
                                break;
                            case '07':
                                if (ICL_LANGUAGE_CODE == "ga" ? $Month='IÚIL' : $Month='JULY');
                                break;
                            case '08':
                                if (ICL_LANGUAGE_CODE == "ga" ? $Month='LÚNASA' : $Month='AUGUST');
                                break;
                            case '09':
                                if (ICL_LANGUAGE_CODE == "ga" ? $Month='MEÁN FÓMHAR' : $Month='SEPTEMBER');
                                break;
                            case '10':
                                if (ICL_LANGUAGE_CODE == "ga" ? $Month='DEIREADH FÓMHAR' : $Month='OCTOBER');
                                break;
                            case '11':
                                if (ICL_LANGUAGE_CODE == "ga" ? $Month='SAMHAIN' : $Month='NOVEMBER');
                                break;
                            case '12':
                                if (ICL_LANGUAGE_CODE == "ga" ? $Month='NOLLAIG' : $Month='DECEMBER');
                                break;
                        } ?>
                        <div class="news-item-wrap">
                            <div><img class="news-art-img" src="<?php echo $imgNews; ?>" alt="<?php echo $jsonBack['documents'][$i]['thumbnail_caption']; ?>"></div>
                            <div class="news-art-content">
                                <h5 class="news-art-sub"><?php echo $textDays.' '.$Month.' '.$textYear.' : '.$textTime; ?></h5>
                                <h5 class="news-art-title"><?php echo $jsonBack ['documents'][$i]['title']; ?></h5>
                                <p class="news-art-desc"><?php echo $jsonBack ['documents'][$i]['content']; ?></p>
                                <p class="news-item-sub"><?php //echo gapp_get_post_pageviews(); ?></p>
                            </div>
                        </div>
                        <div class="news-back">
                            <a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/nuacht' : '/en/irish-news') ?>" class="back-button"><?php echo (ICL_LANGUAGE_CODE == "ga" ? ' SIAR ' : ' BACK '); ?></a>
                        </div>
                    <?php break; } 
                } ?>
                </div>
                <div class="prog-feat-ad mod-ad-dark">
                    <h2 class="mod-ad-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Fógraíocht' : 'Advertisement'); ?></h2>
                    <div class="ad-wrapper">
                        <iframe src="<?php echo get_template_directory_uri(); ?>/mpu-banner.htm" height="250" width="300" scrolling="no" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } else { ?>
    <section class="prog-feat-section">
        <div class="section-panel-white">
            <div class="prog-feat center-panel">
                <div class="prog-feat-wrap">
                <ul class="news-list">
                    <?php
                    $rawData = file_get_contents('https://api.rte.ie/rteapi/list/?category=nuacht&format=json');
                    $jsonBack = json_decode($rawData, true);
                    for ($i=0; $i<$jsonBack['rows']; $i++) {
                        $itemId   = $jsonBack ['documents'][$i]['id'];
                        $textTime = substr($jsonBack ['documents'][$i]['date_modified'],11,5);
                        $textDate = substr($jsonBack ['documents'][$i]['date_modified'],0,10);
                        $textYear = substr($textDate,0,4);
                        $textMonth = substr($textDate,5,2);
                        $textDays = substr($textDate,8,2);
                        switch ($textMonth) {
                            case '01':
                                if (ICL_LANGUAGE_CODE == "ga" ? $Month='EANÁIR' : $Month='JANUARY');
                                break;
                            case '02':
                                if (ICL_LANGUAGE_CODE == "ga" ? $Month='FEABHRA' : $Month='FEBRUARY');
                                break;
                            case '03':
                                if (ICL_LANGUAGE_CODE == "ga" ? $Month='MÁRTA' : $Month='MARCH');
                                break;
                            case '04':
                                if (ICL_LANGUAGE_CODE == "ga" ? $Month='AIBREÁN' : $Month='APRIL');
                                break;
                            case '05':
                                if (ICL_LANGUAGE_CODE == "ga" ? $Month='BEALTAINE' : $Month='MAY');
                                break;
                            case '06':
                                if (ICL_LANGUAGE_CODE == "ga" ? $Month='MEITHEAMH' : $Month='JUNE');
                                break;
                            case '07':
                                if (ICL_LANGUAGE_CODE == "ga" ? $Month='IÚIL' : $Month='JULY');
                                break;
                            case '08':
                                if (ICL_LANGUAGE_CODE == "ga" ? $Month='LÚNASA' : $Month='AUGUST');
                                break;
                            case '09':
                                if (ICL_LANGUAGE_CODE == "ga" ? $Month='MEÁN FÓMHAR' : $Month='SEPTEMBER');
                                break;
                            case '10':
                                if (ICL_LANGUAGE_CODE == "ga" ? $Month='DEIREADH FÓMHAR' : $Month='OCTOBER');
                                break;
                            case '11':
                                if (ICL_LANGUAGE_CODE == "ga" ? $Month='SAMHAIN' : $Month='NOVEMBER');
                                break;
                            case '12':
                                if (ICL_LANGUAGE_CODE == "ga" ? $Month='NOLLAIG' : $Month='DECEMBER');
                                break;
                        }
                        if ($jsonBack['documents'][$i]['highest_rank'] == 1) {  ?>
                            <li>
                                <div class="news-item-wrap">
                                    <a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/nuacht' : '/en/irish-news') ?>?id=<?php echo $itemId ?>" class="news-item-img MH-sched-item" style="background-image: url(https://placehold.it/197x111);"><img src="<?php echo $jsonBack ['documents'][$i]['thumbnail_url'] ?>" alt="<?php echo $jsonBack['documents'][$i]['thumbnail_caption']; ?>"></a>
                                    <div class="news-item-content">
                                        <h5 class="news-item-sub"><?php echo $textDays.' '.$Month.' '.$textYear.', '.$textTime; ?></h5>
                                        <a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/nuacht' : '/en/irish-news') ?>?id=<?php echo $itemId ?>"><h5 class="news-item-title"><?php echo $jsonBack ['documents'][$i]['title']; ?></h5></a>
                                        <p class="news-item-desc"><?php echo $jsonBack ['documents'][$i]['excerpt']; ?></p>
                                        <p class="news-item-sub"><?php //echo gapp_get_post_pageviews(); ?></p>
                                    </div>
                                </div>
                            </li>
                        <?php } else {  ?>
                            <li>
                                <div class="news-item-wrap">
                                    <a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/nuacht' : '/en/irish-news') ?>?id=<?php echo $itemId ?>" class="news-item-img MH-sched-item" style="background-image: url(https://placehold.it/197x111);"><img src="<?php echo $jsonBack ['documents'][$i]['thumbnail_url'] ?>" alt="<?php echo $jsonBack['documents'][$i]['thumbnail_caption']; ?>"></a>
                                    <div class="news-item-content">
                                        <h5 class="news-item-sub"><?php echo $textDays.' '.$Month.' '.$textYear.' : '.$textTime; ?></h5>
                                        <a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/nuacht' : '/en/irish-news') ?>?id=<?php echo $itemId ?>"><h4 class="news-item-title"><?php echo $jsonBack ['documents'][$i]['title']; ?></h4></a>
                                        <p class="news-item-desc"><?php echo $jsonBack ['documents'][$i]['excerpt']; ?></p>
                                        <p class="news-item-sub"><?php //echo gapp_get_post_pageviews(); ?></p>
                                    </div>
                                </div>
                            </li>
                        <?php }
                    } ?>
                    <div style="float: left; clear: none; width: 38.8%; margin-left: 0; margin-right: 2%; padding: 10px 0px;">
                        <span style="display: block; padding: 0px 10px 0px 10px; background-color: #c7dae2; float: left; font-size: 14px;">Soláthraí:  Nuacht RTÉ/TG4</span>
                    </div>
                </ul>
                </div>
                <div class="prog-feat-ad mod-ad-dark">
                    <h2 class="mod-ad-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Fógraíocht' : 'Advertisement'); ?></h2>
                    <div class="ad-wrapper">
                        <iframe src="<?php echo get_template_directory_uri(); ?>/mpu-banner.htm" height="250" width="300" scrolling="no" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } 
get_footer(); ?>