<?php /* Template Name: MolScéal Article - API */ ?>

<?php include(locate_template('/header-molsceal-article.php'));

$parent = get_post($post->post_parent);
$slug = $parent->post_name;

if(isset($_GET['CID']) && $_GET['CID'] != '') {
    $videoID = ctype_digit($_GET['CID']) ? $_GET['CID'] : '';
} else {
    if (is_numeric(substr(rtrim($_SERVER["REQUEST_URI"], "/"), -13)) == 1) {
       $videoID = substr(rtrim($_SERVER["REQUEST_URI"], "/"), -13);
    } elseif (is_numeric(substr(stristr(rtrim($_SERVER["REQUEST_URI"], "/"), "/alt/"), 5, 13)) == 1) {
       $videoID = substr(stristr(rtrim($_SERVER["REQUEST_URI"], "/"), "/alt/"), 5, 13);
    }
}

switch ($slug) {
     case 'nua':
        $strNewCSS = "current_page_nua";
        $strPlaylistID = "5775228437001";
        break;
    case 'nuacht':
        $strNewsCSS = "current_page_nuacht";
        $strPlaylistID = "5772339413001";
        break;
    case 'sport':
        $strSportCSS = "current_page_sport";
        $strPlaylistID = "5772600968001";
        break;
    case 'siamsaiocht':
        $strEntCSS = "current_page_siamsaiocht";
        $strPlaylistID = "5772600967001";
        break;
    case 'trendail':
        $strTrendCSS = "current_page_trendail";
        $strPlaylistID = "5772244063001";
        break;
    case 'cartlann':
        $strArcCSS = "current_page_cartlann";
        $strPlaylistID = "5772600969001";
        break;
    case 'toradh':
        $strArcCSS = "current_page_toradh";
        break;
    case 'connachta':
        $strPlaylistID = "5772600970001";
        break;
    case 'mumha':
        $strPlaylistID = "5771851311001";
        break;
    case 'laighean':
        $strPlaylistID = "5772600971001";
        break;
    case 'uladh':
        $strPlaylistID = "5772183599001";
        break;
    default:
}
?>

<style>
.socialicon {
  padding: 1px;
  font-size: 25px;
  width: 30px;
  text-align: center;
  border: 0px solid #999999;
}

/* .fa:hover {
    color: #cccccc;
} */

.fa:active, .fa:focus {
    color: #cccccc;
    outline: none;
}

.fa-facebook {
  color: #999999;
}

.fa-twitter {
  color: #999999;
  margin-right: 2px;
}

.fa-google {
  color: #999999;
}

.fa-linkedin {
  color: #999999;
  margin-right: 4px;
}

.fa-youtube {
  color: #999999;
}

.fa-instagram {
  color: #999999;
  margin-right: 2px;
}

.fa-pinterest {
  color: #999999;
}

.fa-snapchat-ghost {
  color: #999999;
  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
}

.fa-skype {
  color: #999999;
}

.fa-android {
  color: #999999;
}

.fa-dribbble {
  color: #999999;
}

.fa-vimeo {
  color: #999999;
}

.fa-tumblr {
  color: #999999;
}

.fa-vine {
  color: #999999;
}

.fa-foursquare {
  color: #999999;
}

.fa-stumbleupon {
  color: #999999;
}

.fa-flickr {
  color: #999999;
}

.fa-yahoo {
  color: #999999;
}

.fa-envelope-o {
  color: #999999;
}

.fa-whatsapp {
  color: #999999;
}

.fa-share-alt {
  color: #999999;
}

.mod-5_<?php echo $slug; ?> {
    display:none;
}

.white-space-pre {
    white-space: pre-wrap;
}

.video-js .vjs-dock-title {
    color: #00d6f2;
    font-size: 1.2em;
}

.video-js .vjs-dock-description {
    display: none;
}
</style>

<!-- Sub-navigation -->
<div class="nav-bar-molsceal">
    <div class="submenu-wrapper">
        <ul class="section-submenu-molsceal">
            <li class="page_item <?php echo $strNewCSS; ?>"><a href="<?php echo site_url(); ?>/nua/">Nua</a></li>
            <li class="page_item <?php echo $strNewsCSS; ?>"><a href="<?php echo site_url(); ?>/nuacht/">Nuacht</a></li>
            <li class="page_item <?php echo $strSportCSS; ?>"><a href="<?php echo site_url(); ?>/sport/">Spórt</a></li>
            <li class="page_item <?php echo $strEntCSS; ?>"><a href="<?php echo site_url(); ?>/siamsaiocht/">Siamsaíocht</a></li>
            <li class="page_item <?php echo $strTrendCSS; ?>"><a href="<?php echo site_url(); ?>/trendail/">Trendáil</a></li>
            <li class="page_item <?php echo $strArcCSS; ?>"><a href="<?php echo site_url(); ?>/cartlann/">Cartlann</a></li>
            &nbsp;
        </ul>
    </div>
</div>

<!-- Angular -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-1.0.3.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular-sanitize.js"></script>
<!-- Angular -->

<section class="molsceal-plyrmods">
    <h2 class="visuallyhidden">Tuilleadh ar Molscéal</h2>
    <div class="molscealplyr-wrapper">
        <div class="molsceal-plyr-wrap">
            <section class="mod-6_{{pVideo.custom_fields.genre}}">
                <div class="molsceal-mod-notice"><!-- {{pVideo.duration}} -->
                    <div style="position:relative; display:block; max-width:800px;">
                        <div style="padding-top: 56.25%;" ng-show="pVideo.duration > 2000">
                            <video data-video-id="<?php echo $videoID ?>" data-account="5561472261001" data-player="default" data-embed="default" data-application-id class="video-js" controls style="position:absolute; top:0px; right:0px; bottom:0px; left:0px; width:100%; height:100%;"></video>
                            <script src="//players.brightcove.net/5561472261001/default_default/index.min.js"></script>
                        </div>
                        <span ng-show="pVideo.duration <= 2000"><img ng-if="pVideo.poster.length>0" ng-src="{{pVideo.poster}}" alt=""></span>
                    </div>
                    <div class="molsceal-article">
                        <div class="molsceal-articletext">
                            <?php 
                            /* echo is_numeric(substr(rtrim($_SERVER["REQUEST_URI"], "/"), -13));
                            echo is_numeric(substr(stristr(rtrim($_SERVER["REQUEST_URI"], "/"), "/alt/"), 5, 13));
                            echo "</br>" . substr(stristr(rtrim($_SERVER["REQUEST_URI"], "/"), "/alt/"), 5, 13); */
                            ?>
                            <p class="molsceal-genreheader" ng-bind="pVideo.custom_fields.teideal"></p>
                            <p ng-bind="pVideo.long_description" class="white-space-pre"></p>
                            <!-- <p class="molsceal-genretext" ng-bind-html="deliberatelyTrustDangerousSnippet()"></p> -->
                        </div>
                        <!-- <div ng-bind-html="deliberatelyTrustDangerousSnippet()"></div> -->
                        <div>
                            <div style="width:65%;">
                                <p ng-if="pVideo.custom_fields.leiritheoir != ''" class="molsceal-newssrc">Scéal ó <span ng-bind="pVideo.custom_fields.leiritheoir"></span></p>
                                <hr></hr>
                                <p ng-if="pVideo.custom_fields.totalviews.length>0" class="molsceal-newssrc">&nbsp;<img ng-if="pVideo.custom_fields.totalviews.length>0" src='https://d1og0s8nlbd0hm.cloudfront.net/images/MolSceal-Suil-1.png' style='margin-top:-2px;'>&nbsp;<span ng-bind="pVideo.custom_fields.totalviews | number : fractionSize"></span></p>
                                <hr ng-if="pVideo.custom_fields.totalviews.length>0"></hr>
                                <a ng-if="pVideo.custom_fields.fblink != ''" href="#" ng-Click="openFbWindow();" class="fa fa-facebook socialicon"></a>
                                <a ng-if="pVideo.custom_fields.fblink == ''" href="#" ng-Click="openFbWindow();" class="fa fa-facebook socialicon"></a>
                                <a ng-if="pVideo.custom_fields.twlink != ''" href="#" ng-Click="openTwWindow();" class="fa fa-twitter socialicon"></a>
                                <a ng-if="pVideo.custom_fields.twlink == ''" href="#" ng-Click="openTwWindow();" class="fa fa-twitter socialicon"></a>
                                <a class="fa fa-share-alt sharetoggle socialicon"></a>
                                <div id="sharedd">
                                    <a href="#" onClick="window.open('https://plus.google.com/share?url=<?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>','Molscéal/Google Share','resizable,height=500,width=500'); return false;" class="fa fa-google socialicon"></a>
                                    <a href="mailto:?Subject=Molscéal - <?php echo 'https://' . $_SERVER['HTTP_HOST']; ?>&amp;Body=A%20Chara,%0A%0AScéal%20ó%20Molscéal%20a%20thaitneoidh%20leat!%0A%0A<?php echo str_replace('"',"'", $molTitle) . ' - https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" class="fa fa-envelope-o socialicon"></a>
                                    <!--
                                    <a href="#" onClick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=<?php //echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>&title=<?php //echo urlencode($molTitle); ?>&source=Molscéal','Molscéal/LinkedIn Share','resizable,height=500,width=500'); return false;" class="fa fa-linkedin socialicon"></a>
                                    <a href="#" class="fa fa-youtube"></a>&nbsp;
                                    <a href="#" class="fa fa-instagram"></a><br/><br/>
                                    <a href="#" class="fa fa-pinterest"></a>&nbsp;
                                    <a href="#" class="fa fa-snapchat-ghost"></a>&nbsp;
                                    <a href="#" class="fa fa-whatsapp"></a>&nbsp;
                                    -->
                                </div>
                                <hr></hr>
                            </div>
                            <div style="width:100%;">
                                <p class="molsceal-newsdate" ng-bind="(ScheduledDate)"></p>
                                <p class="molsceal-genretitle" style="margin-top:-25px; float:right;" ng-bind="pVideo.custom_fields.genre"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>

<div class="more-bar-molsceal">
    <div class="submenu-wrapper">Tuilleadh mar seo...</div>
</div>

<section class="molsceal-moremods">
    <h2 class="visuallyhidden">Tuilleadh mar seo...</h2>
    <div class="molscealmods-wrapper">
        <div class="molsceal-mod-wrap">
            <section ng-if="p.id!=<?php echo $videoID; ?>" ng-repeat="p in filterData = pAll | limitTo:10:10*(page-1)" class="mod-5_{{p.custom_fields.genre}}">
                <div class="molsceal-mod-notice">
                    <div class="molsceal-img{{$index}}">
                        <a href="<?php echo site_url(); ?>/<?php echo $slug; ?>/alt?CID={{p.id}}" class="prog-panel">
                            <span ng-if="p.poster.length>0"><img ng-src="{{p.poster}}" class="molsceal-articleimg{{$index}}"></span>
                            <span ng-if="p.poster.length<0"><img src="https://via.placeholder.com/300x169"></span>
                        </a>
                        <span ng-show="p.duration > 2000" class="videoDur"><i class="fa fa-play" style="margin-right:5px;"></i> <span ng-bind="p.duration | milliSecondsToTimeCode"></span></span>
                    </div>
                    <div class="molsceal-textholder{{$index}}">
                        <div class="molsceal-text">
                            <p class="molsceal-genretext"  ng-bind="p.custom_fields.teideal"></p>
                        </div>
                        <div class="molsceal-view{{$index}}">
                            <div class="molsceal-genreview"><img ng-if="p.custom_fields.totalviews>100" src='https://d1og0s8nlbd0hm.cloudfront.net/images/MolSceal-Suil-1.png' style='margin-top:-2px;'>&nbsp;<span ng-if="p.custom_fields.totalviews>100" ng-bind="p.custom_fields.totalviews | number : fractionSize"></span></div>
                            <div class="molsceal-genretitle" ng-bind="p.custom_fields.genre"></div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div ng-show="pCnt>9" style="margin: 10px 0 20px 0; text-align:center;">
            <uib-pagination class="pagination" total-items="filterData.length" ng-model="page"
      ng-change="pageChanged()" previous-text="&lsaquo;" next-text="&rsaquo;" items-per-page=9></uib-pagination>
        </div>
    </div>
</section>

<script>
var molscealApp=angular.module("molscealApp", ["ui.bootstrap", "ngSanitize"]);

molscealApp.filter('milliSecondsToTimeCode', function () {
return function msToTime(duration) {
    var milliseconds = parseInt((duration % 1000) / 100)
        , seconds = parseInt((duration / 1000) % 60)
        , minutes = parseInt((duration / (1000 * 60)) % 60)
        , hours = parseInt((duration / (1000 * 60 * 60)) % 24);

    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;

    if (hours<1) {
        return minutes + ":" + seconds;
    } else {
        return hours + ":" + minutes + ":" + seconds;
    }
    };
});

molscealApp.controller('molscealCtrl', function($scope, $sce, $http, $window) {
    $scope.page = 1;    

    //* Check querystring parameters for Article ID
    var params={};
    decodeURIComponent(location.search).replace(/[?&]+([^=&]+)=([^&]*)/gi,function(m,k,v){ params[k]=v });

    var headerParam = { headers: {
        "Accept": "application/json;odata=verbose;pk=BCpkADawqM3NldaK46lJCpDvZHF4oJAikn_67MO2s3FacOBR2qBakkoIpKGFayXcSJBAhjTM8zoluB8TYwSTBftfCcKEY5qEZQFyO4XRmeXyxCmn-jF2IcS0X5HrweheUB-wUzBMcOZobiXR"
        }
    };

    var tagPoint = "";
    //var endPointVideo = '/videos/' + params.CID;
    var endPointVideo = '/videos/' + <?php echo $videoID; ?>;

    $http.get('https://edge.api.brightcove.com/playback/v1/accounts/5561472261001' + endPointVideo, headerParam).then(function(d) {
        $scope.pVideo = d.data;
        //console.log($scope.pVideo, $scope.pVideo.tags, $scope.pVideo.tags.length);
            
        $scope.pFacebook = d.data.custom_fields.fblink;
        $scope.pTwitter= d.data.custom_fields.twlink;
        /* console.log($scope.pFacebook);
        console.log($scope.pTwitter); */

        if ($scope.pTwitter != undefined) {
            var splitTwitter = $scope.pTwitter.split("/");
        }

        //console.log(splitTwitter[5]);

        for(var intCnt=0; intCnt<$scope.pVideo.tags.length; intCnt++) {
            $scope.RelatedTags = $scope.pVideo.tags[intCnt];
            if($scope.RelatedTags.indexOf("$") != '-1') {
                tagPoint += $scope.RelatedTags;
            }
        }

        /* console.log($scope.pVideo, $scope.pVideo.tags, $scope.pVideo.custom_fields.eolasgaeilge);
        $scope.pVideoDesc = '<iframe width="730" height="411" src="https://www.youtube.com/embed/9-PjnwVfmlo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
        $scope.pVideoDesc = $scope.pVideo.custom_fields.eolasgaeilge; */
        $scope.pVideoDesc = $scope.pVideo.long_description;
        $scope.pVideoDate = $scope.pVideo.custom_fields.date;

        var splitDate = $scope.pVideoDate.split("/");
        $scope.ScheduledDate = splitDate[2].substring(0, 2) + "." + splitDate[1] + "." + splitDate[0] + " " + splitDate[2].substring(2, 12);

        //console.log("Date: ", $scope.pVideoDate, $scope.ScheduledDate);
        
        $scope.deliberatelyTrustDangerousSnippet = function() {
            return $sce.trustAsHtml($scope.pVideoDesc);
        };

        $scope.openFbWindow = function() {
            if ($scope.pFacebook) {
                $window.open('http://www.facebook.com/sharer.php?u=' + $scope.pFacebook, 'Molscéal/Facebook Share', 'width=500, height=500');
            } else {
                $window.open('http://www.facebook.com/sharer.php?u=<?php echo "https://". $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>', 'Molscéal/Facebook Share', 'width=500, height=500');
            }
        };

        $scope.openTwWindow = function() {
            if ($scope.pTwitter) {
                $window.open('https://twitter.com/intent/retweet?tweet_id=' + splitTwitter[5], 'Molscéal/Twitter Share', 'width=500, height=500');
            } else {
                $window.open('https://twitter.com/share?text=Féach seo - <?php echo urlencode($molTitle); ?> ...&amp;url=<?php echo "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>&amp;hashtags=Molscéal', 'Molscéal/Twitter Share', 'width=500, height=500');
            }
        };

        //console.log("Tags: ", tagPoint.split('$'));

        var arrTag = tagPoint.split('$');
        var tagQry = '';

        for(var tagCnt=0; tagCnt<arrTag.length; tagCnt++) {
            if(arrTag[tagCnt] != '') {
                /* console.log("Tags: ", arrTag[tagCnt], arrTag.length);
                console.log(arrTag[tagCnt], "Space:", arrTag[tagCnt].includes(" ")); */
                if (arrTag[tagCnt].includes(" ") == true) {
                    tagQry += "%22%24"+arrTag[tagCnt]+"%22%2C";
                } else {
                    tagQry += "%24"+arrTag[tagCnt]+"%2C";
                }
            }
        }

        //console.log("Tag Query: ", tagQry);

        //var endPoint = '/playlists/<?php echo $strPlaylistID;?>';
        //var endPoint = '/videos?q=%2Bplayable%3Atrue+tags%3A%22%24d%C3%B3nall+%C3%B3+conaill%22%2C%24derrynane%2C%22%24daniel+o%27connell%22%2C%24stair%2C%24history';
        //var endPoint = '/videos?q=%2Bplayable%3Atrue+tags%3A%24daniel%20o%27connell%2C%24derrynane%2C%24d%C3%B3nall%20%C3%B3%20conaill%2C%24history%2C%24stair';

        var endPoint = '/videos?q=%2Bplayable%3Atrue+tags%3A'+ tagQry +'%A1';
        $http.get('https://edge.api.brightcove.com/playback/v1/accounts/5561472261001' + endPoint, headerParam).then(function(d) {
            $scope.pAll = d.data.videos;
            /* $scope.pDisplay = $scope.pAll.slice(0, 10);
            $scope.pCnt = d.data.videos.length;
            console.log($scope.pAll, $scope.pCnt, $scope.pDisplay); */

            $scope.pageChanged = function() {
                var startPos = ($scope.page - 1) * 10;
                $scope.pDisplay = $scope.pAll.slice(startPos, startPos + 10);
                //console.log($scope.page, $scope.pDisplay);
            };
        });
    });
});
</script>

<?php include(locate_template('/footer-molsceal.php')); ?>