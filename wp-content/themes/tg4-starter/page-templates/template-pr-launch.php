<?php /* Template Name: Press Release - Launch */ ?>

<?php get_header(); ?>

<div class="section-header">
	<h1 class="section-title">
		<?php echo get_the_title($post->post_parent); ?>
	</h1>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
    <div class="section-submenu-wrap">
        <ul class="section-submenu-list">
			<?php 
			//echo $post->ID . " - " . $post->post_parent
			if (ICL_LANGUAGE_CODE == "ga") {
				wp_list_pages('sort_column=menu_order&title_li=&child_of=1003&depth=1');
			} else {
				wp_list_pages('sort_column=menu_order&title_li=&child_of=982&depth=1');
			}
			?>
            &nbsp;
        </ul>
    </div>
</div>

<?php
$feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));

if ($feat_image != '') {
	if (get_field("centre_feature_image") == '0' ||  get_field("centre_feature_image") == 'no') {
		$feat_Class = "prog-head";
	} elseif (get_field("centre_feature_image") == '1' ||  get_field("centre_feature_image") == 'yes')  {
		$feat_Class = "prog-headcenter";
	} else {
		$feat_Class = "prog-head";
	}
?>
	<section class="<?php echo $feat_Class; ?>" style="background-image: url(<?php echo $feat_image; ?>);">
		<div class="content">
	        <?php 
	        if (get_field("intro_text")) { ?>
			<div class="press-block">
	            <h2 class="content-subtitle"><?php echo get_field("intro_title"); ?></h2>
	        </div>
	        <?php } ?>
		</div>
	</section>
<?php }
if (get_field("promo_video")) { ?>
	<!-- Video Anchor Tag -->
	<a name="video"></a>
	<section class="prog-feat-section">
		<div class="section-panel-dark-3">
			<div class="title-tab-wrap">
				<h2 class="title-tab-dark-3"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Físeáin Faoi Thrácht' : 'Featured Video'); ?></h2>
			</div>
			<div class="prog-feat center-panel">
				<div class="prog-feat-wrap">
					<video id="PromoVideo" class="vjs-big-play-centered"
					controls preload="auto" width="100%" height="auto">
					<source src="<?php echo get_field("promo_video"); ?>" type='video/mp4' />
						<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
					</video>
				</div>
				<div class="prog-feat-ad mod-ad-dark">
					<h2 class="mod-ad-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Fógraíocht' : 'Advertisement'); ?></h2>
					<div class="ad-wrapper">
                		<iframe src="<?php echo get_template_directory_uri(); ?>/mpu-banner.htm" height="250" width="300" scrolling="no" frameborder="0"></iframe>
            		</div>
				</div>
			</div>
		</div>
	</section>
	<div class="prog-feat-intro-wrap">
		<div class="center-panel">
			<div class="prog-feat-intro">
				<div class="prog-feat-toolbar">
					<div class="prog-remind">
						<div class="prog-remind-txt"><?php echo get_field("promo_title") ?></div>
					</div>
					<div class="prog-share">
						<!-- Facebook -->
						<a href="#" onClick="window.open('http://www.facebook.com/sharer.php?u=<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>','TG4/Facebook Share','resizable,height=350,width=500'); return false;" class="facebook-share">Facebook</a>
						<noscript><a href="http://www.facebook.com/sharer.php?u=<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" target="_blank" class="facebook-share">Facebook</a></noscript>

						<!-- Twitter -->	
						<a href="#" onClick="window.open('https://twitter.com/share?text=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Féach seo...' : 'Féach seo...'); ?>&amp;url=<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>&amp;hashtags=TG4','TG4/Twitter Share','resizable,height=350,width=500'); return false;" class="twitter-share">Twitter</a>
						<noscript><a href="https://twitter.com/share?text=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Féach seo...' : 'Féach seo...'); ?>&amp;url=<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>&amp;hashtags=TG4" target="_blank" class="twitter-share">Twitter</a></noscript>
						
						<!-- Email -->
						<a href="mailto:?Subject=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Suíomh TG4 - ' . site_url() . '' : 'TG4 Website - ' . site_url() . ''); ?>&amp;Body=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'A%20Chara,%0A%0AB\'fhéidir%20go%20mbeadh%20suim%20agat%20sa%20chlár%20seo%20ar%20suíomh%20idirlín%20TG4!%0A%0A' : 'A%20Chara,%0A%0AI%20saw%20this%20on%20the%20TG4%20website%20and%20thought%20you%20might%20be%20interested!%0A%0A'); ?><?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" class="email-share">Email</a>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php }

if (get_field("intro_text")) { ?>
	<section class="prog-feat-section">
		<div class="section-panel-pale-4">
		    <div class="prog-feat center-panel">
		    	<div class="prog-feat-wrap"><br /><?php echo get_field("intro_text"); ?></div>
		    </div>
		</div>
	</section>
<?php } ?>

<section class="prog-feat-section">
	<div class="section-panel-white">
	    <div class="prog-feat center-panel">
	    	<div class="prog-feat-wrap"><?php echo apply_filters('the_content', get_post_field('post_content', $post_id)); ?>
	    		<?php if (get_field("content_blue_box")) { ?>
			    <div class="prog-feat center-panel" style="margin-bottom: 10px;">
			    	<div class="content-blue-box-1"><?php echo get_field("content_blue_box"); ?></div>
			    </div>
			    <?php } ?>
	    	</div>
			<?php if(have_rows('file_name')): ?>
				<div class="prog-feat-file mod-file">
			    <?php while(have_rows('file_name')): the_row();
					if (get_sub_field('file_title')) { ?>
				    	<div class="file-content-wrap">
	                        <div class="file-btn"><a href="<?php the_sub_field('upload_file') ?>" class="mod-file" target="_blank"><div class="file-btnImg"></div></a></div>
	                        <div class="file-content">
	                            <a href="<?php the_sub_field('upload_file') ?>" class="mod-file" target="_blank"><?php the_sub_field('file_title'); ?></a>
	                         </div>
	                    </div>
				        <!-- <a href="<?php //the_sub_field('upload_file') ?>" class="mod-file" target="_blank"><?php //the_sub_field('file_title'); ?></a> -->
			    <?php } endwhile; ?>
	        	</div>
			<?php endif; 
			if (!get_field("promo_video")) { ?>
				<div class="prog-feat-ad mod-ad-dark" style="margin-top: 10px;">
		            <h2 class="mod-ad-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Fógraíocht' : 'Advertisement'); ?></h2>
		            <div class="ad-wrapper">
	                	<iframe src="<?php echo get_template_directory_uri(); ?>/mpu-banner.htm" height="250" width="300" scrolling="no" frameborder="0"></iframe>
	            	</div>
		        </div>
		    <?php } ?>
	    </div>
	</div>
</section>

<?php get_footer(); ?>