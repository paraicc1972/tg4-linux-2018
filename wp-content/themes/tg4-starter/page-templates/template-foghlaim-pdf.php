<?php /* Template Name: Foghlaim - PDF Listing */ ?>

<?php get_header(); ?>

<div class="section-header">
	<h1 class="section-title">
		<?php echo get_the_title($post->post_parent); ?>
	</h1>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
	<div class="section-submenu-wrap">
		<ul class="section-submenu-list-foghlaim">
            <?php
			wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=1&exclude=32582,93940,93943');
            /* if(SwpmMemberUtils::is_member_logged_in()) {
            	wp_list_pages('include=35640&title_li=');
            } */
            ?>&nbsp;
        </ul>
	</div>
</div>

<?php
$feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));

if ($feat_image != '') {
	if (get_field("centre_feature_image") == '0' ||  get_field("centre_feature_image") == 'no') {
		$feat_Class = "prog-head";
	} elseif (get_field("centre_feature_image") == '1' ||  get_field("centre_feature_image") == 'yes')  {
		$feat_Class = "prog-headcenter";
	} else {
		$feat_Class = "prog-head";
	}
?>
	<section class="<?php echo $feat_Class; ?>" style="background-image: url(<?php echo $feat_image; ?>);">
		<div class="content">
	        <?php 
	        if (get_field("intro_text")) { ?>
			<div class="content-block">
	            <h2 class="content-subtitle"><?php echo get_field("intro_title"); ?></h2>
	            <p><?php echo get_field("intro_sub_title") ?></p>
	        </div>
	        <?php } ?>
		</div>
	</section>
<?php } ?>

<section class="governance-mods">
	<h2 class="visuallyhidden">More on TG4</h2>
	<div class="governancemods-wrapper">
		<div class="prog-feat-wrap"><?php echo apply_filters('the_content', get_post_field('post_content', $post_id)); ?></div>
		<?php 
		/* if(SwpmMemberUtils::is_member_logged_in()) {
			$member_id = SwpmMemberUtils::get_logged_in_members_id();

			global $wpdb;
			$foghlaimMembers = $wpdb->get_results("SELECT * FROM tg_swpm_form_builder_custom;");

			foreach($foghlaimMembers as $foghlaimMember) {
				if ($member_id == $foghlaimMember->user_id && $foghlaimMember->field_id == 55) {
					$member_access = $foghlaimMember->value;
				}
			} */

			//if(have_rows('file_name') && $member_access == "teagascóir"):
			if(have_rows('file_name')): ?>
				<div class="prog-feat-wrap"><h2><?php //echo (ICL_LANGUAGE_CODE == "ga" ? '' : ''); ?></h2></div>
				<div class="governance-mod-wrap">
				<?php while(have_rows('file_name')): the_row();
					if (get_sub_field('upload_file')) { ?>
						<section class="mod-1">
			                <a href="<?php the_sub_field('upload_file') ?>" target="_blank">
			                <div class="governance-mod-notice">
			                	<div class="file-btn"><a href="<?php the_sub_field('upload_file') ?>" class="mod-file" target="_blank"><div class="file-btnImg"></div></a></div>
			                    <div class="file-title"><a href="<?php the_sub_field('upload_file') ?>" class="governance-title" target="_blank"><?php the_sub_field('file_title'); ?><br /><?php the_sub_field('file_subtitle'); ?></a></div>
			                </div>
			                </a> 
			            </section>
				<?php } endwhile; ?>
				</div>
		<?php endif; 
		//} ?>
	</div>
</section>

<?php get_footer(); ?>