<?php /* Template Name: Live Player - FleadhTV */ ?>

<?php get_header(); ?>

<script type="text/javascript">
    if (window.innerWidth <= 835) { 
        playerW=window.innerWidth-58;
        playerH=playerW/1.777;
    } else {
        playerW=835;
        playerH=470;
    }

    <?php
    $clientIP = $_SERVER['HTTP_X_FORWARDED_FOR']?: $_SERVER['HTTP_CLIENT_IP']?: $_SERVER['REMOTE_ADDR'];
    /* if ($clientIP == "::1") {
        $clientIP = "77.75.98.34"; //IE
        $clientIP = "92.16.251.233"; //GB
        $clientIP = "94.0.69.150"; //NIR
    } */

    $request       = "https://www.tg4tech.com/api/programmes/countryCode?id=" . $clientIP;
    $ch            = curl_init($request);
    curl_setopt_array($ch, array(
            CURLOPT_POST           => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER     => array('Content-type: application/x-www-form-urlencoded'),
        ));
    $response = curl_exec($ch);

    // Check for errors
    if ($response === FALSE) {
        die(curl_error($ch));
    }
    curl_close($ch);

    $responseData = json_decode($response, TRUE);
    $geoCode = $responseData["country"];
    
    if ($geoCode == 'IE') { ?>
        url = 'http://csm-e.cds1.yospace.com/csm/live/74246540.m3u8';
    <?php } else { ?>
        url = 'http://csm-e.cds1.yospace.com/csm/live/74246610.m3u8';
    <?php } ?>

    console.log(url);
</script>

<!-- <script type="application/javascript" src="http://www.telize.com/geoip?callback=getgeoip"></script>
<script type="application/javascript" src="http://freegeoip.net/json/?callback=getgeoip"></script> -->
<script type="text/javascript" src="https://d1og0s8nlbd0hm.cloudfront.net/yospace/yoplayer-4.1-32.js"></script>

<div class="section-header">
    <h1 class="section-title">
        <?php
        if (get_the_title($post->post_parent) == "Programmes" || get_the_title($post->post_parent) == "Cláir" || get_the_title($post->post_parent) == "Sport" || get_the_title($post->post_parent) == "Spórt") {
            echo get_the_title($post->ID);
        } else {
            echo get_the_title($post->post_parent);
        }
        ?>
    </h1>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
    <div class="section-submenu-wrap">
        <ul class="section-submenu-list">
            <?php wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=1&exclude=6398,6401,15288,15292'); ?>
            &nbsp;
        </ul>
    </div>
</div>

<section class="plyr-live-section">
    <div class="section-panel-dark-3">
        <div class="plyr-feat center-panel">
            <div id="player" class="plyr-feat-wrap">
                <script type="application/javascript">
                function playerSize() {
                    //console.log('Window',window.innerWidth);
                    var viewportwidth;
                    var viewportheight;
              
                    // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
                    if (typeof window.innerWidth != 'undefined')
                    {
                        viewportwidth = window.innerWidth,
                        viewportheight = window.innerHeight
                    }
            
                    if (viewportwidth <= 935) { 
                        playerW=viewportwidth-58;
                        playerH=playerW/1.777;
                        resize(playerW, playerH);
                    } else {
                        playerW=835;
                        playerH=570;
                    }
                    resize(playerW, playerH);
                }
                </script>

                <!-- Yospace Player -->
                <script type="text/javascript">
                    $YOPLAYER('player', {
                        player: 'https://d1og0s8nlbd0hm.cloudfront.net/yospace/yoplayer-4.1-32.swf',
                        height: playerH,
                        width: playerW,
                        buffer: 30,
                        lwm: 5,
                        lss: 5,
                        debug: false,
                        //skin: "https://d1og0s8nlbd0hm.cloudfront.net/yospace/skin2.skin",
                        autostart: true,
                        type: 'hls',
                        adverttext: 'If you like this, why not click to find out more...',
                        file: url,
                        callbacks: {
                            logging: function(obj, msg) {
                                displayMessage(msg);
                            },
                            position: function(id, pos) {
                                playPosition = pos;
                            },
                            advertStart: function(id, data) {
                                displayMessage("AD Start Event Fired at " + data);
                            },
                            advertEnd: function(id, data) {
                                displayMessage("AD End Event Fired at " + data);
                            },
                            advertBreakStart: function(id, pos) {
                                displayMessage("AD Break Start Event Fired at " + pos);
                            },
                            advertBreakEnd: function(id, pos) {
                                displayMessage("AD Break End Event Fired at " + pos);
                            }
                        }
                    });
                    function play(url, stream) {
                        $YOFIND('player').play(url);
                    }
                    function resize(playerW, playerH) {
                        $YOFIND('player').resize(playerW,playerH);
                    }
                    function displayMessage(msg) {
                    }
                </script>
            </div>
        </div>
    </div>
    <div class="plyr-feat-intro-wrap" style="padding:0px;">
        <div class="center-panel">
            <div class="prog-feat-intro">
                <div class="prog-feat-toolbar">
                    <div class="prog-remind">
                        <div class="prog-remind-txt"><?php echo (ICL_LANGUAGE_CODE == "ga" ? "FleadhTV 2017 - Beo: Lúnasa 18 - 20, 8.30pm - 11.30pm" : "FleadhTV 2017 - Live: August 18th - 20th, 8.30pm - 11.30pm"); ?></div>
                    </div>
                    <div class="prog-share">
                        <a href="" class="facebook-share">Facebook</a>
                        <a href="" class="twitter-share">Twitter</a>
                        <a href="" class="email-share">Email</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="prog-feat-section">
    <div class="section-panel-white">
        <div class="prog-feat center-panel">
            <div class="prog-feat-wrap"><?php echo apply_filters('the_content', get_post_field('post_content', $post_id)); ?>
                <?php if (get_field("content_blue_box")) { ?>
                <div class="prog-feat center-panel">
                    <div class="content-blue-box-1"><?php echo get_field("content_blue_box"); ?></div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>

<section class="section-panel-blue">
    <div class="title-tab-wrap">
        <h2 class="title-tab-blue"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Inniu ar TG4' : 'Today on TG4'); ?></h2>
    </div>
    <?php get_template_part('/includes/tonight-slider'); ?>
    <a href="<?php echo $_SERVER["REQUEST_URI"]; ?>schedule" class="view-full"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Sceideal Iomlán' : 'View full schedule'); ?></a>
</section>

<?php get_footer(); ?>