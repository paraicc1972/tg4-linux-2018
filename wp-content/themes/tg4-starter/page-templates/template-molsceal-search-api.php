<?php /* Template Name: MolScéal Search - API */ ?>

<?php include(locate_template('/header-molsceal.php'));

$srchTxt = $_GET['search'] ? $_GET['search'] : '';

switch ($post->post_name) {
    case 'nua':
        $strNewCSS = "current_page_nua";
        break;
    case 'nuacht':
        $strNewsCSS = "current_page_nuacht";
        break;
    case 'sport':
        $strSportCSS = "current_page_sport";
        break;
    case 'siamsaiocht':
        $strEntCSS = "current_page_siamsaiocht";
        break;
    case 'trendail':
        $strTrendCSS = "current_page_trendail";
        break;
    case 'cartlann':
        $strArcCSS = "current_page_cartlann";
        break;
    default:
}
?>

<!-- Sub-navigation -->
<div class="nav-bar-molsceal">
    <div class="submenu-wrapper">
        <ul class="section-submenu-molsceal">
            <li class="page_item <?php echo $strNewCSS; ?>"><a href="<?php echo site_url(); ?>/nua/">Nua</a></li>
            <li class="page_item <?php echo $strNewsCSS; ?>"><a href="<?php echo site_url(); ?>/nuacht/">Nuacht</a></li>
            <li class="page_item <?php echo $strSportCSS; ?>"><a href="<?php echo site_url(); ?>/sport/">Spórt</a></li>
            <li class="page_item <?php echo $strEntCSS; ?>"><a href="<?php echo site_url(); ?>/siamsaiocht/">Siamsaíocht</a></li>
            <li class="page_item <?php echo $strTrendCSS; ?>"><a href="<?php echo site_url(); ?>/trendail/">Trendáil</a></li>
            <li class="page_item <?php echo $strArcCSS; ?>"><a href="<?php echo site_url(); ?>/cartlann/">Cartlann</a></li>
            &nbsp;
        </ul>
    </div>
</div>

<section class="molsceal-innermods">
    <h2 class="visuallyhidden">Tuilleadh ar Molscéal</h2>
    <div class="molscealmods-wrapper" ng-app="molscealApp" ng-controller="molscealCtrl">
        <div class="molsceal-mod-wrap">
        <section ng-repeat="p in filterData = pAll | limitTo:9:9*(page-1)" class="mod-5_{{p.custom_fields.genre}}">
            <div class="molsceal-mod-notice">
                <div class="molsceal-img{{$index}}">
                    <a href="<?php echo site_url(); ?>/<?php echo $post->post_name; ?>/alt?CID={{p.id}}&search=<?php echo $srchTxt; ?>" class="prog-panel">
                        <span ng-if="p.images.poster.src.length>0"><img ng-src="{{p.images.poster.src}}" class="molsceal-articleimg{{$index}}"></span>
                        <span ng-if="p.images.poster.src.length<0"><img src="https://via.placeholder.com/300x169"></span>
                    </a>
                    <span ng-show="p.duration > 2000" class="videoDur"><i class="fa fa-play" style="margin-right:5px;"></i> <span ng-bind="p.duration | milliSecondsToTimeCode"></span></span>
                </div>
                <div class="molsceal-textholder{{$index}}">
                    <div class="molsceal-text">
                        <p class="molsceal-genretext" ng-bind="p.custom_fields.teideal"></p>
                    </div>
                    <div class="molsceal-view{{$index}}">
                        <div class="molsceal-genreview"><img ng-if="p.custom_fields.totalviews>100" src='https://d1og0s8nlbd0hm.cloudfront.net/images/MolSceal-Suil-1.png' style='margin-top:-2px;'>&nbsp;<span ng-if="p.custom_fields.totalviews>100" ng-bind="p.custom_fields.totalviews | number : fractionSize"></span></div>
                        <div class="molsceal-genretitle" ng-bind="p.custom_fields.genre"></div>
                    </div>
                </div>
            </div>
        </section>
        </div>
        <div ng-show="pCnt>9" style="margin: 10px 0 20px 0; text-align:center;">
            <uib-pagination class="pagination" total-items="filterData.length" ng-model="page"
      ng-change="pageChanged()" previous-text="&lsaquo;" next-text="&rsaquo;" items-per-page=9></uib-pagination>
        </div>
        <div ng-if="pCnt<1" style="margin: 10px 0 20px 0; text-align:center;"><h2><br />Gan toradh. Bain triail eile as.</h2></div>
    </div>
</section>

<!-- Angular -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-1.0.3.js"></script>
<!-- Angular -->

<script>
var molscealApp=angular.module("molscealApp", ["ui.bootstrap"]);

molscealApp.filter('milliSecondsToTimeCode', function () {
return function msToTime(duration) {
    var milliseconds = parseInt((duration % 1000) / 100)
        , seconds = parseInt((duration / 1000) % 60)
        , minutes = parseInt((duration / (1000 * 60)) % 60)
        , hours = parseInt((duration / (1000 * 60 * 60)) % 24);

    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;

    if (hours<1) {
        return minutes + ":" + seconds;
    } else {
        return hours + ":" + minutes + ":" + seconds;
    }
    };
});

molscealApp.controller('molscealCtrl', function($scope, $http) {
    $scope.page = 1;    

    //* Check querystring parameters for Article ID
    var params={};
    decodeURIComponent(location.search).replace(/[?&]+([^=&]+)=([^&]*)/gi,function(m,k,v){ params[k]=v });

    $http.post("<?php echo get_template_directory_uri(); ?>/assets/php/molsceal-proxy.php").then(function(response) {
        $scope.tAuth = response.data;
        //console.log($scope.tAuth);
        if ($scope.tAuth) {
            var headerParam = { headers: {
                "Authorization": "Bearer " + $scope.tAuth,
                "Accept": "application/json;odata=verbose"
                }
            };

            /* var headerParam = { headers: {
                "Accept": "application/json;odata=verbose;pk=BCpkADawqM3NldaK46lJCpDvZHF4oJAikn_67MO2s3FacOBR2qBakkoIpKGFayXcSJBAhjTM8zoluB8TYwSTBftfCcKEY5qEZQFyO4XRmeXyxCmn-jF2IcS0X5HrweheUB-wUzBMcOZobiXR"
                }
            };

            var endPoint = '/videos?limit=36&offset=0&sort=&q=%2Bplayable%3Atrue+%22' + params.search + '%22';
            $http.get('https://edge.api.brightcove.com/playback/v1/accounts/5561472261001' + endPoint, headerParam).then(function(d) { */

            var endPoint = '/videos?limit=54&offset=0&sort=&q=%2B' + params.search + '+%2Bplayable%3Atrue';
            //var endPoint = '/videos?limit=36&offset=0&sort=&q=%2BGAA+%2Bplayable%3Atrue';
            $http.get('https://cms.api.brightcove.com/v1/accounts/5561472261001' + endPoint, headerParam).then(function(d) {
                $scope.pAll = d.data;
                $scope.pCnt = d.data.length;
                $scope.pDisplay = $scope.pAll.slice(0, 9);
                if ($scope.pCnt > 0) {
                    $scope.pImage = $scope.pAll[0].images.poster.sources[0].src;
                }

                //$scope.pCnt = d.data.videos.length;
                //console.log($scope.pAll, $scope.pCnt);
                //console.log($scope.pAll[0].images.poster);
                //console.log($scope.pAll[0].images.poster.sources[0].src);
                //console.log("Image: ", $scope.pImage);

                $scope.pageChanged = function() {
                    var startPos = ($scope.page - 1) * 9;
                    $scope.pDisplay = $scope.pAll.slice(startPos, startPos + 9);
                    //console.log($scope.page, $scope.pDisplay);
                };
            });
        }
    });
});
</script>

<?php include(locate_template('/footer-molsceal.php')); ?>