<?php /* Template Name: Corporate */ ?>

<?php get_header(); ?>

<script type="text/javascript">
	jQuery(document).ready(function($) {
	  	$('#SubDate').val('<?php echo date("Y-m-d"); ?>');
	});
</script>

<?php if ($post->ID != 721 && $post->ID != 724 && $post->ID != 2849 && $post->ID != 2868) { ?>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
		  	if ($(":checkbox").prop('checked', false)) {
				$(":checkbox").prop('checked', true);
		  	}
		    $(":checkbox").bind("click", false);
		});
	</script>
<?php } ?>

<div class="section-header">
	<h1 class="section-title">
		<?php echo get_the_title($post->post_parent); ?>
	</h1>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
	<div class="section-submenu-wrap">
		<?php if (is_tree(30204)) { ?>
			<ul class="section-submenu-list-foghlaim">
		<?php } else { ?>
			<ul class="section-submenu-list">
			<?php }
			//echo $post->ID . " - " . $post->post_title . " - " . $post->post_parent;
			if ($post->ID != 721 && $post->ID != 724 && $post->ID != 17054 && $post->ID != 17128) {
	    		if ($post->post_parent != 137 && $post->post_parent != 140) {
					wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=1&exclude=32582');
				} elseif ($post->ID != 137 && $post->ID != 140) {
					wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->ID. '&depth=1&exclude=32582');
				}
			}
			if (is_tree(17054) || is_tree(17128)) {
            	wp_list_pages('include=17054,17128&title_li=');
			}
			if (is_tree(30204)) {
            	wp_list_pages('include=30204&title_li=');
			}
			?>
			&nbsp;&nbsp;
		</ul>
	</div>
</div>

<?php
$feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));

if ($feat_image != '') {
	if (get_field("centre_feature_image") == '0' ||  get_field("centre_feature_image") == 'no') {
		$feat_Class = "prog-head";
	} elseif (get_field("centre_feature_image") == '1' ||  get_field("centre_feature_image") == 'yes')  {
		$feat_Class = "prog-headcenter";
	} else {
		$feat_Class = "prog-head";
	}
?>
	<section class="<?php echo $feat_Class; ?>" style="background-image: url(<?php echo $feat_image; ?>);">
		<div class="content">
	        <?php 
	        if (get_field("intro_text")) { ?>
			<div class="content-block">
	            <h2 class="content-subtitle"><?php echo get_field("intro_title"); ?></h2>
	            <p><?php echo get_field("intro_sub_title") ?></p>
	        </div>
	        <?php } ?>
		</div>
	</section>
<?php }

if (get_field("intro_text")) { ?>
	<section class="prog-feat-section">
		<div class="section-panel-pale-4">
		    <div class="prog-feat center-panel">
		    	<div class="prog-feat-wrap"><br /><?php echo get_field("intro_text"); ?></div>
				<div class="prog-feat-ad mod-ad">
		            <h2 class="mod-ad-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Fógraíocht' : 'Advertisement'); ?></h2>
		            <div class="ad-wrapper">
		                <iframe src="<?php echo get_template_directory_uri(); ?>/mpu-banner.htm" height="250" width="300" scrolling="no" frameborder="0"></iframe>
		            </div>
		        </div>
		    </div>
		</div>
	</section>
<?php } ?>

<section class="prog-feat-section">
	<div class="section-panel-white">
	    <div class="prog-feat center-panel">
	    	<div class="prog-feat-wrap"><?php echo apply_filters('the_content', get_post_field('post_content', $post_id)); ?></div>
			<?php if(have_rows('file_name')): ?>
				<div class="prog-feat-file mod-file">
			    <?php while(have_rows('file_name')): the_row();
					if (get_sub_field('file_title')) { ?>
				    	<div class="file-content-wrap">
                            <div class="file-btn"><a href="<?php the_sub_field('upload_file') ?>" class="mod-file" target="_blank"><div class="file-btnImg"></div></a></div>
                            <div class="file-content">
                                <a href="<?php the_sub_field('upload_file') ?>" class="mod-file" target="_blank"><?php the_sub_field('file_title'); ?></a>
                             </div>
                        </div>
				        <!-- <a href="<?php //the_sub_field('upload_file') ?>" class="mod-file" target="_blank"><?php //the_sub_field('file_title'); ?></a> -->
			    <?php } endwhile; ?>
	        	</div>
			<?php endif; 
			if (!get_field("intro_text")) { ?>
				<div class="prog-feat-ad mod-ad-dark" style="margin-top: 10px;">
		            <h2 class="mod-ad-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Fógraíocht' : 'Advertisement'); ?></h2>
		            <div class="ad-wrapper">
	                	<iframe src="<?php echo get_template_directory_uri(); ?>/mpu-banner.htm" height="250" width="300" scrolling="no" frameborder="0"></iframe>
	            	</div>
		        </div>
		    <?php } ?>
	    </div>
	</div>
</section>

<?php get_footer(); ?>