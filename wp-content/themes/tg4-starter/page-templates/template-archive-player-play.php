<?php /* Template Name: Archive Content - Player */ ?>

<?php include(locate_template('/header-archive.php')); ?>

<div class="section-header">
    <h1 class="section-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Seinnteoir Cartlann TG4' : 'TG4 Archive Player'); ?></h1>
    <p>&nbsp;</p>
</div>

<?php $MatID = $_GET['mat_id']; ?>

<!-- Sub-navigation -->
<div class="section-submenu">
    <div class="section-submenu-wrap">
        <ul class="section-submenu-list">
            <?php wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=1&exclude=6398,6401,15288,15292,16939,16941,16468,16469'); ?>
            &nbsp;
        </ul>
    </div>
</div>
<?php if (SwpmMemberUtils::is_member_logged_in()) { ?>
    <section class="plyr-feat-section" ng-app="myApp" ng-controller="myCtrl">
    <div class="srch-feat-plyr-wrap">
        <div class="center-panel">
            <div class="srch-feat-intro">
               <!-- <div class="srchSel-wrap">
                    <select class="srchSel" ng-model="srt" ng-init="srt='prog_first_tx_date'" ng-change="srtBy(srt)">
                        <option value='prog_first_tx_date' disabled> {{current_Lang=='ga'?'Rangaigh...':'Sort By...'}}</option>
                        <option value='prog_first_tx_date'> {{current_Lang=='ga'?'Chéad Tx':'1st Tx'}}</option>
                        <option value='prog_dynamic_branding_title'> A - Z </option>
                    </select>
                    <i ng-title="{{current_Lang=='ga'?'Athraigh an Ord':'Toggle Order'}}" ng-click="up=!up" class="hot fa {{up?'fa-sort-amount-asc':'fa-sort-amount-desc'}}"></i>
                </div> -->
                <div class="arcSel-wrap">
                    <select ng-cloak id="sSel" name="page-dropdown" ng-model="flt" ng-init="flt='All'" onchange="document.location.href=this.options[this.selectedIndex].value;"> 
                        <option value="All" disabled>{{current_Lang=='ga'?'Séanra':'Genre'}}</option>
                        <option value='../full-archive/?progGenre=All' disabled>{{current_Lang=='ga'?'Baile':'Home'}}</option>
                        <option value='../full-archive/?progGenre=Cursaí Reatha'>{{current_Lang=='ga'?'Cúrsaí Reatha':'Current Affairs'}}</option>
                        <option value='../full-archive/?progGenre=Drama' disabled>{{current_Lang=='ga'?'Drámaíocht':'Drama'}}</option>
                        <option value='../full-archive/?progGenre=Cula4' disabled>{{current_Lang=='ga'?'Cúla4':'Cúla4'}}</option>
                        <option value='../full-archive/?progGenre=Sport' disabled>{{current_Lang=='ga'?'Spórt':'Sport'}}</option>
                        <option value='../full-archive/?progGenre=Ceol' disabled>{{current_Lang=='ga'?'Ceol':'Music'}}</option>
                        <option value='../full-archive/?progGenre=Saolchlar' disabled>{{current_Lang=='ga'?'Saolchlár':'Lifestyle'}}</option>
                        <option value='../full-archive/?progGenre=Siamsaiocht' disabled>{{current_Lang=='ga'?'Siamsaíocht':'Entertainment'}}</option>
                        <option value='../full-archive/?progGenre=Fáisnéis' disabled>{{current_Lang=='ga'?'Faisnéis':'Documentary'}}</option>
                    </select>
                </div>
                <div class="arcform-wrap"><input ng-cloak class="arcform-textbox" name="sbox" id="sbox" ng-model="ss" placeholder="{{current_Lang=='ga'?' Cuimsitheach...':' Advanced...'}}" ng-change="ss1=noFada(ss);" /><a href="#" class="arcform-btn" onclick="document.location.href='../full-archive/?srchTxt=' + document.getElementById('sbox').value">>></a><a class="arcform-hint" title="{{current_Lang=='ga'?'Is féidir cuardach a dhéanamh ar théarma (faoi bhliain ar leith) anseo e.g. Gaillimh 2001':'You can search by a certain term (within a year) here e.g. Galway 2001'}}">?</a><br /><span class="arcform-text" ng-bind="current_Lang=='ga' ? 'Cuardach: Cuimsitheach' : 'Advanced Search'"></span></div>
                <div class="arcform-wrap"><input ng-cloak class="arcform-textbox" name="cbox" id="dbox" ng-model="ds" placeholder="{{current_Lang=='ga'?' Dáta (dd.mm.yyyy)':' Date (dd.mm.yyyy)'}}" ng-change="ss1=noFada(ds);" maxlength="10" /><a href="#" class="arcform-btn" onclick="document.location.href='../full-archive/?srchDate=' + document.getElementById('dbox').value">>></a><br /><span class="arcform-text" ng-bind="current_Lang=='ga' ? 'Dáta' : 'Search: Date'"> (dd.mm.yyyy)</span></div>
                <div class="arcform-wrap"><input ng-cloak class="arcform-textbox" name="tbox" id="tbox" ng-model="ts" placeholder="{{current_Lang=='ga'?' Teideal...':' Title...'}}" ng-change="ss1=noFada(ts);" /><a href="#" class="arcform-btn" onclick="document.location.href='../full-archive/?srchTitle=' + document.getElementById('tbox').value">>></a><br /><span class="arcform-text" ng-bind="current_Lang=='ga' ? 'Cuardach: Teideal' : 'Search: Title'"></span></div>
                <!-- <div class="arcform-wrap"><input ng-cloak class="arcform-textbox" name="cbox" id="cbox" ng-model="cs" placeholder="{{current_Lang=='ga'?' Comhlacht Léiriúchán...':' Production Company...'}}" ng-change="ss1=noFada(cs);" /><a href="#" class="arcform-btn" onclick="document.location.href='../full-archive/?srchComp=' + document.getElementById('cbox').value">>></a><br /><span class="arcform-text" ng-bind="current_Lang=='ga' ? 'Cuardach: Comhlacht Léiriúchán' : 'Search: Production Company'"></span></div> -->
                <div class="arcform-wrap"><input ng-cloak class="arcform-textbox" name="ybox" id="ybox" ng-model="ys" placeholder="{{current_Lang=='ga'?' Bliain...':' Year...'}}" ng-change="ss1=noFada(ys);" maxlength="4" /><a href="#" class="arcform-btn" onclick="document.location.href='../full-archive/?srchYear=' + document.getElementById('ybox').value">>></a><br /><span class="arcform-text" ng-bind="current_Lang=='ga' ? 'Cuardach: Bliain' : 'Search: Year'"></span></div>
            </div>
        </div>
    </div>
<?php } else { ?>
    <section class="plyr-feat-section">
<?php } ?>
    <div class="section-panel-dark-3">
    <?php
    /* Remove need to be logged in for Demo - 24/1/17 */
    if (SwpmMemberUtils::is_member_logged_in()) {
        $tablet_browser = 0;
        $mobile_browser = 0;

        if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
            $tablet_browser++;
        }

        if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
            $mobile_browser++;
        }

        if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
            $mobile_browser++;
        }

        $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
        $mobile_agents = array('w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac','blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno','ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-','maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-','newt','noki','palm','pana','pant','phil','play','port','prox','qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar','sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-','tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp','wapr','webc','winw','winw','xda ','xda-');

        if (in_array($mobile_ua,$mobile_agents)) {
            $mobile_browser++;
        }

        if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
            $mobile_browser++;
            //Check for tablets on opera mini alternative headers
            $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
            if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
                $tablet_browser++;
            }
        }

        // Load Player - 1st check to see if rendition exists
        function isValidURL($url) {
            $file_headers = @get_headers($url);
            if (strpos($file_headers[0], "200 OK") > 0) {
                return true;
            } else {
                return false;
            }
        }

        if (isValidURL("https://dpohx73n66ge6.cloudfront.net/". $MatID . "/" . $MatID . ".mpd")) {
    ?>
            <div class="plyr-feat center-panel">
                <div class="arcplyr-feat-wrap" >
                    <video id="my-video" class="video-js vjs-big-play-centered" style="position:relative; z-index:0;" webkit-playsinline controls autoplay preload="auto" data-setup='{}'>
                        <?php if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'ipad') <= 0) { ?>
                            <!-- MPEG Dash - Cached -->
                            <source src="https://dpohx73n66ge6.cloudfront.net/<?php echo $MatID; ?>/<?php echo $MatID; ?>.mpd" type="application/mpd+xml">
                        <?php } if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'ipad') > 0) { ?>
                            <!-- HLS (for IOS) -->
                            <source src="https://www.tg4tech.com:8081/hls/smil:<?php echo $MatID; ?>.smil/playlist.m3u8" type="application/x-mpegURL">
                        <?php } ?>

                        <!-- MPEG Dash - S3 Bucket -->
                        <!-- <source src="https://tg4-archive-mpd.s3.amazonaws.com/<?php //echo $MatID; ?>/<?php //echo $MatID; ?>.mpd" type="application/mpd+xml"> -->
                        <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
                        <!--track kind="captions" src="subs/<?php //echo $MatID; ?>_eng.vtt" srclang="en" label="English"-->
                    </video>

                    <!-- video.js BC Player Controls -->
                    <script>
                        var myPlayer = videojs("my-video");
                        var titleOverlay = document.getElementsByClassName("vjs-modal-dialog-content")[0]; 
                        var description = document.getElementsByClassName("vjs-modal-dialog-description")[0]; 
                        //var titleOverlay = document.getElementsByClassName("vjs-modal-dialog")[0];
                      
                        //dockSelf.style.widheight = "60px";
                        //dockSelf.style.height = "60px";

                        description.style.height = "60px";
                        //dockSelf.style.backgroundColor = '#FF0000';
                        //dockSelf.style.opacity = 0.72;
                        var cuePoints = [];
                        var restrictions_start = [];
                        var restrictions_end = [];
                        var scene_start = [];
                        var scene_end = [];
                        var scene_metadata = [];
                        var restriction = false;

                        myPlayer.ready(function() {
                            myPlayer.on('loadstart',function() {
                                myPlayer.muted(false);
                                cuePoints = getCuePoints("<?php echo $MatID; ?>");
                            });
                        
                            myPlayer.on("loadedmetadata",function() {
                                // myPlayer.currentTime(71);
                                myPlayer.play();
                            }); //end loadedmetadata
                        
                            myPlayer.on("mouseover",function() {
                                //titleOverlay.style.height = (this.videoHeight() - 50) + "px"
                            });
                        
                            myPlayer.on("mouseout",function() {
                                //titleOverlay.style.height = (this.videoHeight()) + "px"
                            });
                        });
                      
                        myPlayer.on('timeupdate', function (e) {
                            var restriction = false;
                        
                            for (var i = 0; i < restrictions_start.length; i++) {
                                time_now = Math.floor(this.currentTime())
                                if ((time_now >= restrictions_start[i]) && (time_now <= restrictions_end[i])) {
                                    myPlayer.muted(false);
                                    titleOverlay.style.opacity = 0.98;
                                    titleOverlay.style.height = "100%"
                                    titleOverlay.style.backgroundColor = '#000000';
                                    titleOverlay.innerHTML= "<h2>... Tá an ábhar seo srianta | This content is restricted ...</h2>";
                                    restriction = true;
                                } 
                                if (!restriction) {
                                    myPlayer.muted(false);
                                    titleOverlay.style.opacity = 0.0;
                                    description.innerHTML= "";
                                }
                            }
                            for (var i = 0; i < scene_start.length; i++) {
                                if ((time_now >= scene_start[i]) && (time_now <= scene_end[i])) {
                                    description.innerHTML = scene_metadata[i].slice(scene_metadata[i]);
                                }
                            }
                        });
                      
                        function getCuePoints(mat_id) {
                            var url = "https://www.tg4tech.com/api/findbyid?id="+mat_id+"&fields=prog_vod_category,prog_title,prog_production_company,prog_production_year,prog_coord,prog_press_desc_short_eng,prog_press_desc_short_gae,mat_id,prog_dynamic_branding_title,prog_first_tx_date,cue_points&limit=1"
                            var method = "POST";
                            var postData = "";
                            var async = true;
                            var request = new XMLHttpRequest();
                        
                            request.onload = function () {
                                var status = request.status;
                                var data = request.responseText; 
                                var json = JSON.parse(data);
                                var i;

                                console.log("Cuepoint Data", data);
                      
                                cuePoints = json.cue_points
                           
                                for (i in cuePoints) {
                                    if ((cuePoints[i].name == "restriction_start")) {
                                        restrictions_start.push(cuePoints[i].time)
                                    }
                                    if ((cuePoints[i].name == "restriction_end")) {
                                        restrictions_end.push(cuePoints[i].time)
                                    }
                                    if ((cuePoints[i].name == "scene_start")) {
                                        scene_start.push(cuePoints[i].time)
                                        scene_metadata.push(cuePoints[i].metadata)
                                    }
                                    if ((cuePoints[i].name == "scene_end")) {
                                        scene_end.push(cuePoints[i].time)
                                    }
                                }
                            }
                        
                            request.open(method, url, async);
                            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                            request.send();
                        }
                      
                        function detectIphone() {
                            var uagent = navigator.userAgent.toLowerCase();
                            console.log(uagent)
                            if (uagent.search("iphone") > -1)
                                console.log("You have an iPhone!")
                            else
                                console.log("You Don't have an iPhone!")
                        }
                    </script> 
                </div>
                <div class="prog-feat-disc mod-disc">
                    <div style="overflow-y:scroll; height:400px; margin-left:-4px; padding-right:4px;">
                        <ol class="prog-cuepoint">
                            <li ng-repeat="p in pAllScene"> <!--track by p.id"-->
                            <!-- <a href="#" ng-click="cue_log_track(p.incode,p.duration,0)">{{timecode_to_seconds(p.incode,25)}}</a>
                            <a class="arcLink" ng-href="javascript:myPlayer.currentTime('{{(p.incode | limitTo: 8) | timeToSeconds}}'); myPlayer.play();"> -->
                            <a href="#" ng-click="cue_log_track(p.incode,p.duration,0)" class="arcLink" style="cursor:pointer;">{{(p.incode | limitTo: 8)}}: <strong><span ng-if="p.title!=''" ng-bind="p.title"></span></strong></a><span ng-if="p.description!=''"><br /><span ng-bind="p.description"></span></span><span ng-if="p.participants!=''"><br /><span ng-bind="p.participants"></span></span><!-- <span ng-if="p.incode!=''"><br /><span ng-bind="[p.incode]"></span></span> --> (Fad: <span ng-if="p.duration!=''"  ng-bind="(p.duration | limitTo: 8)"></span>)<span ng-if="p.trackTitle!=''"><br /><span ng-bind="p.trackTitle"></span></span></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="plyr-feat-intro-wrap">
            <div class="center-panel">
                <div class="prog-feat-intro">
                    <div class="prog-feat-toolbar">
                        <div class="prog-remind">
                            <div class="prog-remind-txt">
                                <?php 
                                // echo "** " . strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'ipad') . " - " . $_SERVER['HTTP_USER_AGENT'] . " **";
                                /* if (isset($_GET['teideal']) && $_GET['teideal'] != '') {
                                    $progName = $_GET['teideal'];
                                    echo $progName;
                                } */
                                ?>
                                {{pAll.prog_dynamic_branding_title}}, {{pAll.prog_first_tx_date | date: 'dd.MM.yyyy'}}
                                <script>
                                var app = angular.module('myApp', []);
                                /* app.config([
                                    '$compileProvider',
                                    function($compileProvider)
                                    {   
                                        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|javascript|ftp|mailto|chrome-extension):/);
                                        // Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)
                                    }
                                ]); */

                                app.filter('returnInt', function () {
                                    return function (number) {
                                        return parseInt(number, 10);
                                    };
                                })

                                app.filter('timeToSeconds', [
                                    'returnIntFilter',
                                    function (returnIntFilter) {
                                        return function (time) {
                                            return time.split(':')
                                            .reverse()
                                            .map(returnIntFilter)
                                            .reduce(function (pUnit, cUnit, index) {
                                                return pUnit + cUnit * Math.pow(60, index);
                                            });
                                        };
                                    }
                                ]);

                                app.controller('myCtrl', function($scope, $http) {
                                  $http.post("https://www.tg4tech.com/api/findbyid?id=<?php echo $MatID; ?>&fields=prog_vod_category,prog_title,prog_production_company,prog_production_year,prog_coord,prog_press_desc_short_eng,prog_press_desc_short_gae,mat_id,prog_dynamic_branding_title,prog_first_tx_date,cue_points&limit=1")
                                  .then(function(response) {
                                        $scope.timecode_to_seconds = function(timeString, framerate) {
                                            var timeArray = timeString.split(":");
                                            var hours   = timeArray[0] * 60 * 60;
                                            var minutes = timeArray[1] * 60;
                                            var seconds = timeArray[2];
                                            var frames  = timeArray[3]*(1/framerate);
                                          
                                            var totalTime = parseFloat(hours) + parseFloat(minutes) + parseFloat(seconds) + parseFloat(frames) ;
                                            
                                            //console.log(timeString + " = " + totalTime);
                                            //console.log("Totaltime", totalTime);
                                            
                                            return totalTime;
                                        }

                                        $scope.cue_log_track = function(incode, duration, index){
                                            $scope.vidsrc = "https://dpohx73n66ge6.cloudfront.net/<?php echo $MatID; ?>/<?php echo $MatID; ?>.mpd";

                                            var video = document.querySelector('#my-video_html5_api');

                                            $scope.mark_in_tc = $scope.timecode_to_seconds(incode, 25);
                                            $scope.mark_out_tc =$scope.timecode_to_seconds(incode, 25) + parseFloat($scope.timecode_to_seconds(duration, 25));

                                            console.log("Jump to Video time", video, $scope.timecode_to_seconds(incode, 25));
                                                      
                                            video.currentTime = $scope.timecode_to_seconds(incode, 25);
                                            video.play();
                                        }

                                        $scope.pAll = response.data;
                                        console.log("All Data", $scope.pAll);
                                        //console.log("All Scene Tracks", $scope.pAll.scene_track);
                                        $scope.pAllScene = $scope.pAll.scene_track;
                                        $scopeCueSec = $scope.timecode_to_seconds($scope.pAll.scene_track[1].incode, 25);
                                        console.log('POC: ', $scope.pAll.mat_id, $scope.pAll.prog_title, $scope.pAll.scene_track.length, $scope.pAll.video_mpd_url);
                                        //for(var intCnt=0;intCnt<$scope.pAll.scene_track.length; intCnt++) {
                                            //$scopeCueSecs = $scope.timecode_to_seconds($scope.pAll.scene_track[intCnt].incode, 25);
                                            //console.log("Scene Track Info", $scope.pAll.scene_track[intCnt], $scope.pAll.scene_track[intCnt].participants, $scope.timecode_to_seconds($scope.pAll.scene_track[intCnt].incode, 25), $scopeCueSecs);
                                        //}
                                    });
                                });
                                </script>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div>
                            <h2 class="mod-disc-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'EOLAS' : 'INFORMATION'); ?></h2>
                            <div class="disc-wrapper">
                                <?php if (ICL_LANGUAGE_CODE == "ga") { ?>
                                    <p><strong>SÉANADH</strong><br />Bhí an t-ábhar atá ar an láithreán cartlainne seo cruinn an dáta ar craoladh an feasachán nuachta nó an clár cúrsaí reatha ar TG4 den chéad uair.   Mar thoradh ar nithe atá tarlaithe ón dáta sin, áfach, d'fhéadfadh nach mbeadh roinnt den ábhar cruinn an dáta a dtiocfadh daoine aonair air chun críocha oideachais nó taighde.  Dá bhrí sin ní féidir cruinneas an ábhair an dáta a dtiocfar air chun na críocha sin a dhearbhú.</p>
                                    <p><strong>&bull; Urraithe ag an BAI</strong></p>
                                    <p><strong>&bull; Ábhar Nuacht TG4 curtha ar fáil ag RTÉ Archives</strong></p>
                                <?php } else { ?>
                                    <p><strong>DISCLAIMER</strong><br />The content on this archive site was accurate on the date of first publication of the news bulletin or current affairs programme on the TG4 channel, but its possible due to events since the date of first publication that some content may no longer be accurate at the date of access by individuals for the purposes of education or research and no assertion is made as to the accuracy of the content on the date of such access.</p>
                                    <p><strong>&bull; Sponsored by the BAI</strong></p>
                                    <p><strong>&bull; Nuacht TG4 content supplied by RTÉ Archives</strong></p>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- / Schema.org Microdata -->
        <meta itemprop="video" itemscope itemtype="https://schema.org/VideoObject"/>
        <meta itemprop="name" content="<?php echo $progName; ?>" />
        <meta itemprop="description" content="<?php echo $progEngDesc; ?>" />
        <meta itemprop="contentURL" content="<?php echo 'http://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>" />
        <meta itemprop="width" content="960" />
        <meta itemprop="height" content="540" />
        <meta itemprop="thumbnail" content="<?php echo $progVideoStill; ?>" />
        <meta itemprop="inLanguage" content="EN" />
        <meta itemprop="isFamilyFriendly" content="<?php echo $famFriend; ?>" />
        <meta itemprop="contentLocation" content="Ireland" />
        <meta itemprop="encodingFormat" content="MP4" />
        <meta itemprop="videoFrameSize" content="960x540" /> 
        <meta itemprop="about" content="<?php echo (ICL_LANGUAGE_CODE == "ga" ? $progGaeDesc : $progEngDesc); ?>" />
        <meta itemprop="genre" content="<?php echo $progCat; ?>" />
        <meta itemprop="author" content="TG4" /> 
        <meta itemprop="publisher" content="tg4.ie" />
    </div>
    <?php } else { ?>
        <div class="plyr-feat center-panel">
            <div class="plyr-feat-wrap" >
                <div><?php echo (ICL_LANGUAGE_CODE == "ga" ? '<h1>Níl an clár seo ar fáil fós.</h1>':'<h1>This programme is not available yet.</h1>'); ?></div>
            </div>
            <div class="prog-feat-disc mod-disc">
                <h2 class="mod-disc-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'EOLAS' : 'INFORMATION'); ?></h2>
                <div class="disc-wrapper">
                    <?php if (ICL_LANGUAGE_CODE == "ga") { ?>
                        <p><strong>SÉANADH</strong><br />Bhí an t-ábhar atá ar an láithreán cartlainne seo cruinn an dáta ar craoladh an feasachán nuachta nó an clár cúrsaí reatha ar TG4 den chéad uair.   Mar thoradh ar nithe atá tarlaithe ón dáta sin, áfach, d'fhéadfadh nach mbeadh roinnt den ábhar cruinn an dáta a dtiocfadh daoine aonair air chun críocha oideachais nó taighde.  Dá bhrí sin ní féidir cruinneas an ábhair an dáta a dtiocfar air chun na críocha sin a dhearbhú.</p>
                        <p><strong>&bull; Urraithe ag an BAI</strong></p>
                        <p><strong>&bull; Ábhar Nuacht TG4 curtha ar fáil ag RTÉ Archives</strong></p>
                    <?php } else { ?>
                        <p><strong>DISCLAIMER</strong><br />The content on this archive site was accurate on the date of first publication of the news bulletin or current affairs programme on the TG4 channel, but its possible due to events since the date of first publication that some content may no longer be accurate at the date of access by individuals for the purposes of education or research and no assertion is made as to the accuracy of the content on the date of such access.</p>
                        <p><strong>&bull; Sponsored by the BAI</strong></p>
                        <p><strong>&bull; Nuacht TG4 content supplied by RTÉ Archives</strong></p>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <?php } 
    /* Remove need to be logged in for Demo - 24/1/17 */ } else { ?>
        <div class="plyr-feat center-panel">
            <div class="plyr-feat-wrap" >
                <div><?php echo (ICL_LANGUAGE_CODE == "ga" ? '<h1>Cláraigh/Logáil isteach chun féachaint air seo</h1>Caithfidh tú a bheith cláraithe linn chun féachaint ar seo. <ul><li><a href="../../membership-join/membership-registration/">Muna bhfuil tú cláraithe linn is féidir clárú anseo.</a></li><li><a href="../../membership-login/">Má tá tú cláraithe, ní mór duit logáil isteach anseo.</a></li></ul>' : '<h1>Register/Login to view this content</h1>You need to register to view this content. <ul><li><a href="../../membership-join/membership-registration/">If you haven\'t registered yet you can do so by clicking here.</a></li><li><a href="../../membership-login/">If you have registered then click here to login to view the content</a></li></ul>'); ?></div>
            </div>
            <div class="prog-feat-disc mod-disc">
                <h2 class="mod-disc-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'EOLAS' : 'INFORMATION'); ?></h2>
                <div class="disc-wrapper">
                    <?php if (ICL_LANGUAGE_CODE == "ga") { ?>
                        <p><strong>SÉANADH</strong><br />Bhí an t-ábhar atá ar an láithreán cartlainne seo cruinn an dáta ar craoladh an feasachán nuachta nó an clár cúrsaí reatha ar TG4 den chéad uair.   Mar thoradh ar nithe atá tarlaithe ón dáta sin, áfach, d'fhéadfadh nach mbeadh roinnt den ábhar cruinn an dáta a dtiocfadh daoine aonair air chun críocha oideachais nó taighde.  Dá bhrí sin ní féidir cruinneas an ábhair an dáta a dtiocfar air chun na críocha sin a dhearbhú.</p>
                        <p><strong>&bull; Urraithe ag an BAI</strong></p>
                        <p><strong>&bull; Ábhar Nuacht TG4 curtha ar fáil ag RTÉ Archives</strong></p>
                    <?php } else { ?>
                        <p><strong>DISCLAIMER</strong><br />The content on this archive site was accurate on the date of first publication of the news bulletin or current affairs programme on the TG4 channel, but its possible due to events since the date of first publication that some content may no longer be accurate at the date of access by individuals for the purposes of education or research and no assertion is made as to the accuracy of the content on the date of such access.</p>
                        <p><strong>&bull; Sponsored by the BAI</strong></p>
                        <p><strong>&bull; Nuacht TG4 content supplied by RTÉ Archives</strong></p>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <?php /* Remove need to be logged in for Demo - 24/1/17 */ } ?>
</section>

<?php get_footer(); ?>