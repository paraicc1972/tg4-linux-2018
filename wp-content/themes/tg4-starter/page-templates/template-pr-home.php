<?php /* Template Name: Press Releases - Home */ ?>

<?php get_header(); ?>

<div class="section-header">
	<h1 class="section-title">
		<?php echo get_the_title($post->post_parent); ?>
	</h1>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
    <div class="section-submenu-wrap">
        <ul class="section-submenu-list">
            <?php wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=1'); ?>
            &nbsp;
        </ul>
    </div>
</div>

<section class="prog-feat-section">
    <div class="section-panel-white">
        <div class="prog-feat center-panel">
            <div class="prog-feat-wrap">
                <ul class="news-list">
					<?php
					global $id;
					$pages = get_pages('title_li=&child_of='.$id.'&sort_column=menu_order&sort_order=desc&echo=0');
					foreach($pages as $page) {
						//$content = $page->post_content;
						//$content = apply_filters('the_content', $content);
					?>
					<li>
					    <div class="news-item-wrap">
					        <a href="<?php echo get_page_link($page->ID) ?>" style="background-image: url(https://placehold.it/197x111);"></a>
					        <div class="news-item-content">
					            <h5 class="news-item-sub"><?php echo the_field("broadcast_day", $page->ID); ?></h5>
					            <a href="<?php echo get_page_link($page->ID) ?>"><h4 class="pr-list-title"><?php echo the_field("intro_title", $page->ID); ?></h4></a>
					        </div>
					    </div>
					</li>
					<?php } ?>
            	</ul>
            </div>
            <div class="prog-feat-ad mod-ad-dark">
                <h2 class="mod-ad-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Fógraíocht' : 'Advertisement'); ?></h2>
                <div class="ad-wrapper">
                    <iframe src="<?php echo get_template_directory_uri(); ?>/mpu-banner.htm" height="250" width="300" scrolling="no" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="prog-feat-section">
	<div class="section-panel-white">
	    <div class="prog-feat center-panel">
	    	<div class="prog-feat-wrap"><?php echo apply_filters('the_content', get_post_field('post_content', $post_id)); ?></div>
			<?php if(have_rows('file_name')): ?>
				<div class="prog-feat-file mod-file">
			    <?php while(have_rows('file_name')): the_row();
					if (get_sub_field('file_title')) { ?>
				    	<div class="file-content-wrap">
                            <div class="file-btn"><a href="<?php the_sub_field('upload_file') ?>" class="mod-file" target="_blank"><div class="file-btnImg"></div></a></div>
                            <div class="file-content">
                                <a href="<?php the_sub_field('upload_file') ?>" class="mod-file" target="_blank"><?php the_sub_field('file_title'); ?></a>
                             </div>
                        </div>
				        <!-- <a href="<?php //the_sub_field('upload_file') ?>" class="mod-file" target="_blank"><?php //the_sub_field('file_title'); ?></a> -->
			    <?php } endwhile; ?>
	        	</div>
			<?php endif; ?>
	    </div>
	</div>
</section>

<?php get_footer(); ?>