<?php /* Template Name: Weather */ ?>

<?php include(locate_template('/header-weather.php')); ?>

<div class="section-header">
	<h1 class="section-title">
		<?php
		if (get_the_title($post->post_parent) == "Programmes" || get_the_title($post->post_parent) == "Cláir" || get_the_title($post->post_parent) == "Sport" || get_the_title($post->post_parent) == "Spórt") {
			echo get_the_title($post->ID);
		} else {
			echo get_the_title($post->post_parent);
		}
		?>
	</h1>
	<div class="showing-times">
		<?php 
		$broadcast_day = get_field("broadcast_day");
		$broadcast_time = get_field("broadcast_time");
		if($broadcast_day) { ?>
		<div class="showing-box">
			<div class="showing-day"><?php echo (ICL_LANGUAGE_CODE == "ga" ? substr($broadcast_day, 0, 4) : substr($broadcast_day, 0, 3)); ?></div>
			<div class="showing-time"><?php echo $broadcast_time; ?></div>
		</div>
		<?php }
		$repeat_day = get_field("repeat_day");
		$repeat_time = get_field("repeat_time");
		if($repeat_day) { ?>
		<div class="showing-box">
			<div class="showing-day"><?php echo (ICL_LANGUAGE_CODE == "ga" ? substr($repeat_day, 0, 4) : substr($repeat_day, 0, 3)); ?></div>
			<div class="showing-time"><?php echo $repeat_time; ?></div>
		</div>
		<?php } ?>
	</div>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
	<div class="section-submenu-wrap">
		<ul class="section-submenu-list">
			<?php 
			//echo $post->ID . " - " . $post->post_parent;
    		if ($post->post_parent != 137 && $post->post_parent != 140) {
				wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=1');
			} elseif ($post->ID != 137 && $post->ID != 140) {
				wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->ID . '&depth=1');
			}
			?>
			&nbsp;
		</ul>
	</div>
</div>

<?php
//if (is_page(array('programmes/weather/weather-home/', 'clair/an-aimsir/weather-home/', 'programmes/weather/', 'clair/an-aimsir/'))) {
?>
	<!-- <section class="daily-weather-icons">
	    <h2 class="visuallyhidden">Daily Weather</h2>
		<div class="prog-feat center-panel">
			<h2><?php //echo (ICL_LANGUAGE_CODE == "ga" ? 'Do aimsir áitiúil' : 'Your local weather'); ?></h2>
			<div class="srch-weather-wrap">
	    		<div class="center-panel">
	        		<div class="srch-weather">
						<?php 
							//include(locate_template('/includes/weather_summary_v1.php'));
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section-panel-pale-2"> -->
<?php //} else { ?>
	<section class="section-panel-pale-3">
<?php //} ?>

	<div class="title-tab-wrap">
			<?php
			if (is_page(array('programmes/weather/weather-home/', 'clair/an-aimsir/weather-home/', 'programmes/weather/', 'clair/an-aimsir/'))) {
				echo (ICL_LANGUAGE_CODE == "ga" ? '<h2 class="title-tab-pale-3">Aimsir Náisiúnta</h2>' : '<h2 class="title-tab-pale-3">National Weather</h2>');
			} elseif (is_page(array('programmes/weather/weather-movies/', 'clair/an-aimsir/weather-movies/'))) {
				echo (ICL_LANGUAGE_CODE == "ga" ? '<h2 class="title-tab-pale-3">Fiseáin Aimsire</h2>' : '<h2 class="title-tab-pale-3">Weather Movies</h2>');
			} elseif (is_page(array('programmes/weather/weather-cameras/', 'clair/an-aimsir/weather-cameras/'))) {
				echo (ICL_LANGUAGE_CODE == "ga" ? '<h2 class="title-tab-pale-3">Ceamaraí Aimsire</h2>' : '<h2 class="title-tab-pale-3"><h2 class="title-tab-pale-3">Weather Cameras</h2>');
			} else {
				echo (ICL_LANGUAGE_CODE == "ga" ? '<h2 class="title-tab-pale-3">Téarmaí Aimsire</h2>' : '<h2 class="title-tab-pale-3"><h2 class="title-tab-pale-3"><h2 class="title-tab-pale-3">Weather Terms</h2>');
			}
			?>
	</div>
	<div class="prog-feat center-panel">
		<div class="prog-feat-wrap">
			<?php 
			if (is_page(array('programmes/weather/weather-home/', 'clair/an-aimsir/weather-home/', 'programmes/weather/', 'clair/an-aimsir/'))) {
				include(locate_template('/includes/wMap_JS_SettingsV1.php'));
			} elseif (is_page(array('programmes/weather/weather-terms/', 'clair/an-aimsir/weather-terms/')))  {
				include(locate_template('/includes/weather_words_v1.php'));
			} else {
				include(locate_template('/includes/wMap_JS_Tabs.php'));
			}?>
		</div>
		<div class="prog-feat-ad mod-ad">
			<h2 class="mod-ad-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Fógraíocht' : 'Advertisement'); ?></h2>
			<div class="ad-wrapper">
            	<iframe src="<?php echo get_template_directory_uri(); ?>/mpu-banner.htm" height="250" width="300" scrolling="no" frameborder="0"></iframe>
            </div>
		</div>
	</div>
</section>

<section class="section-panel-dark">
	<div class="title-tab-wrap">
		<h2 class="title-tab-dark"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Ár Láithreoirí' : 'Our Presenters'); ?></h2>
	</div>
	<div class="featv-wrap featvp-slider visuallyhidden">
		<section class="prog-module">
            <div class="prog-slide-wrap">
                <h3 class="prog-title">Orla Ní Fhinneadha</h3>
                <img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2018/08/Orla-Ni-Fhinneadha.jpg" alt="" class="prog-img">
            </div>
        </section>
		<section class="prog-module">
            <div class="prog-slide-wrap">
                <h3 class="prog-title">Caitríona Ní Chualáin</h3>
                <img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2018/08/Caitriona-Ni-Chualain.jpg" alt="" class="prog-img">
            </div>
        </section>
		<section class="prog-module">
            <div class="prog-slide-wrap">
                <h3 class="prog-title">Neasa Bheáid</h3>
                <img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2018/08/Neasa-Bheaid.jpg" alt="" class="prog-img">
            </div>
        </section>
		<section class="prog-module">
            <div class="prog-slide-wrap">
                <h3 class="prog-title">Mairéad Ní Chuaig</h3>
                <img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2015/11/mairead-2.jpg" alt="" class="prog-img">
            </div>
        </section>
		<section class="prog-module">
            <div class="prog-slide-wrap">
                <h3 class="prog-title">Fiona Ní Fhlaithearta</h3>
                <img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2015/11/fiona-2.jpg" alt="" class="prog-img">
            </div>
        </section>
		<section class="prog-module">
            <div class="prog-slide-wrap">
                <h3 class="prog-title">Caitlín Nic Aodh</h3>
                <img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2015/11/caitlin-2.jpg" alt="" class="prog-img">
            </div>
        </section>
		<section class="prog-module">
            <div class="prog-slide-wrap">
                <h3 class="prog-title">Irial Ó Ceallaigh</h3>
                <img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2015/11/irial-2.jpg" alt="" class="prog-img">
            </div>
        </section>
	</div>

	<button type="button" data-role="none" class="featvp-prev" aria-label="next">Next</button>
	<button type="button" data-role="none" class="featvp-next" aria-label="next">Next</button>
</section>

<?php get_footer(); ?>