<?php /* Template Name: Vóta 1 */ ?>

<?php get_header(); ?>

<script>
$(function() {
    $('#vertScroller').vTicker({
    speed: 700,
    pause: 4000,
    showItems: 1,
    mousePause: true,
    height: 0,
    animate: true,
    margin: 0,
    padding: 0,
    startPaused: false});
});
</script>

<div class="section-header">
	<h1 class="section-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Vóta 2016' : 'Vóta 2016'); ?></h1>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
    <div class="section-submenu-wrap">
        <ul class="section-submenu-list">
            <?php wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=1'); ?>
            &nbsp;
        </ul>
    </div>
</div>

<div class="electionseats-wrap">
    <div class="center-panel">
        <?php
            // Begin Snippet - Pull back Seats Filled results in the light blue bar at top of the Constituency & Most Votes pages
            $filename1 = "http://direct.tg4.ie/election/ElectionData/xml/PublishingNationalSummary.xml";
            $file_headers = @get_headers($filename1);
            if ($file_headers[0] == 'HTTP/1.1 200 OK') {
                echo "<p><strong>" . (ICL_LANGUAGE_CODE == "ga" ? 'Suíocháin Líonta' : 'Seats Filled') . "</strong></p>";
                $xml=simplexml_load_file($filename1) or die("Error: Cannot create object");

                foreach($xml->SUMMARY->PARTIES->children() as $partyResult) {
                    if ($partyResult->PARTY_MNEMONIC != "MISC") {
                        if ($partyResult->PARTY_MNEMONIC == "AAA-PBP") {
                            $partyResult->PARTY_MNEMONIC = "AP";
                        }
                        if (ICL_LANGUAGE_CODE == "ga") {
                            if ($partyResult->PARTY_MNEMONIC == "LAB") {
                                $partyResult->PARTY_MNEMONIC = "LO";
                                $partyResult->PARTY_NAME = "Lucht Oibre";
                            }
                            if ($partyResult->PARTY_MNEMONIC == "GP") {
                                $partyResult->PARTY_MNEMONIC = "CG";
                                $partyResult->PARTY_NAME = "Comhaontas Glas";
                            }
                            if ($partyResult->PARTY_MNEMONIC == "IND") {
                                $partyResult->PARTY_MNEMONIC = "NS";
                                $partyResult->PARTY_NAME = "Neamhspleach";
                            }
                            if ($partyResult->PARTY_MNEMONIC == "SD") {
                                $partyResult->PARTY_MNEMONIC = "DS";
                                $partyResult->PARTY_NAME = "Daonlathaigh Sóisialta";
                            }
                        }
                        echo "<div class='election-party-mnemonic-$partyResult->PARTY_MNEMONIC'>";
                        if ($partyResult->PARTY_MNEMONIC == "AP") { 
                            $partyResult->PARTY_MNEMONIC = "A/P";
                        }
                        echo $partyResult->PARTY_MNEMONIC . "<br />" . $partyResult->SEATS_WON;
                        echo "</div>";
                    }
                }
            }
            // End Snippet - Pull back Seats Filled results in the light blue bar at top of the the Constituency & Most Votes pages
        ?>
    </div>
</div>

<?php
if (is_page('programmes/vota-2016/') || is_page('programmes/vota-2016/home/') || is_page('clair/vota-2016/') || is_page('clair/vota-2016/baile/')) {
    // Begin Snippet - Pull back Constituency data in the light blue boxes in the body of the homepage
    echo "<section class='constituency-mods'>";
    echo "<h2 class='visuallyhidden'>More on TG4</h2>";
    echo "<div class='constituencymods-wrapper'>";
    echo "<div class='constituency-mod-wrap'>";
    $filename2 = "http://direct.tg4.ie/election/ElectionData/xml/ListOfConstituencies.xml";
    $file_headers = @get_headers($filename2);
    if ($file_headers[0] == 'HTTP/1.1 200 OK') {
        echo "<h1>" . (ICL_LANGUAGE_CODE == "ga" ? 'Na Dáilcheantair' : 'Constituencies') . "</h1>";
        $xml=simplexml_load_file($filename2) or die("Error: Cannot create object");
        $count = 1;

        foreach($xml->CONSTITUENCIES->children() as $constituency) { 
            $electorateNum = number_format(intval($constituency->ELECTORATE));
            ?> 
            <section class="mod-1">
                <a href="../constituency/?CID=<?php echo sprintf('%02d', $count);?>">
                <div class="constituency-mod-notice">
                    <h5 class="vote-constituency">
                        <?php
                        if (ICL_LANGUAGE_CODE == "ga") {
                            if (isset($constituency->CONSTITUENCY_IRISH_NAME) && $constituency->CONSTITUENCY_IRISH_NAME != "") {
                                echo $constituency->CONSTITUENCY_IRISH_NAME;
                            } else {
                                echo $constituency->CONSTITUENCY_NAME;
                            }
                        } else {
                            echo $constituency->CONSTITUENCY_NAME;
                        }
                        ?>
                    </h5>
                    <p class="vote-seats"><?php echo "<strong>" . (ICL_LANGUAGE_CODE == "ga" ? 'Suíocháin: ' : 'Seats: ') . "</strong>"; echo $constituency->NUMBER_OF_SEATS; ?>, <?php echo "<strong>" . (ICL_LANGUAGE_CODE == "ga" ? 'Toghthóirí: ' : 'Electorate: ') . "</strong>"; echo $electorateNum; ?></p>
                </div>
                </a> 
            </section>
        <?php 
        $count++;
        }
    }
    echo "</div>";
    echo "</div>";
    echo "</section>";
    // End Snippet - Pull back Constituency data in the light blue boxes in the body of the homepage
} elseif (is_page('programmes/vota-2016/constituency/') ||  is_page('clair/vota-2016/constituency/')) {
    echo "<section class='constituency-feat-section'>";
    echo "<div class='section-panel-white'>";
    echo "<div class='constituency-feat center-panel'>";
    // Begin Snippet - Pull back Constituency count results based on a selection from the dropdown in Constituency page
    echo "<div class='constituency-feat-wrap'>";
    echo "<a href='javascript:history.go(0)'><img src='https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2015/12/refresh-page-2.png' alt='Refresh'>&nbsp;&nbsp;&nbsp;" . (ICL_LANGUAGE_CODE == "ga" ? 'Cliceáil anso chun na torthaí is déanaí a fheiceáil' : 'Click here to view the latest results') . "</a>";
    $constituencyID = ctype_digit($_GET['CID']) ? $_GET['CID'] : '01';
    $filename3 = "http://direct.tg4.ie/election/ElectionData/xml/CountDetails$constituencyID.xml";
    $file_headers = @get_headers($filename3);
    if ($file_headers[0] == 'HTTP/1.1 200 OK') {
        $xml=simplexml_load_file($filename3) or die("Error: Cannot create object");
        $count = 1;
        $electFlag = "";
        echo "<h1>";
        if (ICL_LANGUAGE_CODE == "ga") {
            if (isset($xml->CONSTITUENCY->CONSTITUENCY_IRISH_NAME) && $xml->CONSTITUENCY->CONSTITUENCY_IRISH_NAME != "") {
                echo $xml->CONSTITUENCY->CONSTITUENCY_IRISH_NAME;
            } else {
                echo $xml->CONSTITUENCY->CONSTITUENCY_NAME;
            }
        } else {
            echo $xml->CONSTITUENCY->CONSTITUENCY_NAME;
        }
        echo "</h1>";
        echo "<div class='election-Quota'>" . (ICL_LANGUAGE_CODE == "ga" ? 'Cuóta' : 'Quota') . ": " . $xml->CONSTITUENCY->RESULT_HEADER->QUOTA . "</div>";
        echo "<div class='election-Count'>";
        if ($xml->CONSTITUENCY->RESULT_HEADER->REASON_CODE = "2") {
            echo (ICL_LANGUAGE_CODE == "ga" ? 'Aistrithe ó' : 'Transferred from') . ": " . $xml->CONSTITUENCY->RESULT_HEADER->REASON_TEXT;
        }
        echo "<span style='float:right;'>" . (ICL_LANGUAGE_CODE == "ga" ? 'Comhaireamh' : 'Count') . ": " . $xml->CONSTITUENCY->COUNT_NUMBER . "</span>";
        echo "</div>";
        echo "<ul class='party-list'>";
        foreach($xml->CONSTITUENCY->RESULT_HEADER->COUNT_DETAILS->children() as $countDetails) { 
            if ($countDetails->PARTY_MNEMONIC == "AAA-PBP") {
                $countDetails->PARTY_MNEMONIC = "AP";
            }
            if (ICL_LANGUAGE_CODE == "ga") {
                if ($countDetails->PARTY_MNEMONIC == "LAB") {
                    $countDetails->PARTY_MNEMONIC = "LO";
                    $countDetails->PARTY_NAME = "Lucht Oibre";
                }
                if ($countDetails->PARTY_MNEMONIC == "GP") {
                    $countDetails->PARTY_MNEMONIC = "CG";
                    $countDetails->PARTY_NAME = "Comhaontas Glas";
                }
                if ($countDetails->PARTY_MNEMONIC == "IND") {
                    $countDetails->PARTY_MNEMONIC = "NS";
                    $countDetails->PARTY_NAME = "Neamhspleach";
                }
                if ($partyResult->PARTY_MNEMONIC == "SD") {
                    $partyResult->PARTY_MNEMONIC = "DS";
                    $partyResult->PARTY_NAME = "Daonlathaigh Sóisialta";
                }
            }
            if ($countDetails->ELECTED_FLAG == "Y") {
                $electFlag = $electFlag . "<span style=\"border:1px solid #ffffff; height:15px; width:15px; margin-top:6px;\" class=\"election-party-mnemonic-$countDetails->PARTY_MNEMONIC\"></span>";
            }
        ?>
            <li>
                <div class="election-results-wrap">
                    <div class="election-party-mnemonic-<?php echo $countDetails->PARTY_MNEMONIC ?>"><?php if ($countDetails->PARTY_MNEMONIC == "AP") { $countDetails->PARTY_MNEMONIC = "A/P"; }; echo $countDetails->PARTY_MNEMONIC; ?></div>
                    <div class="election-party-candidate"><?php echo $countDetails->FIRSTNAME . " " . $countDetails->SURNAME; ?></div>
                    <div class="election-party-vote"><?php if ($countDetails->VOTES != "EXCL") { echo $countDetails->VOTES; } else { echo (ICL_LANGUAGE_CODE == "ga" ? 'As an Áireamh' : 'Excluded'); } ?></div>
                    <div class="election-party-transfer"><?php if ($countDetails->TRANSFERS != "") { echo "+" . $countDetails->TRANSFERS; } else { echo "&nbsp;"; } ?></div>
                    <div class="election-party-elected"><?php if ($countDetails->ELECTED_FLAG == "Y") { echo (ICL_LANGUAGE_CODE == "ga" ? 'Tofa' : 'Elected'); } else { echo "&nbsp;"; } ?></div>
                </div>
            </li>
        <?php 
        $count++;
        }
        echo "</ul>";
        echo "<div class='election-Seats'>" . $electFlag . "<span style='float:right;'>" . (ICL_LANGUAGE_CODE == "ga" ? 'Suíocháin' : 'Seats') . ": " . $xml->CONSTITUENCY->TOTALSEATS . "</span></div>";
    } else {
        echo "<span><h1>" . (ICL_LANGUAGE_CODE == "ga" ? 'Níl aon sonraí tagtha isteach don Dáilcheantair seo fós.' : 'No data in for this Constituency yet.') . "</h1></span>";
    }
    echo "</div>";
    // End Snippet - Pull back Constituency count results based on a selection from the dropdown in Constituency page

    echo "<div class='vote-side'>";
    // Begin Snippet - Pull back recent Irish/English messages in the scroller on the Constituency page
    echo "<div id='vertScroller' style='margin-left:40px; padding:20px 10px 0px 10px; text-align:left; border-top:1px solid #c7dae2; border-bottom:1px solid #c7dae2; margin-bottom:15px;'>";
    echo "<ul style='list-style-type:square; font-size:13px;'>";
    $fileMessage = "http://direct.tg4.ie/election/ElectionData/xml/" . (ICL_LANGUAGE_CODE == "ga" ? 'RecentIrishMessages.xml' : 'RecentEnglishMessages.xml');
    $file_headers = @get_headers($fileMessage);
    if ($file_headers[0] == 'HTTP/1.1 200 OK') {
        $xml=simplexml_load_file($fileMessage) or die("Error: Cannot create object");
        foreach($xml->MESSAGES->children() as $message) {
            echo "<li><strong>" . $message->TIME . "</strong> (" . $message->TITLE . ")<br />" . $message->TEXT . "</li>";
        }
    }
    echo "</ul>";
    echo "</div>";
    // End Snippet - Pull back recent Irish/English messages in the scroller on the Constituency page

    // Begin Snippet - Populate the dropdown in the Constituency page
    echo "<div class='mod-ad' style='padding-left:40px;'>";
    $filename4 = "http://direct.tg4.ie/election/ElectionData/xml/ListOfConstituencies.xml";
    $file_headers = @get_headers($filename4);
    if ($file_headers[0] == 'HTTP/1.1 200 OK') {
        $xml=simplexml_load_file($filename4) or die("Error: Cannot create object");
        $newcount = 1;
        echo "<select onchange='this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);'>";
        echo "<option value=''>" . (ICL_LANGUAGE_CODE == "ga" ? 'Roghnaigh Dáilcheantar' : 'Choose Constituency') . "</option>";
        foreach($xml->CONSTITUENCIES->children() as $constituency) {
            echo "<option value='?CID=" . sprintf('%02d', $newcount) . "'>";
            if (ICL_LANGUAGE_CODE == "ga") {
                if (isset($constituency->CONSTITUENCY_IRISH_NAME) && $constituency->CONSTITUENCY_IRISH_NAME != "") {
                    echo $constituency->CONSTITUENCY_IRISH_NAME;
                } else {
                    echo $constituency->CONSTITUENCY_NAME;
                }
            } else {
                echo $constituency->CONSTITUENCY_NAME;
            }
            echo "</option>";
            $newcount++;
        }
        echo "</select>";
    }
    echo "</div>";
    // End Snippet - Populate the dropdown in the Constituency page
    echo "</div>";
    echo "</div>";
    echo "</div>";
    echo "</section>";
} elseif (is_page('programmes/vota-2016/most-votes/') ||  is_page('clair/vota-2016/most-votes/')) {
    echo "<section class='constituency-feat-section'>";
    echo "<div class='section-panel-white'>";
    echo "<div class='constituency-feat center-panel'>";
    // Begin Snippet - Pull back Constituency count results based on a selection from the dropdown in Most Votes page
    echo "<div class='constituency-feat-wrap'>";
    echo "<a href='javascript:history.go(0)'><img src='https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2015/12/refresh-page-2.png' alt='Refresh'>&nbsp;&nbsp;&nbsp;" . (ICL_LANGUAGE_CODE == "ga" ? 'Cliceáil anso chun na torthaí is déanaí a fheiceáil' : 'Click here to view the latest results') . "</a>";
    $regionID = ctype_digit($_GET['RID']) ? $_GET['RID'] : '01';
    $filename3 = "http://direct.tg4.ie/election/ElectionData/xml/RegionalTop10ByVotes$regionID.xml";
    $file_headers = @get_headers($filename3);
    if ($file_headers[0] == 'HTTP/1.1 200 OK') {
        $xml=simplexml_load_file($filename3) or die("Error: Cannot create object");
        $count = 1;
        echo "<h1>" . (ICL_LANGUAGE_CODE == "ga" ? 'Vótaí is Airde sa Reigiún' : 'Most Votes in the Region') . "</h1>";
        echo "<div class='election-Quota'>" . (ICL_LANGUAGE_CODE == "ga" ? $xml->SUMMARY->REGION_IRISH_NAME : $xml->SUMMARY->REGION_NAME) . "</div>";
        echo "<ul class='party-list'>";
        foreach($xml->CANDIDATES->children() as $candidate) {
            if ($candidate->PARTY_MNEMONIC == "AAA-PBP") {
                $candidate->PARTY_MNEMONIC = "AP";
            }
            if (ICL_LANGUAGE_CODE == "ga") {
                if ($candidate->PARTY_MNEMONIC == "LAB") {
                    $candidate->PARTY_MNEMONIC = "LO";
                    $candidate->PARTY_NAME = "Lucht Oibre";
                }
                if ($candidate->PARTY_MNEMONIC == "GP") {
                    $candidate->PARTY_MNEMONIC = "CG";
                    $candidate->PARTY_NAME = "Comhaontas Glas";
                }
                if ($candidate->PARTY_MNEMONIC == "IND") {
                    $candidate->PARTY_MNEMONIC = "NS";
                    $candidate->PARTY_NAME = "Neamhspleach";
                }
                if ($partyResult->PARTY_MNEMONIC == "SD") {
                    $partyResult->PARTY_MNEMONIC = "DS";
                    $partyResult->PARTY_NAME = "Daonlathaigh Sóisialta";
                }
            }
            ?>
            <li>
                <div class="election-results-wrap">
                    <div class="election-party-mnemonic-<?php echo $candidate->PARTY_MNEMONIC ?>"><?php if ($candidate->PARTY_MNEMONIC == "AP") { $candidate->PARTY_MNEMONIC = "A/P"; }; echo $candidate->PARTY_MNEMONIC; ?></div>
                    <div class="election-party-candidate"><?php echo $candidate->FIRSTNAME . " " . $candidate->SURNAME; ?></div>
                    <div class="election-party-constituency">
                    <?php 
                        if (ICL_LANGUAGE_CODE == "ga") {
                            if (isset($candidate->CONSTITUENCY_IRISH_NAME) && $candidate->CONSTITUENCY_IRISH_NAME != "") {
                                echo $candidate->CONSTITUENCY_IRISH_NAME;
                            } else {
                                echo $candidate->CONSTITUENCY_NAME;
                            }
                        } else {
                            echo $candidate->CONSTITUENCY_NAME;
                        }
                    ?>
                    </div>
                    <div class="election-party-vote"><?php if ($candidate->VOTES != "EXCL") { echo $candidate->VOTES; } else { echo (ICL_LANGUAGE_CODE == "ga" ? 'As an Áireamh' : 'Excluded'); } ?></div>
                </div>
            </li>
        <?php 
        $count++;
        }
        echo "</ul>";
    } else {
        echo "<span><h1>" . (ICL_LANGUAGE_CODE == "ga" ? 'Níl aon sonraí tagtha isteach don Réigiún seo fós.' : 'No data in for this Region yet.') . "</h1></span>";
    }
    echo "</div>";
    // End Snippet - Pull back Constituency count results based on a selection from the dropdown in Most Votes page
    echo "<div class='vote-side'>";
    // Begin Snippet - Pull back recent Irish/English messages in the scroller on the Most Votes page
    echo "<div id='vertScroller' style='margin-left:40px; padding:20px 10px 0px 10px; text-align:left; border-top:1px solid #c7dae2; border-bottom:1px solid #c7dae2;'>";
    echo "<ul style='list-style-type:square; font-size:13px;'>";
    $fileMessage = "http://direct.tg4.ie/election/ElectionData/xml/" . (ICL_LANGUAGE_CODE == "ga" ? 'RecentIrishMessages.xml' : 'RecentEnglishMessages.xml');
    $file_headers = @get_headers($fileMessage);
    if ($file_headers[0] == 'HTTP/1.1 200 OK') {
        $xml=simplexml_load_file($fileMessage) or die("Error: Cannot create object");
        foreach($xml->MESSAGES->children() as $message) {
            echo "<li><strong>" . $message->TIME . "</strong> (" . $message->TITLE . ")<br />" . $message->TEXT . "</li>";
        }
    }
    echo "</ul>";
    echo "</div>";
    // End Snippet - Pull back recent Irish/English messages in the scroller on the Most Votes page

    // Begin Snippet - Populate the dropdown in the Most Votes page
    echo "<div class='mod-ad' style='padding:20px 0px 0px 40px;'>";
    $filename4 = "http://direct.tg4.ie/election/ElectionData/xml/ListOfRegions.xml";
    $file_headers = @get_headers($filename4);
    if ($file_headers[0] == 'HTTP/1.1 200 OK') {
        $xml=simplexml_load_file($filename4) or die("Error: Cannot create object");
        $newcount = 1;
        echo "<select onchange='this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);'>";
        echo "<option value=''>" . (ICL_LANGUAGE_CODE == "ga" ? 'Roghnaigh Réigiún' : 'Choose Region') . "</option>";
        foreach($xml->REGIONS->children() as $region) {
            echo "<option value='?RID=" . sprintf('%02d', $newcount) . "'>";
            if (ICL_LANGUAGE_CODE == "ga") {
                if (isset($region->REGION_IRISH_NAME) && $region->REGION_IRISH_NAME != "") {
                    echo $region->REGION_IRISH_NAME;
                } else {
                    echo $region->REGION_NAME;
                }
            } else {
                echo $region->REGION_NAME;
            }
            echo "</option>";
            $newcount++;
        }
        echo "</select>";
    }
    echo "</div>";
    // End Snippet - Populate the dropdown in the Most Votes page
    echo "</div>";
    echo "</div>";
    echo "</div>";
    echo "</section>";
}
get_footer(); ?>