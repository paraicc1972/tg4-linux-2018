<?php /* Template Name: Archive Content - List - Angular */ ?>

<?php include(locate_template('/header-angular.php')); ?>

<div class="section-header">
    <h1 class="section-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Cartlann TG4' : 'TG4 Archive'); ?></h1>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
    <div class="section-submenu-wrap">
        <ul class="section-submenu-list">
            <?php wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=1&exclude=6398,6401,15288,15292,16939,16941,16468,16469,20944,20946'); ?>
            &nbsp;
        </ul>
    </div>
</div>
<?php
$feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
$feat_Class = "prog-head";
?>
<section class="<?php echo $feat_Class; ?>" style="background-image: url(<?php echo $feat_image; ?>);">
    <div class="blockContainer">
        <?php 
        if (get_field("intro_text")) { ?>
            <div class="content-block">
                <h2 class="content-subtitle"><?php echo get_field("intro_sub_title"); ?></h2>
                <p><?php echo get_field("intro_text"); ?></p>
            </div>
        <?php } ?>
    </div>
</section>

<div class="srch-feat-intro-wrap" style="margin-top: 0px;">
    <div class="center-panel">
        <div class="srch-feat-intro">
           <!-- <div class="srchSel-wrap">
                <select class="srchSel" ng-model="srt" ng-init="srt='prog_first_tx_date'" ng-change="srtBy(srt)">
                    <option value='prog_first_tx_date' disabled> {{current_Lang=='ga'?'Rangaigh...':'Sort By...'}}</option>
                    <option value='prog_first_tx_date'> {{current_Lang=='ga'?'Chéad Tx':'1st Tx'}}</option>
                    <option value='prog_dynamic_branding_title'> A - Z </option>
                </select>
                <i ng-title="{{current_Lang=='ga'?'Athraigh an Ord':'Toggle Order'}}" ng-click="up=!up" class="hot fa {{up?'fa-sort-amount-asc':'fa-sort-amount-desc'}}"></i>
            </div> -->
            <div class="arcSel-wrap">
                <select ng-cloak id="sSel" name="page-dropdown" ng-model="flt" ng-init="flt='All'" onchange="document.location.href=this.options[this.selectedIndex].value;"> 
                    <option value="All" disabled>{{current_Lang=='ga'?'Séanra':'Genre'}}</option>
                    <option value='?progGenre=All' disabled>{{current_Lang=='ga'?'Baile':'Home'}}</option>
                    <option value='?progGenre=Cursaí Reatha'>{{current_Lang=='ga'?'Cúrsaí Reatha':'Current Affairs'}}</option>
                    <option value='?progGenre=Drama' disabled>{{current_Lang=='ga'?'Drámaíocht':'Drama'}}</option>
                    <option value='?progGenre=Cula4' disabled>{{current_Lang=='ga'?'Cúla4':'Cúla4'}}</option>
                    <option value='?progGenre=Sport' disabled>{{current_Lang=='ga'?'Spórt':'Sport'}}</option>
                    <option value='?progGenre=Ceol' disabled>{{current_Lang=='ga'?'Ceol':'Music'}}</option>
                    <option value='?progGenre=Saolchlar' disabled>{{current_Lang=='ga'?'Saolchlár':'Lifestyle'}}</option>
                    <option value='?progGenre=Siamsaiocht' disabled>{{current_Lang=='ga'?'Siamsaíocht':'Entertainment'}}</option>
                    <option value='?progGenre=Fáisnéis' disabled>{{current_Lang=='ga'?'Faisnéis':'Documentary'}}</option>
                </select>
            </div>
            <div class="arcform-wrap"><input ng-cloak class="arcform-textbox" name="sbox" id="sbox" ng-model="ss" placeholder="{{current_Lang=='ga'?' Cuimsitheach...':' Advanced...'}}" ng-change="ss1=noFada(ss);" /><a href="#" id="srchCompBtn" class="arcform-btn" onclick="this.href='?srchTxt=' + document.getElementById('sbox').value">>></a><a class="arcform-hint" title="{{current_Lang=='ga'?'Is féidir cuardach a dhéanamh ar théarma (faoi bhliain ar leith) anseo e.g. Gaillimh 2001':'You can search by a certain term (within a year) here e.g. Galway 2001'}}">?</a><br /><span class="arcform-text" ng-bind="current_Lang=='ga' ? 'Cuardach: Cuimsitheach' : 'Advanced Search'"></span> </div>
            <div class="arcform-wrap"><input ng-cloak class="arcform-textbox" name="dbox" id="dbox" ng-model="ds" placeholder="{{current_Lang=='ga'?' Dáta (dd.mm.yyyy)':' Date (dd.mm.yyyy)'}}" ng-change="ss1=noFada(ds);" maxlength="10" /><a href="#" id="srchDateBtn" class="arcform-btn" onclick="this.href='?srchDate=' + document.getElementById('dbox').value">>></a><br /><span class="arcform-text" ng-bind="current_Lang=='ga' ? 'Dáta' : 'Search: Date'"> (dd.mm.yyyy)</span></div>
            <div class="arcform-wrap"><input ng-cloak class="arcform-textbox" name="tbox" id="tbox" ng-model="ts" placeholder="{{current_Lang=='ga'?' Teideal...':' Title...'}}" ng-change="ss1=noFada(ts);" /><a href="#" id="srchTitleBtn" class="arcform-btn" onclick="this.href='?srchTitle=' + document.getElementById('tbox').value">>></a><br /><span class="arcform-text" ng-bind="current_Lang=='ga' ? 'Cuardach: Teideal' : 'Search: Title'"></span></div>
            <!-- <div class="arcform-wrap"><input ng-cloak class="arcform-textbox" name="cbox" id="cbox" ng-model="cs" placeholder="{{current_Lang=='ga'?' Comhlacht Léiriúchán...':' Production Company...'}}" ng-change="ss1=noFada(cs);" /><a href="#" class="arcform-btn" onclick="this.href='?srchComp=' + document.getElementById('cbox').value">>></a><br /><span class="arcform-text" ng-bind="current_Lang=='ga' ? 'Cuardach: Comhlacht Léiriúchán' : 'Search: Production Company'"></span></div> -->
            <div class="arcform-wrap"><input ng-cloak class="arcform-textbox" name="ybox" id="ybox" ng-model="ys" placeholder="{{current_Lang=='ga'?' Bliain...':' Year...'}}" ng-change="ss1=noFada(ys);" maxlength="4" /><a href="#" id="srchYearBtn" class="arcform-btn" onclick="this.href='?srchYear=' + document.getElementById('ybox').value">>></a><br /><span class="arcform-text" ng-bind="current_Lang=='ga' ? 'Cuardach: Bliain' : 'Search: Year'"></span></div>
        </div>

        <script>
        var input1 = document.getElementById("sbox");
        input1.addEventListener("keyup", function(event) {
            event.preventDefault();
            if (event.keyCode === 13) {
                document.getElementById("srchCompBtn").click();
            }
        });
        var input2 = document.getElementById("dbox");
        input2.addEventListener("keyup", function(event) {
            event.preventDefault();
            if (event.keyCode === 13) {
                document.getElementById("srchDateBtn").click();
            }
        });
        var input3 = document.getElementById("tbox");
        input3.addEventListener("keyup", function(event) {
            event.preventDefault();
            if (event.keyCode === 13) {
                document.getElementById("srchTitleBtn").click();
            }
        });
        var input4 = document.getElementById("ybox");
        input4.addEventListener("keyup", function(event) {
            event.preventDefault();
            if (event.keyCode === 13) {
                document.getElementById("srchYearBtn").click();
            }
        });
        </script>
    </div>
</div>

<section class="player-mods">
    <h2 class="visuallyhidden">More on TG4</h2>
    <div class="plyrmods-wrapper">
        <!-- PHP code to pull the info back from the querystring - Genre, Alphbetical and Search term -->
        <?php
        $pageCnt = ctype_digit($_GET['pageCnt']) ? $_GET['pageCnt'] : 1;
        $progGenre = $_GET['progGenre'];
        $srchYear = $_GET['srchYear'];
        $srchTitle = $_GET['srchTitle'];
        $srchTxt = $_GET['srchTxt'];
        $srchDate = $_GET['srchDate'];

        //Check for the programme genre in querystring
        if (isset($progGenre)) {
            $genreQuery = "&progGenre=" . $progGenre;
            $listTitle = $progGenre;
        }
        //Check for the search term in querystring
        if (isset($srchYear)) {
            $srchQuery = "&srchYear=" . $srchYear;
            $listTitle = $srchYear;
        }
        //Check for the search term in querystring
        if (isset($srchTitle)) {
            $srchQuery = "&srchTitle=" . $srchTitle;
            $listTitle = $srchTitle;
        }
        //Check for the search term in querystring
        if (isset($srchTxt)) {
            $srchQuery = "&srchTxt=" . $srchTxt;
            $listTitle = $srchTxt;
        }
        //Check for the search term in querystring
        if (isset($srchDate)) {
            $srchQuery = "&srchDate=" . $srchDate;
            $listTitle = $srchDate;
        }
        ?>

        <!-- Jquery Paging -->
        <div class="container">
            <script src='https://d1og0s8nlbd0hm.cloudfront.net/js/min/jquery.bootpag.min.js'></script>
            <p id="pagination-here"></p>
        </div>

        <div class="player-mod-wrap">
            <h2 class="archiveLtr">
                <?php
                if (isset($listTitle)) {
                    echo stripslashes($listTitle);
                } else {
                    //echo "A";
                    echo "Nuacht & Cursaí Reatha";
                }
                ?>
                <span class="archivePg"> (<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'LTH. ' : 'PAGE '); echo $pageCnt; ?>)</span>
            </h2>
            <!-- Programme content added into boxes below -->
            <section class="mod-1" ng-if="p.prog_vod_category=='Nuacht & Cursaí Reatha'" ng-repeat="p in allProg=(pAll | orderBy:-p.prog_first_tx_date) | limitTo:20"> <!--track by p.id"-->
                <div class="player-mod-notice" ng-if="p.prog_vod_category=='Nuacht & Cursaí Reatha'">
                    <a ng-cloak href="<?php echo site_url(); ?>{{current_Lang=='ga'?'/ga/tg4-archive/new-archive-player/?mat_id='+p.mat_id+'&teideal='+p.prog_dynamic_branding_title+'&series='+p.series_title+'':'/en/tg4-archive/new-archive-player/?mat_id='+p.mat_id+'&teideal='+p.prog_dynamic_branding_title+'&series='+p.series_title+''}}" class="prog-panel">
                        <h3 class="player-title" ng-bind="p.prog_dynamic_branding_title.length>0?p.prog_dynamic_branding_title:p.prog_title"></h3>
                        <img ng-cloak ng-src="https://d2m9n7jejbgumr.cloudfront.net/{{p.mat_id}}/{{p.mat_id}}_640x360.jpg" alt="{{p.prog_dynamic_branding_title}}" class="prog-img">
                        <!-- <img ng-cloak ng-src="https://tg4-archive-images2.s3.amazonaws.com/{{p.mat_id}}/{{p.mat_id}}_640x360.jpg" alt="{{p.prog_dynamic_branding_title}}" class="prog-img"> -->
                        <div class="prog-footer">
                            <h4 class="prog-episode-title" ng-bind="p.prog_dynamic_branding_title!=p.series_branding_title?p.prog_dynamic_branding_title:''"></h4>
                            <div class="prog-season"><span ng-bind="p.prog_first_tx_date | date: 'yyyy'"></span></div>
                            <!-- <div class="prog-season"><span ng-bind="p.series_season_number.length>0?'S'+p.series_season_number:(p.prog_first_tx_date | date: 'yyyy')"></span></div>
                            <div class="prog-episode"><span ng-if="p.prog_episode_no!=''">E<span ng-bind="p.prog_episode_no"></span></span></div> -->
                            <div class="arrow-box"></div>
                        </div>
                        <div class="prog-infobar">
                            <div class="prog-firstshow" ng-bind="(current_Lang=='ga'?'Chéad Clár: ':'First Shown: ') + (p.prog_first_tx_date | date:'dd.MM.yy')"></div>
                            <div ng-if="p.file_duration!=''" class="prog-dur"><span ng-bind="(current_Lang=='ga'?'Fad: ':'Dur: ') + (p.file_duration.substr(0,8))"></span></div>
                        </div>
                        <div class="prog-desc" ng-bind="(current_Lang=='ga'?p.prog_press_desc_short_gae:p.prog_press_desc_short_eng).substr(0,200)+'...'"></div>
                    </a>
                </div>
            </section>
            <div ng-hide="allProg.length" ng-bind="current_Lang=='ga'?'Níl aon fiseáin ar fáil.':'No videos available.'"></div>
        </div>

        <!-- Angular -->
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular-touch.min.js"></script>
        <!-- Angular -->

        <script>
            var app=angular.module('tg4App',[]);
            app.controller('tg4Ctrl', function($scope,$http) {
                $scope.current_Lang='<?php echo ICL_LANGUAGE_CODE ?>';
                $scope.total = 0;
                $scope.currentPage = <?php echo $pageCnt; ?>;

                $scope.noFada = function(value){ 
                    return value.toLowerCase().replace(/á/g,'a').replace(/é/g,'e').replace(/í/g,'i').replace(/ó/g,'o').replace(/ú/g,'u'); 
                };

                //* Check querystring parameters for Archive Genre, Page Count, Alphabetical link & Search text
                var params={};
                decodeURIComponent(location.search).replace(/[?&]+([^=&]+)=([^&]*)/gi,function(m,k,v){ params[k]=v });
                //* Check querystring parameters for Archive Genre
                if (!params.progGenre) { 
                    params.progGenre='Cursaí%20Reatha';
                }
                //* Check querystring parameters for Page Count, if empty load 1st page
                if (!params.pageCnt) { 
                    params.pageCnt=1;
                }
                //* Check querystring parameters for Search entered
                if (!params.srchYear) { 
                    params.srchYear='';
                }
                //* Check querystring parameters for Search entered
                if (!params.srchTitle) { 
                    params.srchTitle='';
                }
                //* Check querystring parameters for Search entered
                if (!params.srchTxt) { 
                    params.srchTxt='';
                }
                //* Check querystring parameters for Search entered
                if (!params.srchDate) { 
                    params.srchDate='';
                }

                //* Calls to the API - if genre selected bring back genre listing, else display listing alphabetically
                if (params.srchYear) {
                    $http.post('https://www.tg4tech.com/api/programmes/basicSearch?q='+params.srchYear+'&searchfield=prog_first_tx_date&orderon=prog_vod_category&orderby=desc&page='+params.pageCnt).then(function(d){
                        $scope.allDataLoaded(d.data);
                        $scope.total = d.data.total_count;

                        $scope.pageTotal = Math.ceil($scope.total/20);
                        console.log("Total Pages ", $scope.pageTotal);

                        $(document).ready(function () {
                            $('#pagination-here').bootpag({
                                total: $scope.pageTotal,
                                page: <?php echo $pageCnt; ?>,
                                maxVisible: 20,
                                leaps: false,
                                //href: "?pageCnt={{number}}<?php echo $sortQuery; ?>",
                                firstLastUse: true,
                                first: '←',
                                last: '→'
                            }).on('page', function(event, num){
                                window.location.href = '?pageCnt=' + num + '<?php echo $genreQuery; echo $ltrQuery; echo $srchQuery; ?>';
                            });
                        });
                    });
                } else if (params.srchComp) {
                    $http.post('https://www.tg4tech.com/api/programmes/basicSearch?q='+params.srchComp+'&searchfield=prog_production_company&orderon=prog_vod_category&orderby=desc&page='+params.pageCnt).then(function(d){
                        $scope.allDataLoaded(d.data);
                        $scope.total = d.data.total_count;

                        $scope.pageTotal = Math.ceil($scope.total/20);
                        console.log("Total Pages ", $scope.pageTotal);

                        $(document).ready(function () {
                            $('#pagination-here').bootpag({
                                total: $scope.pageTotal,
                                page: <?php echo $pageCnt; ?>,
                                maxVisible: 20,
                                leaps: false,
                                //href: "?pageCnt={{number}}<?php echo $sortQuery; ?>",
                                firstLastUse: true,
                                first: '←',
                                last: '→'
                            }).on('page', function(event, num){
                                window.location.href = '?pageCnt=' + num + '<?php echo $genreQuery; echo $ltrQuery; echo $srchQuery; ?>';
                            });
                        });
                    });
                } else if (params.srchTitle) {
                    $http.post('https://www.tg4tech.com/api/programmes/basicSearch?q='+params.srchTitle+'&searchfield=prog_dynamic_branding_title&orderon=prog_first_tx_date&orderby=desc&page='+params.pageCnt).then(function(d){
                        $scope.allDataLoaded(d.data);
                        $scope.total = d.data.total_count;

                        $scope.pageTotal = Math.ceil($scope.total/20);
                        console.log("Total Pages ", $scope.pageTotal);

                        $(document).ready(function () {
                            $('#pagination-here').bootpag({
                                total: $scope.pageTotal,
                                page: <?php echo $pageCnt; ?>,
                                maxVisible: 20,
                                leaps: false,
                                //href: "?pageCnt={{number}}<?php echo $sortQuery; ?>",
                                firstLastUse: true,
                                first: '←',
                                last: '→'
                            }).on('page', function(event, num){
                                window.location.href = '?pageCnt=' + num + '<?php echo $genreQuery; echo $ltrQuery; echo $srchQuery; ?>';
                            });
                        });
                    });
                } else if (params.srchTxt) {
                    $http.post('https://www.tg4tech.com/api/programmes/_search?q='+params.srchTxt+'&fields=prog_title,prog_production_company,prog_production_year,prog_coord,prog_press_desc_short_eng,prog_press_desc_short_gae,mat_id,prog_dynamic_branding_title,series_branding_title,prog_first_tx_date,prog_vod_category,file_duration&limit=50&orderon=prog_vod_category&orderby=desc&page='+params.pageCnt).then(function(d){
                        $scope.allDataLoaded(d.data); 
                        $scope.total = d.data.total_count; 

                        $scope.pageTotal = Math.ceil($scope.total/20);
                        console.log("Total Pages ", $scope.pageTotal);

                        $(document).ready(function () {
                            $('#pagination-here').bootpag({
                                total: $scope.pageTotal,
                                page: <?php echo $pageCnt; ?>,
                                maxVisible: 20,
                                leaps: false,
                                //href: "?pageCnt={{number}}<?php echo $sortQuery; ?>",
                                firstLastUse: true,
                                first: '←',
                                last: '→'
                            }).on('page', function(event, num){
                                window.location.href = '?pageCnt=' + num + '<?php echo $genreQuery; echo $ltrQuery; echo $srchQuery; ?>';
                            });
                        });
                    });
                } else if (params.srchDate) {
                    function addZero(number) {
                        console.log(number.length);
                        if (number.length == 1) {
                            if (number < 10) {
                                return "0" + number;
                            }
                        } else {
                            return number;
                        }
                    }

                    //console.log('- ', params.srchDate.indexOf('-'), '/ ', params.srchDate.indexOf('/'), '. ', params.srchDate.indexOf('.'));

                    if (params.srchDate.indexOf('/') !== -1) {
                        var strSplitDate = params.srchDate.split('/');
                    } else if (params.srchDate.indexOf('.') !== -1) {
                        var strSplitDate = params.srchDate.split('.');
                    } else if (params.srchDate.indexOf('-') !== -1) {
                        var strSplitDate = params.srchDate.split('-');
                    }

                    //console.log('POC', strSplitDate[2]);

                    var dbDate = strSplitDate[2] + '-' + addZero(strSplitDate[1]) + '-' + addZero(strSplitDate[0]);
                    //console.log(dbDate);

                    $http.post('https://www.tg4tech.com/api/programmes/archiveTextDateSearch?q='+params.srchTxt+'&date='+dbDate+'&fields=prog_thumbnail_src,prog_poster_src,prog_title,prog_vod_category,prog_production_company,prog_production_year,prog_press_desc_short_eng,prog_press_desc_short_gae,mat_id,prog_dynamic_branding_title,prog_first_tx_date,cue_points&limit=20&searchfield=prog_dynamic_branding_title&orderon=prog_dynamic_branding_title&page='+params.pageCnt).then(function(d){
                        $scope.allDataLoaded(d.data);
                        $scope.total = d.data.total_count;

                        $scope.pageTotal = Math.ceil($scope.total/20);
                        console.log("Total Pages ", $scope.pageTotal);

                        $(document).ready(function () {
                            $('#pagination-here').bootpag({
                                total: $scope.pageTotal,
                                page: <?php echo $pageCnt; ?>,
                                maxVisible: 20,
                                leaps: false,
                                //href: "?pageCnt={{number}}<?php echo $sortQuery; ?>",
                                firstLastUse: true,
                                first: '←',
                                last: '→'
                            }).on('page', function(event, num){
                                window.location.href = '?pageCnt=' + num + '<?php echo $genreQuery; echo $ltrQuery; echo $srchQuery; ?>';
                            });
                        });
                    });
                } else {
                    $http.post('https://www.tg4tech.com/api/category?id=Cursaí%20Reatha&orderon=prog_first_tx_date&orderby=desc&page='+params.pageCnt).then(function(d){
                        $scope.allDataLoaded(d.data);
                        $scope.total = d.data.total_count;

                        $scope.pageTotal = Math.ceil($scope.total/20);
                        console.log("Total Pages ", $scope.pageTotal);

                        $(document).ready(function () {
                            $('#pagination-here').bootpag({
                                total: $scope.pageTotal,
                                page: <?php echo $pageCnt; ?>,
                                maxVisible: 20,
                                leaps: false,
                                //href: "?pageCnt={{number}}<?php echo $sortQuery; ?>",
                                firstLastUse: true,
                                first: '←',
                                last: '→'
                            }).on('page', function(event, num){
                                window.location.href = '?pageCnt=' + num + '<?php echo $genreQuery; echo $ltrQuery; echo $srchQuery; ?>';
                            });
                        });
                    });
                }

                $scope.allDataLoaded = function(da){
                    $scope.pAll=da.results;
                    console.log('Results - ', $scope.pAll);
                }
            });
        </script>
    </div>
</section>

<?php get_footer(); ?>