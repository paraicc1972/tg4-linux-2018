<?php /* Template Name: MolScéal Home - API */ ?>

<?php include(locate_template('/header-molsceal.php')); ?>

<!-- Sub-navigation -->
<div class="nav-bar-molsceal">
    <div class="submenu-wrapper">
        <ul class="section-submenu-molsceal">
            <li class="page_item"><a href="<?php echo site_url(); ?>/nua/">Nua</a></li>
            <li class="page_item"><a href="<?php echo site_url(); ?>/nuacht/">Nuacht</a></li>
            <li class="page_item"><a href="<?php echo site_url(); ?>/sport/">Spórt</a></li>
            <li class="page_item"><a href="<?php echo site_url(); ?>/siamsaiocht/">Siamsaíocht</a></li>
            <li class="page_item"><a href="<?php echo site_url(); ?>/trendail/">Trendáil</a></li>
            <li class="page_item"><a href="<?php echo site_url(); ?>/cartlann/">Cartlann</a></li>
            &nbsp;
        </ul>
    </div>
</div>

<!-- Angular -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-1.0.3.js"></script>
<!-- Angular -->

<div class="section-header">
    <h1 class="section-title">MolScéal: Suíomh úrnua Gaeilge</h1>
</div>

<section class="molsceal-mods">
    <h2 class="visuallyhidden">Tuilleadh ar Molscéal</h2>
    <div class="molscealhome-wrapper">
        <div class="molsceal-home-wrap" ng-app="molscealApp" ng-controller="molscealCtrl">
            <section class="mod-1">
                <a href="<?php echo site_url(); ?>/nua/alt?CID={{pNew.id}}" class="prog-panel">
                    <!-- <div class="molsceal-mod-home" ng-style="{'background-image': 'url(' + pNewImage + ')'}" style="background-position:top center; box-shadow:inset 0 0 0 2000px rgba(0,214,242,0.6);"> -->
                    <div class="molsceal-mod-home" style="background-image: url('https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2018/06/Nua-hp.png'); background-position:top center; box-shadow:inset 0 0 0 2000px rgba(0,214,242,0.6);">
                        <h3 class="molsceal-hometitle molsceal-title-new">NUA</h3>
                    </div>
                </a>
            </section>
            <section class="mod-2">
                <a href="<?php echo site_url(); ?>/nuacht/alt?CID={{pNews.id}}" class="prog-panel">
                    <!-- <div class="molsceal-mod-home" ng-style="{'background-image': 'url(' + pNewsImage + ')'}" style="background-position:top center; box-shadow:inset 0 0 0 2000px rgba(3,111,173,0.6);"> -->
                    <div class="molsceal-mod-home" style="background-image: url('https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2018/06/Nuacht-hp.png'); background-position:top center; box-shadow:inset 0 0 0 2000px rgba(3,111,173,0.6);">
                        <h3 class="molsceal-hometitle molsceal-title-news">Nuacht</h3>
                    </div>
                </a>
            </section>
            <section class="mod-2">
                <a href="<?php echo site_url(); ?>/sport/alt?CID={{pSport.id}}" class="prog-panel">
                    <div class="molsceal-mod-home" style="background-image: url('https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2018/06/Sport-hp.png'); background-position:top center; box-shadow:inset 0 0 0 2000px rgba(41,159,27,0.6);">
                    <!-- <div ng-style="{'background-image': 'url(\{{pSport.poster}}\)'}"> -->
                        <h3 class="molsceal-hometitle molsceal-title-sport">Spórt</h3>
                    </div>
                </a>
            </section>
            <section class="mod-3">
                <a href="<?php echo site_url(); ?>/siamsaiocht/alt?CID={{pEnt.id}}" class="prog-panel">
                    <div class="molsceal-mod-home" style="background-image: url('https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2018/06/Siamsaiocht-hp.png'); background-position:top center; box-shadow:inset 0 0 0 2000px rgba(104,45,203,0.6);">
                        <h3 class="molsceal-hometitle molsceal-title-ent">Siamsaíocht</h3>
                    </div>
                </a>
            </section>
            <section class="mod-4">
                <a href="<?php echo site_url(); ?>/trendail/alt?CID={{pTrend.id}}" class="prog-panel">
                    <div class="molsceal-mod-home" style="background-image: url('https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2018/06/Trendail-hp.png'); background-position:top center; box-shadow:inset 0 0 0 2000px rgba(255,3,129,0.6);">
                        <h3 class="molsceal-hometitle molsceal-title-trend">Trendáil</h3>
                    </div>
                </a>
            </section>
            <section class="mod-3">
                <a href="<?php echo site_url(); ?>/cartlann/alt?CID={{pArchive.id}}" class="prog-panel">
                    <div class="molsceal-mod-home" style="background-image: url('https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2018/06/Cartlann-hp.png'); background-position:top center; box-shadow:inset 0 0 0 2000px rgba(239,196,72,0.6);">
                        <h3 class="molsceal-hometitle molsceal-title-arch">Cartlann</h3>
                    </div>
                </a>
            </section>
        </div>
    </div>
</section>

<script>
var molscealApp=angular.module("molscealApp", []);
molscealApp.controller('molscealCtrl', function($scope, $http) {

    var headerParam = { headers: {
        "Accept": "application/json;odata=verbose;pk=BCpkADawqM00hynZPtkz2ibF_WYPuTtQQ21mfOjHdetwlbmsgch-d_OiQmEfApPpYznCFUABBVlvZqDWmXYYs8-dXK3s2DSVWdyi-dajLTfYm42tZ1iBY6gxBlUN134_MBnsZQ6_36mt-RDP"
        }
    };

    var endPointNew = '/playlists/5775228437001';
    $http.get('https://edge.api.brightcove.com/playback/v1/accounts/5561472261001' + endPointNew, headerParam).then(function(dNew) {
        $scope.pNew = dNew.data.videos[0];
        $scope.pCnt = dNew.data.videos.length;
        $scope.pNewImage = dNew.data.videos[0].poster;
        //console.log("New: ", $scope.pNewImage);
    });

    var endPointNews = '/playlists/5772339413001';
    $http.get('https://edge.api.brightcove.com/playback/v1/accounts/5561472261001' + endPointNews, headerParam).then(function(dNews) {
        $scope.pNews = dNews.data.videos[0];
        $scope.pNewsImage = dNews.data.videos[0].poster;
        //console.log("News: ", $scope.pNews);
    });

    var endPointSport = '/playlists/5772600968001';
    $http.get('https://edge.api.brightcove.com/playback/v1/accounts/5561472261001' + endPointSport, headerParam).then(function(dSport) {
        $scope.pSport = dSport.data.videos[0];
        /* $scope.pSportImage = dSport.data.videos[0].poster;
        console.log("Sport: ", $scope.pSport); */
    });

    var endPointEnt = '/playlists/5772600967001';
    $http.get('https://edge.api.brightcove.com/playback/v1/accounts/5561472261001' + endPointEnt, headerParam).then(function(dEnt) {
        $scope.pEnt = dEnt.data.videos[0];
        /* $scope.pEntImage = dEnt.data.videos[0].poster;
        console.log("Entertainment: ", $scope.pEnt); */
    });

    var endPointTrend = '/playlists/5772244063001';
    $http.get('https://edge.api.brightcove.com/playback/v1/accounts/5561472261001' + endPointTrend, headerParam).then(function(dTrend) {
        $scope.pTrend = dTrend.data.videos[0];
        /* $scope.pTrendImage = dTrend.data.videos[0].poster;
        console.log("Trending: ", $scope.pTrend); */
    });

    var endPointArchive = '/playlists/5772600969001';
    $http.get('https://edge.api.brightcove.com/playback/v1/accounts/5561472261001' + endPointArchive, headerParam).then(function(dArc) {
        $scope.pArchive = dArc.data.videos[0];
        /* $scope.pArchiveImage = dArc.data.videos[0].poster;
        console.log("Archive: ", $scope.pArchive); */
    });
});
</script>

<?php include(locate_template('/footer-molsceal.php')); ?>