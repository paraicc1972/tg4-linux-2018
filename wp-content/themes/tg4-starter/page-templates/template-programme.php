<?php /* Template Name: Programme */ ?>

<?php include(locate_template('/header-prog.php'));

if (!post_password_required()) { ?>
	<div class="section-header">
		<h1 class="section-title">
			<?php
			if (get_the_title($post->post_parent) == "Programmes" || get_the_title($post->post_parent) == "Cláir" || get_the_title($post->post_parent) == "Sport" || get_the_title($post->post_parent) == "Spórt") {
				echo get_the_title($post->ID);
			} else {
				echo get_the_title($post->post_parent);
			}
			?>
		</h1>
		<div class="showing-times">
			<?php 
			$broadcast_day = get_field("broadcast_day");
			$broadcast_time = get_field("broadcast_time");
			if($broadcast_day) { ?>
			<div class="showing-box">
				<div class="showing-day"><?php echo substr($broadcast_day, 0, 5); ?></div>
				<div class="showing-time"><?php echo $broadcast_time; ?></div>
			</div>
			<?php }
			$repeat_day = get_field("repeat_day");
			$repeat_time = get_field("repeat_time");
			if($repeat_day) { ?>
			<div class="showing-box">
				<div class="showing-day"><?php echo substr($repeat_day, 0, 5); ?></div>
				<div class="showing-time"><?php echo $repeat_time; ?></div>
			</div>
			<?php } ?>
		</div>
	</div>

	<!-- Sub-navigation -->
	<div class="section-submenu">
		<div class="section-submenu-wrap">
			<ul class="section-submenu-list">
				<?php
				$ancestors = get_post_ancestors($post->ID);
				//echo $post->ID . " - " . $post->post_parent;
				//echo $ancestors[2];
				if ($ancestors[2] == 137 || $ancestors[2] == 140) {
					wp_list_pages('sort_column=menu_order&title_li=&child_of='. $ancestors[1] . '&depth=0');
				} else {
					if ($post->post_parent != 137 && $post->post_parent != 140 && $post->post_parent != 390 && $post->post_parent != 393 || ($ancestors[2] == 137 && $ancestors[2] == 140)) {
						wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=0');
					} elseif ($post->ID != 137 && $post->ID != 140) {
						wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->ID . '&depth=0');
					}
				}
				?>
				&nbsp;
			</ul>
		</div>
	</div>

	<?php
	$feat_image = '';
	$imgCode = '';

	if (get_field("Series_Code") != '') {
		$imgCode = get_field("Series_Code");
	} 
	if (get_field("Programme_Code") != '') {
		$imgCode = get_field("Programme_Code");
	} 
	if ($imgCode == '') {
		$feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
	}

	if (get_field("centre_feature_image") == '0' || get_field("centre_feature_image") == 'no') {
		$feat_Class = "prog-head";
	} elseif (get_field("centre_feature_image") == '1' || get_field("centre_feature_image") == 'yes')  {
		$feat_Class = "prog-headcenter";
	} else {
		$feat_Class = "prog-head";
	}
	
	if ($feat_image != '') { ?>
		<section class="<?php echo $feat_Class; ?>" style="background-image: url(<?php echo $feat_image; ?>);">
			<div class="blockContainer">
				<?php 
				if (get_field("intro_text")) { ?>
					<div class="content-block">
						<h2 class="content-subtitle"><?php echo get_field("intro_sub_title"); ?></h2>
						<p><?php echo get_field("intro_text"); ?></p>
					</div>
				<?php } ?>
			</div>
		</section>
	<?php } else { ?>
		<script>
			//window.alert("Your screen resolution is: " + window.innerHeight + 'x' + window.innerWidth);
			if (window.innerWidth <= 900) {
			    document.write('<section class="<?php echo $feat_Class; ?>" style="background-image: url(\'https://res.cloudinary.com/tg4/image/upload/w_700,h_395,f_auto,q_auto/<?php echo $imgCode; ?>.jpg\');">');
			} else if (window.innerWidth > 901 && window.innerWidth <= 1408) {
			    document.write('<section class="<?php echo $feat_Class; ?>" style="background-image: url(\'https://res.cloudinary.com/tg4/image/upload/w_1310,h_737,f_auto,q_auto/<?php echo $imgCode; ?>.jpg\');">');
			} else {
			    document.write('<section class="<?php echo $feat_Class; ?>" style="background-image: url(\'https://res.cloudinary.com/tg4/image/upload/f_auto,q_auto/<?php echo $imgCode; ?>.jpg\');">');
			}
		</script>
			<div class="blockContainer">
				<?php 
				if (get_field("intro_text")) { ?>
					<div class="content-block">
						<h2 class="content-subtitle"><?php echo get_field("intro_sub_title"); ?></h2>
						<p><?php echo get_field("intro_text"); ?></p>
					</div>
				<?php } ?>
			</div>
		</section>
	<?php }
	if ($progID) { ?>
		<section class="plyr-feat-section">
	        <div class="section-panel-dark-3">

	            <!-- Load Player -->
	            <div class="plyr-feat center-panel">
	                <div class="plyr-feat-wrap">

	                    <?php if (($Econ=="AD_SUPPORTED") && ($geoCode == "IE")) { ?>
	                    	<!-- SSAI Enabled Player | 2x PreRolls | 1x Bumper -->
                            <video id="Subtitles" class="video-js vjs-big-play-centered"
                                controls preload="auto" width="100%" height="auto"
                                data-account="1555966122001"
                                data-player="rkPVmp4yM"
                                data-embed="default"
                                data-video-id="<?php echo $progID ?>">
                                <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p> 
                            </video>
                            <script src="https://players.brightcove.net/1555966122001/rkPVmp4yM_default/index.min.js"></script>

	                    <?php } else { ?>
	                        <!-- NoAds Player -->
                            <video id="Subtitles" class="video-js vjs-big-play-centered"
                                controls preload="auto" width="100%" height="auto"
                                data-account="1555966122001"
                                data-player="HJlOg6KUz"
                                data-embed="default"
                                data-video-id="<?php echo $progID ?>">
                                <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p> 
                            </video>
                            <script src="https://players.brightcove.net/1555966122001/HJlOg6KUz_default/index.min.js"></script>

	                    <?php } ?>

	                    <!-- google analytics plugin -->
	                    <script src="https://d1og0s8nlbd0hm.cloudfront.net/js/min/videojs.ga.videocloud.min.js"></script>

	                    <!-- video.js BC Player Controls -->
	                    <script>
	                        var myPlayer = videojs("Subtitles");
	                        var tg4Player = videojs("Subtitles");

	                        videojs('Subtitles').one('bc-catalog-error', function() {
	                            var myPlayer = this,
	                            specificError;
	                            myPlayer.errors({
	                            'errors': {
	                              '-3': {
                                	'headline': '<h1>Geo-Restricted!</h1>Níl an clár seo le fáil i do Réigiúnsa.<br />Breis eolas le fáil san leathanach <a href="<?php echo site_url(); ?>/ga/faq/frequently-asked-questions/">Ceisteanna Coitianta</a>.<br /><br />This programme is not avaialble in your region.<br />Further information from our <a href="<?php echo site_url(); ?>/en/faq/frequently-asked-questions/">FAQ</a> page.',
	                                'type': 'CLIENT_GEO'
	                                    }
	                                }
	                            });
	                            if (typeof(myPlayer.catalog.error) !== 'undefined') {
	                                specificError = myPlayer.catalog.error.data[0];
	                                if (specificError !== 'undefined' & specificError.error_subcode == "CLIENT_GEO") {
	                                    myPlayer.error({code:'-3'});
	                                };
	                            };
	                        });

	                        // GA Custom Script
	                        tg4Player.on('loadstart',function() {
	                            tg4Player = this;
	                            //console.log("catalog object: ",this.catalog.load()); 
	                            //console.log("mediainfo object: ", this.mediainfo); 
	                            tg4Player.ga();
	                        });

	                        myPlayer.ready(function() {
	                            // get a reference to the player
	                            myPlayer = this;

	                            myPlayer.on("play", function (evt) {
	                                console.log('Asset Play');
	                            });

	                            myPlayer.on("pause", function (evt) {
	                                console.log('Asset Paused');
	                            });

	                            myPlayer.on("ended", function (evt) {
	                                console.log('Asset Ended');
	                            });
	                        });
	                    </script>
	                </div>
	                <div class="prog-feat-ad mod-ad">
	                    <h2 class="mod-ad-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Fógraíocht' : 'Advertisement'); ?></h2>
	                    <div class="ad-wrapper">
			                <!-- /172054193/MPU//Side1234 -->
			                <div id='div-gpt-ad-1484321672116-0' style='height:250px; width:300px;'>
			                    <script>
			                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1484321672116-0'); });
			                    </script>
			                </div>
	                        <!--iframe src="<?php //echo site_url() . '/wp-content/themes/tg4-starter/'; ?>mpu-banner.htm" height="250" width="300" scrolling="no" frameborder="0"></iframe-->
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="plyr-feat-intro-wrap">
	            <div class="center-panel">
	                <div class="prog-feat-intro">
	                    <div class="prog-feat-toolbar">
	                        <div class="prog-remind">
	                            <div class="prog-remind-txt"><?php echo $progTitle . ' ' . $progSeries . '-' . $progEpisode; ?></div>
	                            <!-- <div class="prog-remind-count"><span><?php //echo $daysLeft; ?></span><?php //echo (ICL_LANGUAGE_CODE == "ga" ? 'Lá Fagtha' : 'Days Left'); ?></div> -->
	                        </div>
	                        <div class="prog-share">
	                            
	                            <!-- Facebook -->
	                            <a href="#" onClick="window.open('https://www.facebook.com/sharer.php?u=<?php echo 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>','TG4/Facebook Share','resizable,height=350,width=500'); return false;" class="facebook-share">Facebook</a>
	                            <noscript><a href="https://www.facebook.com/sharer.php?u=<?php echo 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" target="_blank" class="facebook-share">Facebook</a></noscript>

	                            <!-- Twitter -->    
	                            <a href="#" onClick="window.open('https://twitter.com/share?text=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Féach seo...' : 'Féach seo...'); ?>&amp;url=<?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>&amp;hashtags=TG4','TG4/Twitter Share','resizable,height=350,width=500'); return false;" class="twitter-share">Twitter</a>
	                            <noscript><a href="https://twitter.com/share?text=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Féach seo...' : 'Féach seo...'); ?>&amp;url=<?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>&amp;hashtags=TG4" target="_blank" class="twitter-share">Twitter</a></noscript>
	                            
	                            <!-- Email -->
	                            <a href="mailto:?Subject=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Suíomh TG4 - ' . site_url() . '' : 'TG4 Website - ' . site_url() . ''); ?>&amp;Body=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'A%20Chara,%0A%0AB\'fhéidir%20go%20mbeadh%20suim%20agat%20sa%20chlár%20seo%20ar%20suíomh%20idirlín%20TG4!%0A%0A' : 'A%20Chara,%0A%0AI%20saw%20this%20on%20the%20TG4%20website%20and%20thought%20you%20might%20be%20interested!%0A%0A'); ?><?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" class="email-share">Email</a>
	                        </div>
	                    </div>
	                    <div class="prog-feat-txt">
	                        <p class="bold"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Rátáil Tuismitheora: ' . $progPG : 'Parental Rating: ' . $progPG); ?></p>
	                        <p><?php echo (ICL_LANGUAGE_CODE == "ga" ? $progGaeDesc : $progEngDesc); ?></p>
	                        <p class="bold"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Chéad Taispeántas: ' . date("j ", strtotime($showStartDate)) . schedule_daily_abbr(date("M", strtotime($showStartDate))) : 'First Shown: ' . $showStartDate); ?></p>
	                        <p><a href="<?php echo $linkURL; ?>" target="_New"><?php echo $linkText; ?></a></p>
	                    </div>
	                    <?php 
	                    $noSubs = array('Nuacht TG4','GAA Beo','GAA 2016','Rugbaí Beo','7 Lá','Peil na mBan Beo');
	                    if (!in_array($progSeriesTitle, $noSubs)) { ?>
	                        <div class="prog-feat-keyword" style="background-color:#ffffff; border: 1px solid #043244; padding:8px; margin: 5px 0px 5px 0px;">
	                            <p class="bold"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Rogha Fotheidil' : 'Optional Subtitles'); ?></p>
	                            <p style="font-size:small;"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Le fotheidil a roghnú, brú [CC][<img src="https://s3.amazonaws.com/tg4-docs/images/player-subs.png">] ag bun an scáileáin.' : 'To choose subtitles, press [CC][<img src="https://s3.amazonaws.com/tg4-docs/images/player-subs.png">] in the player controls.'); ?></p>
	                            <p class="bold"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'An Seinnteoir ar IOS' : 'Player on IOS devices'); ?></p>
	                            <p style="font-size:small;"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Le breathnú ar an Seinnteoir ar IOS, íoslódáil Aip TG4 anseo... <a href="https://itunes.apple.com/ie/app/tg4-player/id598591024?mt=8" target="_New">https://itunes.apple.com/ie/app/tg4-player/id598591024?mt=8</a>' : 'To access the player on an IOS device, you need to download the TG4 Player App. Free to download here... <a href="https://itunes.apple.com/ie/app/tg4-player/id598591024?mt=8" target="_New">https://itunes.apple.com/ie/app/tg4-player/id598591024?mt=8</a>'); ?></p>
	                        </div>
	                    <?php  }  ?>
	                </div>
	            </div>
	        </div>
	        <!-- / Schema.org Microdata -->
	        <meta itemprop="video" itemscope itemtype="https://schema.org/VideoObject"/>
	        <meta itemprop="name" content="<?php echo $progName; ?>" />
	        <meta itemprop="description" content="<?php echo $progEngDesc; ?>" />
	        <meta itemprop="contentURL" content="<?php echo 'https://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>" />
	        <meta itemprop="width" content="960" />
	        <meta itemprop="height" content="540" />
	        <meta itemprop="thumbnail" content="<?php echo $progBCImage; ?>" />
	        <meta itemprop="inLanguage" content="EN" />
	        <meta itemprop="isFamilyFriendly" content="<?php echo $famFriendly; ?>" />
	        <meta itemprop="contentLocation" content="Ireland" />
	        <meta itemprop="encodingFormat" content="MP4" />
	        <meta itemprop="videoFrameSize" content="960x540" /> 
	        <meta itemprop="about" content="<?php echo (ICL_LANGUAGE_CODE == "ga" ? $progGaeDesc : $progEngDesc); ?>" />
	        <meta itemprop="genre" content="<?php echo $progCategory; ?>" />
	        <meta itemprop="author" content="TG4" /> 
	        <meta itemprop="publisher" content="tg4.ie" />
	    </section>
	<?php } elseif (get_field("promo_video")) { ?>
		<!-- Video Anchor Tag -->
		<a name="video"></a>
		<section class="prog-feat-section">
			<div class="section-panel-dark-3">
				<div class="title-tab-wrap">
					<h2 class="title-tab-dark-3"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Físeáin Faoi Thrácht' : 'Featured Video'); ?></h2>
				</div>
				<div class="prog-feat center-panel">
					<div class="prog-feat-wrap">
						<video id="PromoVideo" class="vjs-big-play-centered"
						controls preload="auto" width="100%" height="auto">
						<source src="<?php echo get_field("promo_video"); ?>" type='video/mp4' />
							<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
						</video>
					</div>
					<div class="prog-feat-ad mod-ad">
						<h2 class="mod-ad-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Fógraíocht' : 'Advertisement'); ?></h2>
						<div class="ad-wrapper">
                        	<iframe src="<?php echo site_url() . '/wp-content/themes/tg4-starter/'; ?>mpu-banner.htm" height="250" width="300" scrolling="no" frameborder="0"></iframe>
                    	</div>
					</div>
				</div>
			</div>
		</section>
		<div class="prog-feat-intro-wrap">
			<div class="center-panel">
				<div class="prog-feat-intro">
					<div class="prog-feat-toolbar">
						<div class="prog-remind">
							<div class="prog-remind-txt"><?php echo get_field("promo_title") ?></div>
						</div>
						<div class="prog-share">
							<!-- Facebook -->
							<a href="#" onClick="window.open('https://www.facebook.com/sharer.php?u=<?php echo 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>','TG4/Facebook Share','resizable,height=350,width=500'); return false;" class="facebook-share">Facebook</a>
							<noscript><a href="https://www.facebook.com/sharer.php?u=<?php echo 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" target="_blank" class="facebook-share">Facebook</a></noscript>

							<!-- Twitter -->	
							<a href="#" onClick="window.open('https://twitter.com/share?text=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Féach seo...' : 'Féach seo...'); ?>&amp;url=<?php echo 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>&amp;hashtags=TG4','TG4/Twitter Share','resizable,height=350,width=500'); return false;" class="twitter-share">Twitter</a>
							<noscript><a href="https://twitter.com/share?text=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Féach seo...' : 'Féach seo...'); ?>&amp;url=<?php echo 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>&amp;hashtags=TG4" target="_blank" class="twitter-share">Twitter</a></noscript>
							
							<!-- Email -->
							<a href="mailto:?Subject=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Suíomh TG4 - ' . site_url() . '' : 'TG4 Website - ' . site_url() . ''); ?>&amp;Body=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'A%20Chara,%0A%0AB\'fhéidir%20go%20mbeadh%20suim%20agat%20sa%20chlár%20seo%20ar%20suíomh%20idirlín%20TG4!%0A%0A' : 'A%20Chara,%0A%0AI%20saw%20this%20on%20the%20TG4%20website%20and%20thought%20you%20might%20be%20interested!%0A%0A'); ?><?php echo 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" class="email-share">Email</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php }

	if (get_post_field("post_content")) { ?>
		<section class="prog-feat-section">
			<div class="section-panel-white">
			    <div class="prog-feat center-panel">
			    	<?php echo '<!-- REMOTE: ' . $_SERVER['REMOTE_ADDR'] . ' -->'; ?>
			    	<!-- POFL: 27.2.16 - Add Refresh & Timer for Social Walls -->
			    	<?php if ($post->ID == 13735 || $post->ID == 13733 || $post->ID == 13322 || $post->ID == 13211) { ?>
					    <script>
						function checklength(i) {
						    'use strict';
						    if (i < 10) {
						        i = "0" + i;
						    }
						    return i;
						}

						var minutes, seconds, count, counter, timer;
						count = 600; //seconds
						counter = setInterval(timer, 1000);

						function timer() {
						    'use strict';
						    count = count - 1;
						    minutes = checklength(Math.floor(count / 60));
						    seconds = checklength(count - minutes * 60);
						    if (count < 0) {
						        clearInterval(counter);
						        return;
						    }
						    document.getElementById("timer").innerHTML = 'Athlódáil/Refresh ' + minutes + ':' + seconds + ' ';
						    if (count === 0) {
						        location.reload();
						    }
						}
						</script>
						<span id="timer"></span>
					<?php } ?>	

			    	<div class="prog-feat-wrap">
		    		<?php echo apply_filters('the_content', get_post_field('post_content', $post_id)); ?>
		    		<?php if (get_field("content_blue_box")) { ?>
				    	<div class="prog-feat center-panel">
				    		<div class="content-blue-box-1"><?php echo get_field("content_blue_box"); ?></div>
				    	</div>
				    <?php } ?>
			    	</div>
			    
					<?php if(have_rows('file_name') || have_rows('image_name')) { ?>
					<div class="prog-feat-ad mod-ad">
						<!-- Uploaded Files to Appear here -->
						<div class="prog-feat-file mod-file">
					    <?php while(have_rows('file_name')): the_row();
					    	if (get_sub_field('file_title')) { ?>
						    	<div class="file-content-wrap">
		                            <div class="file-btn"><a href="<?php the_sub_field('upload_file') ?>" class="mod-file" target="_blank"><div class="file-btnImg"></div></a></div>
		                            <div class="file-content"><a href="<?php the_sub_field('upload_file') ?>" class="mod-file" target="_blank"><?php the_sub_field('file_title'); ?></a></div>
		                        </div>
					    <?php } endwhile; ?>
			        	</div>
						<!-- Uploaded Image to Appear here -->
						<div>
					    <?php
					    while(have_rows('image_name')): the_row();
					    	$sideImage = get_sub_field('upload_image');
					    	if (!empty($sideImage)) {
					    ?>
	                        	<image src="<?php echo $sideImage['url']; ?>" style="border:1px solid #043244; margin-bottom:10px;">
					    <?php } endwhile; ?>
			        	</div>
			        </div>
					<?php } ?>
			    </div>
			</div>
		</section>
	<?php } ?>

	<!-- Angular -->
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular-touch.min.js"></script>
    <!-- Angular -->
	<section class="section-panel-dark-2">
		<div class="title-tab-wrap">
			<h2 class="title-tab-dark-2"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Físeáin ar Fáil' : 'Available Videos'); ?></h2>
		</div>
		<?php include(locate_template('/includes/featured-videos-api.php')); ?>
	</section>

	<?php if (get_post_field("social_stream_tag")) { ?>
		<section class="prog-feat-section">
			<div class="section-panel-pale-2">
			    <div class="prog-feat center-panel">
					<?php echo apply_filters('the_content', get_post_field('social_stream_tag', $post_id)); ?>
			    </div>
			</div>
		</section>
	<?php }
} else { ?>
    <section class="prog-feat-section">
        <div class="prog-feat center-panel">
            <div class="prog-feat-wrap">
                <h2><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Pasfhocal Riachtanach'  : 'Password Required'); ?></h2>
                <p><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Chun an leathanach seo a fheiceáil, cuir isteach na pasfhocal thíos:'  : 'To view this protected page, enter the password below:'); ?></p>
                <?php
                echo my_password_form();
                ?>
            </div>
        </div>    
    </section>
<?php } 
get_footer(); ?>