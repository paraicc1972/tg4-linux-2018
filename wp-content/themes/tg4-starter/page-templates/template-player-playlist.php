<?php /* Template Name: Player - Playlist */ ?>

<?php 
    get_header(); 

    // For FairPlay versus Widevine & PlayReady - Need to Know if Safari is the Browser
    $user_agent = $_SERVER['HTTP_USER_AGENT']; 
    if (stripos( $user_agent, 'Chrome') !== false) {
        $is_Safari = 'N';
    } elseif (stripos( $user_agent, 'Safari') !== false) {
        $is_Safari = 'Y';
    } else {$is_Safari = 'N';}

?>

<style type="text/css">
    .vjs-playlist {
        height: 266px;
        width: 350px;
        float: left;
        margin: 15px 0px 0px 2px;
    }
    .vjs-playlist-name {
        font-size: 12px;
    }
    .vjs-playlist-thumbnail {
    }
    .vjs-playlist-name {
        font-size: 12px;
    }
    .vjs-playlist-description {
        font-size: 12px;
    }
</style>

<div class="section-header">
	<h1 class="section-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Seinnteoir ' . get_field("intro_title") : get_field("intro_title") .' Player'); ?></h1>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
    <div class="section-submenu-wrap">
        <ul class="section-submenu-list">
            <?php wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=1&exclude=28146,28148,73751,73753,73759,,73762,73764,73775,73777,73780'); ?>
            &nbsp;
        </ul>
    </div>
</div>

<section class="plyr-feat-section">
    <div class="section-panel-dark-3">
        <div class="plyr-feat center-panel">
            <div id="player" class="plyr-feat-wrap">

                <video class="video-js"
                    controls preload="auto" width="100%" height="auto" id="c4Player"
                    data-playlist-id="<?php echo get_field("series_tag") ?>"
                    data-account="1555966122001"
                    data-player="HJlOg6KUz"
                    data-embed="default">
                </video>
                <script src="https://players.brightcove.net/1555966122001/HJlOg6KUz_default/index.min.js"></script>

            </div>
        
            <div class="news-feat-link mod-link">
                <h2 class="link-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Roghliosta' : 'Playlist'); ?></h2>
                <div class="link-content-wrap">
                    <ul class="vjs-playlist"></ul>
                    <script>
                        //<!-- video.js BC Player Controls -->
                        var moPlayer;
                        videojs("c4Player").ready(function() {
                            // get a reference to the player
                            moPlayer = this;
                            console.log(this.catalog);

                            this.on("play", function (evt) {
                                console.log('Asset Play');
                            });

                            this.on("pause", function (evt) {
                                console.log('Asset Paused');
                            });

                            this.on("ended", function (evt) {
                                console.log('Asset Ended');
                            });
                        }); 
                    </script>
                </div>
            </div>
        </div>
    </div>
    <div class="plyr-feat-intro-wrap">
        <div class="center-panel">
            <div class="prog-feat-intro">
                <div class="prog-feat-toolbar">
                    <div class="prog-remind">
                        <div class="prog-remind-txt"><?php echo get_field("intro_title"); ?></div>
                    </div>
                    <div class="prog-share">
                        <a href="" class="facebook-share">Facebook</a>
                        <a href="" class="twitter-share">Twitter</a>
                        <a href="" class="email-share">Email</a>
                    </div>
                </div>
                <div class="prog-feat-txt">
                    <p></p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Angular -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular-touch.min.js"></script>
<!-- Angular -->
<section class="section-panel-dark-2">
    <div class="title-tab-wrap">
        <h2 class="title-tab-dark-2"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Físeáin sa Rannóg' : 'Category Videos'); ?></h2>
    </div>
    <?php 
    $categoryTag = get_field("category_tag");
    //$seriesTag = get_field("series_tag");
    include(locate_template('/includes/featured-videos-api.php')); ?>
</section>

<?php get_footer(); ?>