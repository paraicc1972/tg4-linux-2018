<?php /* Template Name: Foghlaim - Inner */ ?>

<?php get_header(); ?>

<div class="section-header">
	<h1 class="section-title"><?php echo /* get_the_title($post->post_parent) . $post->post_parent . ": " . */ get_the_title($post->ID); ?></h1>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
	<div class="section-submenu-wrap">
		<ul class="section-submenu-list-foghlaim">
            <?php
			if ($post->post_parent != 30206 && $post->post_parent != 40224 && $post->post_parent != 30397 && $post->post_parent != 40227) {
				wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=1&exclude=32582,93940,93943');
			}
			/* if(SwpmMemberUtils::is_member_logged_in()) {
            	wp_list_pages('include=35640&title_li=');
            } */
            ?>&nbsp;
        </ul>
	</div>
</div>

<?php
$feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));

if ($feat_image != '') {
	if (get_field("centre_feature_image") == '0' || get_field("centre_feature_image") == 'no') {
		$feat_Class = "prog-head";
	} elseif (get_field("centre_feature_image") == '1' || get_field("centre_feature_image") == 'yes')  {
		$feat_Class = "prog-headcenter";
	} else {
		$feat_Class = "prog-head";
	}
?>
	<section class="<?php echo $feat_Class; ?>" style="background-image: url(<?php echo $feat_image; ?>);">
		<div class="blockContainer">
			<?php 
			if (get_field("intro_text")) { ?>
			<div class="content-block">
				<h2 class="content-subtitle"><?php echo get_field("intro_sub_title"); ?></h2>
				<p><?php echo get_field("intro_text"); ?></p>
			</div>
			<?php } ?>
		</div>
	</section>
<?php }

if (get_field("lesson_video")) { ?>
	<!-- Video Anchor Tag -->
	<a name="video"></a>
	<section class="prog-feat-section" style="margin-top: -40px;">
		<div class="section-panel-foghlaim">
			<div class="prog-feat center-panel">
				<div class="prog-feat-wrap">
					<video id="PromoVideo" class="vjs-big-play-centered"
					controls preload="auto" width="100%" height="auto">
					<source src="<?php echo get_field("lesson_video"); ?>" type='video/mp4' />
						<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
					</video>
				</div>
				<div class="prog-feat-intro">
					<?php if (get_field("video_title")) { ?>
						<div class="prog-feat-toolbar">
							<div class="prog-remind">
								<div class="prog-remind-txt"><?php echo get_field("video_title") ?></div>
							</div>
						</div>
					<?php } ?>
					<div><p><?php if (get_field("video_transcript")) { echo get_field("video_transcript"); } ?></p></div>
				</div>
			</div>
		</div>
	</section>
<?php }

if (get_post_field("post_content")) { ?>
	<section class="prog-feat-section">
		<div class="section-panel-white">
		    <div class="prog-feat center-panel">
		    	<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
				    <p><?php
				    if(function_exists('bcn_display')) {
				        bcn_display();
				    }
				    ?></p>
				    <p>&nbsp;</p>
				</div>
		    	<div class="prog-feat-wrap">
		    		<?php
		    		echo apply_filters('the_content', get_post_field('post_content', $post_id));

		    		$pages = get_pages('child_of='.$post->ID.'&sort_column=post_date&sort_order=asc&parent='.$post->ID);
		    		$recCnt = 1;
		    		foreach($pages as $page) {
		    			if ($recCnt==1) {
		    				echo "<h2> <a href=". get_permalink($page->ID) . ">[ >> " . get_the_title($page->ID) . " (Cliceáil Anseo) <<]</h2>";
		    			}
		    			$recCnt++;
		    		}
		    		?>
		    	</div>
				<div class="learn-feat-ad learn-link">
					<?php if(have_rows('file_name') || have_rows('image_name')) { ?>
					<!-- Uploaded Files to Appear here -->
					<div class="prog-feat-file learn-file">
				    <?php while(have_rows('file_name')): the_row();
						if (get_sub_field('file_title')) { ?>
					    	<div class="file-content-wrap">
	                            <div class="file-btn"><a href="<?php the_sub_field('upload_file') ?>" class="learn-file" target="_blank"><div class="file-btnImg"></div></a></div>
	                            <div class="file-content">
	                                <a href="<?php the_sub_field('upload_file') ?>" class="learn-file" target="_blank"><?php the_sub_field('file_title'); ?></a>
	                             </div>
	                        </div>
				    <?php } endwhile; ?>
		        	</div>
					<!-- Uploaded Image to Appear here -->
					<div>
				    <?php
				    while(have_rows('image_name')): the_row();
				    	$sideImage = get_sub_field('upload_image');
				    ?>
                        <image src="<?php echo $sideImage['url']; ?>" style="border:1px solid #043244; margin-bottom:10px;">
				    <?php endwhile; ?>
		        	</div>
				<?php } ?>
		        </div>
		    </div>
		</div>
	</section>
<?php }

get_footer(); ?>