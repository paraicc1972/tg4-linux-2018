<?php /* Template Name: TG4 Homepage */ ?>

<?php get_header(); ?>

<h1 class="visuallyhidden">TG4</h1>

<!-- <style>
#note {
    position: absolute;
    z-index: 6001;
    top: 0;
    left: 0;
    right: 0;
    background: #EEE;
    font-size: 1.4rem;
    text-align: center;
    line-height: 1.4;
    overflow: hidden; 
    -webkit-box-shadow: 0 0 5px black;
    -moz-box-shadow: 0 0 5px black;
    box-shadow: 0 0 5px black;
}

.cssanimations.csstransforms #note {
    -webkit-transform: translateY(-250px);
    -webkit-animation: slideDown 12s 0.5s 1 ease forwards;
    -moz-transform: translateY(-250px);
    -moz-animation: slideDown 12s 0.5s 1 ease forwards;
}

#close {
  cursor: pointer;
}

@-webkit-keyframes slideDown {
    0%, 100% { -webkit-transform: translateY(-250px); }
    10%, 90% { -webkit-transform: translateY(0px); }
}

@-moz-keyframes slideDown {
    0%, 100% { -moz-transform: translateY(-250px); }
    10%, 90% { -moz-transform: translateY(0px); }
}
</style> -->

<!-- <div id="note"><?php //echo (ICL_LANGUAGE_CODE == "ga" ? 'Beidh leagan amach suíomh tg4.ie á athrú go luath.<br />Tá an leagan nua á thástáil againn faoi láthair.<br />An mbeadh suim agat triail a bhaint as an dearadh nua?&nbsp;&nbsp;<a href="https://nua.tg4.ie"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/YES.png" alt="Yes Icon"></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a id="close"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/NO.png" alt="No Icon"></a>' : 'TG4.ie will upgrade soon to a newly designed Online Player.<br />The new player is available as a Beta Version (test version) at the moment.<br />Would you like to test the new design?&nbsp;&nbsp;<a href="https://nua.tg4.ie"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/YES.png" alt="Yes Icon"></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a id="close"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/NO.png" alt="No Icon"></a>'); ?></div> -->

<?php include(locate_template('/includes/home-slider.php')); ?>

<a name="inniu"></a>
<section class="section-panel-blue">
    <div class="title-tab-wrap">
        <h2 class="title-tab-blue"><a href="#inniu" style="color:white"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Inniu ar TG4' : 'Today on TG4'); ?></a></h2>
    </div>
    <?php include(locate_template('/includes/tonight-slider.php')); ?>
    <a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/sceideal/?dTime='.time() : '/en/irish-tv-schedule/?dTime='.time()); ?>" class="view-full"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Sceideal Iomlán' : 'View full schedule'); ?></a>
</section>

<section class="home-mods">
    <h2 class="visuallyhidden">More on TG4</h2>
    <div class="mods-wrapper">
        <div class="home-mod-wrap">
            <section class="mod-1">
                <div class="mod-livesport">
                    <div class="livesport-item-wrap">
                        <div class="livesport-head"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Spórt Beo ar TG4' : 'Live Sport on TG4'); ?></div>
                        <?php
                        if (ICL_LANGUAGE_CODE=="ga") {
                            $post_id = 4413;
                        } else {
                            $post_id = 4412;
                        }
                        if(get_field('tg4_sport', $post_id)) {
                            while(the_repeater_field('tg4_sport', $post_id))
                            {
                                echo "<div class='livesport-content-wrap'>";
                                echo "<div class='livesport-img'><span class='live-" . get_sub_field('sport_icon') . "'></span></div>";
                                echo "<div class='livesport-content'>";
                                echo "<p class='livesport-fixture'>" . get_sub_field('sport_title') . "</p>";
                                echo "<p class='livesport-date'>" . get_sub_field('sport_day') . "</p>";
                                echo "</div>";
                                echo "</div>";
                           }
                        } ?>
                    </div>
                </div>
            </section>
            <section class="mod-2">
                <div class="mod-notice">
                    <?php
                    if (ICL_LANGUAGE_CODE == "ga") {
                        $posts = get_posts(array(
                        'numberposts' => -1,
                        'post_type' => 'Page',
                        'meta_key' => 'mini_feature',
                        'meta_value' => '1'
                        ));
                    } else {
                        $posts = get_posts(array(
                        'numberposts' => -1,
                        'post_type' => 'Page',
                        'meta_key' => 'mini_feature',
                        'meta_value' => 'yes'
                        ));
                    }

                    if($posts)
                    { ?>
                    <section class="mini-slider-wrapper">
                        <div id="mini-width-slider" class="royalminiSlider rsMinW">
                        <?php foreach($posts as $post)
                        { ?>
                            <section class="rsContent">
                                <div class="blockContainer">
                                    <h2 class="mini-title-block rsABlock" data-fade-effect="true" data-move-effect="left" data-move-offset="50" data-speed="400" data-easing="easeOutSine"><?php echo get_field("mini_feature_title") ?></h2>
                                    <!-- Remove because Slider hardcodes style that will not work on mobile...
                                    <div class="mini-content-block rsABlock" data-fade-effect="true" data-move-effect="left" data-move-offset="50" data-speed="400" data-easing="easeOutSine">
                                        <h3 class="mini-content-subtitle"><?php //echo get_field("mini_feature_sub_title") ?></h3>
                                    </div>
                                    -->
                                    <?php if (ICL_LANGUAGE_CODE == "ga") { ?>
                                        <div class="rsABlock" data-fade-effect="true" data-move-effect="left" data-move-offset="50" data-speed="400" data-easing="easeOutSine">
                                            <a href="<?php echo get_permalink($post->ID); ?>" class="btn-header">Féach Leat...<span></span></a>
                                        </div>
                                    <?php } else { ?>
                                        <div class="rsABlock" data-fade-effect="true" data-move-effect="left" data-move-offset="50" data-speed="400" data-easing="easeOutSine">
                                            <a href="<?php echo get_permalink($post->ID); ?>" class="btn-header">See More...<span></span></a>
                                        </div>
                                    <?php } ?>
                                </div>
                                <img class="rsImg" src="<?php echo(get_field("mini_feature_image"));?>" alt="<?php echo get_field("mini_feature_title") ?>" title="<?php echo get_field("mini_feature_title") ?>" width="582" height="360">
                            </section>
                        <?php } ?>
                        </div>
                    </section>
                    <?php } ?>
                </div>
            </section>
        </div>
        <section class="mod-ad">
            <h3 class="mod-ad-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Fógraíocht' : 'Advertisement'); ?></h3>
            <div class="ad-wrapper">
                <script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
                <script>
                  var googletag = googletag || {};
                  googletag.cmd = googletag.cmd || [];
                </script>

                <script>
                  googletag.cmd.push(function() {
                    googletag.defineSlot('/172054193/MPU//Side1234', [300, 250], 'div-gpt-ad-1484321672116-0').addService(googletag.pubads());
                    googletag.pubads().enableSingleRequest();
                    googletag.enableServices();
                  });
                </script>

                <!-- /172054193/MPU//Side1234 -->
                <div id='div-gpt-ad-1484321672116-0' style='height:250px; width:300px;'>
                    <script>
                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1484321672116-0'); });
                    </script>
                </div>
                <!--iframe src="<?php //echo get_template_directory_uri(); ?>/mpu-banner.htm" height="250" width="300" scrolling="no" frameborder="0"></iframe-->
            </div>
        </section>
    </div>
</section>

<section class="section-panel-dark online-section">
    <div class="title-tab-wrap">
        <h2 class="title-tab-dark"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Ar líne anois' : 'Online Now'); ?></h2>
    </div>
    <?php include(locate_template('/includes/online-now.php')); ?>
    <?php //include(locate_template('/includes/online-now-channel-2.php')); ?>
</section>

<!-- Panel don tOireachtais -->
<!--section class="section-panel-dark-2 featv-section">
    <div class="title-tab-wrap">
        <h2 class="title-tab-dark-2">An tOireachtas</h2>
    </div>
    <?php //include(locate_template('/includes/featured-oireachtas.php')); ?>
</section-->

<!-- Angular -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular-touch.min.js"></script>
<!-- Angular -->
<section class="section-panel-dark-3 featv-section">
    <div class="title-tab-wrap">
        <h2 class="title-tab-dark-3"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Físeáin (rogha reatha)' : 'Featured Videos'); ?></h2>
    </div>
    <?php include(locate_template('/includes/featured-videos-api.php')); ?>
</section>

<script>
    close = document.getElementById("close");
    close.addEventListener('click', function() {
        note = document.getElementById("note");
        note.style.display = 'none';
    }, false);
</script>

<?php 
if (ICL_LANGUAGE_CODE == "ga") { ?>
    <!--
    Start of DoubleClick Floodlight Tag: Please do not remove
    Activity name of this tag: TG4 Home
    URL of the webpage where the tag is expected to be placed: http://www.tg4.ie/ga/
    This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
    Creation Date: 11/15/2016
    -->
    <script type="text/javascript">
    var axel = Math.random() + "";
    var a = axel * 10000000000000;
    document.write('<iframe src="https://5474423.fls.doubleclick.net/activityi;src=5474423;type=1;cat=tg4ho0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
    </script>
    <noscript>
    <iframe src="https://5474423.fls.doubleclick.net/activityi;src=5474423;type=1;cat=tg4ho0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
    </noscript>
    <!-- End of DoubleClick Floodlight Tag: Please do not remove -->
<?php }
get_footer(); ?>