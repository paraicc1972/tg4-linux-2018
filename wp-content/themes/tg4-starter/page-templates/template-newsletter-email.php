<?php /* Template Name: Newsletter - Email */ ?>

<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Nuachtlitir TG4</title>                                                                                                                                               
<style type="text/css">
	.ReadMsgBody { width:100%; background-color:#838383; }
	.ExternalClass { width:100%; background-color:#838383; }
	body { width:100%; background-color:#838383; margin:0; padding:0; -webkit-font-smoothing:antialiased; font-family:Helvetica, Arial, sans-serif }
	table { border-collapse:collapse; }
	@media only screen and (max-width:720px)  {
		body[yahoo] .deviceWidth { width:440px!important; padding:0; }	
		body[yahoo] .center { text-align:center!important; }	 
	}
	@media only screen and (max-width:479px) {
		body[yahoo] .deviceWidth { width:280px!important; padding:0; }	
		body[yahoo] .center { text-align:center!important; }	 
	}
</style>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix" style="font-family: Helvetica, Arial, sans-serif">
<!-- Wrapper -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td width="100%" valign="top" bgcolor="#838383" style="padding-top:20px">
			<!-- Start Unsubscribe/Website Link -->
			<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">  
				<tr>
					<td valign="top" style="font-size: 13px; line-height:24px; color: #f1f1f1; color:#afafaf; font-family: Helvetica, Arial, sans-serif; padding-bottom:10px" class="center" align="center"><a href="<?php echo site_url(); ?>/newsletter" style="color:#afafaf;text-decoration:none;">M&aacute; t&aacute; deacracht agat leis an r-phost seo, t&eacute;igh anseo</a><br /><a href="<?php echo site_url(); ?>/newsletter" style="color:#afafaf;text-decoration:none;">If you are having trouble viewing the images in this email, go here.</a></td>
				</tr>
			</table>
			<!-- End Unsubscribe/Website Link -->

			<!-- Start Header-->
			<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">  
				<tr>
					<td valign="top" style="padding:0" bgcolor="#838383"><h1 style="font-size: 40px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:10px 15px 10px 15px" bgcolor="#009BCD">
						<?php
						$charIn = array("á", "é", "í", "ó", "ú", "Á", "É", "Í", "Ó", "Ú", "-", " - ", "’", "‘");
						$charOut = array("&aacute;", "&eacute;", "&iacute;", "&oacute;", "&uacute;", "&Aacute;", "&Eacute;", "&Iacute;", "&Oacute;", "&Uacute;", "-", " - ", "'", "'");
						echo str_replace($charIn, $charOut, get_the_title($post->ID));
						?>
					</h1></td>
					<td align="right" style="padding:10px 15px 10px 15px"><img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2016/04/TG4-nl-logo.png" alt="TG4 Logo" class="logo"></td>
				</tr> 
			</table>
			<!-- End Header -->
			<!-- One Column -->
			<?php
			if(have_rows('newsletter_content')) {
			    $row_num = 0;
			    while(have_rows('newsletter_content')): the_row(); 
			        if ($row_num == 0) {
			?><br />
						<table width="700" class="deviceWidth" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#838383">
							<tr>
								<td style="font-size: 24px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:10px 15px 10px 15px" bgcolor="#009BCD"><p style="mso-table-lspace:0;mso-table-rspace:0; margin:0"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_title')); ?></p></td>
							</tr>   
							<tr>
								<td valign="top" style="padding:0" bgcolor="#838383"><a href="<?php the_sub_field('newsletter_link'); ?>"><img class="deviceWidth" src="<?php the_sub_field('newsletter_image'); ?>" width="700" alt="" border="0" style="display: block;" /></a></td>
							</tr> 
							<tr>
								<td style="font-size: 13px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:10px 15px 10px 15px" bgcolor="#000000"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_time')); ?></td>
							</tr>   
							<tr>
								<td style="font-size: 13px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:0px 15px 0px 15px" bgcolor="#838383"><p><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_text_ie')); ?></p><p style="color: #c8f3f2;"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_text_en')); ?></p><?php if (get_sub_field("newsletter_video")) { ?><a href="<?php echo site_url() . '/popup.php?vid='; echo the_sub_field('newsletter_video'); ?>" target="New"  title="Newsletter Video" class="btn-nl-play"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/player-icon.png" border="0"></a><?php } ?></td>
							</tr>              
						</table>
			<?php   }
			        $row_num++;
			    endwhile; 
			} 
			?>
			<!-- End One Column -->
			<!-- Two Column (Images Stacked over Text) -->
			<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" bgcolor="#838383">
				<tr>
					<td class="center" style="padding:5px 0">
						<?php
						if(have_rows('newsletter_content')) {
						    $row_num = 0;
						    while(have_rows('newsletter_content')): the_row(); 
						        if ($row_num == 1) {
						?>
									<table width="49%" border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth" bgcolor="#838383">
										<tr>
											<td style="font-size: 16px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:10px 15px 10px 15px" bgcolor="#009BCD"><p style="mso-table-lspace:0;mso-table-rspace:0; margin:0"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_title')); ?></p></td>
										</tr> 
										<tr>
											<td align="center">
												<!-- The paragraph tag is important here to ensure that this table floats properly in Outlook 2007, 2010, and 2013
												To learn more about this fix check out this link: http://www.emailonacid.com/blog/details/C13/removing_unwanted_spacing_or_gaps_between_tables_in_outlook_2007_2010
												This fix is used for all floating tables in this responsive template
												The margin set to 0 is for Gmail -->
												<p style="mso-table-lspace:0;mso-table-rspace:0; margin:0"><a href="<?php the_sub_field('newsletter_link'); ?>"><img width="343" src="<?php the_sub_field('newsletter_image'); ?>" alt="" border="0" style="width: 343px" class="deviceWidth" /></a></p>
											</td>
										</tr>
										<tr>
											<td style="font-size: 13px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:10px 15px 10px 15px" bgcolor="#000000"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_time')); ?></td>
										</tr>  
										<tr>
											<td style="font-size: 12px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:0px 15px 0px 15px" bgcolor="#838383"><p><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_text_ie')); ?></p><p style="color: #c8f3f2;"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_text_en')); ?></p><?php if (get_sub_field("newsletter_video")) { ?><a href="<?php echo site_url() . '/popup.php?vid='; echo the_sub_field('newsletter_video'); ?>" target="New"  title="Newsletter Video" class="btn-nl-play"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/player-icon.png" border="0"></a><?php } ?></td>
										</tr>                         
									</table>
						<?php   }
						        $row_num++;
						    endwhile; 
						}

						if(have_rows('newsletter_content')) {
						    $row_num = 0;
						    while(have_rows('newsletter_content')): the_row(); 
						        if ($row_num == 2) {
						?>
									<table width="49%" border="0" cellpadding="0" cellspacing="0" align="right" class="deviceWidth" bgcolor="#838383">
										<tr>
											<td style="font-size: 16px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:10px 15px 10px 15px" bgcolor="#009BCD"><p style="mso-table-lspace:0;mso-table-rspace:0; margin:0"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_title')); ?></p></td>
										</tr> 
										<tr>
											<td align="center">
												<!-- The paragraph tag is important here to ensure that this table floats properly in Outlook 2007, 2010, and 2013
												To learn more about this fix check out this link: http://www.emailonacid.com/blog/details/C13/removing_unwanted_spacing_or_gaps_between_tables_in_outlook_2007_2010
												This fix is used for all floating tables in this responsive template
												The margin set to 0 is for Gmail -->
												<p style="mso-table-lspace:0;mso-table-rspace:0; margin:0"><a href="<?php the_sub_field('newsletter_link'); ?>"><img width="343" src="<?php the_sub_field('newsletter_image'); ?>" alt="" border="0" style="width: 343px" class="deviceWidth" /></a></p>
											</td>
										</tr>
										<tr>
											<td style="font-size: 13px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:10px 15px 10px 15px" bgcolor="#000000"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_time')); ?></td>
										</tr>  
										<tr>
											<td style="font-size: 12px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:0px 15px 0px 15px" bgcolor="#838383"><p><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_text_ie')); ?></p><p style="color: #c8f3f2;"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_text_en')); ?></p><?php if (get_sub_field("newsletter_video")) { ?><a href="<?php echo site_url() . '/popup.php?vid='; echo the_sub_field('newsletter_video'); ?>" target="New"  title="Newsletter Video" class="btn-nl-play"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/player-icon.png" border="0"></a><?php } ?></td>
										</tr>                         
									</table>
						<?php   }
						        $row_num++;
						    endwhile; 
						} 
						?>                        
					</td>
				</tr>
			</table>
			<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" bgcolor="#838383">
				<tr>
					<td class="center" style="padding:5px 0">
						<?php
						if(have_rows('newsletter_content')) {
						    $row_num = 0;
						    while(have_rows('newsletter_content')): the_row(); 
						        if ($row_num == 3) {
						?>
									<table width="49%" border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth" bgcolor="#838383">
										<tr>
											<td style="font-size: 16px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:10px 15px 10px 15px" bgcolor="#009BCD"><p style="mso-table-lspace:0;mso-table-rspace:0; margin:0"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_title')); ?></p></td>
										</tr> 
										<tr>
											<td align="center">
												<!-- The paragraph tag is important here to ensure that this table floats properly in Outlook 2007, 2010, and 2013
												To learn more about this fix check out this link: http://www.emailonacid.com/blog/details/C13/removing_unwanted_spacing_or_gaps_between_tables_in_outlook_2007_2010
												This fix is used for all floating tables in this responsive template
												The margin set to 0 is for Gmail -->
												<p style="mso-table-lspace:0;mso-table-rspace:0; margin:0"><a href="<?php the_sub_field('newsletter_link'); ?>"><img width="343" src="<?php the_sub_field('newsletter_image'); ?>" alt="" border="0" style="width: 343px" class="deviceWidth" /></a></p>
											</td>
										</tr>
										<tr>
											<td style="font-size: 13px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:10px 15px 10px 15px" bgcolor="#000000"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_time')); ?></td>
										</tr>  
										<tr>
											<td style="font-size: 12px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:0px 15px 0px 15px" bgcolor="#838383"><p><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_text_ie')); ?></p><p style="color: #c8f3f2;"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_text_en')); ?></p><?php if (get_sub_field("newsletter_video")) { ?><a href="<?php echo site_url() . '/popup.php?vid='; echo the_sub_field('newsletter_video'); ?>" target="New"  title="Newsletter Video" class="btn-nl-play"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/player-icon.png" border="0"></a><?php } ?></td>
										</tr>                         
									</table>
						<?php   }
						        $row_num++;
						    endwhile; 
						}

						if(have_rows('newsletter_content')) {
						    $row_num = 0;
						    while(have_rows('newsletter_content')): the_row(); 
						        if ($row_num == 4) {
						?>
									<table width="49%" border="0" cellpadding="0" cellspacing="0" align="right" class="deviceWidth" bgcolor="#838383">
										<tr>
											<td style="font-size: 16px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:10px 15px 10px 15px" bgcolor="#009BCD"><p style="mso-table-lspace:0;mso-table-rspace:0; margin:0"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_title')); ?></p></td>
										</tr> 
										<tr>
											<td align="center">
												<!-- The paragraph tag is important here to ensure that this table floats properly in Outlook 2007, 2010, and 2013
												To learn more about this fix check out this link: http://www.emailonacid.com/blog/details/C13/removing_unwanted_spacing_or_gaps_between_tables_in_outlook_2007_2010
												This fix is used for all floating tables in this responsive template
												The margin set to 0 is for Gmail -->
												<p style="mso-table-lspace:0;mso-table-rspace:0; margin:0"><a href="<?php the_sub_field('newsletter_link'); ?>"><img width="343" src="<?php the_sub_field('newsletter_image'); ?>" alt="" border="0" style="width: 343px" class="deviceWidth" /></a></p>
											</td>
										</tr>
										<tr>
											<td style="font-size: 13px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:10px 15px 10px 15px" bgcolor="#000000"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_time')); ?></td>
										</tr>  
										<tr>
											<td style="font-size: 12px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:0px 15px 0px 15px" bgcolor="#838383"><p><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_text_ie')); ?></p><p style="color: #c8f3f2;"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_text_en')); ?></p><?php if (get_sub_field("newsletter_video")) { ?><a href="<?php echo site_url() . '/popup.php?vid='; echo the_sub_field('newsletter_video'); ?>" target="New"  title="Newsletter Video" class="btn-nl-play"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/player-icon.png" border="0"></a><?php } ?></td>
										</tr>                         
									</table>
						<?php   }
						        $row_num++;
						    endwhile; 
						} 
						?>                          
					</td>
				</tr>
			</table>
			<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" bgcolor="#838383">
				<tr>
					<td class="center" style="padding:5px 0">
						<?php
						if(have_rows('newsletter_content')) {
						    $row_num = 0;
						    while(have_rows('newsletter_content')): the_row(); 
						        if ($row_num == 5) {
						?>
									<table width="49%" border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth" bgcolor="#838383">
										<tr>
											<td style="font-size: 16px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:10px 15px 10px 15px" bgcolor="#009BCD"><p style="mso-table-lspace:0;mso-table-rspace:0; margin:0"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_title')); ?></p></td>
										</tr> 
										<tr>
											<td align="center">
												<!-- The paragraph tag is important here to ensure that this table floats properly in Outlook 2007, 2010, and 2013
												To learn more about this fix check out this link: http://www.emailonacid.com/blog/details/C13/removing_unwanted_spacing_or_gaps_between_tables_in_outlook_2007_2010
												This fix is used for all floating tables in this responsive template
												The margin set to 0 is for Gmail -->
												<p style="mso-table-lspace:0;mso-table-rspace:0; margin:0"><a href="<?php the_sub_field('newsletter_link'); ?>"><img width="343" src="<?php the_sub_field('newsletter_image'); ?>" alt="" border="0" style="width: 343px" class="deviceWidth" /></a></p>
											</td>
										</tr>
										<tr>
											<td style="font-size: 13px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:10px 15px 10px 15px" bgcolor="#000000"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_time')); ?></td>
										</tr>  
										<tr>
											<td style="font-size: 12px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:0px 15px 0px 15px" bgcolor="#838383"><p><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_text_ie')); ?></p><p style="color: #c8f3f2;"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_text_en')); ?></p><?php if (get_sub_field("newsletter_video")) { ?><a href="<?php echo site_url() . '/popup.php?vid='; echo the_sub_field('newsletter_video'); ?>" target="New"  title="Newsletter Video" class="btn-nl-play"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/player-icon.png" border="0"></a><?php } ?></td>
										</tr>                         
									</table>
						<?php   }
						        $row_num++;
						    endwhile; 
						}

						if(have_rows('newsletter_content')) {
						    $row_num = 0;
						    while(have_rows('newsletter_content')): the_row(); 
						        if ($row_num == 6) {
						?>
									<table width="49%" border="0" cellpadding="0" cellspacing="0" align="right" class="deviceWidth" bgcolor="#838383">
										<tr>
											<td style="font-size: 16px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:10px 15px 10px 15px" bgcolor="#009BCD"><p style="mso-table-lspace:0;mso-table-rspace:0; margin:0"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_title')); ?></p></td>
										</tr> 
										<tr>
											<td align="center">
												<!-- The paragraph tag is important here to ensure that this table floats properly in Outlook 2007, 2010, and 2013
												To learn more about this fix check out this link: http://www.emailonacid.com/blog/details/C13/removing_unwanted_spacing_or_gaps_between_tables_in_outlook_2007_2010
												This fix is used for all floating tables in this responsive template
												The margin set to 0 is for Gmail -->
												<p style="mso-table-lspace:0;mso-table-rspace:0; margin:0"><a href="<?php the_sub_field('newsletter_link'); ?>"><img width="343" src="<?php the_sub_field('newsletter_image'); ?>" alt="" border="0" style="width: 343px" class="deviceWidth" /></a></p>
											</td>
										</tr>
										<tr>
											<td style="font-size: 13px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:10px 15px 10px 15px" bgcolor="#000000"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_time')); ?></td>
										</tr>  
										<tr>
											<td style="font-size: 12px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:0px 15px 0px 15px" bgcolor="#838383"><p><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_text_ie')); ?></p><p style="color: #c8f3f2;"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_text_en')); ?></p><?php if (get_sub_field("newsletter_video")) { ?><a href="<?php echo site_url() . '/popup.php?vid='; echo the_sub_field('newsletter_video'); ?>" target="New"  title="Newsletter Video" class="btn-nl-play"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/player-icon.png" border="0"></a><?php } ?></td>
										</tr>                         
									</table>
						<?php   }
						        $row_num++;
						    endwhile; 
						} 
						?>                
					</td>
				</tr>
			</table>
			<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" bgcolor="#838383">
				<tr>
					<td class="center" style="padding:5px 0">
          				<?php
						if(have_rows('newsletter_content')) {
						    $row_num = 0;
						    while(have_rows('newsletter_content')): the_row(); 
						        if ($row_num == 7) {
						?>
									<table width="49%" border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth" bgcolor="#838383">
										<tr>
											<td style="font-size: 16px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:10px 15px 10px 15px" bgcolor="#009BCD"><p style="mso-table-lspace:0;mso-table-rspace:0; margin:0"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_title')); ?></p></td>
										</tr> 
										<tr>
											<td align="center">
												<!-- The paragraph tag is important here to ensure that this table floats properly in Outlook 2007, 2010, and 2013
												To learn more about this fix check out this link: http://www.emailonacid.com/blog/details/C13/removing_unwanted_spacing_or_gaps_between_tables_in_outlook_2007_2010
												This fix is used for all floating tables in this responsive template
												The margin set to 0 is for Gmail -->
												<p style="mso-table-lspace:0;mso-table-rspace:0; margin:0"><a href="<?php the_sub_field('newsletter_link'); ?>"><img width="343" src="<?php the_sub_field('newsletter_image'); ?>" alt="" border="0" style="width: 343px" class="deviceWidth" /></a></p>
											</td>
										</tr>
										<tr>
											<td style="font-size: 13px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:10px 15px 10px 15px" bgcolor="#000000"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_time')); ?></td>
										</tr>  
										<tr>
											<td style="font-size: 12px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:0px 15px 0px 15px" bgcolor="#838383"><p><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_text_ie')); ?></p><p style="color: #c8f3f2;"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_text_en')); ?></p><?php if (get_sub_field("newsletter_video")) { ?><a href="<?php echo site_url() . '/popup.php?vid='; echo the_sub_field('newsletter_video'); ?>" target="New"  title="Newsletter Video" class="btn-nl-play"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/player-icon.png" border="0"></a><?php } ?></td>
										</tr>                         
									</table>
						<?php   }
						        $row_num++;
						    endwhile; 
						}

						if(have_rows('newsletter_content')) {
						    $row_num = 0;
						    while(have_rows('newsletter_content')): the_row(); 
						        if ($row_num == 8) {
						?>
									<table width="49%" border="0" cellpadding="0" cellspacing="0" align="right" class="deviceWidth" bgcolor="#838383">
										<tr>
											<td style="font-size: 16px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:10px 15px 10px 15px" bgcolor="#009BCD"><p style="mso-table-lspace:0;mso-table-rspace:0; margin:0"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_title')); ?></p></td>
										</tr> 
										<tr>
											<td align="center">
												<!-- The paragraph tag is important here to ensure that this table floats properly in Outlook 2007, 2010, and 2013
												To learn more about this fix check out this link: http://www.emailonacid.com/blog/details/C13/removing_unwanted_spacing_or_gaps_between_tables_in_outlook_2007_2010
												This fix is used for all floating tables in this responsive template
												The margin set to 0 is for Gmail -->
												<p style="mso-table-lspace:0;mso-table-rspace:0; margin:0"><a href="<?php the_sub_field('newsletter_link'); ?>"><img width="343" src="<?php the_sub_field('newsletter_image'); ?>" alt="" border="0" style="width: 343px" class="deviceWidth" /></a></p>
											</td>
										</tr>
										<tr>
											<td style="font-size: 13px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:10px 15px 10px 15px" bgcolor="#000000"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_time')); ?></td>
										</tr>  
										<tr>
											<td style="font-size: 12px; color: #ffffff; font-weight: normal; text-align: left; font-family: Helvetica, Arial, sans-serif; line-height: 24px; vertical-align: top; padding:0px 15px 0px 15px" bgcolor="#838383"><p><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_text_ie')); ?></p><p style="color: #c8f3f2;"><?php echo str_replace($charIn, $charOut, get_sub_field('newsletter_text_en')); ?></p><?php if (get_sub_field("newsletter_video")) { ?><a href="<?php echo site_url() . '/popup.php?vid='; echo the_sub_field('newsletter_video'); ?>" target="New"  title="Newsletter Video" class="btn-nl-play"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/player-icon.png" border="0"></a><?php } ?></td>
										</tr>                         
									</table>
						<?php   }
						        $row_num++;
						    endwhile; 
						} 
						?>                               
					</td>
				</tr>
			</table>
			<!-- End Two Column (Images Stacked over Text) -->
			<!-- Footer -->
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td bgcolor="#363636" style="padding:30px 0">
						<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
							<tr>
								<td>    
                					<table width="60%" cellpadding="0" cellspacing="0"  border="0" align="right" class="deviceWidth">
                						<tr>
                							<td valign="top" style="font-size: 12px; color: #f1f1f1; font-weight: normal; font-family: Helvetica, Arial, sans-serif; line-height: 26px; vertical-align: top; text-align:right" class="center">
											<!-- <a href="http://www.tg4.tv"><img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2016/04/player-nl.png" alt="TG4 Player" title="Tg4 Player" border="0" /></a>&nbsp;&nbsp; -->
              								<a href="https://www.facebook.com/TG4TV"><img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2016/04/facebook-nl.png" width="42" height="42" alt="Facebook" title="Facebook" border="0" /></a>&nbsp;&nbsp;
        									<a href="https://twitter.com/TG4TV"><img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2016/04/twitter-nl.png" width="42" height="42" alt="Twitter" title="Twitter" border="0" /></a>&nbsp;&nbsp;
                  							<a href="https://www.youtube.com/TG4"><img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2016/04/youtube-nl.png" width="42" height="42" alt="Youtube" title="Youtube" border="0" /></a>&nbsp;&nbsp;
              								<a href="https://www.flickr.com/photos/tg4"><img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2016/04/flickr-nl.png" width="42" height="42" alt="Flickr" title="Flickr" border="0" /></a>&nbsp;&nbsp;
              								<a href="https://www.instagram.com/tg4tv"><img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2016/04/instagram-nl.png" width="42" height="42" alt="Instagram" title="Instagram" border="0" /></a>&nbsp;&nbsp;
              								<a href="https://www.snapchat.com/add/tg4tv"><img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2016/04/snapchat-nl.png" width="42" height="42" alt="Snapchat" title="Snapchat" border="0" /></a><br />
          									<p>Cain&eacute;al 4 Saorview; Cain&eacute;al 104 Sky<br />Cain&eacute;al 104 UPC; 137 HD ar UPC, 602 C&uacute;la4 ar UPC<br />Tuaisceart &Eacute;ireann: 163 (Sky); 877 (Virgin Cable); 51 (Freeview)<br />Ar fud na Cruinne: www.tg4.tv</p>
         									</td>
                    					</tr>
                					</table>
								</td>
							</tr>
						</table>
						<!-- Start Unsubscribe/Website Link -->
						<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">  
							<tr>
								<td><br /> 
                					<table width="60%" cellpadding="0" cellspacing="0"  border="0" align="right" class="deviceWidth">
                						<tr>
											<td valign="top" style="font-size: 13px; line-height:24px; color: #f1f1f1; color:#afafaf; font-family: Helvetica, Arial, sans-serif; padding-bottom:10px" align="right"><a href="mailto:webmaster@tg4.ie?subject=Unsubscribe from TG4 Newsletter" style="color:#afafaf;text-decoration:none;">D&iacute;chl&aacute;raigh</a> | <a href="mailto:webmaster@tg4.ie?subject=Unsubscribe from TG4 Newsletter" style="color:#afafaf;text-decoration:none;">Unsubscribe</a></td>
                    					</tr>
                					</table>
								</td>
							</tr>
						</table>
						<!-- End Unsubscribe/Website Link -->                                       		
					</td>
				</tr>
			</table>		
			<!-- End Footer -->	                     
		</td>
	</tr>
</table>
<!-- End Wrapper -->
</body>
</html>