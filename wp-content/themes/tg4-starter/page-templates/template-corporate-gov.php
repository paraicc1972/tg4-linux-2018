<?php /* Template Name: Corporate Governance */ ?>

<?php get_header(); ?>

<div class="section-header">
	<h1 class="section-title">
		<?php echo get_the_title($post->post_parent); ?>
	</h1>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
	<div class="section-submenu-wrap">
		<ul class="section-submenu-list">
			<?php 
			//echo $post->ID . " - " . $post->post_parent;
    		if ($post->post_parent != 137 && $post->post_parent != 140) {
				wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=1');
			} elseif ($post->ID != 137 && $post->ID != 140) {
				wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->ID. '&depth=1');
			}
			?>
			&nbsp;
		</ul>
	</div>
</div>

<?php
$feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));

if ($feat_image != '') {
	if (get_field("centre_feature_image") == '0' ||  get_field("centre_feature_image") == 'no') {
		$feat_Class = "prog-head";
	} elseif (get_field("centre_feature_image") == '1' ||  get_field("centre_feature_image") == 'yes')  {
		$feat_Class = "prog-headcenter";
	} else {
		$feat_Class = "prog-head";
	}
?>
	<section class="<?php echo $feat_Class; ?>" style="background-image: url(<?php echo $feat_image; ?>);">
		<div class="content">
	        <?php 
	        if (get_field("intro_text")) { ?>
			<div class="content-block">
	            <h2 class="content-subtitle"><?php echo get_field("intro_title"); ?></h2>
	            <p><?php echo get_field("intro_sub_title") ?></p>
	        </div>
	        <?php } ?>
		</div>
	</section>
<?php } ?>

<section class="governance-mods">
	<h2 class="visuallyhidden">More on TG4</h2>
	<div class="governancemods-wrapper">
		<div class="prog-feat-wrap"><?php echo apply_filters('the_content', get_post_field('post_content', $post_id)); ?></div>
		<?php if(have_rows('another_file_name')): ?>
			<div class="prog-feat-wrap"><h2><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Tuairisc' : 'Reports'); ?></h2></div>
			<div class="governance-mod-wrap">
			<?php while(have_rows('another_file_name')): the_row();
				if (get_sub_field('file_title')) { ?>
					<section class="mod-1">
		                <a href="<?php the_sub_field('upload_file') ?>" target="_blank">
		                <div class="governance-mod-notice">
		                	<div class="file-btn"><a href="<?php the_sub_field('upload_file') ?>" class="mod-file" target="_blank"><div class="file-btnImg"></div></a></div>
		                    <div class="file-title"><a href="<?php the_sub_field('upload_file') ?>" class="governance-title" target="_blank"><?php the_sub_field('file_title'); ?></a></div>
		                </div>
		                </a> 
		            </section>
			<?php } endwhile; ?>
			</div>
		<?php endif; 
		if(have_rows('file_name')): ?>
		<div class="prog-feat-wrap"><h2><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Cóid & Ráitis' : 'Codes &amp; Statements'); ?></h2></div>
		<div class="governance-mod-wrap">
		<?php while(have_rows('file_name')): the_row();
			if (get_sub_field('file_title')) { ?>
				<section class="mod-1">
	                <a href="<?php the_sub_field('upload_file') ?>" target="_blank">
	                <div class="governance-mod-notice">
	                	<div class="file-btn"><a href="<?php the_sub_field('upload_file') ?>" class="mod-file" target="_blank"><div class="file-btnImg"></div></a></div>
	                    <div class="file-title"><a href="<?php the_sub_field('upload_file') ?>" class="governance-title" target="_blank"><?php the_sub_field('file_title'); ?></a></div>
	                </div>
	                </a> 
	            </section>
		<?php } endwhile; ?>
		</div>
		<?php endif; ?>
	</div>
</section>

<?php get_footer(); ?>