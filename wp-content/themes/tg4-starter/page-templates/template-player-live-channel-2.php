<?php /* Template Name: Live Player - Channel 2 */ ?>

<?php get_header();  

$clientIP = $_SERVER['HTTP_X_FORWARDED_FOR']?: $_SERVER['HTTP_CLIENT_IP']?: $_SERVER['REMOTE_ADDR'];
//echo "** " . $clientIP;
if ($clientIP == "::1") {
    $clientIP = "77.75.98.34"; //IE
    //$clientIP = "194.32.31.1"; //NIR
    //$clientIP = "94.0.69.150"; //NIR
    //$clientIP = "185.86.151.11"; //GB
    //$clientIP = "217.91.35.16"; //GER
    //$clientIP = "176.164.135.240"; //FRA
}

$request = "https://www.tg4tech.com/api/programmes/countryCode?id=" . $clientIP;
$ch = curl_init($request);
curl_setopt_array($ch, array(
        CURLOPT_POST           => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_SSL_VERIFYPEER => FALSE,
        CURLOPT_HTTPHEADER     => array('Content-type: application/x-www-form-urlencoded'),
    ));
$response = curl_exec($ch);

// Check for errors
if ($response === FALSE) {
    die(curl_error($ch));
}
curl_close($ch);

$responseData = json_decode($response, TRUE);
$geoCode = $responseData["country"];

//echo "* " . $geoCode;
?>

<script type="text/javascript">
    if (window.innerWidth <= 835) { 
        playerW=window.innerWidth-58;
        playerH=playerW/1.777;
    } else {
        playerW=835;
        playerH=470;
    }

    <?php if ($geoCode == 'IE') { ?>
        // Open Stream
        url = 'http://csm-e.cds1.yospace.com/csm/live/107978697.m3u8'; // Channel 2 Open Stream
    <?php } else { ?>
        // GeoBlocked Stream
        //url = 'http://csm-e.cds1.yospace.com/csm/live/107978697.m3u8'; // Channel 2 Open Stream
        //url = 'http://csm-e.cds1.yospace.com/csm/live/96741527.m3u8'; // Channel 2 GeoBlocked Stream
        url = 'http://csm-e.cds1.yospace.com/csm/live/74246610.m3u8'; // TG4 International Stream
    <?php } ?>

    console.log(url);
</script>

<!-- <script type="application/javascript" src="http://freegeoip.net/json/?callback=getgeoip"></script>
<script type="application/javascript" src="http://www.telize.com/geoip?callback=getgeoip"></script-->
<script type="text/javascript" src="https://d1og0s8nlbd0hm.cloudfront.net/yospace/yoplayer-4.1-32.js"></script>

<div class="section-header">
    <h1 class="section-title">
        <?php
        if (get_the_title($post->post_parent) == "Programmes" || get_the_title($post->post_parent) == "Cláir" || get_the_title($post->post_parent) == "Sport" || get_the_title($post->post_parent) == "Spórt") {
            echo get_the_title($post->ID);
        } else {
            echo get_the_title($post->post_parent);
        }
        ?>
    </h1>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
    <div class="section-submenu-wrap">
        <ul class="section-submenu-list">
            <?php wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=1&exclude=28146,28148'); ?>
            &nbsp;
        </ul>
    </div>
</div>

<section class="plyr-feat-section">
    <div class="section-panel-dark-3">
        <div class="plyr-feat center-panel">
            <!-- Cuir an Fógráiocht Leis an Seinnteoir Beo! -->
            <div class="prog-feat-ad mod-ad">
                <h2 class="mod-ad-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Fógraíocht' : 'Advertisement'); ?></h2>
                <div class="ad-wrapper">
                    <iframe src="https://www.tg4.ie/wp-content/themes/tg4-starter/mpu-banner.htm" height="250" width="300" scrolling="no" frameborder="0"></iframe>
                    <!--iframe src="<?php //echo site_url() . '/wp-content/themes/tg4-starter/'; ?>mpu-banner.htm" height="250" width="300" scrolling="no" frameborder="0"></iframe-->
                </div>
            </div>
            
            <div id="player" class="plyr-feat-wrap">
                <script type="application/javascript">
                    function playerSize() {
                        //console.log('Window',window.innerWidth);
                        var viewportwidth;
                        var viewportheight;
                  
                        // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
                        if (typeof window.innerWidth != 'undefined')
                        {
                            viewportwidth = window.innerWidth,
                            viewportheight = window.innerHeight
                        }
                
                        if (viewportwidth <= 935) { 
                            playerW=viewportwidth-58;
                            playerH=playerW/1.777;
                            resize(playerW, playerH);
                        } else {
                            playerW=835;
                            playerH=570;
                        }
                        resize(playerW, playerH);
                    }
                </script>

                <!-- Yospace Player -->
                <script type="text/javascript">
                    $YOPLAYER('player', {
                        player: 'https://d1og0s8nlbd0hm.cloudfront.net/yospace/yoplayer-4.1-32.swf',
                        height: playerH,
                        width: playerW,
                        buffer: 30,
                        lwm: 5,
                        lss: 5,
                        debug: false,
                        skin: "https://d1og0s8nlbd0hm.cloudfront.net/yospace/skin2.skin",
                        autostart: true,
                        type: 'hls',
                        adverttext: 'If you like this, why not click to find out more...',
                        file: url,
                        callbacks: {
                            logging: function(obj, msg) {
                                displayMessage(msg);
                            },
                            position: function(id, pos) {
                                playPosition = pos;
                            },
                            advertStart: function(id, data) {
                                displayMessage("AD Start Event Fired at " + data);
                            },
                            advertEnd: function(id, data) {
                                displayMessage("AD End Event Fired at " + data);
                            },
                            advertBreakStart: function(id, pos) {
                                displayMessage("AD Break Start Event Fired at " + pos);
                            },
                            advertBreakEnd: function(id, pos) {
                                displayMessage("AD Break End Event Fired at " + pos);
                            }
                        }
                    });
                    function play(url, stream) {
                        $YOFIND('player').play(url);
                    }
                    function resize(playerW, playerH) {
                        $YOFIND('player').resize(playerW,playerH);
                    }
                    function displayMessage(msg) {
                    }
                </script>
            </div>
        </div>
    </div>
    <div class="plyr-feat-intro-wrap">
        <div class="center-panel">
            <div class="prog-feat-intro">
                <div class="prog-feat-toolbar">
                    <div class="prog-remind">
                        <div class="prog-remind-txt"><?php echo (ICL_LANGUAGE_CODE == "ga" ? "TG4 Beo | Cainéal 2 " : "TG4 Live | Channel 2 "); ?></div>
                    </div> 
                    <div class="prog-share">
                        <a href="" class="facebook-share">Facebook</a>
                        <a href="" class="twitter-share">Twitter</a>
                        <a href="" class="email-share">Email</a>
                    </div>
                </div>
                <?php if (get_post_field("post_content")) { ?>
                    <div class="c2-feat-txt">
                        <?php echo apply_filters('the_content', get_post_field('post_content', $post_id)); ?>
                    </div>
                <?php } ?>
            </div>
            <?php if(have_rows('image_name')) { ?>
            <div class="prog-feat-ad mod-ad">
                <br />
                <!-- Uploaded Image to Appear here -->
                <div>
                    <?php
                    while(have_rows('image_name')): the_row();
                        $sideImage = get_sub_field('upload_image');
                    ?>
                        <image src="<?php echo $sideImage['url']; ?>">
                    <?php endwhile; ?>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</section>

<!--section class="section-panel-blue">
    <div class="title-tab-wrap">
        <h2 class="title-tab-blue"><?php //echo (ICL_LANGUAGE_CODE == "ga" ? 'Inniu ar TG4' : 'Today on TG4'); ?></h2>
        <!-- Maybe this needs retitled, e.g. Coming up on TG4. Should maybe show a feed of the next 24 hours of shows. -->
    <!--/div>
    <?php //get_template_part('/includes/tonight-slider'); ?>
    <a href="<?php //echo $_SERVER["REQUEST_URI"]; ?>schedule" class="view-full"><?php //echo (ICL_LANGUAGE_CODE == "ga" ? 'Sceideal Iomlán' : 'View full schedule'); ?></a>
</section-->


<!-- Angular 
<script src= "https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular-touch.min.js"></script>
<!-- Angular 
<section class="section-panel-dark-2">
    <div class="title-tab-wrap">
        <h2 class="title-tab-dark-2"><?php //echo (ICL_LANGUAGE_CODE == "ga" ? 'Físeáin ar Fáil' : 'Available Videos'); ?></h2>
    </div>
    <?php 
        //$categoryTag = get_field("category_tag");
        //$seriesTag = get_field("series_tag");
        //include(locate_template('/includes/featured-videos-api.php')); 
    ?>
</section-->

<?php get_footer(); ?>