<?php /* Template Name: Traenáil & Forbairt */ ?>

<?php get_header(); ?>

<div class="section-header">
	<h1 class="section-title">
		<?php echo get_the_title($post->post_parent); ?>
	</h1>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
    <div class="section-submenu-wrap">
        <ul class="section-submenu-list">
            <?php wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=1'); ?>
            &nbsp;
        </ul>
    </div>
</div>

<section class="player-mods">
	<h2 class="visuallyhidden">More on TG4</h2>
	<div class="pressmods-wrapper">
		<div class="press-mod-wrap">
			<section class="mod-1">
			    <div class="player-mod-notice">
			        <a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/traenail-forbairt/sceim-forbartha-oiliuna-tg4-udaras-na-gaeltachta/' : '/ga/traenail-forbairt/sceim-forbartha-oiliuna-tg4-udaras-na-gaeltachta'); ?>" class="prog-panel">
			            <h3 class="press-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Scéim Oiliúna TG4 / Údarás na Gaeltachta' : 'Scéim Oiliúna TG4 / Údarás na Gaeltachta'); ?></h3>
			            <img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2019/01/Sc%C3%A9im-Forbartha-Hero-Ad_378x166.jpg" height="166px" width="378px" alt="sceim_forbartha">
			            <div class="press-desc"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Scéim Forbartha Oiliúna do Chomhlachtaí...' : 'Scéim Forbartha Oiliúna do Chomhlachtaí...'); ?></div>
			            <div class="press-footer">    
			                <div class="arrow-box"></div>
			            </div>
			        </a>
			    </div>
			</section>
			<section class="mod-1">
			    <div class="player-mod-notice">
			        <a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/traenail-forbairt/taithi-oibre-le-tg4/' : '/ga/traenail-forbairt/taithi-oibre-le-tg4/'); ?>" class="prog-panel">
			            <h3 class="press-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Taithí Oibre le TG4' : 'Taithí Oibre le TG4'); ?></h3>
			            <img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2019/01/tallan_tg4_378x166.jpg" height="166px" width="378px" alt="Tallann" class="prog-img">
			           	<div class="press-desc"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Deis bliain taithí oibre a chaitheamh le TG4...' : 'Deis bliain taithí oibre a chaitheamh le TG4...'); ?></div>
			           	<div class="press-footer">
			                <div class="arrow-box"></div>
			            </div>
			        </a>
			    </div>
			</section>
			<section class="mod-1">
			    <div class="player-mod-notice">
			        <a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/traenail-forbairt/abhar-gearr-fhoirm/' : '/ga/traenail-forbairt/abhar-gearr-fhoirm/'); ?>" class="prog-panel">
			            <!--h3 class="press-title"><?php //echo (ICL_LANGUAGE_CODE == "ga" ? 'Scéalaithe Físe / Mojo' : 'Scéalaithe Físe / Mojo'); ?></h3-->
			            <h3 class="press-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Ábhar Gearr-Fhoirm' : 'Ábhar Gearr-Fhoirm'); ?></h3>
			            <img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2019/01/Mojo1_378x166.jpg" height="166px" width="378px" alt="Céad Seans" class="prog-img">
			            <!--div class="press-desc"><?php //echo (ICL_LANGUAGE_CODE == "ga" ? 'An bhfuil cumas scéalaíochta ionat?...' : 'An bhfuil cumas scéalaíochta ionat?...'); ?></div-->
			            <div class="press-desc"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Cuireann TG4 fáilte roimh smaointe le h-aghaidh mion-sraitheanna Gearr-fhoirm a bheadh oiriúnach do árdáin Molscéal nó do BLOC...' : 'Cuireann TG4 fáilte roimh smaointe le h-aghaidh mion-sraitheanna Gearr-fhoirm a bheadh oiriúnach do árdáin Molscéal nó do BLOC...'); ?></div>
			            <div class="press-footer">
			                <div class="arrow-box"></div>
			            </div>
			        </a>
			    </div>
			</section>
			<section class="mod-1">
			    <div class="player-mod-notice">
			        <a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/traenail-forbairt/clair-oiliuna-do-scoileanna/' : '/ga/traenail-forbairt/clair-oiliuna-do-scoileanna/'); ?>" class="prog-panel">
			            <h3 class="press-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Cláir Oiliúna do Scoileanna' : 'Cláir Oiliúna do Scoileanna'); ?></h3>
			            <img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2019/01/Daltai_Scoile_378x166.jpg" height="166px" width="378px" alt="Taithí Oibre" class="prog-img">
			            <div class="press-desc"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Eolas faoi scéimeanna oiliúna do dhaltaí scoile...' : 'Eolas faoi scéimeanna oiliúna do dhaltaí scoile...'); ?></div>
			            <div class="press-footer">
			                <div class="arrow-box"></div>
			            </div>
			        </a>
			    </div>
			</section>
			<section class="mod-1">
			    <div class="player-mod-notice">
			        <a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/traenail-forbairt/fostaiocht/' : '/ga/traenail-forbairt/fostaiocht/'); ?>" class="prog-panel">
			            <h3 class="press-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Folúntais' : 'Folúntais'); ?></h3>
			            <img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2019/01/Foluntais_378x166.jpg" height="166px" width="378px" alt="Folúntais" class="prog-img">
			            <div class="press-desc"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Folúntais reatha...' : 'Folúntais reatha...'); ?></div>
			            <div class="press-footer">
			                <div class="arrow-box"></div>
			            </div>
			        </a>
			    </div>
			</section>
			<section class="mod-1">
			    <div class="player-mod-notice">
			        <a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/traenail-forbairt/eolas-tacaiochta/' : '/ga/traenail-forbairt/eolas-tacaiochta/'); ?>" class="prog-panel">
			            <h3 class="press-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Eolas Tacaíochta' : 'Eolas Tacaíochta'); ?></h3>
			            <img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2019/01/Eolas_Tacaiochta_378x166.jpg" height="166px" width="378px" alt="Tacaíocht" class="prog-img">
			            <div class="press-desc"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Eolas ar eagrais oiliúna...' : 'Eolas ar eagrais oiliúna...'); ?></div>
			            <div class="press-footer">
			                <div class="arrow-box"></div>
			            </div>
			        </a>
			    </div>
			</section>
		</div>
	</div>
</section>

<?php get_footer(); ?>