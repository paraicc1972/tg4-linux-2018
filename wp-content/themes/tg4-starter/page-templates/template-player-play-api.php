<?php /* Template Name: Player - Play Video */ ?>

<?php include(locate_template('/header-player.php')); ?>

<div class="section-header">
	<h1 class="section-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Seinnteoir TG4' : 'TG4 Player'); ?></h1>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
    <div class="section-submenu-wrap">
        <ul class="section-submenu-list">
            <?php wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=1&exclude=6398,6401,15288,15292,73722'); ?>
            &nbsp;
        </ul>
    </div>
</div>

<?php
$progID = ctype_digit($_GET['pid']) ? $_GET['pid'] : '';
$daysLeft = ctype_digit($_GET['dlft']) ? $_GET['dlft'] : '';
$pageCnt = ctype_digit($_GET['pageCnt']) ? $_GET['pageCnt'] : 1;
$pageOffset = ($pageCnt-1)*20;
$progSort = $_GET['progSort'];
$srchQuery = $_GET['srchTxt'];

//Check for the programme sort in querystring
if (isset($progSort)) {
    $sortQuery = "&progSort=" . $progSort;
}

if (isset($_GET['srchTxt'])) {
    $srchTxt = $_GET['srchTxt'];
    $srchQuery = "&srchTxt=" . $_GET['srchTxt'];
}

if ($progID) { ?>
    <section class="plyr-feat-section">
        <div class="section-panel-dark-3">
            <!-- Load Player -->
            <div class="plyr-feat center-panel">
                <div class="plyr-feat-wrap">
                    <?php if (($Econ=="AD_SUPPORTED") && ($geoCode == "IE")) { ?>
                        <!-- SSAI Enabled Player - 2xPreRolls | 1x Bumper -->
                        <video id="Subtitles" class="video-js vjs-big-play-centered"
                            controls preload="auto" width="100%" height="auto"
                            data-account="1555966122001"
                            data-player="rkPVmp4yM"
                            data-embed="default"
                            data-video-id="<?php echo $progID ?>">
                            <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p> 
                        </video>
                        <script src="https://players.brightcove.net/1555966122001/rkPVmp4yM_default/index.min.js"></script>
                    <?php } else { ?>
                        <!-- No Ads Player -->
                        <video id="Subtitles" class="video-js vjs-big-play-centered"
                            controls preload="auto" width="100%" height="auto"
                            data-account="1555966122001"
                            data-player="HJlOg6KUz"
                            data-embed="default"
                            data-video-id="<?php echo $progID ?>">
                            <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p> 
                        </video>
                        <script src="https://players.brightcove.net/1555966122001/HJlOg6KUz_default/index.min.js"></script>
                    <?php } ?>

                    <!-- google analytics plugin -->
                    <script src="https://d1og0s8nlbd0hm.cloudfront.net/js/min/videojs.ga.videocloud.min.js"></script>
                    <!-- video.js BC Player Controls -->
                    <script>
                        var myPlayer = videojs("Subtitles");
                        var tg4Player = videojs("Subtitles");
                        var part=0;

                        videojs('Subtitles').one('bc-catalog-error', function() {
                            var myPlayer = this,
                            specificError;
                            myPlayer.errors({
                            'errors': {
                              '-3': {
                                'headline': '<h1>Geo-Restricted!</h1>Níl an clár seo le fáil i do Réigiúnsa.<br />Breis eolas le fáil san leathanach <a href="https://www.tg4.ie/ga/faq/frequently-asked-questions/">Ceisteanna Coitianta</a>.<br /><br />This programme is not avaialble in your region.<br />Further information from our <a href="https://www.tg4.ie/en/faq/frequently-asked-questions/">FAQ</a> page.',
                                'type': 'CLIENT_GEO'
                                    }
                                }
                            });
                            if (typeof(myPlayer.catalog.error) !== 'undefined') {
                                specificError = myPlayer.catalog.error.data[0];
                                if (specificError !== 'undefined' & specificError.error_subcode == "CLIENT_GEO") {
                                    myPlayer.error({code:'-3'});
                                };
                            };
                        });

                        // GA Custom Script
                        tg4Player.on('loadstart',function() {
                            tg4Player = this;
                            /* console.log("catalog object: ",this.catalog.load()); 
                            console.log("mediainfo object: ", this.mediainfo); */
                            tg4Player.ga();
                        });

                        myPlayer.ready(function() {
                            // get a reference to the player
                            myPlayer = this;

                            myPlayer.on("play", function (evt) {
                                console.log('Asset Play');
                            });

                            myPlayer.on("pause", function (evt) {
                                console.log('Asset Paused');
                            });

                            myPlayer.on("ended", function (evt) {
                                console.log('Asset Ended');
                            });

                        });
                    </script>
                </div>
                <div class="prog-feat-ad mod-ad">
                    <h2 class="mod-ad-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Fógraíocht' : 'Advertisement'); ?></h2>
                    <div class="ad-wrapper">
                        <iframe src="https://www.tg4.ie/wp-content/themes/tg4-starter/mpu-banner.htm" height="250" width="300" scrolling="no" frameborder="0"></iframe>
                        <!-- <iframe src="<?php //echo site_url() . '/wp-content/themes/tg4-starter/'; ?>mpu-banner.htm" height="250" width="300" scrolling="no" frameborder="0"></iframe> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="plyr-feat-intro-wrap">
            <div class="center-panel">
                <div class="prog-feat-intro">
                    <div class="prog-feat-toolbar">
                        <div class="prog-remind">
                            <div class="prog-remind-txt"><?php echo $progTitle . ' ' . $progSeries . '-' . $progEpisode; ?></div>
                            <?php if ($daysLeft < 35) { ?>
                                <div class="prog-remind-count"><span><?php echo $daysLeft; ?></span><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Lá Fagtha' : 'Days Left'); ?></div>
                            <?php } ?>
                        </div>
                        <div class="prog-share">
                            <!-- Facebook -->
                            <a href="#" onClick="window.open('https://www.facebook.com/sharer.php?u=<?php echo 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>','TG4/Facebook Share','resizable,height=350,width=500'); return false;" class="facebook-share">Facebook</a>
                            <noscript><a href="https://www.facebook.com/sharer.php?u=<?php echo 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" target="_blank" class="facebook-share">Facebook</a></noscript>
                            <!-- Twitter -->    
                            <a href="#" onClick="window.open('https://twitter.com/share?text=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Féach seo...' : 'Féach seo...'); ?>&amp;url=<?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>&amp;hashtags=TG4','TG4/Twitter Share','resizable,height=350,width=500'); return false;" class="twitter-share">Twitter</a>
                            <noscript><a href="https://twitter.com/share?text=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Féach seo...' : 'Féach seo...'); ?>&amp;url=<?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>&amp;hashtags=TG4" target="_blank" class="twitter-share">Twitter</a></noscript>
                            <!-- Email -->
                            <a href="mailto:?Subject=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Suíomh TG4 - https://www.tg4.ie' : 'TG4 Website - https://www.tg4.ie'); ?>&amp;Body=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'A%20Chara,%0A%0AB\'fhéidir%20go%20mbeadh%20suim%20agat%20sa%20chlár%20seo%20ar%20suíomh%20idirlín%20TG4!%0A%0A' : 'A%20Chara,%0A%0AI%20saw%20this%20on%20the%20TG4%20website%20and%20thought%20you%20might%20be%20interested!%0A%0A'); ?><?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" class="email-share">Email</a>
                        </div>
                    </div>
                    <div class="prog-feat-txt">
                        <p class="bold"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Rátáil Tuismitheora: ' . $progPG : 'Parental Rating: ' . $progPG); ?></p>
                        <p><?php echo (ICL_LANGUAGE_CODE == "ga" ? $progGaeDesc : $progEngDesc); ?></p>
                        <p class="bold"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Chéad Taispeántas: ' . date("j ", strtotime($showStartDate)) . schedule_daily_abbr(date("M", strtotime($showStartDate))) : 'First Shown: ' . $showStartDate); ?></p>
                        <p><a href="<?php echo $linkURL; ?>" target="_New"><?php echo $linkText; ?></a></p>
                    </div>
                    <?php 
                    $noSubs = array('Nuacht TG4','GAA Beo','GAA 2017','Rugbaí Beo','7 Lá','Peil na mBan Beo');
                    if (!in_array($progSeriesTitle, $noSubs)) { ?>
                        <div class="prog-feat-keyword" style="background-color:#ffffff; border: 1px solid #043244; padding:8px; margin: 5px 0px 5px 0px;">
                            <p class="bold"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Rogha Fotheidil' : 'Optional Subtitles'); ?></p>
                            <p style="font-size:small;"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Le fotheidil a roghnú, brú [CC][<img src="https://s3.amazonaws.com/tg4-docs/images/player-subs.png">] ag bun an scáileáin.' : 'To choose subtitles, press [CC][<img src="https://s3.amazonaws.com/tg4-docs/images/player-subs.png">] in the player controls.'); ?></p>
                            <p class="bold"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'An Seinnteoir ar IOS' : 'Player on IOS devices'); ?></p>
                            <p style="font-size:small;"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Le breathnú ar an Seinnteoir ar IOS, íoslódáil Aip TG4 anseo... <a href="https://itunes.apple.com/ie/app/tg4-player/id598591024?mt=8" target="_New">https://itunes.apple.com/ie/app/tg4-player/id598591024?mt=8</a>' : 'To access the player on an IOS device, you need to download the TG4 Player App. Free to download here... <a href="https://itunes.apple.com/ie/app/tg4-player/id598591024?mt=8" target="_New">https://itunes.apple.com/ie/app/tg4-player/id598591024?mt=8</a>'); ?></p>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <!-- / Schema.org Microdata -->
        <meta itemprop="video" itemscope itemtype="https://schema.org/VideoObject"/>
        <meta itemprop="name" content="<?php echo $progName; ?>" />
        <meta itemprop="description" content="<?php echo $progEngDesc; ?>" />
        <meta itemprop="contentURL" content="<?php echo 'https://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>" />
        <meta itemprop="width" content="960" />
        <meta itemprop="height" content="540" />
        <meta itemprop="thumbnail" content="<?php echo $progBCImage; ?>" />
        <meta itemprop="inLanguage" content="EN" />
        <meta itemprop="isFamilyFriendly" content="<?php echo $famFriendly; ?>" />
        <meta itemprop="contentLocation" content="Ireland" />
        <meta itemprop="encodingFormat" content="MP4" />
        <meta itemprop="videoFrameSize" content="960x540" /> 
        <meta itemprop="about" content="<?php echo (ICL_LANGUAGE_CODE == "ga" ? $progGaeDesc : $progEngDesc); ?>" />
        <meta itemprop="genre" content="<?php echo $progCategory; ?>" />
        <meta itemprop="author" content="TG4" /> 
        <meta itemprop="publisher" content="tg4.ie" />
    </section>

    <section class="section-panel-dark-2">
        <div class="title-tab-wrap">
            <h2 class="title-tab-dark-2"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Físeáin sa tSraith' : 'Series Videos'); ?></h2>
        </div>
        <?php include(locate_template('/includes/featured-videos-series-api.php')); ?>
    </section>

    <section class="section-panel-white">
        <div class="title-tab-wrap">
            <h2 class="title-tab-white"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Físeáin Faoi Thrácht' : 'Featured Videos'); ?></h2>
        </div>
        <?php include(locate_template('/includes/featured-videos-feature-api.php')); ?>
    </section>

    <!-- Angular -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular-touch.min.js"></script>
    <script src='https://d1og0s8nlbd0hm.cloudfront.net/js/angular-slick.js'></script>
    <script src="https://d1og0s8nlbd0hm.cloudfront.net/js/app-slick-v1.js"></script>
    <!-- Angular -->
<?php } else { ?>
    <div class="srch-feat-intro-wrap">
        <div class="center-panel">
            <div class="srch-feat-intro">
                <?php if (isset($srchTxt) && ($srchTxt != "")) { ?>
                    <div class="srchSel-wrap">
                        <select ng-cloak id="sSel" name="page-dropdown" ng-model="flt" ng-init="flt='All'" onchange="document.location.href=this.options[this.selectedIndex].value;"> 
                            <option value="All" disabled>{{current_Lang=='ga'?'Rangaigh...':'Sort By...'}}</option>
                            <option value='?progSort=RecentDesc<?php echo $srchQuery; ?>'>{{current_Lang=='ga'?'Is Déanaí':'Latest Video'}}</option>
                            <option value='?progSort=RecentAsc<?php echo $srchQuery; ?>'>{{current_Lang=='ga'?'Is Luaithe':'Earliest Video'}}</option>
                            <option value='?progSort=PopDesc<?php echo $srchQuery; ?>'>{{current_Lang=='ga'?'Is Mo Tóir':'Most Views'}}</option>
                            <option value='?progSort=PopAsc<?php echo $srchQuery; ?>'>{{current_Lang=='ga'?'Is lú Tóir':'Least Views'}}</option>
                        </select>
                    </div>
                <?php } ?>
                <div style="margin-top:30px; float:right;"><input ng-cloak style="display:inline;" name="sbox" id="sbox" ng-model="ss" placeholder="{{current_Lang=='ga'?' Cuardaigh':' Search'}}" ng-change="ss1=noFada(ss);"/><a href="#" style="border:1px solid #b2afaf; background:#fff; padding:2px; margin-left:-1px; margin-right:10px;" id="srchBtn" onclick="this.href='?srchTxt=' + document.getElementById('sbox').value">>></a></div>
                <script>
                var input = document.getElementById("sbox");
                input.addEventListener("keyup", function(event) {
                    event.preventDefault();
                    if (event.keyCode === 13) {
                        document.getElementById("srchBtn").click();
                    }
                });
                </script>
            </div>
        </div>
    </div>

    <section class="player-mods" ng-app="tg4App">
        <h2 class="visuallyhidden">More on TG4</h2>
        <div class="plyrmods-wrapper">
            <!-- TáGaeilge Agam Text
            <div class="container">   
                <h2><strong>Tá Gaeilge Agam</strong></h2>
                <?php //echo (ICL_LANGUAGE_CODE == "ga" ? '<p>Tá feachtas nua, spraíúil seolta ag TG4, a bhfuil meascán de dhaoine clúiteacha páirteach ann, ina measc an bhanríon draig Panti Bliss, an spásaire Chris Hadfield, an ropaire rubair, Rubberbandit ‘Blindboy Boatclub’, réalta rugbaí na mban Sene agus an láithreoir aimsire Caitlín Nic Aoidh, agus iad ag tabhairt ugach do dhaoine an méid Gaeilge atá acu a úsáid agus a cheiliúradh... Tuilleadh Eolas <a href="https://www.tg4.ie/ga/clair/ta-gaeilge-agam/">anseo</a>!</p><h2><strong>Roghliosta Cláir</strong></h2>' : '<p>TG4 has launched a quirky new advertising campaign featuring an eclectic bunch of celebrities including drag queen Panti Bliss, astronaut Chris Hadfield, Rubberbandit ‘Blindboy Boatclub’, women’s rugby sensation Sene and weather presenter Caitlín Nic Aoidh, to encourage everyone to use and celebrate whatever Irish they have. More Information <a href="https://www.tg4.ie/en/programmes/ta-gaeilge-agam/">here</a>!</p><h2><strong>Selected Programmes</strong></h2>'); ?>
            </div-->

            <!-- Roghliosta Cláir | Selected Programmes -->
            <div class="container">   
                <h2><strong><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Roghliosta Cláir' : 'Selected Programmes'); ?></strong></h2>
            </div>

            <!-- Jquery Paging -->
            <div class="container">
                <script src='https://d1og0s8nlbd0hm.cloudfront.net/js/min/jquery.bootpag.min.js'></script>
                <?php if (isset($srchTxt) && ($srchTxt != "")) { ?>
                    <p id="pagination-here"></p>
                <?php } ?>
            </div>
                
            <div class="player-mod-wrap">
                <?php if (isset($srchTxt) && ($srchTxt != "")) { ?>
                    <div style="margin:10px 0 20px;"><a href="<?php echo get_permalink($post->post_id); ?>" style="border:1px solid #3e8eb4; border-radius:4px; background:#ddd; padding:5px;"><?php echo $srchTxt; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;x</a></div>
                <?php } ?>
                <!-- Programme content added into boxes below -->
                <section class="mod-1" ng-repeat="p in allProg=(pAll) | limitTo:28"> <!--track by p.id"-->
                    <div class="player-mod-notice">
                        <!-- <a ng-cloak href="<?php //echo site_url(); ?>{{current_Lang=='ga'?'/ga/player/baile/?pid='+p.id+'&teideal='+p.custom_fields.title+'&series='+p.custom_fields.seriestitle+'&dlft='+diffDate((p.schedule.ends_at | date : 'yyyy-MM-dd HH:mm:ss'))+''.time():'/en/player/home/?pid='+p.id+'&teideal='+p.custom_fields.title+'&series='+p.custom_fields.seriestitle+'&dlft='+diffDate((p.schedule.ends_at | date : 'yyyy-MM-dd HH:mm:ss'))+''.time()}}" class="prog-panel"> -->
                        <a ng-cloak href="{{current_Lang=='ga'?'<?php echo site_url(); ?>/ga/player/baile/?pid='+p.id+'&teideal='+p.custom_fields.title+'&series='+p.custom_fields.seriestitle+'&dlft='+diffDate((p.schedule.ends_at | date : 'yyyy-MM-dd HH:mm:ss'))+''.time():'<?php echo site_url(); ?>/en/player/home/?pid='+p.id+'&teideal='+p.custom_fields.title+'&series='+p.custom_fields.seriestitle+'&dlft='+diffDate((p.schedule.ends_at | date : 'yyyy-MM-dd HH:mm:ss'))+''.time()}}" class="prog-panel">
                            <!-- <h3 class="player-title" ng-bind="p.custom_fields.seriestitle.length>0?p.custom_fields.seriestitle:p.custom_fields.title"></h3> -->
                            <h3 ng-if="p.custom_fields.seriestitle.length>0" class="player-title" ng-bind-html="highlight(p.custom_fields.seriestitle, '<?php echo $srchTxt; ?>')"></h3>
                            <h3 ng-if="p.custom_fields.seriestitle.length<1" class="player-title" ng-bind-html="highlight(p.custom_fields.title, '<?php echo $srchTxt; ?>')"></h3>
                            <div ng-if="p.custom_fields.p_prodcode.length>0">
                                <img ng-src="https://res.cloudinary.com/tg4/image/upload/w_700,h_395,f_auto,q_auto/{{p.custom_fields.p_prodcode}}.jpg" alt="{{p.custom_fields.seriestitle}}" class="prog-img">
                            </div>
                            <div ng-if="p.custom_fields.s_prodcode.length>0 && !p.custom_fields.p_prodcode.length>0">
                                <img ng-src="https://res.cloudinary.com/tg4/image/upload/w_700,h_395,f_auto,q_auto/{{p.custom_fields.s_prodcode}}.jpg" alt="{{p.custom_fields.seriestitle}}" class="prog-img">
                            </div>
                            <div ng-if="!p.custom_fields.s_prodcode.length>0 && !p.custom_fields.p_prodcode.length>0">
                                <img ng-src="{{p.images.poster.sources[0].src}}" alt="{{p.custom_fields.seriestitle}}" class="prog-img">
                            </div>
                            <div class="prog-footer">
                                <!-- <h4 class="prog-episode-title" ng-bind="p.custom_fields.title!=p.custom_fields.seriestitle?p.custom_fields.title:''"></h4> -->
                                <h4 class="prog-episode-title" ng-if="p.custom_fields.title!=p.custom_fields.seriestitle" ng-bind-html="highlight(p.custom_fields.title, '<?php echo $srchTxt; ?>')"></h4>
                                <div class="prog-season">S<span ng-bind="p.custom_fields.series"></span></div>
                                <div class="prog-episode">E<span ng-bind="p.custom_fields.episode"></span></div>
                                <div class="prog-episode"><span ng-bind="p.custom_fields.parental_guide"></span></div>
                                <div class="arrow-box"></div>
                            </div>
                            <div class="prog-infobar">
                                <div class="prog-firstshow"><span ng-bind="current_Lang=='ga'?'Chéad Clár: '+p.custom_fields.date:'First Shown: '+p.custom_fields.date"></div>
                                <div class="prog-dur"><span ng-bind="{{p.duration/60000}} | number : 0"></span> <span ng-bind="current_Lang=='ga'?'Nóim':'Mins'"></span></div>
                                <div ng-if="diffDate((p.schedule.ends_at | date : 'yyyy-MM-dd HH:mm:ss')) > 35" class="prog-daysleft"> <span ng-bind="current_Lang=='ga'?'Gan Srian':'Unlimited'"></span></div>
                                <div ng-if="diffDate((p.schedule.ends_at | date : 'yyyy-MM-dd HH:mm:ss')) <= 35" class="prog-daysleft"><span ng-bind="diffDate((p.schedule.ends_at | date : 'yyyy-MM-dd HH:mm:ss'))"></span> <span ng-bind="current_Lang=='ga'?'Lá Fagtha':'Days Left'"></span></div>
                            </div>
                            <!-- <div class="prog-desc"><span ng-bind="current_Lang=='ga'?(p.custom_fields.longdescgaeilge).substr(0,250)+'...':p.description"></span></div> -->
                            <div ng-if="current_Lang=='ga'" class="prog-desc"><span ng-bind-html="highlight(p.custom_fields.longdescgaeilge.substr(0,220)+'...', '<?php echo $srchTxt; ?>')"></span></div>
                            <div ng-if="current_Lang!='ga'" class="prog-desc"><span ng-bind-html="highlight(p.description.substr(0,220)+'...', '<?php echo $srchTxt; ?>')"></span></div>
                        </a>
                    </div>
                </section>
                <div ng-if="pTotal.count>0" ng-hide="allProg.length"><h2><i class="fa fa-refresh fa-spin"></i>&nbsp;&nbsp;&nbsp;<span ng-bind="current_Lang=='ga'?'Ag Lódáil...':'Loading...'"></h2></div>
                <div ng-if="pTotal.count<1"><h2><span ng-bind="current_Lang=='ga'?'Gan toradh. Bain triail eile as.':'No results found for that search. Try again.'"></h2></div>
            </div>
        </div>
    </section>

    <!-- Angular -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular-touch.min.js"></script>
    <!-- Angular -->

    <script>
        var app=angular.module('tg4App',[]);
        var tg4App=angular.module("tg4App", []);
        tg4App.controller('tg4Ctrl', function($scope,$http,$sce) {
            $scope.current_Lang='<?php echo ICL_LANGUAGE_CODE ?>';
            $scope.total = 0;
            $scope.currentPage = <?php echo $pageCnt; ?>;

            //* Check querystring parameters for Sort, Page Count...
            var params={};
            decodeURIComponent(location.search).replace(/[?&]+([^=&]+)=([^&]*)/gi,function(m,k,v){ params[k]=v });
            //* Check querystring parameters for Sort
            if (!params.progSort) { 
                params.progSort='-schedule_starts_at';
            }
            if (params.progSort=='RecentAsc') { 
                params.progSort='schedule_starts_at';
            }
            if (params.progSort=='RecentDesc') { 
                params.progSort='-schedule_starts_at';
            }
            if (params.progSort=='PopAsc') { 
                params.progSort='plays_total';
            }
            if (params.progSort=='PopDesc') { 
                params.progSort='-plays_total';
            }
            if (params.progSort=='AZ') { 
                params.progSort='name';
            }
            if (params.progSort=='ZA') { 
                params.progSort='-name';
            }
            if (params.srchTxt) { 
                var qrySrch=params.srchTxt;
            }
            //* Check querystring parameters for Page Count, if empty load 1st page
            if (!params.pageCnt) { 
                params.pageCnt=1;
            }

            $http.post("<?php echo get_template_directory_uri(); ?>/assets/php/tg4-proxy.php").then(function(response) {
                $scope.tAuth = response.data;
                //console.log($scope.tAuth);
                if ($scope.tAuth) {
                    var headerParam = { headers: {
                        "Authorization": "Bearer " + $scope.tAuth,
                        "Accept": "application/json;odata=verbose"
                        }
                    };

                    <?php if ($geoCode != 'IE') { ?>
                        console.log('Not IE');
                        if (params.srchTxt) {
                            endPointCnt = '/counts/videos?q=%2B"' + qrySrch + '"+%2Dseriestitle:"TG4-Beo"+%2Bgeo_restrict:"N"+%2Bplayable%3Atrue';
                        } else {
                            endPointCnt = '/counts/videos?q=%2Dseriestitle:"TG4-Beo"+%2Bgeo_restrict:"N"+%2Bplayable%3Atrue+%2Btags:"baile"';
                        }
                    <?php } else { ?> 
                        console.log('Is IE');
                        if (params.srchTxt) {
                            endPointCnt = '/counts/videos?q=%2B"' + qrySrch + '"+%2Dseriestitle:"TG4-Beo"+%2Bplayable%3Atrue';
                        } else {
                            endPointCnt = '/counts/videos?q=%2Dseriestitle:"TG4-Beo"+%2Bplayable%3Atrue+%2Btags:"baile"';
                        }
                    <?php } ?>

                    $http.get('https://cms.api.brightcove.com/v1/accounts/1555966122001' + endPointCnt, headerParam).then(function(cnt){
                        $scope.pTotal = cnt.data;
                        $scope.total = cnt.data.count;
                        console.log("Total: ", $scope.total);

                        if ($scope.total > 0) {
                            <?php if ($geoCode != 'IE') { ?>
                                if (params.srchTxt) {
                                    var endPoint = '/videos?q=%2B"' + qrySrch + '"+%2Dseriestitle:"TG4-Beo"+%2Bgeo_restrict:"N"+%2Bplayable%3Atrue&limit=20&offset=<?php echo $pageOffset; ?>&sort='+ params.progSort;
                                } else {
                                    var endPoint = '/videos?q=%2Dseriestitle:"TG4-Beo"+%2Bgeo_restrict:"N"+%2Bplayable%3Atrue+%2Btags:"baile"&limit=28&offset=<?php echo $pageOffset; ?>&sort='+ params.progSort;
                                }
                            <?php } else { ?>
                                if (params.srchTxt) {
                                    var endPoint = '/videos?q=%2B"' + qrySrch + '"+%2Dseriestitle:"TG4-Beo"+%2Bplayable%3Atrue&limit=20&offset=<?php echo $pageOffset; ?>&sort='+ params.progSort;
                                } else {
                                    var endPoint = '/videos?q=%2Dseriestitle:"TG4-Beo"+%2Bplayable%3Atrue+%2Btags:"baile"&limit=28&offset=<?php echo $pageOffset; ?>&sort='+ params.progSort;
                                }
                            <?php } ?>

                            $http.get('https://cms.api.brightcove.com/v1/accounts/1555966122001' + endPoint, headerParam).then(function(d) {
                                $scope.pAll = d.data;
                                $scope.pCnt = d.data.length;
                                console.log($scope.pAll);
                            });

                            $scope.highlight = function(haystack, needle) {
                                if(!needle) {
                                    return $sce.trustAsHtml(haystack);
                                }
                                return $sce.trustAsHtml(haystack.replace(new RegExp(needle, "gi"), function(match) {
                                    return '<span class="highlightedText">' + match + '</span>';
                                })
                                );
                            };
                            
                            <?php if (isset($srchTxt) && ($srchTxt != "")) { ?>
                                $scope.pageTotal = Math.ceil($scope.total/20);
                                console.log($scope.pageTotal);

                                $(document).ready(function () {
                                    $('#pagination-here').bootpag({
                                        total: $scope.pageTotal,
                                        page: <?php echo $pageCnt; ?>,
                                        maxVisible: 20,
                                        leaps: false,
                                        firstLastUse: true,
                                        first: '←',
                                        last: '→'
                                    }).on('page', function(event, num){
                                        window.location.href = '?pageCnt=' + num + '<?php echo $srchQuery . $sortQuery; ?>';
                                    });
                                });
                            <?php } ?>
                        }
                    });
                }
            });

            $scope.diffDate = function(date1){
                var dateOut1 = new Date(Date.parse(date1, "yyyy-MM-dd HH:mm:ss")); // It will work if date1 is in ISO format
                var dateOut2 = new Date(); // Today's date
                var timeDiff = Math.abs(dateOut1.getTime() - dateOut2.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                return diffDays;
            };
        });
    </script>
<?php } ?>

<?php get_footer(); ?>