<?php /* Template Name: Player - New Design */ ?>

<?php include(locate_template('/header-angular-nd.php')); ?>

<section class="section-panel-plyr">
    <?php include(locate_template('/includes/featured-drama-api.php')); ?>
</section>

<section class="prog-head" style="background-image: url(https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2015/08/7_La_2015.jpg);">
</section>

<?php
$clientIP = $_SERVER['HTTP_X_FORWARDED_FOR']?: $_SERVER['HTTP_CLIENT_IP']?: $_SERVER['REMOTE_ADDR'];
if ($clientIP == "::1") {
    $clientIP = "77.75.98.34"; //IE
    //$clientIP = "185.86.151.11"; //GB
    //$clientIP = "94.0.69.150"; //NIR
}
$seriesTag = "Faisneis";
$pageCnt = ctype_digit($_GET['pageCnt']) ? $_GET['pageCnt'] : 1;
$pageOffset = ($pageCnt-1)*20;
$progSort = $_GET['progSort'];
$srchQuery = $_GET['srchTxt'];

//Check for the programme sort in querystring
if (isset($progSort)) {
    $sortQuery = "&progSort=" . $progSort;
}

if (isset($_GET['srchTxt'])) {
    $srchTxt = $_GET['srchTxt'];
    $srchQuery = "&srchTxt=" . $_GET['srchTxt'];
}
?>

<section class="player-mods-nd" ng-app="tg4App">
    <h2 class="visuallyhidden">More on TG4</h2>
    <div class="plyrmods-wrapper-new">
        <!-- Jquery Paging -->
        <div class="container">
            <script src='https://d1og0s8nlbd0hm.cloudfront.net/js/min/jquery.bootpag.min.js'></script>
            <p id="pagination-here"></p>
        </div>

        <div class="player-mod-wrap">
            <?php if (isset($srchTxt) && ($srchTxt != "")) { ?>
                <div style="margin:10px 0 20px;"><a href="<?php echo get_permalink($post->post_id); ?>" style="border:1px solid #3e8eb4; border-radius:4px; background:#ddd; padding:5px;"><?php echo $srchTxt; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;x</a></div>
            <?php } ?>
            <!-- Programme content added into boxes below -->
            <section class="mod-1" ng-repeat="p in allProg=(pAll) | limitTo:20"> <!--track by p.id"-->
                <div class="player-mod-block">
                    <a ng-cloak href="<?php echo site_url(); ?>{{current_Lang=='ga'?'/ga/player/baile/?pid='+p.id+'&teideal='+p.custom_fields.title+'&series='+p.custom_fields.seriestitle+'&dlft='+diffDate((p.schedule.ends_at | date : 'yyyy-MM-dd HH:mm:ss'))+'':'/en/player/home/?pid='+p.id+'&teideal='+p.custom_fields.title+'&series='+p.custom_fields.seriestitle+'&dlft='+diffDate((p.schedule.ends_at | date : 'yyyy-MM-dd HH:mm:ss'))+''}}" class="prog-panel">
                        <span><img ng-cloak ng-src="{{p.images.poster.sources[0].src}}" alt="{{p.custom_fields.seriestitle}}" class="prog-img"></span>
                        <!-- <img ng-src="{{p.custom_fields.seriesimgurl}}" alt="{{p.custom_fields.seriestitle}}" class="prog-img"> -->
                        <div class="prog-desc-plyr">
                            <p class="prog-series-title" ng-bind="p.custom_fields.seriestitle.length>0?p.custom_fields.seriestitle:p.custom_fields.title"></p>
                            <p class="prog-episode-title" ng-bind="p.custom_fields.title!=p.custom_fields.seriestitle?p.custom_fields.title:''"></p>
                        </div>
                        <div class="prog-desc-plyr">
                            <div class="prog-episode-info">S<span ng-bind="p.custom_fields.series"></span> E<span ng-bind="p.custom_fields.episode"></span></div>
                            <div class="prog-episode-date" ng-bind="p.custom_fields.date"></div>
                        </div>
                    </a>
                </div>
            </section>
            <div ng-if="pTotal.count>0" ng-hide="allProg.length"><h2><i class="fa fa-refresh fa-spin"></i>&nbsp;&nbsp;&nbsp;<span ng-bind="current_Lang=='ga'?'Ag Lódáil...':'Loading...'"></h2></div>
            <div ng-if="pTotal.count<1"><h2><span ng-bind="current_Lang=='ga'?'Gan toradh. Bain triail eile as.':'No results found for that search. Try again.'"></h2></div>
        </div>

        <!-- Angular -->
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular-touch.min.js"></script>
        <!-- Angular -->

        <script>
        var app=angular.module('tg4App',[]);
        var tg4App=angular.module("tg4App", []);
        tg4App.controller('tg4Ctrl', function($scope,$http) {
            $scope.current_Lang='<?php echo ICL_LANGUAGE_CODE ?>';
            $scope.total = 0;
            $scope.currentPage = <?php echo $pageCnt; ?>;

            //* Check querystring parameters for Sort, Page Count...
            var params={};
            decodeURIComponent(location.search).replace(/[?&]+([^=&]+)=([^&]*)/gi,function(m,k,v){ params[k]=v });
            //* Check querystring parameters for Sort
            if (!params.progSort) { 
                params.progSort='-schedule_starts_at';
            }
            if (params.progSort=='RecentAsc') { 
                params.progSort='schedule_starts_at';
            }
            if (params.progSort=='RecentDesc') { 
                params.progSort='-schedule_starts_at';
            }
            if (params.progSort=='PopAsc') { 
                params.progSort='plays_total';
            }
            if (params.progSort=='PopDesc') { 
                params.progSort='-plays_total';
            }
            if (params.progSort=='AZ') { 
                params.progSort='name';
            }
            if (params.progSort=='ZA') { 
                params.progSort='-name';
            }
            if (params.srchTxt) { 
                var qrySrch='%2Bseriestitle:"' + params.srchTxt;
            }
            //* Check querystring parameters for Page Count, if empty load 1st page
            if (!params.pageCnt) { 
                params.pageCnt=1;
            }

            $http.post("<?php echo get_template_directory_uri(); ?>/assets/php/tg4-proxy.php").then(function(response) {
                $scope.tAuth = response.data;
                //console.log($scope.tAuth);
                if ($scope.tAuth) {
                    var headerParam = { headers: {
                        "Authorization": "Bearer " + $scope.tAuth,
                        "Accept": "application/json;odata=verbose"
                        }
                    };

                    <?php if ($geoCode != 'IE') { ?>
                        if (params.srchTxt) {
                            endPointCnt = '/counts/videos?q=%2Dseriestitle:"TG4-Beo"+%2Bcategory_c:"<?php echo $seriesTag; ?>"' + qrySrch + '"+%2Bgeo_restrict:"N"+%2Bplayable%3Atrue';
                        } else {
                            endPointCnt = '/counts/videos?q=%2Dseriestitle:"TG4-Beo"+%2Bcategory_c:"<?php echo $seriesTag; ?>"+%2Bgeo_restrict:"N"+%2Bplayable%3Atrue';
                        }
                    <?php } else { ?> 
                        if (params.srchTxt) {
                            endPointCnt = '/counts/videos?q=%2Dseriestitle:"TG4-Beo"+%2Bcategory_c:"<?php echo $seriesTag; ?>"' + qrySrch + '"+%2Bplayable%3Atrue';
                        } else {
                            endPointCnt = '/counts/videos?q=%2Dseriestitle:"TG4-Beo"+%2Bcategory_c:"<?php echo $seriesTag; ?>"+%2Bplayable%3Atrue';
                        }
                    <?php } ?>

                    $http.get('https://cms.api.brightcove.com/v1/accounts/1555966122001' + endPointCnt, headerParam).then(function(cnt) {
                        $scope.pTotal = cnt.data;
                        $scope.total = cnt.data.count;
                        console.log($scope.total);

                        if ($scope.total > 0) {
                            <?php if ($geoCode != 'IE') { ?>
                                if (params.srchTxt) {
                                    var endPoint = '/videos?q=%2Dseriestitle:"TG4-Beo"+%2Bcategory_c:"<?php echo $seriesTag;?>"' + qrySrch + '"+%2Bgeo_restrict:"N"+%2Bplayable%3Atrue&limit=20&offset=<?php echo $pageOffset; ?>&sort='+ params.progSort;
                                } else {
                                    var endPoint = '/videos?q=%2Dseriestitle:"TG4-Beo"+%2Bcategory_c:"<?php echo $seriesTag;?>"+%2Bgeo_restrict:"N"+%2Bplayable%3Atrue&limit=20&offset=<?php echo $pageOffset; ?>&sort='+params.progSort;
                                }
                            <?php } else { ?>
                                if (params.srchTxt) {
                                    var endPoint = '/videos?q=%2Dseriestitle:"TG4-Beo"+%2Bcategory_c:"<?php echo $seriesTag;?>"' + qrySrch + '"+%2Bplayable%3Atrue&limit=20&offset=<?php echo $pageOffset; ?>&sort='+ params.progSort;
                                } else {
                                    var endPoint = '/videos?q=%2Dseriestitle:"TG4-Beo"+%2Bcategory_c:"<?php echo $seriesTag;?>"+%2Bplayable%3Atrue&limit=20&offset=<?php echo $pageOffset; ?>&sort='+ params.progSort;
                                }
                            <?php } ?>

                            $http.get('https://cms.api.brightcove.com/v1/accounts/1555966122001' + endPoint, headerParam).then(function(d) {
                                $scope.pAll = d.data;
                                $scope.pCnt = d.data.length;
                                console.log($scope.pAll);
                            });
                            
                            $scope.pageTotal = Math.ceil($scope.total/20);
                            console.log($scope.pageTotal);

                            $(document).ready(function () {
                                $('#pagination-here').bootpag({
                                    total: $scope.pageTotal,
                                    page: <?php echo $pageCnt; ?>,
                                    maxVisible: 20,
                                    leaps: false,
                                    firstLastUse: true,
                                    first: '←',
                                    last: '→'
                                }).on('page', function(event, num){
                                    window.location.href = '?pageCnt=' + num + '<?php echo $sortQuery; ?>';
                                });
                            });
                        }
                    });
                }
            });

            $scope.diffDate = function(date1){
                var dateOut1 = new Date(Date.parse(date1, "yyyy-MM-dd HH:mm:ss")); // It will work if date1 is in ISO format
                var dateOut2 = new Date(); // Today's date
                var timeDiff = Math.abs(dateOut1.getTime() - dateOut2.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                return diffDays;
            };
        });
        </script>
    </div>
    <div class="prog-feat-ad-plyr mod-ad">
        <div class="ad-wrapper"><img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2015/06/sean-nos-hp.jpg" width="300" height="250"></div>
    </div>
</section>

<?php get_footer(); ?>