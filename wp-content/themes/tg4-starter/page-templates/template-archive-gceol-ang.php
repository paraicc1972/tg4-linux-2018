<?php /* Template Name: Gradam Ceoil Archive - List - Angular */ ?>

<?php include(locate_template('/header-angular.php')); ?>

<div class="section-header">
    <h1 class="section-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Cartlann Ghradam Ceoil' : 'Gradam Ceoil Archive'); ?></h1>
    <p>&nbsp;</p>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
    <div class="section-submenu-wrap">
        <ul class="section-submenu-list">
            <?php wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=0'); ?>
            &nbsp;
        </ul>
    </div>
</div>
<?php
$feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
$feat_Class = "prog-head";

if (get_field("centre_feature_image") == '0' || get_field("centre_feature_image") == 'no') {
    $feat_Class = "prog-head";
} elseif (get_field("centre_feature_image") == '1' || get_field("centre_feature_image") == 'yes')  {
    $feat_Class = "prog-headcenter";
} else {
    $feat_Class = "prog-head";
}
?>
<section class="<?php echo $feat_Class; ?>" style="background-image: url(<?php echo $feat_image; ?>);">
    <div class="blockContainer">
        <?php 
        if (get_field("intro_text")) { ?>
            <div class="content-block">
                <h2 class="content-subtitle"><?php echo get_field("intro_sub_title"); ?></h2>
                <p><?php echo get_field("intro_text"); ?></p>
            </div>
        <?php } ?>
    </div>
</section>

<div class="srch-feat-intro-wrap" style="margin-top:0px;">
    <div class="center-panel">
        <div class="srch-feat-intro">
            <div class="arcform-wrap"><input ng-cloak class="arcform-textbox" name="sbox" id="sbox" ng-model="ss" placeholder="{{current_Lang=='ga'?' Cuimsitheach...':' Advanced Search...'}}" ng-change="ss1=noFada(ss);" /><a href="#" id="srchCompBtn" class="arcform-btn" onclick="this.href='?srchTxt=' + document.getElementById('sbox').value">>></a><a class="arcform-hint" title="{{current_Lang=='ga'?'Is féidir cuardach a dhéanamh ar théarma (faoi bhliain ar leith) anseo e.g. Gaillimh 2001':'You can search by a certain term (within a year) here e.g. Galway 2001'}}">?</a><br /><span class="arcform-text" ng-bind="current_Lang=='ga' ? 'Cuardach: Cuimsitheach' : 'Advanced Search'"></span></div>
            <div class="arcform-wrap"><input ng-cloak class="arcform-textbox" name="mbox" id="mbox" ng-model="ms" placeholder="{{current_Lang=='ga'?' Ceol...':' Music...'}}" ng-change="ss1=noFada(ms);" /><a href="#" id="srchMusicBtn" class="arcform-btn" onclick="this.href='?srchMusic=' + document.getElementById('mbox').value">>></a><br /><span class="arcform-text" ng-bind="current_Lang=='ga' ? 'Cuardach: Teideal an Cheoil' : 'Search: Music Title'"></span></div>
            <div class="arcform-wrap"><input ng-cloak class="arcform-textbox" name="pbox" id="pbox" ng-model="ps" placeholder="{{current_Lang=='ga'?' Rannpháirtí...':' Participants...'}}" ng-change="ss1=noFada(ps);" /><a href="#" id="srchPartBtn" class="arcform-btn" onclick="this.href='?srchPart=' + document.getElementById('pbox').value">>></a><br /><span class="arcform-text" ng-bind="current_Lang=='ga' ? 'Cuardach: Rannpháirtí' : 'Search: Participants'"></span></div>
            <div class="arcform-wrap">
                <select ng-model="yearSel" style="margin-right:10px; width:148px; height:27px;">
                    <option value="">{{current_Lang=='ga'?' Gach Bliain...':' All Years...'}}</option>
                    <option value="2018">2018</option>
                    <option value="2017">2017</option>
                    <option value="2016">2016</option>
                    <option value="2015">2015</option>>
                    <option value="2014">2014</option>
                    <option value="2013">2013</option>>
                    <option value="2012">2012</option>
                    <option value="2011">2011</option>>
                    <option value="2010">2010</option>
                    <option value="2009">2009</option>
                    <option value="2008">2008</option>
                    <option value="2007">2007</option>
                    <option value="2006">2006</option>
                    <option value="2005">2005</option>
                    <option value="2004">2004</option>
                    <option value="2003">2003</option>
                    <option value="2002">2002</option>
                    <option value="2001">2001</option>
                    <option value="2000">2000</option>
                    <option value="1999">1999</option>
                    <option value="1998">1998</option>
                </select><!-- <span class="arcform-text" ng-bind="current_Lang=='ga' ? 'Cuardach: Bliain' : 'Search: Year'"></span> -->
            </div>

            <script>
            var input1 = document.getElementById("sbox");
            input1.addEventListener("keyup", function(event) {
                event.preventDefault();
                if (event.keyCode === 13) {
                    document.getElementById("srchCompBtn").click();
                }
            });
            var input2 = document.getElementById("mbox");
            input2.addEventListener("keyup", function(event) {
                event.preventDefault();
                if (event.keyCode === 13) {
                    document.getElementById("srchMusicBtn").click();
                }
            });
            var input3 = document.getElementById("pbox");
            input3.addEventListener("keyup", function(event) {
                event.preventDefault();
                if (event.keyCode === 13) {
                    document.getElementById("srchPartBtn").click();
                }
            });
            /* var input4 = document.getElementById("ybox");
            input4.addEventListener("keyup", function(event) {
                event.preventDefault();
                if (event.keyCode === 13) {
                    document.getElementById("srchYearBtn").click();
                }
            }); */
            </script>
        </div>
    </div>
</div>

<section class="player-mods" style="padding-bottom:20px;">
    <h2 class="visuallyhidden">More on TG4</h2>
    <div class="plyrmods-wrapper">
        <!-- PHP code to pull the info back from the querystring - Genre, Alphbetical and Search term -->
        <?php
        $pageCnt = ctype_digit($_GET['pageCnt']) ? $_GET['pageCnt'] : 1;
        $srchYear = $_GET['srchYear'];
        $srchComp = $_GET['srchComp'];
        $srchTxt = $_GET['srchTxt'];
        $srchMusic = $_GET['srchMusic'];
        $srchPart = $_GET['srchPart'];

        //Check for the search term in querystring
        /* if (isset($srchYear)) {
            $srchQuery = "&srchYear=" . $srchYear;
            $listTitle = $srchYear;
        } */

        if (isset($srchComp)) {
            $srchQuery = "&srchComp=" . $srchComp;
            $listTitle = $srchComp;
        }

        if (isset($srchTxt)) {
            $srchQuery = "&srchTxt=" . $srchTxt;
            $listTitle = $srchTxt;
        }

        if (isset($srchMusic)) {
            $srchQuery = "&srchMusic=" . $srchMusic;
            $listTitle = $srchMusic;
        }
        
        if (isset($srchPart)) {
            $srchQuery = "&srchPart=" . $srchPart;
            $listTitle = $srchPart;
        }
        
        if (isset($srchYear)) {
            $srchQuery = "&srchYear=" . $srchYear;
            $listTitle = $srchYear;
        }
        ?>

        <!-- Jquery Paging -->
        <div class="container">
            <script src='https://d1og0s8nlbd0hm.cloudfront.net/js/min/jquery.bootpag.min.js'></script>
            <p id="pagination-here"></p>
        </div>

        <div class="player-mod-wrap">
            <?php if (isset($srchTxt) && ($srchTxt != "") || isset($srchMusic) && ($srchMusic != "") || isset($srchPart) && ($srchPart != "") || isset($srchYear) && ($srchYear != "")) { ?>
                <div style="margin:10px 0 20px;"><a href="<?php echo get_permalink($post->post_id); ?>" style="border:1px solid #3e8eb4; border-radius:4px; background:#ddd; padding:5px;"><?php echo str_replace("\'", "’", $listTitle); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;x</a></div>
            <?php } ?>
            <!-- Programme content added into boxes below -->
            <section class="mod-1" ng-if="p.prog_dynamic_branding_title.toLowerCase().indexOf('gradam ceoil') > -1" ng-repeat="p in (pAll | orderBy:'-prog_production_year') | limitTo:20 | filter:(!!yearSel || undefined) && {prog_production_year:yearSel}">
                <div class="player-mod-notice" ng-if="p.prog_dynamic_branding_title.toLowerCase().indexOf('gradam ceoil') > -1">
                    <a ng-cloak href="<?php echo site_url(); ?>{{current_Lang=='ga'?'/ga/clair/gradam-ceoil/cartlann/seinnteoir/?mat_id='+p.mat_id+'&teideal='+p.prog_dynamic_branding_title+'&srchTxt=<?php echo $listTitle; ?>':'/en/programmes/gradam-ceoil/archive/player/?mat_id='+p.mat_id+'&teideal='+p.prog_dynamic_branding_title+'&srchTxt=<?php echo $listTitle; ?>'}}" class="prog-panel">
                        <h3 class="player-title" ng-bind-html="highlight(p.prog_dynamic_branding_title.length>0?p.prog_dynamic_branding_title:p.prog_title, '<?php echo $listTitle; ?>')"></h3>
                        <img ng-cloak ng-src="https://d2m9n7jejbgumr.cloudfront.net/{{p.mat_id}}/{{p.mat_id}}_640x360.jpg" alt="{{p.prog_dynamic_branding_title}}" class="prog-img">
                        <div class="prog-footer">
                            <!-- <h4 class="prog-episode-title" ng-bind="p.prog_dynamic_branding_title!=p.series_branding_title?p.prog_dynamic_branding_title:''"></h4>
                            <div class="prog-season"><span ng-bind="p.prog_first_tx_date | date: 'yyyy'"></span></div> -->
                            <div class="prog-season"><span ng-bind="p.prog_production_year"></span></div>
                            <div class="arrow-box"></div>
                        </div>
                        <div class="prog-infobar">
                            <!-- <div class="prog-firstshow" ng-bind="(current_Lang=='ga'?'Chéad Clár: ':'First Shown: ') + (p.prog_first_tx_date | date:'dd.MM.yy')"></div>
                            <div ng-if="p.file_duration!=''" class="prog-dur"><span ng-bind="(current_Lang=='ga'?'Fad: ':'Dur: ') + (p.file_duration.substr(0,8))"></span></div> -->
                            <!-- <div class="prog-firstshow"><span ng-bind="p.prog_production_year"></span></div>
                            <div class="prog-dur"><span ng-bind="p.mat_id"></span></div> -->
                        </div>
                        <div ng-if="current_Lang=='ga'" class="prog-desc"><span ng-bind-html="highlight(p.prog_press_desc_short_gae.substr(0,220)+'...', '<?php echo $srchTxt; ?>')"></span></div>
                        <div ng-if="current_Lang!='ga'" class="prog-desc"><span ng-bind-html="highlight(p.prog_press_desc_short_eng.substr(0,220)+'...', '<?php echo $srchTxt; ?>')"></span></div>
                    </a>
                </div>
            </section>
            <div ng-hide="pAll.length" ng-bind="current_Lang=='ga'?'Níl aon fiseáin ar fáil.':'No videos available.'" style="margin-bottom:20px;"></div>
        </div>

        <!-- Angular -->
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular-touch.min.js"></script>
        <!-- Angular -->

        <script>
            /* var replaceChars={ "'":" " , "’":" AND " };
            console.log("Cobbler's Polka".replace("'s ", "’s AND "));
            console.log("Downey's Polka".replace(/'|_/g,function(match) {return (match=="'")?"’":" ";}));
            console.log("Downey's Polka".replace(/#|_/g,function(match) {return replaceChars[match];})); */
            var app=angular.module('tg4App',[]);
            app.controller('tg4Ctrl', function($scope, $http, $sce) {
                $scope.current_Lang='<?php echo ICL_LANGUAGE_CODE ?>';
                $scope.total = 0;
                $scope.currentPage = <?php echo $pageCnt; ?>;

                $scope.noFada = function(value){ 
                    return value.toLowerCase().replace(/á/g,'a').replace(/é/g,'e').replace(/í/g,'i').replace(/ó/g,'o').replace(/ú/g,'u'); 
                };

                //* Check querystring parameters for Archive Genre, Page Count, Alphabetical link & Search text
                var params={};
                decodeURIComponent(location.search).replace(/[?&]+([^=&]+)=([^&]*)/gi,function(m,k,v){ params[k]=v });
                //* Check querystring parameters for Page Count, if empty load 1st page
                if (!params.pageCnt) { 
                    params.pageCnt=1;
                }

                //* Check querystring parameters for Search entered
                /* if (!params.srchYear) { 
                    params.srchYear='';
                } */

                if (!params.srchComp) { 
                    params.srchComp='';
                }

                if (!params.srchTxt) { 
                    params.srchTxt='';
                }

                if (!params.srchMusic) { 
                    params.srchMusic='';
                } /* else { 
                    params.srchMusic=srchMusic.replace(" ", " AND ");
                } */

                if (!params.srchPart) { 
                    params.srchPart='';
                } /* else { 
                    params.srchPart=srchPart.replace(" ", " AND ");
                } */

                if (!params.srchYear) { 
                    params.srchYear='';
                }

                //console.log(params.srchYear);

                //* Calls to the API - if genre selected bring back genre listing, else display listing alphabetically
                if (params.srchComp) {
                    $http.post('https://www.tg4tech.com/api/programmes/basicSearch?q='+params.srchComp+'&searchfield=prog_production_company&orderon=prog_vod_category&orderby=desc&page='+params.pageCnt).then(function(d){
                        $scope.allDataLoaded(d.data);
                        $scope.total = d.data.total_count;

                        $scope.pageTotal = Math.ceil($scope.total/20);
                        console.log("Total Pages ", $scope.pageTotal);

                        $(document).ready(function () {
                            $('#pagination-here').bootpag({
                                total: $scope.pageTotal,
                                page: <?php echo $pageCnt; ?>,
                                maxVisible: 20,
                                leaps: false,
                                firstLastUse: true,
                                first: '←',
                                last: '→'
                            }).on('page', function(event, num){
                                window.location.href = '?pageCnt=' + num + '<?php echo $srchQuery; ?>';
                            });
                        });
                    });
                } else if (params.srchYear) {
                    $http.post('https://www.tg4tech.com/api/programmes/basicSearch?q='+params.srchYear+'&searchfield=prog_production_year&orderon=prog_dynamic_branding_title&orderby=asc&page='+params.pageCnt).then(function(d){
                        $scope.allDataLoaded(d.data);
                        $scope.total = d.data.total_count; 

                        $scope.pageTotal = Math.ceil($scope.total/20);
                        console.log("Total ", $scope.total, "Pages ", $scope.pageTotal);

                        $(document).ready(function () {
                            $('#pagination-here').bootpag({
                                total: $scope.pageTotal,
                                page: <?php echo $pageCnt; ?>,
                                maxVisible: 20,
                                leaps: false,
                                //href: "?pageCnt={{number}}<?php echo $sortQuery; ?>",
                                firstLastUse: true,
                                first: '←',
                                last: '→'
                            }).on('page', function(event, num){
                                window.location.href = '?pageCnt=' + num + '<?php echo $srchQuery; ?>';
                            });
                        });
                    });
                } else if (params.srchTxt) {
                    $http.post('https://www.tg4tech.com/api/programmes/_search?q=Gradam%20Ceoil '+params.srchTxt+' AND gradam&fields=prog_title,prog_production_company,prog_production_year,prog_coord,prog_press_desc_short_eng,prog_press_desc_short_gae,mat_id,prog_dynamic_branding_title,series_branding_title,prog_first_tx_date,prog_vod_category,file_duration,scene_track&limit=50&orderon=prog_vod_category&orderby=desc&page='+params.pageCnt).then(function(d){
                        $scope.allDataLoaded(d.data); 
                        $scope.total = d.data.total_count; 

                        $scope.pageTotal = Math.ceil($scope.total/20);
                        console.log("Total Pages ", $scope.pageTotal);

                        $(document).ready(function () {
                            $('#pagination-here').bootpag({
                                total: $scope.pageTotal,
                                page: <?php echo $pageCnt; ?>,
                                maxVisible: 20,
                                leaps: false,
                                firstLastUse: true,
                                first: '←',
                                last: '→'
                            }).on('page', function(event, num){
                                window.location.href = '?pageCnt=' + num + '<?php echo $srchQuery; ?>';
                            });
                        });
                    });
                } else if (params.srchMusic) {
                    $http.post('https://www.tg4tech.com/api/programmes/_search?q=Gradam%20Ceoil '+params.srchMusic+' AND gradam&fields=prog_title,scene_track&limit=50&orderon=prog_vod_category&orderby=desc&page='+params.pageCnt).then(function(d){
                        //console.log('POC', d.data);
                        $scope.allDataLoaded(d.data); 
                        $scope.total = d.data.total_count; 

                        $scope.pageTotal = Math.ceil($scope.total/20);
                        console.log("Total Pages ", $scope.pageTotal);

                        $(document).ready(function () {
                            $('#pagination-here').bootpag({
                                total: $scope.pageTotal,
                                page: <?php echo $pageCnt; ?>,
                                maxVisible: 20,
                                leaps: false,
                                firstLastUse: true,
                                first: '←',
                                last: '→'
                            }).on('page', function(event, num){
                                window.location.href = '?pageCnt=' + num + '<?php echo $srchQuery; ?>';
                            });
                        });
                    });
                } else if (params.srchPart) {
                    $http.post('https://www.tg4tech.com/api/programmes/_search?q=Gradam%20Ceoil '+params.srchPart+' AND gradam&fields=prog_title,prog_production_company,prog_production_year,prog_coord,prog_press_desc_short_eng,prog_press_desc_short_gae,mat_id,prog_dynamic_branding_title,series_branding_title,prog_first_tx_date,prog_vod_category,file_duration,scene_track&limit=50&orderon=prog_vod_category&orderby=desc&page='+params.pageCnt).then(function(d){
                        $scope.allDataLoaded(d.data); 
                        $scope.total = d.data.total_count; 

                        $scope.pageTotal = Math.ceil($scope.total/20);
                        console.log("Total Pages ", $scope.pageTotal);

                        $(document).ready(function () {
                            $('#pagination-here').bootpag({
                                total: $scope.pageTotal,
                                page: <?php echo $pageCnt; ?>,
                                maxVisible: 20,
                                leaps: false,
                                firstLastUse: true,
                                first: '←',
                                last: '→'
                            }).on('page', function(event, num){
                                window.location.href = '?pageCnt=' + num + '<?php echo $srchQuery; ?>';
                            });
                        });
                    });
                } else {
                    $http.post('https://www.tg4tech.com/api/programmes/basicSearch?q=Gradam%20Ceoil&searchfield=prog_dynamic_branding_title&orderon=prog_first_tx_date&orderby=desc&page='+params.pageCnt).then(function(d){
                        $scope.allDataLoaded(d.data);
                        $scope.total = d.data.total_count;

                        $scope.pageTotal = Math.ceil($scope.total/20);
                        console.log("Total Pages ", $scope.pageTotal);

                        $(document).ready(function () {
                            $('#pagination-here').bootpag({
                                total: $scope.pageTotal,
                                page: <?php echo $pageCnt; ?>,
                                maxVisible: 20,
                                leaps: false,
                                //href: "?pageCnt={{number}}<?php echo $sortQuery; ?>",
                                firstLastUse: true,
                                first: '←',
                                last: '→'
                            }).on('page', function(event, num){
                                window.location.href = '?pageCnt=' + num + '<?php echo $srchQuery; ?>';
                            });
                        });
                    });
                }

                $scope.highlight = function(haystack, needle) {
                    if(!needle) {
                        return $sce.trustAsHtml(haystack);
                    }
                    return $sce.trustAsHtml(haystack.replace(new RegExp(needle, "gi"), function(match) {
                        return '<span class="highlightedText">' + match + '</span>';
                    })
                    );
                };

                $scope.allDataLoaded = function(da){
                    $scope.pAll=da.results;
                    console.log('Results - ', $scope.pAll);
                }
            });
        </script>
        <div class="gradam-feat-file mod-file">
            <div class="file-content-wrap">
                <div class="file-btn"><a href="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2018/06/<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Gradam-Ceoil-GA.pdf' : 'Gradam-Ceoil-EN.pdf'); ?>" class="mod-file" target="_blank"><div class="file-btnImg"></div></a></div>
                    <div class="file-content">
                        <a href="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2018/06/<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Gradam-Ceoil-GA.pdf' : 'Gradam-Ceoil-EN.pdf'); ?>" class="mod-file" target="_blank"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Gradam Ceoil - Cúlra' : 'Gradam Ceoil - Background'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="player-mods" style="padding-bottom:20px;">
    <div class="plyrmods-wrapper">
        <div class="disc-wrapper">
            <?php if (ICL_LANGUAGE_CODE == "ga") { ?>
                <p><strong>Is de thoradh comhthionscadail idir TG4 agus Taisce Cheol Dúchais Éireann an suíomh seo.</strong></p>
            <?php } else { ?>
                <p><strong>This site is the result of a collaborative project between TG4 and The Irish Traditional Music Archive.</strong></p>
            <?php } ?>
        </div>
        <div style="width:50%; float:left;"><img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2018/07/TG4-Logo-2.png" alt="TG4 Logo"></a></div>
        <div style="width:50%; float:right;"><a href="http://www.itma.ie" target="_blank"><img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2018/07/ITMA-Logo-1.png" alt="ITMA Logo"></a></div>
    </div>
</section>

<?php get_footer(); ?>