<?php /* Template Name: Gradam Ceoil - Player */ ?>

<?php include(locate_template('/header-angular.php')); ?>

<div class="section-header">
	<h1 class="section-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Seinnteoir: Gradam Ceoil TG4' : 'Player: Gradam Ceoil TG4'); ?></h1>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
    <div class="section-submenu-wrap">
        <ul class="section-submenu-list">
            <?php
            $ancestors = get_post_ancestors($post->ID);
            //echo $post->ID . " - " . $post->post_parent;
            if ($ancestors[2] == 137 || $ancestors[2] == 140) {
                wp_list_pages('sort_column=menu_order&title_li=&child_of='. $ancestors[1] . '&depth=0');
            } else {
                if ($post->post_parent != 137 && $post->post_parent != 140 && $post->post_parent != 390 && $post->post_parent != 393 || ($ancestors[2] == 137 && $ancestors[2] == 140)) {
                    wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=0');
                } elseif ($post->ID != 137 && $post->ID != 140) {
                    wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->ID . '&depth=0');
                }
            }
            ?>
            &nbsp;
        </ul>
    </div>
</div>

<div class="srch-feat-intro-wrap">
    <div class="center-panel">
        <div class="srch-feat-intro">
            <div class="srchSel-wrap">
                <select ng-cloak id="sSel" name="page-dropdown" ng-model="flt" ng-init="flt='All'" onchange="document.location.href=this.options[this.selectedIndex].value;"> 
                    <option value="All" disabled>{{current_Lang=='ga'?'Rangaigh...':'Sort By...'}}</option>
                    <option value='?progSort=RecentDesc'>{{current_Lang=='ga'?'Is Déanaí':'Latest Video'}}</option>
                    <option value='?progSort=RecentAsc'>{{current_Lang=='ga'?'Is Luaithe':'Earliest Video'}}</option>
                    <option value='?progSort=PopDesc'>{{current_Lang=='ga'?'Is Mo Tóir':'Most Views'}}</option>
                    <option value='?progSort=PopAsc'>{{current_Lang=='ga'?'Is lú Tóir':'Least Views'}}</option>
                </select>
            </div>
            <!-- <div><input ng-cloak class="srchText" name="sbox" id="sbox" ng-model="ss" placeholder="{{current_Lang=='ga'?' Cuardaigh gach Fiseáin':' Search all Videos'}}" ng-change="ss1=noFada(ss);" ng-blur="searchPI();"/></div> -->
        </div>
    </div>
</div>

<section class="player-mods" ng-app="tg4App">
    <h2 class="visuallyhidden">More on TG4</h2>
    <div class="plyrmods-wrapper">
        <!-- PHP code to pull the info back from the querystring - Page Count, Sort... -->
        <?php
        $pageCnt = ctype_digit($_GET['pageCnt']) ? $_GET['pageCnt'] : 1;
        $pageOffset = ($pageCnt-1)*5;
        $progSort = $_GET['progSort'];

        //Check for the programme sort in querystring
        if (isset($progSort)) {
            $sortQuery = "&progSort=" . $progSort;
            $listTitle = $progSort;
        }
        ?>

        <!-- Jquery Paging -->
        <div class="container">
            <script src='https://d1og0s8nlbd0hm.cloudfront.net/js/min/jquery.bootpag.min.js'></script>
            <p id="pagination-here"></p>
        </div>

        <div class="player-mod-wrap">
            <!-- Programme content added into boxes below -->
            <section class="mod-1" ng-repeat="p in allProg=(pAll) | limitTo:20"> <!--track by p.id"-->
                <div class="player-mod-notice">
                    <a ng-cloak href="{{current_Lang=='ga'?'<?php echo site_url(); ?>/ga/player/baile/?pid='+p.id+'&teideal='+p.custom_fields.title+'&series='+p.custom_fields.seriestitle+'&dlft='+diffDate((p.schedule.ends_at | date : 'yyyy-MM-dd HH:mm:ss'))+'':'<?php echo site_url(); ?>/en/player/home/?pid='+p.id+'&teideal='+p.custom_fields.title+'&series='+p.custom_fields.seriestitle+'&dlft='+diffDate((p.schedule.ends_at | date : 'yyyy-MM-dd HH:mm:ss'))+''}}" class="prog-panel">
                        <h3 class="player-title" ng-bind="p.custom_fields.seriestitle.length>0?p.custom_fields.seriestitle:p.custom_fields.title"></h3>
                        <span><img ng-cloak ng-src="{{p.images.poster.sources[0].src}}" alt="{{p.custom_fields.seriestitle}}" class="prog-img"></span>
                        <!-- <img ng-src="{{p.custom_fields.seriesimgurl}}" alt="{{p.custom_fields.seriestitle}}" class="prog-img"> -->
                        <div class="prog-footer">
                            <h4 class="prog-episode-title" ng-bind="p.custom_fields.title!=p.custom_fields.seriestitle?p.custom_fields.title:''"></h4>
                            <div class="prog-season">S<span ng-bind="p.custom_fields.series"></span></div>
                            <div class="prog-episode">E<span ng-bind="p.custom_fields.episode"></span></div>
                            <div class="prog-episode"><span ng-bind="p.custom_fields.parental_guide"></span></div>
                            <div class="arrow-box"></div>
                        </div>
                        <div class="prog-infobar">
                            <div class="prog-firstshow">{{current_Lang=='ga'?'Chéad Clár: '+p.custom_fields.date:'First Shown: '+p.custom_fields.date}}</div>
                            <div class="prog-dur"><span ng-bind="p.duration | date : 'm' "></span> <span ng-bind="current_Lang=='ga'?'Nóim':'Mins'"></span></div>
                            <div ng-if="diffDate((p.schedule.ends_at | date : 'yyyy-MM-dd HH:mm:ss')) > 35" class="prog-daysleft"> <span ng-bind="current_Lang=='ga'?'Gan Srian':'Unlimited'"></span></div>
                            <div ng-if="diffDate((p.schedule.ends_at | date : 'yyyy-MM-dd HH:mm:ss')) <= 35" class="prog-daysleft"><span ng-bind="diffDate((p.schedule.ends_at | date : 'yyyy-MM-dd HH:mm:ss'))"></span> <span ng-bind="current_Lang=='ga'?'Lá Fagtha':'Days Left'"></span></div>
                        </div>
                        <div class="prog-desc"><span ng-bind="current_Lang=='ga'?(p.custom_fields.longdescgaeilge).substr(0,250)+'...':p.description"></span></div>
                    </a>
                </div>
            </section>
            <div ng-hide="allProg.length"><h2><i class="fa fa-refresh fa-spin"></i>&nbsp;&nbsp;&nbsp;<span ng-bind="current_Lang=='ga'?'Ag Lódáil...':'Loading...'"></h2></div>
        </div>

        <!-- Angular -->
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular-touch.min.js"></script>
        <!-- Angular -->

        <script>
        var app=angular.module('tg4App',[]);
        var tg4App=angular.module("tg4App", []);
        tg4App.controller('tg4Ctrl', function($scope,$http) {
            $scope.current_Lang='<?php echo ICL_LANGUAGE_CODE ?>';
            $scope.total = 0;
            $scope.currentPage = <?php echo $pageCnt; ?>;

            //* Check querystring parameters for Sort, Page Count...
            var params={};
            decodeURIComponent(location.search).replace(/[?&]+([^=&]+)=([^&]*)/gi,function(m,k,v){ params[k]=v });
            //* Check querystring parameters for Sort
            if (!params.progSort) { 
                params.progSort='-schedule_starts_at';
            }
            if (params.progSort=='RecentAsc') { 
                params.progSort='schedule_starts_at';
            }
            if (params.progSort=='RecentDesc') { 
                params.progSort='-schedule_starts_at';
            }
            if (params.progSort=='PopAsc') { 
                params.progSort='plays_total';
            }
            if (params.progSort=='PopDesc') { 
                params.progSort='-plays_total';
            }
            if (params.progSort=='AZ') { 
                params.progSort='name';
            }
            if (params.progSort=='ZA') { 
                params.progSort='-name';
            }
            //* Check querystring parameters for Page Count, if empty load 1st page
            if (!params.pageCnt) { 
                params.pageCnt=1;
            }

            $http.post("<?php echo get_template_directory_uri(); ?>/assets/php/tg4-proxy.php").then(function(response) {
                $scope.tAuth = response.data;
                //console.log($scope.tAuth);
                if ($scope.tAuth) {
                    var headerParam = { headers: {
                        "Authorization": "Bearer " + $scope.tAuth,
                        "Accept": "application/json;odata=verbose"
                        }
                    };

                    endPointCnt = '/counts/videos?q=%2Bseriestitle:"Gradam Ceoil TG4"+%2Bplayable:true';
                    $http.get('https://cms.api.brightcove.com/v1/accounts/1555966122001' + endPointCnt, headerParam).then(function(cnt) {
                        $scope.pTotal = cnt.data;
                        $scope.total = cnt.data.count;
                        console.log($scope.total);
                    });

                    var endPoint = '/videos?q=%2Bseriestitle:"Gradam Ceoil TG4"+%2Bplayable:true&limit=20&offset=<?php echo $pageOffset; ?>&sort='+params.progSort;
                    $http.get('https://cms.api.brightcove.com/v1/accounts/1555966122001' + endPoint, headerParam).then(function(d) {
                        $scope.pAll = d.data;
                        $scope.pCnt = d.data.length;
                        console.log($scope.pAll);
                    });
                    
                    $scope.pageTotal = Math.ceil($scope.total/5);
                    console.log($scope.pageTotal);

                    $(document).ready(function () {
                        $('#pagination-here').bootpag({
                            total: $scope.pageTotal,
                            page: <?php echo $pageCnt; ?>,
                            maxVisible: 5,
                            leaps: false,
                            firstLastUse: true,
                            first: '←',
                            last: '→'
                        }).on('page', function(event, num){
                            window.location.href = '?pageCnt=' + num + '<?php echo $sortQuery; ?>';
                        });
                    });
                }
            });

            $scope.diffDate = function(date1){
                var dateOut1 = new Date(Date.parse(date1, "yyyy-MM-dd HH:mm:ss")); // It will work if date1 is in ISO format
                var dateOut2 = new Date(); // Today's date
                var timeDiff = Math.abs(dateOut1.getTime() - dateOut2.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                return diffDays;
            };
        });
        </script>
    </div>
</section>

<?php get_footer(); ?>