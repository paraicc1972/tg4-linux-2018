<?php /* Template Name: Player Homepage - New Design */ ?>

<?php include(locate_template('/header-angular-nd.php')); ?>

<section class="plyr-headimg" style="background-image: url(https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2015/08/7_La_2015.jpg);">
</section>

<section class="player-mods-nd">
    <div class="plyrmods-wrapper-nd">
        <div class="prog-feat-ad-plyr mod-ad">
            <div class="ad-wrapper"><img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2017/06/ad-test-1.gif" width="300" height="250"></div>
        </div>
        <div class="player-mod-wrap-nd">
            <!-- Programme content added into boxes below -->
            <section class="mod-1" ng-repeat="p in allProg=(pAll) | limitTo:6"> <!--track by p.id"-->
                <div class="player-mod-block">
                    <a ng-cloak href="<?php echo site_url(); ?>{{current_Lang=='ga'?'/ga/player/baile/?pid='+p.id+'&teideal='+p.custom_fields.title+'&series='+p.custom_fields.seriestitle+'&dlft='+diffDate((p.schedule.ends_at | date : 'yyyy-MM-dd HH:mm:ss'))+'':'/en/player/home/?pid='+p.id+'&teideal='+p.custom_fields.title+'&series='+p.custom_fields.seriestitle+'&dlft='+diffDate((p.schedule.ends_at | date : 'yyyy-MM-dd HH:mm:ss'))+''}}" class="prog-panel">
                        <img ng-src="{{p.custom_fields.seriesimgurl.length>0?p.custom_fields.seriesimgurl:p.images.poster.sources[0].src}}" alt="{{p.custom_fields.seriestitle}}" class="prog-img">
                        <div class="prog-desc-plyr">
                            <!-- <p class="plyr-series-title" ng-bind="p.custom_fields.category_c"></p> -->
                            <p class="plyr-series-title" ng-if="p.custom_fields.category_c=='Ceol'">CEOL</p>
                            <p class="plyr-series-title" ng-if="p.custom_fields.category_c=='Cula4'">DAOINE ÓGA</p>
                            <p class="plyr-series-title" ng-if="p.custom_fields.category_c=='Cursai Reatha'">CÚRSAÍ REATHA</p>
                            <p class="plyr-series-title" ng-if="p.custom_fields.category_c=='Drama'">DRAMAÍOCHT</p>
                            <p class="plyr-series-title" ng-if="p.custom_fields.category_c=='Faisneis'">FAISNÉIS</p>
                            <p class="plyr-series-title" ng-if="p.custom_fields.category_c=='Siamsaiocht'">SIAMSAÍOCHT</p>
                            <p class="plyr-series-title" ng-if="p.custom_fields.category_c=='Sport'">SPÓRT</p>
                            <p class="plyr-series-title" ng-if="p.custom_fields.category_c=='Saolchlar'">SAOLCHLÁIR</p>
                            <p class="plyr-episode-title" ng-bind="p.custom_fields.seriestitle.length>0?p.custom_fields.seriestitle:p.custom_fields.title"></p>
                            <!-- <p class="plyr-episode-title" ng-bind="p.custom_fields.title!=p.custom_fields.seriestitle?p.custom_fields.title:'&nbsp;'"></p> -->
                            <div class="plyr-episode-info">S<span ng-bind="p.custom_fields.series"></span> E<span ng-bind="p.custom_fields.episode"></span></div>
                            <div class="plyr-episode-date" ng-bind="p.custom_fields.date"></div>
                        </div>
                    </a>
                </div>
            </section>
        </div>

        <!-- Angular -->
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular-touch.min.js"></script>
        <!-- Angular -->

        <script>
        var app=angular.module('tg4App',[]);
        var tg4App=angular.module("tg4App", []);
        tg4App.controller('tg4Ctrl', function($scope,$http) {
            $scope.current_Lang='<?php echo ICL_LANGUAGE_CODE ?>';
            $scope.total = 0;

            $http.post("<?php echo get_template_directory_uri(); ?>/assets/php/tg4-proxy.php").then(function(response) {
                $scope.tAuth = response.data;
                //console.log($scope.tAuth);
                if ($scope.tAuth) {
                    var headerParam = { headers: {
                        "Authorization": "Bearer " + $scope.tAuth,
                        "Accept": "application/json;odata=verbose"
                        }
                    };

                    var endPoint1 = '/videos?q=%2Dseriestitle:"TG4-Beo"+%2Btags:"xfeature"+%2Bplayable%3Atrue&sort=-schedule_starts_at';
                      $http.get('https://cms.api.brightcove.com/v1/accounts/1555966122001' + endPoint1, headerParam).then(function(d) {
                          $scope.pAll = d.data;
                          $scope.pCntAll = d.data.length;
                          console.log($scope.pAll);
                          console.log($scope.pCntAll);
                      });
                    
                    $scope.pageTotal = Math.ceil($scope.total/5);
                    console.log($scope.pageTotal);

                    $(document).ready(function () {
                        $('#pagination-here').bootpag({
                            total: $scope.pageTotal,
                            page: <?php echo $pageCnt; ?>,
                            maxVisible: 5,
                            leaps: false,
                            firstLastUse: true,
                            first: '←',
                            last: '→'
                        }).on('page', function(event, num){
                            window.location.href = '?pageCnt=' + num + '<?php echo $sortQuery; ?>';
                        });
                    });
                }
            });

            $scope.diffDate = function(date1){
                var dateOut1 = new Date(Date.parse(date1, "yyyy-MM-dd HH:mm:ss")); // It will work if date1 is in ISO format
                var dateOut2 = new Date(); // Today's date
                var timeDiff = Math.abs(dateOut1.getTime() - dateOut2.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                return diffDays;
            };
        });
        </script>
    </div>
</section>

<?php get_footer(); ?>