<?php /* Template Name: BLOC */ ?>

<?php get_header(); ?>

<div class="section-header-bloc">
	<div class="prog-feat center-panel">
		<div class="prog-feat-wrap"><img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2018/05/BLOC-Logo-1.png" alt="BLOC Logo" title="BLOC Logo"></div>
	</div>
</div>

<section class="prog-feat-section">
	<div class="section-panel-white">
	    <div class="prog-feat center-panel">
	    	<div class="bloc-wrap">
	    		<?php
	    		echo apply_filters('the_content', get_post_field('post_content', $post_id));

	    		if (get_field("youtube_video") != '') { ?>
					&nbsp;
					<div class="video-container-yt">
						<iframe src="<?php echo get_field("youtube_video"); ?>?rel=0;&autoplay=1" width="800" height="450" frameborder="0" allowfullscreen></iframe>
					</div>
				<?php } elseif (get_field("promo_video")) { ?>
					&nbsp;
					<div class="video-container-yt">
						<video id="PromoVideo" class="vjs-big-play-centered" controls preload="auto" width="100%" height="auto" poster="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2019/07/Blocadoiri.jpg">
							<source src="<?php echo get_field("promo_video"); ?>" type='video/mp4' />
							<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
						</video>
					</div>
				<?php } ?>
	    	</div>
	    </div>
	</div>
</section>

<?php get_footer(); ?>