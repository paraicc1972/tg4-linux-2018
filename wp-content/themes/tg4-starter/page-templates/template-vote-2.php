<?php /* Template Name: Vóta 2 */ ?>

<?php get_header(); ?>

<script>
$(function() {
    $('#vertScroller').vTicker({
    speed: 700,
    pause: 4000,
    showItems: 1,
    mousePause: true,
    height: 0,
    animate: true,
    margin: 0,
    padding: 0,
    startPaused: false});
});
</script>

<div class="section-header">
	<h1 class="section-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Vóta 2016' : 'Vóta 2016'); ?></h1>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
    <div class="section-submenu-wrap">
        <ul class="section-submenu-list">
            <?php wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=1'); ?>
            &nbsp;
        </ul>
    </div>
</div>

<div class="electionseats-wrap">
    <div class="center-panel">
        <?php
            // Begin Snippet - Pull back Seats Filled results in the light blue bar at top of the National Share & Changes from 2011 pages
            $filename1 = "http://direct.tg4.ie/election/ElectionData/xml/PublishingNationalSummary.xml";
            $file_headers = @get_headers($filename1);
            if ($file_headers[0] == 'HTTP/1.1 200 OK') {
                echo "<p><strong>" . (ICL_LANGUAGE_CODE == "ga" ? 'Suíocháin Líonta' : 'Seats Filled') . "</strong></p>";
                $xml=simplexml_load_file($filename1) or die("Error: Cannot create object");

                foreach($xml->SUMMARY->PARTIES->children() as $partyResult) {
                    if ($partyResult->PARTY_MNEMONIC != "MISC") {
                        if ($partyResult->PARTY_MNEMONIC == "AAA-PBP") {
                            $partyResult->PARTY_MNEMONIC = "AP";
                        }
                        if (ICL_LANGUAGE_CODE == "ga") {
                            if ($partyResult->PARTY_MNEMONIC == "LAB") {
                                $partyResult->PARTY_MNEMONIC = "LO";
                            }
                            if ($partyResult->PARTY_MNEMONIC == "GP") {
                                $partyResult->PARTY_MNEMONIC = "CG";
                            }
                            if ($partyResult->PARTY_MNEMONIC == "IND") {
                                $partyResult->PARTY_MNEMONIC = "NS";
                            }
                            if ($partyResult->PARTY_MNEMONIC == "SD") {
                                $partyResult->PARTY_MNEMONIC = "DS";
                                $partyResult->PARTY_NAME = "Daonlathaigh Sóisialta";
                            }
                        }
                        echo "<div class='election-party-mnemonic-$partyResult->PARTY_MNEMONIC'>";
                        if ($partyResult->PARTY_MNEMONIC == "AP") { 
                            $partyResult->PARTY_MNEMONIC = "A/P";
                        }
                        echo $partyResult->PARTY_MNEMONIC . "<br />" . $partyResult->SEATS_WON;
                        echo "</div>";
                    }
                }
            }
            // End Snippet - Pull back Seats Filled results in the light blue bar at top of the National Share & Changes from 2011 pages
        ?>
    </div>
</div>

<?php
if (is_page('programmes/vota-2016/national-share/') ||  is_page('clair/vota-2016/national-share/')) {
    echo "<section class='constituency-feat-section'>";
    echo "<div class='section-panel-white'>";
    echo "<div class='constituency-feat center-panel'>";
    // Begin Snippet - Pull back National Share data in a Bar Chart on the National Share page
    echo "<div class='constituency-feat-wrap'>";
    echo "<a href='javascript:history.go(0)'><img src='https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2015/12/refresh-page-2.png' alt='Refresh'>&nbsp;&nbsp;&nbsp;" . (ICL_LANGUAGE_CODE == "ga" ? 'Cliceáil anso chun na torthaí is déanaí a fheiceáil' : 'Click here to view the latest results') . "</a>";
    $filename2 = "http://direct.tg4.ie/election/ElectionData/xml/PublishingNationalSummary.xml";
    $file_headers = @get_headers($filename2);
    if ($file_headers[0] == 'HTTP/1.1 200 OK') {
        echo "<h1>" . (ICL_LANGUAGE_CODE == "ga" ? '% Sciar Náisiúnta' : '% National Share') . "</h1>";
        $xml=simplexml_load_file($filename2) or die("Error: Cannot create object");
        //$count = 1;

        /* echo "<div id='canvas-holder'>\n";
        echo "<canvas id='chart-area' width='400' height='400'/>\n";
        echo "</div>\n"; */ //PIE_CHART

        if (ICL_LANGUAGE_CODE == "ga") {
            $BarLabel = "% Sciar";
        } else {
            $BarLabel = "% Share";
        }
        
        $partyInitial = "";
        $partyColour = "";
        $partyData = "";
        foreach($xml->SUMMARY->PARTIES->children() as $partyName) {
            if ($partyName->PARTY_MNEMONIC != "MISC") {
                if ($partyName->PARTY_MNEMONIC == "AAA-PBP") {
                    $partyName->PARTY_MNEMONIC = "A/P";
                }
                if (ICL_LANGUAGE_CODE == "ga") {
                    if ($partyName->PARTY_MNEMONIC == "LAB") {
                        $partyName->PARTY_MNEMONIC = "LO";
                    }
                    if ($partyName->PARTY_MNEMONIC == "GP") {
                        $partyName->PARTY_MNEMONIC = "CG";
                    }
                    if ($partyName->PARTY_MNEMONIC == "IND") {
                        $partyName->PARTY_MNEMONIC = "NS";
                    }
                    if ($partyName->PARTY_MNEMONIC == "SD") {
                        $partyName->PARTY_MNEMONIC = "DS";
                    }
                }
                $partyInitial = $partyInitial . "'" . $partyName->PARTY_MNEMONIC . "', ";
                $partyData = $partyData . $partyName->SHARE_OF_THE_VOTE . ", ";
                if ($partyName->PARTY_MNEMONIC == "FF") {
                    $partyColour = $partyColour . "'#18B75C',\n";
                } else if ($partyName->PARTY_MNEMONIC == "FG") {
                    $partyColour = $partyColour . "'#4450B7',\n";
                } else if ($partyName->PARTY_MNEMONIC == "LAB") {
                    $partyColour = $partyColour . "'#EE3A46',\n";
                } else if ($partyName->PARTY_MNEMONIC == "LO") {
                    $partyColour = $partyColour . "'#EE3A46',\n";
                } else if ($partyName->PARTY_MNEMONIC == "SF") {
                    $partyColour = $partyColour . "'#0D7572',\n";
                } else if ($partyName->PARTY_MNEMONIC == "IND") {
                    $partyColour = $partyColour . "'#FBB216',\n";
                } else if ($partyName->PARTY_MNEMONIC == "NS") {
                    $partyColour = $partyColour . "'#FBB216',\n";
                } else if ($partyName->PARTY_MNEMONIC == "IA") {
                    $partyColour = $partyColour . "'#F15A24',\n";
                } else if ($partyName->PARTY_MNEMONIC == "RN") {
                    $partyColour = $partyColour . "'#62CAE8',\n";
                } else if ($partyName->PARTY_MNEMONIC == "SD") {
                    $partyColour = $partyColour . "'#752F8B',\n";
                } else if ($partyName->PARTY_MNEMONIC == "DS") {
                    $partyColour = $partyColour . "'#752F8B',\n";
                } else if ($partyName->PARTY_MNEMONIC == "UL") {
                    $partyColour = $partyColour . "'#CE76A2',\n";
                } else if ($partyName->PARTY_MNEMONIC == "SP") {
                    $partyColour = $partyColour . "'#FF3E76',\n";
                } else if ($partyName->PARTY_MNEMONIC == "OTH") {
                    $partyColour = $partyColour . "'#999999',\n";
                } else if ($partyName->PARTY_MNEMONIC == "WP") {
                    $partyColour = $partyColour . "'#FF3E76',\n";
                } else if ($partyName->PARTY_MNEMONIC == "AP") {
                    $partyColour = $partyColour . "'#971A41',\n";
                } else if ($partyName->PARTY_MNEMONIC == "A/P") {
                    $partyColour = $partyColour . "'#971A41',\n";
                } else if ($partyName->PARTY_MNEMONIC == "AAA") {
                    $partyColour = $partyColour . "'#FAE800',\n";
                } else if ($partyName->PARTY_MNEMONIC == "PBP") {
                    $partyColour = $partyColour . "'#971A41',\n";
                } else if ($partyName->PARTY_MNEMONIC == "GP") {
                    $partyColour = $partyColour . "'#BDD86E',\n";
                } else if ($partyName->PARTY_MNEMONIC == "CG") {
                    $partyColour = $partyColour . "'#BDD86E',\n";
                } else if ($partyName->PARTY_MNEMONIC == "DDI") {
                    $partyColour = $partyColour . "'#87CEFA',\n";
                } else if ($partyName->PARTY_MNEMONIC == "CD") {
                    $partyColour = $partyColour . "'#8A8AE0',\n";
                } else if ($partyName->PARTY_MNEMONIC == "FN") {
                    $partyColour = $partyColour . "'#FF7F50',\n";
                } else if ($partyName->PARTY_MNEMONIC == "CPI") {
                    $partyColour = $partyColour . "'#E3170D',\n";
                } else if ($partyName->PARTY_MNEMONIC == "IDP") {
                    $partyColour = $partyColour . "'#DDDDDD',\n";
                }
            }
        }

        // BAR CHART
        echo "<div>\n";
        echo "<canvas id='canvas' width='600' height='400'></canvas>\n";
        echo "</div>\n";
        
        echo "<script>\n";
        echo "var barChartData = {\n";
        echo "labels: [$partyInitial],\n";
        echo "datasets: [{\n";
        echo "    label: '$BarLabel',\n";
        echo "    backgroundColor: [$partyColour],\n";
        echo "    data: [$partyData]\n";
        echo "  }]\n";
        echo "};\n";

        /*
        echo "var pieData = [\n";
        foreach($xml->SUMMARY->PARTIES->children() as $partyName) {
            echo "{\n";
            echo "value: $partyName->SHARE_OF_THE_VOTE,\n";
            if ($partyName->PARTY_MNEMONIC == "FF") {
                echo "color: '#18B75C',\n";
            } else if ($partyName->PARTY_MNEMONIC == "FG") {
                echo "color: '#4450B7',\n";
            } else if ($partyName->PARTY_MNEMONIC == "LAB") {
                echo "color: '#EE3A46',\n";
            } else if ($partyName->PARTY_MNEMONIC == "SF") {
                echo "color: '#0D7572',\n";
            } else if ($partyName->PARTY_MNEMONIC == "IND") {
                echo "color: '#FBB216',\n";
            } else if ($partyName->PARTY_MNEMONIC == "IA") {
                echo "color: '#F15A24',\n";
            } else if ($partyName->PARTY_MNEMONIC == "RN") {
                echo "color: '#62CAE8',\n";
            } else if ($partyName->PARTY_MNEMONIC == "SD") {
                echo "color: '#752F8B',\n";
            } else if ($partyName->PARTY_MNEMONIC == "UL") {
                echo "color: '#CE76A2',\n";
            } else if ($partyName->PARTY_MNEMONIC == "SP") {
                echo "color: '#FF3E76',\n";
            } else if ($partyName->PARTY_MNEMONIC == "OTH") {
                echo "color: '#999999',\n";
            } else if ($partyName->PARTY_MNEMONIC == "WP") {
                echo "color: '#FF3E76',\n";
            } else if ($partyName->PARTY_MNEMONIC == "AAA") {
                echo "color: '#FAE800',\n";
            } else if ($partyName->PARTY_MNEMONIC == "PBP") {
                echo "color: '#971A41',\n";
            } else if ($partyName->PARTY_MNEMONIC == "GP") {
                echo "color: '#BDD86E',\n";
            } else if ($partyName->PARTY_MNEMONIC == "SKIA") {
                echo "color: '#999999',\n";
            } else {
                echo "color: '#CCCCCC',\n";
            }
            echo "highlight: '#C7DAE2',\n";
            echo "label: \"$partyName->PARTY_NAME\"\n";
            echo "},\n";
        }
        echo "];\n";
        echo "window.onload = function(){\n";
        echo "var ctx = document.getElementById('chart-area').getContext('2d');\n";
        echo "window.myPie = new Chart(ctx).Pie(pieData);\n";
        echo "};\n";
        */

        echo "window.onload = function(){\n";
        echo "var ctx = document.getElementById('canvas').getContext('2d');\n";
        echo "window.myBar = new Chart(ctx).Bar({\n";
        echo "data: barChartData,\n";
        echo "options: {\n";
        echo "responsive: true,\n";
        echo "}\n";
        echo "});\n";
        echo "};\n";        
        echo "</script>\n";
    } else {
        echo "<span><h1>" . (ICL_LANGUAGE_CODE == "ga" ? 'Níl aon sonraí tagtha isteach don Sciar Náisiúnta fós.' : 'No data in for the National Share yet.') . "</h1></span>";
    }
    echo "</div>";
    // End Snippet - Pull back National Share data in a Bar Chart on the National Share page
    echo "<div class='vote-side'>";
    // Begin Snippet - Pull back recent Irish/English messages in the scroller on the National Share page
    echo "<div id='vertScroller' style='margin-left:40px; padding:20px 10px 0px 10px; text-align:left; border-top:1px solid #c7dae2; border-bottom:1px solid #c7dae2; margin-bottom:15px;'>";
    echo "<ul style='list-style-type:square; font-size:13px;'>";
    $fileMessage = "http://direct.tg4.ie/election/ElectionData/xml/" . (ICL_LANGUAGE_CODE == "ga" ? 'RecentIrishMessages.xml' : 'RecentEnglishMessages.xml');
    $file_headers = @get_headers($fileMessage);
    if ($file_headers[0] == 'HTTP/1.1 200 OK') {
        $xml=simplexml_load_file($fileMessage) or die("Error: Cannot create object");
        foreach($xml->MESSAGES->children() as $message) {
            echo "<li><strong>" . $message->TIME . "</strong> (" . $message->TITLE . ")<br />" . $message->TEXT . "</li>";
        }
    }
    echo "</ul>";
    echo "</div>";
    // End Snippet - Pull back recent Irish/English messages in the scroller on the National Share page

    // Begin Snippet - List the Parties, their Mnemonic, Colour and Percentage in the RHS of the National Share page
    echo "<div class='mod-ad' style='padding-left:40px;'>";
    $filename3 = "http://direct.tg4.ie/election/ElectionData/xml/PublishingNationalSummary.xml";
    $file_headers = @get_headers($filename3);
    if ($file_headers[0] == 'HTTP/1.1 200 OK') {
        $xml=simplexml_load_file($filename3) or die("Error: Cannot create object");

        foreach($xml->SUMMARY->PARTIES->children() as $partyResult) {
            if ($partyResult->PARTY_MNEMONIC != "MISC") {
                if ($partyResult->PARTY_MNEMONIC == "AAA-PBP") {
                    $partyResult->PARTY_MNEMONIC = "AP";
                }
                if (ICL_LANGUAGE_CODE == "ga") {
                    if ($partyResult->PARTY_MNEMONIC == "LAB") {
                        $partyResult->PARTY_MNEMONIC = "LO";
                        $partyResult->PARTY_NAME = "Lucht Oibre";
                    }
                    if ($partyResult->PARTY_MNEMONIC == "GP") {
                        $partyResult->PARTY_MNEMONIC = "CG";
                        $partyResult->PARTY_NAME = "Comhaontas Glas";
                    }
                    if ($partyResult->PARTY_MNEMONIC == "IND") {
                        $partyResult->PARTY_MNEMONIC = "NS";
                        $partyResult->PARTY_NAME = "Neamhspleach";
                    }
                    if ($partyResult->PARTY_MNEMONIC == "SD") {
                        $partyResult->PARTY_MNEMONIC = "DS";
                        $partyResult->PARTY_NAME = "Daonlathaigh Sóisialta";
                    }
                }
                echo "<div style=\"width:85%; float:left; text-align:right; font-size:12px; margin:2px;\"><span style=\"width:50px;\" class='election-party-mnemonic-$partyResult->PARTY_MNEMONIC'>";
                if ($partyResult->PARTY_MNEMONIC == "AP") { 
                    $partyResult->PARTY_MNEMONIC = "A/P";
                }
                echo $partyResult->PARTY_MNEMONIC . "</span>" . $partyResult->PARTY_NAME . " (" . $partyResult->SHARE_OF_THE_VOTE . "%)";
                echo "</div>";
            }
        }
    }
    echo "</div>";
    // End Snippet - List the Parties, their Mnemonic, Colour and Percentage in the RHS of the National Share page
    echo "</div>";
    echo "</div>";
    echo "</div>";
    echo "</section>";
} else {
    echo "<section class='constituency-feat-section'>";
    echo "<div class='section-panel-white'>";
    echo "<div class='constituency-feat center-panel'>";
    // Begin Snippet - Pull back Changes from 2011 data in a Bar Chart on the Changes from 2011 page
    echo "<div class='constituency-feat-wrap'>";
    echo "<a href='javascript:history.go(0)'><img src='https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2015/12/refresh-page-2.png' alt='Refresh'>&nbsp;&nbsp;&nbsp;" . (ICL_LANGUAGE_CODE == "ga" ? 'Cliceáil anso chun na torthaí is déanaí a fheiceáil' : 'Click here to view the latest results') . "</a>";
    $filename2 = "http://direct.tg4.ie/election/ElectionData/xml/PublishingNationalSummary.xml";
    $file_headers = @get_headers($filename2);
    if ($file_headers[0] == 'HTTP/1.1 200 OK') {
        echo "<h1>" . (ICL_LANGUAGE_CODE == "ga" ? '% Athruithe ó 2011' : '% Changes from 2011') . "</h1>";
        $xml=simplexml_load_file($filename2) or die("Error: Cannot create object");

        if (ICL_LANGUAGE_CODE == "ga") {
            $BarLabel = "% Athrú";
        } else {
            $BarLabel = "% Change";
        }

        $partyInitial = "";
        $partyColour = "";
        $partyData = "";
        foreach($xml->SUMMARY->PARTIES->children() as $partyName) {
            if ($partyName->PARTY_MNEMONIC != "MISC") {
                if ($partyName->PARTY_MNEMONIC == "AAA-PBP") {
                    $partyName->PARTY_MNEMONIC = "A/P";
                }
                if (ICL_LANGUAGE_CODE == "ga") {
                    if ($partyName->PARTY_MNEMONIC == "LAB") {
                        $partyName->PARTY_MNEMONIC = "LO";
                    }
                    if ($partyName->PARTY_MNEMONIC == "GP") {
                        $partyName->PARTY_MNEMONIC = "CG";
                    }
                    if ($partyName->PARTY_MNEMONIC == "IND") {
                        $partyName->PARTY_MNEMONIC = "NS";
                    }
                    if ($partyName->PARTY_MNEMONIC == "SD") {
                        $partyName->PARTY_MNEMONIC = "DS";
                    }
                }
                $partyInitial = $partyInitial . "'" . $partyName->PARTY_MNEMONIC . "', ";
                $partyData = $partyData . $partyName->COMPARISON_1->CHANGE . ", ";
                if ($partyName->PARTY_MNEMONIC == "FF") {
                    $partyColour = $partyColour . "'#18B75C',\n";
                } else if ($partyName->PARTY_MNEMONIC == "FG") {
                    $partyColour = $partyColour . "'#4450B7',\n";
                } else if ($partyName->PARTY_MNEMONIC == "LAB") {
                    $partyColour = $partyColour . "'#EE3A46',\n";
                } else if ($partyName->PARTY_MNEMONIC == "LO") {
                    $partyColour = $partyColour . "'#EE3A46',\n";
                } else if ($partyName->PARTY_MNEMONIC == "SF") {
                    $partyColour = $partyColour . "'#0D7572',\n";
                } else if ($partyName->PARTY_MNEMONIC == "IND") {
                    $partyColour = $partyColour . "'#FBB216',\n";
                } else if ($partyName->PARTY_MNEMONIC == "NS") {
                    $partyColour = $partyColour . "'#FBB216',\n";
                } else if ($partyName->PARTY_MNEMONIC == "IA") {
                    $partyColour = $partyColour . "'#F15A24',\n";
                } else if ($partyName->PARTY_MNEMONIC == "RN") {
                    $partyColour = $partyColour . "'#62CAE8',\n";
                } else if ($partyName->PARTY_MNEMONIC == "SD") {
                    $partyColour = $partyColour . "'#752F8B',\n";
                } else if ($partyName->PARTY_MNEMONIC == "DS") {
                    $partyColour = $partyColour . "'#752F8B',\n";
                } else if ($partyName->PARTY_MNEMONIC == "UL") {
                    $partyColour = $partyColour . "'#CE76A2',\n";
                } else if ($partyName->PARTY_MNEMONIC == "SP") {
                    $partyColour = $partyColour . "'#FF3E76',\n";
                } else if ($partyName->PARTY_MNEMONIC == "OTH") {
                    $partyColour = $partyColour . "'#999999',\n";
                } else if ($partyName->PARTY_MNEMONIC == "WP") {
                    $partyColour = $partyColour . "'#FF3E76',\n";
                } else if ($partyName->PARTY_MNEMONIC == "A/P") {
                    $partyColour = $partyColour . "'#971A41',\n";
                } else if ($partyName->PARTY_MNEMONIC == "AP") {
                    $partyColour = $partyColour . "'#971A41',\n";
                } else if ($partyName->PARTY_MNEMONIC == "AAA") {
                    $partyColour = $partyColour . "'#FAE800',\n";
                } else if ($partyName->PARTY_MNEMONIC == "PBP") {
                    $partyColour = $partyColour . "'#971A41',\n";
                } else if ($partyName->PARTY_MNEMONIC == "GP") {
                    $partyColour = $partyColour . "'#BDD86E',\n";
                } else if ($partyName->PARTY_MNEMONIC == "CG") {
                    $partyColour = $partyColour . "'#BDD86E',\n";
                } else if ($partyName->PARTY_MNEMONIC == "SKIA") {
                    $partyColour = $partyColour . "'#999999',\n";
                } else if ($partyName->PARTY_MNEMONIC == "DDI") {
                    $partyColour = $partyColour . "'#87CEFA',\n";
                } else if ($partyName->PARTY_MNEMONIC == "CD") {
                    $partyColour = $partyColour . "'#8A8AE0',\n";
                } else if ($partyName->PARTY_MNEMONIC == "FN") {
                    $partyColour = $partyColour . "'#FF7F50',\n";
                } else if ($partyName->PARTY_MNEMONIC == "CPI") {
                    $partyColour = $partyColour . "'#E3170D',\n";
                } else if ($partyName->PARTY_MNEMONIC == "IDP") {
                    $partyColour = $partyColour . "'#DDDDDD',\n";
                }
            }
        }

        echo "<div id='canvas-holder'>\n";
        echo "<canvas id='canvas' width='600' height='400'/>\n";
        echo "</div>\n";
        echo "<script>\n";
        echo "var barChartData = {\n";
        echo "labels: [$partyInitial],\n";
        echo "datasets: [{\n";
        echo "    label: '$BarLabel',\n";
        echo "    backgroundColor: [$partyColour],\n";
        echo "    data: [$partyData]\n";
        echo "}]\n";
        echo "};\n";
        echo "window.onload = function(){\n";
        echo "var ctx = document.getElementById('canvas').getContext('2d');\n";
        echo "window.myBar = new Chart(ctx).Bar({\n";
        echo "data: barChartData,\n";
        echo "options: {\n";
        echo "  responsive: true,\n";
        echo "}\n";
        echo "});\n";
        echo "};\n";
        echo "</script>\n";
    } else {
        echo "<span><h1>" . (ICL_LANGUAGE_CODE == "ga" ? 'Níl aon sonraí tagtha isteach do na hAthruithe ó 2011 fós.' : 'No data in for the Changes from 2011 yet.') . "</h1></span>";
    }
    echo "</div>";
    // End Snippet - Pull back Changes from 2011 data in a Bar Chart on the Changes from 2011 page
    echo "<div class='vote-side'>";
    // Begin Snippet - Pull back recent Irish/English messages in the scroller on the Changes from 2011 page
    echo "<div id='vertScroller' style='margin-left:40px; padding:20px 10px 0px 10px; text-align:left; border-top:1px solid #c7dae2; border-bottom:1px solid #c7dae2; margin-bottom:15px;'>";
    echo "<ul style='list-style-type:square; font-size:13px;'>";
    $fileMessage = "http://direct.tg4.ie/election/ElectionData/xml/" . (ICL_LANGUAGE_CODE == "ga" ? 'RecentIrishMessages.xml' : 'RecentEnglishMessages.xml');
    $file_headers = @get_headers($fileMessage);
    if ($file_headers[0] == 'HTTP/1.1 200 OK') {
        $xml=simplexml_load_file($fileMessage) or die("Error: Cannot create object");
        foreach($xml->MESSAGES->children() as $message) {
            echo "<li><strong>" . $message->TIME . "</strong> (" . $message->TITLE . ")<br />" . $message->TEXT . "</li>";
        }
    }
    echo "</ul>";
    echo "</div>";
    // End Snippet - Pull back recent Irish/English messages in the scroller on the Changes from 2011 page
    
    // Begin Snippet - List the Parties, their Mnemonic, Colour and Percentage in the RHS of the Changes from 2011 page
    echo "<div class='mod-ad' style='padding-left:40px;'>";
    $filename3 = "http://direct.tg4.ie/election/ElectionData/xml/PublishingNationalSummary.xml";
    $file_headers = @get_headers($filename3);
    if ($file_headers[0] == 'HTTP/1.1 200 OK') {
        $xml=simplexml_load_file($filename3) or die("Error: Cannot create object");

        foreach($xml->SUMMARY->PARTIES->children() as $partyResult) {
            if ($partyResult->PARTY_MNEMONIC != "MISC") {
                if ($partyResult->PARTY_MNEMONIC == "AAA-PBP") {
                    $partyResult->PARTY_MNEMONIC = "AP";
                }
                if (ICL_LANGUAGE_CODE == "ga") {
                    if ($partyResult->PARTY_MNEMONIC == "LAB") {
                        $partyResult->PARTY_MNEMONIC = "LO";
                        $partyResult->PARTY_NAME = "Lucht Oibre";
                    }
                    if ($partyResult->PARTY_MNEMONIC == "GP") {
                        $partyResult->PARTY_MNEMONIC = "CG";
                        $partyResult->PARTY_NAME = "Comhaontas Glas";
                    }
                    if ($partyResult->PARTY_MNEMONIC == "IND") {
                        $partyResult->PARTY_MNEMONIC = "NS";
                        $partyResult->PARTY_NAME = "Neamhspleach";
                    }
                    if ($partyResult->PARTY_MNEMONIC == "SD") {
                        $partyResult->PARTY_MNEMONIC = "DS";
                        $partyResult->PARTY_NAME = "Daonlathaigh Sóisialta";
                    }
                }
                echo "<div style=\"width:85%; float:left; text-align:right; font-size:12px; margin:2px;\"><span style=\"width:50px;\" class='election-party-mnemonic-$partyResult->PARTY_MNEMONIC'>";
                if ($partyResult->PARTY_MNEMONIC == "AP") { 
                    $partyResult->PARTY_MNEMONIC = "A/P";
                }
                echo $partyResult->PARTY_MNEMONIC . "</span>" . $partyResult->PARTY_NAME . " (" . $partyResult->COMPARISON_1->CHANGE . "%)";
                echo "</div>";
            }
        }
    }
    echo "</div>";
    // End Snippet - List the Parties, their Mnemonic, Colour and Percentage in the RHS of the Changes from 2011 page
    echo "</div>";
    echo "</div>";
    echo "</div>";
    echo "</section>";
}
get_footer(); ?>