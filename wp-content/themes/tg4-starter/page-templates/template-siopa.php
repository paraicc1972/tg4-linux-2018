<?php /* Template Name: Siopa */ ?>

<?php get_header(); ?>

<div class="section-header">
    <h1 class="section-title"><?php echo get_the_title($post->post_parent); ?></h1>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
    <div class="section-submenu-wrap">
        <ul class="section-submenu-list">
            <?php wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=1'); ?>
            &nbsp;
        </ul>
    </div>
</div>

<section class="shop-content-mods">
    <div class="shopmods-wrapper">
        <div class="shop-mod-wrap">
        <?php
        if(have_rows('siopa_addons')) {
            while(have_rows('siopa_addons')): the_row(); 
                {
        ?>
                <section class="mod-1">
                    <div class="shop-mod-notice">
                        <h3 class="siopa_title"><?php the_sub_field('siopa_title'); ?></h3>
                        <a href="<?php the_sub_field('siopa_link'); ?>" target="_new"><img src="<?php the_sub_field('siopa_image'); ?>"></a>
                    </div>
                </section>
        <?php
                }
            endwhile;
        } ?>
</section>

<?php get_footer(); ?>