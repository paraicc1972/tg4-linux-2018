<?php /* Template Name: Sport - Home */ ?>

<?php get_header(); ?>

<div class="section-header">
	<h1 class="section-title">
		<?php echo get_the_title($post->post_parent); ?>
	</h1>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
	<div class="section-submenu-wrap">
		<ul class="section-submenu-list">
			<?php wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->ID . '&depth=1'); ?>
			&nbsp;
		</ul>
	</div>
</div>

<section class="sport-lists" id="sched-lists">
	<!--h2 class="sched-lists-title"><?php //echo (ICL_LANGUAGE_CODE == "ga" ? 'Spórt ar TG4' : 'Sports on TG4'); ?></i></h2-->
	<div id="sched-list-wrap"><?php get_template_part('/includes/sport-schedule'); ?></div>
</section>

<!-- Angular -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular-touch.min.js"></script>
<!-- Angular -->
<section class="section-panel-dark-3 featv-section">
    <div class="title-tab-wrap">
        <h2 class="title-tab-dark-3"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Físeáin (rogha reatha)' : 'Featured Videos'); ?></h2>
    </div>
    <?php include(locate_template('/includes/featured-videos-api.php')); ?>
</section>

<?php get_footer(); ?>