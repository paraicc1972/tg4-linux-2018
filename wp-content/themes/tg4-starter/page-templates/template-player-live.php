<?php /* Template Name: Live Player - Channel 1 */ ?>

<?php get_header(); 

$clientIP = $_SERVER['HTTP_X_FORWARDED_FOR']?: $_SERVER['HTTP_CLIENT_IP']?: $_SERVER['REMOTE_ADDR'];
//echo "** " . $clientIP;
if ($clientIP == "::1") {
    $clientIP = "77.75.98.34"; //IE
    //$clientIP = "194.32.31.1"; //GB
    //$clientIP = "94.0.69.150"; //NIR
    //$clientIP = "217.91.35.16"; //GER
    //$clientIP = "176.164.135.240"; //FRA
}

$request = "https://www.tg4tech.com/api/programmes/countryCode?id=" . $clientIP;
$ch = curl_init($request);
curl_setopt_array($ch, array(
        CURLOPT_POST           => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_SSL_VERIFYHOST => FALSE,
        CURLOPT_SSL_VERIFYPEER => FALSE,
        CURLOPT_HTTPHEADER     => array('Content-type: application/x-www-form-urlencoded'),
    ));
$response = curl_exec($ch);

// Check for errors
if ($response === FALSE) {
    die(curl_error($ch));
}
curl_close($ch);

$responseData = json_decode($response, TRUE);
$geoCode = $responseData["country"];

//echo "* " . $geoCode;
?>

<script type="text/javascript">
    if (window.innerWidth <= 835) { 
        playerW=window.innerWidth-58;
        playerH=playerW/1.777;
    } else {
        playerW=835;
        playerH=470;
    }

    <?php if ($geoCode === 'IE') { ?>
        // Open Stream
        url = 'http://csm-e.cds1.yospace.com/csm/live/74246540.m3u8';
    <?php } else { ?>
        // GeoBlocked Stream
        url = 'http://csm-e.cds1.yospace.com/csm/live/74246610.m3u8';
    <?php } ?>

    console.log(url);
</script>

<script type="text/javascript" src="https://d1og0s8nlbd0hm.cloudfront.net/yospace/yoplayer-4.1-32.js"></script>

<div class="section-header">
	<h1 class="section-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Beo' : 'Live'); ?></h1>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
    <div class="section-submenu-wrap">
        <ul class="section-submenu-list">
            <?php
            if ($post->ID == 397 || $post->ID == 401) {
                wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->ID. '&depth=1');
            } else {
                wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=1');
            } ?>
            &nbsp;
        </ul>
    </div>
</div>

<section class="plyr-live-section">
    <div class="section-panel-dark-3">
        <div class="plyr-feat center-panel">
            <!-- Cuir an Fógraíocht Leis an Seinnteoir Beo! -->
            <div class="prog-feat-ad mod-ad">
                <h2 class="mod-ad-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Fógraíocht' : 'Advertisement'); ?></h2>
                <div class="ad-wrapper">
                    <iframe src="https://www.tg4.ie/wp-content/themes/tg4-starter/mpu-banner.htm" height="250" width="300" scrolling="no" frameborder="0"></iframe>
                    <!--iframe src="<?php //echo site_url() . '/wp-content/themes/tg4-starter/'; ?>mpu-banner.htm" height="250" width="300" scrolling="no" frameborder="0"></iframe-->
                </div>
            </div>

            <div id="player" class="plyr-feat-wrap">
                <script type="application/javascript">
                    function playerSize() {
                        //console.log('Window',window.innerWidth);
                        var viewportwidth;
                        var viewportheight;
                  
                        // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
                        if (typeof window.innerWidth != 'undefined')
                        {
                            viewportwidth = window.innerWidth,
                            viewportheight = window.innerHeight
                        }
                
                        if (viewportwidth <= 935) { 
                            playerW=viewportwidth-58;
                            playerH=playerW/1.777;
                            resize(playerW, playerH);
                        } else {
                            playerW=835;
                            playerH=570;
                        }
                        resize(playerW, playerH);
                    }
                </script>

                <!-- Yospace Player -->
                <script type="text/javascript">
                    $YOPLAYER('player', {
                        player: 'https://d1og0s8nlbd0hm.cloudfront.net/yospace/yoplayer-4.1-32.swf',
                        height: playerH,
                        width: playerW,
                        buffer: 30,
                        lwm: 5,
                        lss: 5,
                        debug: false,
                        //skin: "https://d1og0s8nlbd0hm.cloudfront.net/yospace/skin2.skin",
                        autostart: true,
                        type: 'hls',
                        adverttext: 'If you like this, why not click to find out more...',
                        file: url,
                        callbacks: {
                            logging: function(obj, msg) {
                                displayMessage(msg);
                            },
                            position: function(id, pos) {
                                playPosition = pos;
                            },
                            advertStart: function(id, data) {
                                displayMessage("AD Start Event Fired at " + data);
                            },
                            advertEnd: function(id, data) {
                                displayMessage("AD End Event Fired at " + data);
                            },
                            advertBreakStart: function(id, pos) {
                                displayMessage("AD Break Start Event Fired at " + pos);
                            },
                            advertBreakEnd: function(id, pos) {
                                displayMessage("AD Break End Event Fired at " + pos);
                            }
                        }
                    });
                    function play(url, stream) {
                        $YOFIND('player').play(url);
                    }
                    function resize(playerW, playerH) {
                        $YOFIND('player').resize(playerW,playerH);
                    }
                    function displayMessage(msg) {
                    }
                </script>
            </div>
        </div>
    </div>
    <div class="plyr-feat-intro-wrap">
        <div class="center-panel">
            <div class="prog-feat-intro">
                <div class="prog-feat-toolbar">
                    <div class="prog-remind">
                        <div class="prog-remind-txt"><?php echo (ICL_LANGUAGE_CODE == "ga" ? "TG4 Beo" : "Tg4 Live"); ?></div>
                    </div>
                    <div class="prog-share">
                        <a href="" class="facebook-share">Facebook</a>
                        <a href="" class="twitter-share">Twitter</a>
                        <a href="" class="email-share">Email</a>
                    </div>
                </div>
                <?php if (get_post_field("post_content")) { ?>
                    <div class="c2-feat-txt">
                        <?php echo apply_filters('the_content', get_post_field('post_content', $post_id)); ?>
                    </div>
                <?php } ?>
                <div class="prog-feat-keyword" style="background-color:#ffffff; width: 100%; border: 1px solid #043244; padding:8px; margin: 5px 0px 20px 0px;">
                    <p class="bold"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Ábhar Srianta' : 'Geoblocked Content'); ?></p>
                    <p style="font-size:small;"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Tá srianta réigún ar roinnt d’ábhar TG4, rud a fhágann nach ceadmhach dúinn é a thaispeáint lasmuigh d’oileán na hÉireann.' : 'Some (but not all) of TG4’s content has a rights restriction, allowing us to make it available on the island of Ireland only.'); ?> </p>
                    <p style="font-size:small;"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Léireoidh an leathanach <a href="<?php echo site_url(); ?>/ga/sceideal/baile/">Sceidil</a> cé na srianta atá ar chlár; feicfidh tú siombal (é) d’ábhar atá srianta d’Éire amháin agus an siombail (🚫) d’ábhar nach bhfuil ceadaithe dúinn a sholáthar ar-líne  ar chor ar bith.' : 'Our <a href="<?php echo site_url(); ?>/en/schedule/home/">listings</a> page carries an indicator of which programmes are geoblocked to Ireland only (with [é] symbol) and which are not online (with 🚫 symbol).'); ?></p>
                    <p style="font-size:small;"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Do gach clár eile, ba cheart go mbeadh sé le feiceáil i ngach áit ar domhan.' : 'All other programmes should be  available worldwide.'); ?></p>
                    <p style="font-size:small;"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Má tá tú ag breathú ar tg4.ie, is gá duit cinntiú go bhfuil do sheoladh IP cláraithe in Éirinn, le deimhniú go seinnfidh an clár duit. (<a href="https://www.ipinfodb.com/my_ip_location.php" target="_New">ipinfodb.com</a>)' : 'If viewing on tg4.ie, you need to ensure your IP address is registered in Ireland to ensure the content is not restricted. (<a href="https://www.ipinfodb.com/my_ip_location.php" target="_New">ipinfodb.com</a>)'); ?></p>
                    <p style="font-size:small;"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Má tá tú ag breathnú ag úsáid <a href="https://itunes.apple.com/ie/app/tg4-player/id598591024?mt=8" target="_New">APP IOS TG4</a>, is gá duit cinntiú go bhfuil do ‘Location Services’ ar siúl ar do ghaireas.' : 'Alternatively, you can download our <a href="https://itunes.apple.com/ie/app/tg4-player/id598591024?mt=8" target="_New">IOS App</a>. App access is determined by Geolocation. To ensure  the fullest permissible access to TG4 content for your location, ensure the Location Services on your device are set to ON for the TG4 App.'); ?></p>
                </div>
                <div class="prog-feat-intro">
                    <?php
                    /* echo "<ul>";
                    echo '<li>Country Code: ' . $geoCode . '</li>';
                    echo '<li>CLIENT: ' . getenv('HTTP_CLIENT_IP') . '</li>';
                    echo '<li>SERVER: ' . getenv('SERVER_ADDR') . '</li>';
                    echo '<li>REMOTE: ' . getenv('REMOTE_ADDR') . '</li>';
                    echo '<li>XFORWARD: ' . getenv('HTTP_X_FORWARDED');
                    echo '<li>XFORWARD 4: ' . getenv('HTTP_X_FORWARDED_FOR') . '</li>';
                    echo '<li>FORWARD: ' . getenv('HTTP_FORWARDED') . '</li>';
                    echo '<li>FORWARD 4: ' . getenv('HTTP_FORWARDED_FOR') . '</li>';
                    echo "</ul>"; */
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-panel-blue">
    <div class="title-tab-wrap">
        <h2 class="title-tab-blue"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Inniu ar TG4' : 'Today on TG4'); ?></h2>
    </div>
    <?php get_template_part('/includes/tonight-slider'); ?>
    <a href="<?php echo (ICL_LANGUAGE_CODE == "ga" ? '/ga/sceideal/baile/' : '/en/schedule/home/');?>" class="view-full"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Sceideal Iomlán' : 'View full schedule'); ?></a>
</section>

<?php get_footer(); ?>