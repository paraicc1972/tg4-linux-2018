<?php /* Template Name: Newsletter - Webpage */ ?>

<?php get_header(); ?>

<?php
if(have_rows('newsletter_content')) {
    $row_num = 0;
    while(have_rows('newsletter_content')): the_row(); 
        if ($row_num == 0) {
?>
            <section class="prog-head" style="background-image: url(<?php the_sub_field('newsletter_image'); ?>);">
                <a href="<?php the_sub_field('newsletter_link'); ?>">
                <div class="nlblockContainer">
                    <div class="nlcontent-block">
                        <h1 class="nlcontent-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Nuachtlitir: ' : 'Newsletter: '); echo get_the_title($post->ID); ?></h1>
                        <h2 class="nlcontent-subtitle"><?php the_sub_field('newsletter_title'); ?></h2>
                    </div>
                    <div><?php if (get_sub_field("newsletter_video")) { ?><a href="<?php echo site_url() . '/popup.php?vid='; echo the_sub_field('newsletter_video'); ?>" target="New"  title="Newsletter Video" class="btn-header"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Féach seo' : 'Watch here'); ?><span></span></a><?php } ?></div>
                </div>
                </a>
            </section>
<?php   }
        $row_num++;
    endwhile; 
} 
?>

<section class="newsletter-mods">
    <h2 class="visuallyhidden">More on TG4</h2>
    <div class="nlmods-wrapper">
        <div class="newsletter-mod-wrap">
            <?php
            if(have_rows('newsletter_content')) {
                $row_num = 0;
                while(have_rows('newsletter_content')): the_row(); 
                    if ($row_num > 0) {
            ?>
                    <section class="mod-2">
                        <div class="newsletter-mod-notice">
                            <h3 class="newsletter-title"><?php the_sub_field('newsletter_title'); ?></h3>
                            <a href="<?php the_sub_field('newsletter_link'); ?>" class="prog-panel"><img src="<?php the_sub_field('newsletter_image'); ?>" width="380"></a>
                            <?php if (get_sub_field("newsletter_video")) { ?>
                                <a href="<?php echo site_url() . '/popup.php?vid='; echo the_sub_field('newsletter_video'); ?>" target="New" class="prog-panel">
                                <div class="prog-footer">
                                    <h4 class="prog-episode-title"><?php the_sub_field('newsletter_time'); ?></h4>
                                    <div class="arrow-box"></div>
                                </div>
                                </a>
                            <?php } else { ?>
                                <div class="prog-footer">
                                    <h4 class="prog-episode-title"><?php the_sub_field('newsletter_time'); ?></h4>
                                </div>
                            <?php } ?>
                            <div class="prog-desc-ie"><?php the_sub_field('newsletter_text_ie'); ?></div>
                            <div class="prog-desc-en"><?php the_sub_field('newsletter_text_en'); ?></div>
                        </div>
                    </section>
            <?php
                    }
                    $row_num++;
                endwhile;
            } ?>
</section>

<div class="footer-details-wrap">
    <div class="center-panel">
        <div class="nl-reg-btn">
            <select name="page-dropdown"
                onchange='document.location.href=this.options[this.selectedIndex].value;'> 
                <option value=""><?php echo esc_attr(((ICL_LANGUAGE_CODE == "ga" ? 'Féach ar Nuachtlitir' : 'View Newsletter'))); ?></option> 
                <?php
                if (ICL_LANGUAGE_CODE == "ga") {
                    $NewsletterPageID = 1093;
                } else { 
                    $NewsletterPageID = 1096;
                } 
                //$pages = get_pages();
                $pages = get_pages(array('child_of' => $NewsletterPageID, 'sort_column' => 'post_modified', 'sort_order' => 'desc', 'exclude' => '16670, 16673'));
                foreach ($pages as $page) {
                    $option = '<option value="' . get_page_link($page->ID) . '">';
                    $option .= $page->post_title;
                    $option .= '</option>';
                    echo $option;
                }
                ?>
            </select>

            [ <?php echo (ICL_LANGUAGE_CODE == "ga" ? '<a href="../claraigh-linn/">Cliceáil anseo chun clárú don Nuachtlitir</a>' : '<a href="../newsletter-register/">Click here to register for the Newsletter</a>'); ?> ]
        </div>
        <div class="footer-desc-ie">
            <p>Cainéal 4 Saorview; Cainéal 104 Sky<br />
            Cainéal 104 UPC; 137 HD ar UPC, 602 Cúla4 ar UPC<br />
            Tuaisceart Éireann: 163 (Sky); 877 (Virgin Cable); 51 (Freeview)<br />
            Ar fud na Cruinne: www.tg4.tv</p>
        </div>
    </div>
</div>

<div class="srch-feat-intro-wrap">
    <div class="center-panel">&nbsp;
    </div>
</div>

<!-- Angular -->
<script src= "https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular-touch.min.js"></script>
<!-- Angular -->
<section class="section-panel-dark-3 featv-section">
    <div class="title-tab-wrap">
        <h2 class="title-tab-dark-3"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Físeáin (rogha reatha)' : 'Featured Videos'); ?></h2>
    </div>
    <?php include(locate_template('/includes/featured-videos-api.php')); ?>
</section>

<?php get_footer(); ?>