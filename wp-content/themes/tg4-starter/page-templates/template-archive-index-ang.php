<?php /* Template Name: Archive Content - Index - Angular */ ?>

<?php include(locate_template('/header-angular.php')); ?>

<div class="section-header">
    <h1 class="section-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Cartlann TG4' : 'TG4 Archive'); ?></h1>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
    <div class="section-submenu-wrap">
        <ul class="section-submenu-list">
            <?php wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=1'); ?>
            &nbsp;
        </ul>
    </div>
</div>
<?php
$feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
$feat_Class = "prog-head";
?>
<section class="<?php echo $feat_Class; ?>" style="background-image: url(<?php echo $feat_image; ?>);">
    <div class="blockContainer">
        <?php 
        if (get_field("intro_text")) { ?>
            <div class="content-block">
                <h2 class="content-subtitle"><?php echo get_field("intro_sub_title"); ?></h2>
                <p><?php echo get_field("intro_text"); ?></p>
            </div>
        <?php } ?>
    </div>
</section>

<div class="srch-feat-intro-wrap" style="margin-top: 0px;">
    <div class="center-panel">
        <div class="srch-feat-intro">
           <!-- <div class="srchSel-wrap">
                <select class="srchSel" ng-model="srt" ng-init="srt='prog_first_tx_date'" ng-change="srtBy(srt)">
                    <option value='prog_first_tx_date' disabled> {{current_Lang=='ga'?'Rangaigh...':'Sort By...'}}</option>
                    <option value='prog_first_tx_date'> {{current_Lang=='ga'?'Chéad Tx':'1st Tx'}}</option>
                    <option value='prog_dynamic_branding_title'> A - Z </option>
                </select>
                <i ng-title="{{current_Lang=='ga'?'Athraigh an Ord':'Toggle Order'}}" ng-click="up=!up" class="hot fa {{up?'fa-sort-amount-asc':'fa-sort-amount-desc'}}"></i>
            </div> -->
            <div class="arcSel-wrap">
                <select ng-cloak id="sSel" name="page-dropdown" ng-model="flt" ng-init="flt='All'" onchange="document.location.href=this.options[this.selectedIndex].value;"> 
                    <option value="All" disabled>{{current_Lang=='ga'?'Séanra':'Genre'}}</option>
                    <option value='?progGenre=All' disabled>{{current_Lang=='ga'?'Baile':'Home'}}</option>
                    <option value='?progGenre=Cursaí Reatha'>{{current_Lang=='ga'?'Cúrsaí Reatha':'Current Affairs'}}</option>
                    <option value='?progGenre=Drama' disabled>{{current_Lang=='ga'?'Drámaíocht':'Drama'}}</option>
                    <option value='?progGenre=Cula4' disabled>{{current_Lang=='ga'?'Cúla4':'Cúla4'}}</option>
                    <option value='?progGenre=Sport' disabled>{{current_Lang=='ga'?'Spórt':'Sport'}}</option>
                    <option value='?progGenre=Ceol' disabled>{{current_Lang=='ga'?'Ceol':'Music'}}</option>
                    <option value='?progGenre=Saolchlar' disabled>{{current_Lang=='ga'?'Saolchlár':'Lifestyle'}}</option>
                    <option value='?progGenre=Siamsaiocht' disabled>{{current_Lang=='ga'?'Siamsaíocht':'Entertainment'}}</option>
                    <option value='?progGenre=Fáisnéis' disabled>{{current_Lang=='ga'?'Faisnéis':'Documentary'}}</option>
                </select>
            </div>
            <div class="arcform-wrap"><input ng-cloak class="arcform-textbox" name="sbox" id="sbox" ng-model="ss" placeholder="{{current_Lang=='ga'?' Cuimsitheach...':' Advanced...'}}" ng-change="ss1=noFada(ss);" /><a href="#" class="arcform-btn" onclick="this.href='?srchTxt=' + document.getElementById('sbox').value">>></a><a class="arcform-hint" title="{{current_Lang=='ga'?'Is féidir cuardach a dhéanamh ar théarma (faoi bhliain ar leith) anseo e.g. Gaillimh 2001':'You can search by a certain term (within a year) here e.g. Galway 2001'}}">?</a><br /><span class="arcform-text" ng-bind="current_Lang=='ga' ? 'Cuardach: Cuimsitheach' : 'Advanced Search'"></span> </div>
            <div class="arcform-wrap"><input ng-cloak class="arcform-textbox" name="cbox" id="dbox" ng-model="ds" placeholder="{{current_Lang=='ga'?' Dáta (dd.mm.yyyy)':' Date (dd.mm.yyyy)'}}" ng-change="ss1=noFada(ds);" maxlength="10" /><a href="#" class="arcform-btn" onclick="this.href='?srchDate=' + document.getElementById('dbox').value">>></a><br /><span class="arcform-text" ng-bind="current_Lang=='ga' ? 'Dáta' : 'Search: Date'"> (dd.mm.yyyy)</span></div>
            <div class="arcform-wrap"><input ng-cloak class="arcform-textbox" name="tbox" id="tbox" ng-model="ts" placeholder="{{current_Lang=='ga'?' Teideal...':' Title...'}}" ng-change="ss1=noFada(ts);" /><a href="#" class="arcform-btn" onclick="this.href='?srchTitle=' + document.getElementById('tbox').value">>></a><br /><span class="arcform-text" ng-bind="current_Lang=='ga' ? 'Cuardach: Teideal' : 'Search: Title'"></span></div>
            <!-- <div class="arcform-wrap"><input ng-cloak class="arcform-textbox" name="cbox" id="cbox" ng-model="cs" placeholder="{{current_Lang=='ga'?' Comhlacht Léiriúchán...':' Production Company...'}}" ng-change="ss1=noFada(cs);" /><a href="#" class="arcform-btn" onclick="this.href='?srchComp=' + document.getElementById('cbox').value">>></a><br /><span class="arcform-text" ng-bind="current_Lang=='ga' ? 'Cuardach: Comhlacht Léiriúchán' : 'Search: Production Company'"></span></div> -->
            <div class="arcform-wrap"><input ng-cloak class="arcform-textbox" name="ybox" id="ybox" ng-model="ys" placeholder="{{current_Lang=='ga'?' Bliain...':' Year...'}}" ng-change="ss1=noFada(ys);" maxlength="4" /><a href="#" class="arcform-btn" onclick="this.href='?srchYear=' + document.getElementById('ybox').value">>></a><br /><span class="arcform-text" ng-bind="current_Lang=='ga' ? 'Cuardach: Bliain' : 'Search: Year'"></span></div>
        </div>
    </div>
</div>

<section class="player-mods">
    <h2 class="visuallyhidden">More on TG4</h2>
    <div class="plyrmods-wrapper">
        <!-- PHP code to pull the info back from the querystring - Genre, Alphbetical and Search term -->
        <?php
        $pageCnt = ctype_digit($_GET['pageCnt']) ? $_GET['pageCnt'] : 1;
        $progGenre = $_GET['progGenre'];
        $progLtr = $_GET['progLtr'];
        $srchYear = $_GET['srchYear'];
        $srchComp = $_GET['srchComp'];
        $srchTitle = $_GET['srchTitle'];
        $srchTxt = $_GET['srchTxt'];

        //Check for the programme genre in querystring
        if (isset($progGenre)) {
            $genreQuery = "&progGenre=" . $progGenre;
            $listTitle = $progGenre;
        }
        //Check for the letter clicked in querystring
        if (isset($progLtr)) {
            $ltrQuery = "&progLtr=" . $progLtr;
            $listTitle = $progLtr;
        }
        //Check for the search term in querystring
        if (isset($srchYear)) {
            $srchQuery = "&srchYear=" . $srchYear;
            $listTitle = $srchYear;
        }
        //Check for the search term in querystring
        if (isset($srchComp)) {
            $srchQuery = "&srchComp=" . $srchComp;
            $listTitle = $srchComp;
        }
        //Check for the search term in querystring
        if (isset($srchTitle)) {
            $srchQuery = "&srchTitle=" . $srchTitle;
            $listTitle = $srchTitle;
        }
        //Check for the search term in querystring
        if (isset($srchTxt)) {
            $srchQuery = "&srchTxt=" . $srchTxt;
            $listTitle = $srchTxt;
        }
        ?>
        <!-- Alphabetical link list -->
        <ul class="pagination">
            <li><a href="?progLtr=A">A</a></li>
            <li><a href="?progLtr=B">B</a></li>
            <li><a href="?progLtr=C">C</a></li>
            <li><a href="?progLtr=D">D</a></li>
            <li><a href="?progLtr=E">E</a></li>
            <li><a href="?progLtr=F">F</a></li>
            <li><a href="?progLtr=G">G</a></li>
            <li><a href="?progLtr=H">H</a></li>
            <li><a href="?progLtr=I">I</a></li>
            <li><a href="?progLtr=J">J</a></li>
            <li><a href="?progLtr=K">K</a></li>
            <li><a href="?progLtr=L">L</a></li>
            <li><a href="?progLtr=M">M</a></li>
            <li><a href="?progLtr=N">N</a></li>
            <li><a href="?progLtr=O">O</a></li>
            <li><a href="?progLtr=P">P</a></li>
            <li><a href="?progLtr=Q">Q</a></li>
            <li><a href="?progLtr=R">R</a></li>
            <li><a href="?progLtr=S">S</a></li>
            <li><a href="?progLtr=T">T</a></li>
            <li><a href="?progLtr=U">U</a></li>
            <li><a href="?progLtr=V">V</a></li>
            <li><a href="?progLtr=W">W</a></li>
            <li><a href="?progLtr=X">X</a></li>
            <li><a href="?progLtr=Y">Y</a></li>
            <li><a href="?progLtr=Z">Z</a></li>
        </ul>

        <!-- Jquery Paging -->
        <div class="container">
            <script src='https://d1og0s8nlbd0hm.cloudfront.net/js/min/jquery.bootpag.min.js'></script>
            <p id="pagination-here"></p>
        </div>

        <div class="player-mod-wrap">
            <h2 class="archiveLtr">
                <?php
                if (isset($listTitle)) {
                    echo $listTitle;
                } else {
                    echo "A";
                }
                ?>
                <span class="archivePg"> (<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'LTH. ' : 'PAGE '); echo $pageCnt; ?>)</span>
            </h2>
            <table width="100%" class="archive-list">
                <tr>
                    <th width="10%"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'BLIAIN' : 'YEAR'); ?></th>
                    <th width="20%"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'CHÉAD Tx' : '1st Tx'); ?></th>
                    <th width="20%"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'COMHLACHT' : 'COMPANY'); ?></th>
                    <th width="35%"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'TEIDEAL' : 'TITLE'); ?></th>
                    <th><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'SRAITH' : 'SERIES'); ?></th>
                </tr>
                <tr ng-repeat="p in allProg=(pAll | orderBy:'prog_dynamic_branding_title') | limitTo:20" ng-if="p.prog_first_tx_date != ''">
                    <td><span ng-bind="p.prog_production_year"></span><!-- <span ng-bind="p.prog_vod_category"></span> --></td>
                    <td><span ng-bind="p.prog_first_tx_date | date :'dd.MM.yyyy'"></span></td>
                    <td><a href="../full-archive/?srchComp={{p.prog_production_company}}"><span ng-bind="p.prog_production_company"></span></a></td>
                    <td ng-if="p.prog_title.indexOf('Fód') !== -1"><a ng-if="p.prog_vod_category == 'Nuacht & Cursaí Reatha'" href="<?php echo site_url(); ?>{{current_Lang=='ga'?'/ga/tg4-archive/new-archive-player/?mat_id='+p.mat_id+'&teideal='+p.prog_dynamic_branding_title+'&series='+p.series_title+'':'/en/tg4-archive/new-archive-player/?mat_id='+p.mat_id+'&teideal='+p.prog_dynamic_branding_title+'&series='+p.series_title+''}}"><span ng-bind="p.prog_title | limitTo: 6"></span></a><span ng-if="p.prog_vod_category !== 'Nuacht & Cursaí Reatha'" ng-bind="p.prog_title | limitTo: 6"></span></td>
                    <td ng-if="p.prog_title.indexOf('Fód') === -1"><a ng-if="p.prog_vod_category == 'Nuacht & Cursaí Reatha'" href="<?php echo site_url(); ?>{{current_Lang=='ga'?'/ga/tg4-archive/new-archive-player/?mat_id='+p.mat_id+'&teideal='+p.prog_dynamic_branding_title+'&series='+p.series_title+'':'/en/tg4-archive/new-archive-player/?mat_id='+p.mat_id+'&teideal='+p.prog_dynamic_branding_title+'&series='+p.series_title+''}}"><span ng-bind="p.prog_title"></span></a><span ng-if="p.prog_vod_category !== 'Nuacht & Cursaí Reatha'" ng-bind="p.prog_title"></span></td>
                    <td><span ng-bind="p.prog_title!=p.series_title?p.series_title:''"></span></td>
                </tr>
            </table>
            <div ng-hide="allProg.length"><p><br /><span ng-bind="current_Lang=='ga'?'Níl aon fiseáin ar fáil.':'No videos available.'"></span></p></div>
        </div>

        <!-- Angular -->
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular-touch.min.js"></script>
        <!-- Angular -->

        <script>
            var app=angular.module('tg4App',[]);
            app.controller('tg4Ctrl', function($scope,$http) {
                
                $scope.current_Lang='<?php echo ICL_LANGUAGE_CODE ?>';
                $scope.total = 0;
                $scope.currentPage = <?php echo $pageCnt-1; ?>;

                $scope.noFada = function(value){ 
                    return value.toLowerCase().replace(/á/g,'a').replace(/é/g,'e').replace(/í/g,'i').replace(/ó/g,'o').replace(/ú/g,'u'); 
                };

                //* Check querystring parameters for Archive Genre, Page Count, Alphabetical link & Search text
                var params={};
                decodeURIComponent(location.search).replace(/[?&]+([^=&]+)=([^&]*)/gi,function(m,k,v){ params[k]=v });
                //* Check querystring parameters for Archive Genre
                if (!params.progGenre) { 
                    params.progGenre='';
                }
                //* Check querystring parameters for Page Count, if empty load 1st page
                if (!params.pageCnt) { 
                    params.pageCnt=1;
                }
                //* Check querystring parameters for letter clicked
                if (!params.progLtr) { 
                    params.progLtr='';
                }
                //* Check querystring parameters for Search entered
                if (!params.srchYear) { 
                    params.srchYear='';
                }
                //* Check querystring parameters for Search entered
                if (!params.srchComp) { 
                    params.srchComp='';
                }
                //* Check querystring parameters for Search entered
                if (!params.srchTitle) { 
                    params.srchTitle='';
                }
                //* Check querystring parameters for Search entered
                if (!params.srchTxt) { 
                    params.srchTxt='';
                }

                //* Calls to the API - if genre selected bring back genre listing, else display listing alphabetically
                if (params.progGenre) {
                    $http.post('https://www.tg4tech.com/api/category?id='+params.progGenre+'&page='+params.pageCnt).then(function(d){
                        $scope.allDataLoaded(d.data);
                        $scope.total = d.data.total_count;
                        //console.log('Count - '+ $scope.total);

                        $scope.pageTotal = Math.ceil($scope.total/20);
                        console.log("Total ", $scope.total, "Pages ", $scope.pageTotal);

                        $(document).ready(function () {
                            $('#pagination-here').bootpag({
                                total: $scope.pageTotal,
                                page: <?php echo $pageCnt; ?>,
                                maxVisible: 20,
                                leaps: false,
                                //href: "?pageCnt={{number}}<?php echo $sortQuery; ?>",
                                firstLastUse: true,
                                first: '←',
                                last: '→'
                            }).on('page', function(event, num){
                                window.location.href = '?pageCnt=' + num + '<?php echo $genreQuery; echo $ltrQuery; echo $srchQuery; ?>';
                            });
                        });
                    });
                } else if (params.progLtr) {
                    $http.post('https://www.tg4tech.com/api/programmes/basicSearch?q=^'+params.progLtr+'&searchfield=prog_dynamic_branding_title&orderon=prog_dynamic_branding_title&orderby=asc&page='+params.pageCnt).then(function(d){
                        $scope.allDataLoaded(d.data); 
                        $scope.total = d.data.total_count;

                        $scope.pageTotal = Math.ceil($scope.total/20);
                        console.log("Total ", $scope.total, "Pages ", $scope.pageTotal);

                        $(document).ready(function () {
                            $('#pagination-here').bootpag({
                                total: $scope.pageTotal,
                                page: <?php echo $pageCnt; ?>,
                                maxVisible: 20,
                                leaps: false,
                                //href: "?pageCnt={{number}}<?php echo $sortQuery; ?>",
                                firstLastUse: true,
                                first: '←',
                                last: '→'
                            }).on('page', function(event, num){
                                window.location.href = '?pageCnt=' + num + '<?php echo $genreQuery; echo $ltrQuery; echo $srchQuery; ?>';
                            });
                        });
                    });
                } else if (params.srchYear) {
                    $http.post('https://www.tg4tech.com/api/programmes/basicSearch?q='+params.srchYear+'&searchfield=prog_production_year&orderon=prog_dynamic_branding_title&orderby=asc&page='+params.pageCnt).then(function(d){
                        $scope.allDataLoaded(d.data);
                        $scope.total = d.data.total_count; 

                        $scope.pageTotal = Math.ceil($scope.total/20);
                        console.log("Total ", $scope.total, "Pages ", $scope.pageTotal);

                        $(document).ready(function () {
                            $('#pagination-here').bootpag({
                                total: $scope.pageTotal,
                                page: <?php echo $pageCnt; ?>,
                                maxVisible: 20,
                                leaps: false,
                                //href: "?pageCnt={{number}}<?php echo $sortQuery; ?>",
                                firstLastUse: true,
                                first: '←',
                                last: '→'
                            }).on('page', function(event, num){
                                window.location.href = '?pageCnt=' + num + '<?php echo $genreQuery; echo $ltrQuery; echo $srchQuery; ?>';
                            });
                        });
                    });
                } else if (params.srchComp) {
                    $http.post('https://www.tg4tech.com/api/programmes/basicSearch?q='+params.srchComp+'&searchfield=prog_production_company&orderon=prog_production_company&orderby=asc&page='+params.pageCnt).then(function(d){
                        $scope.allDataLoaded(d.data);
                        $scope.total = d.data.total_count; 

                        $scope.pageTotal = Math.ceil($scope.total/20);
                        console.log("Total ", $scope.total, "Pages ", $scope.pageTotal);

                        $(document).ready(function () {
                            $('#pagination-here').bootpag({
                                total: $scope.pageTotal,
                                page: <?php echo $pageCnt; ?>,
                                maxVisible: 20,
                                leaps: false,
                                //href: "?pageCnt={{number}}<?php echo $sortQuery; ?>",
                                firstLastUse: true,
                                first: '←',
                                last: '→'
                            }).on('page', function(event, num){
                                window.location.href = '?pageCnt=' + num + '<?php echo $genreQuery; echo $ltrQuery; echo $srchQuery; ?>';
                            });
                        });
                    });
                } else if (params.srchTitle) {
                    $http.post('https://www.tg4tech.com/api/programmes/basicSearch?q='+params.srchTitle+'&searchfield=prog_dynamic_branding_title&page='+params.pageCnt).then(function(d){
                        $scope.allDataLoaded(d.data); 
                        $scope.total = d.data.total_count;

                        $scope.pageTotal = Math.ceil($scope.total/20);
                        console.log("Total ", $scope.total, "Pages ", $scope.pageTotal);

                        $(document).ready(function () {
                            $('#pagination-here').bootpag({
                                total: $scope.pageTotal,
                                page: <?php echo $pageCnt; ?>,
                                maxVisible: 20,
                                leaps: false,
                                //href: "?pageCnt={{number}}<?php echo $sortQuery; ?>",
                                firstLastUse: true,
                                first: '←',
                                last: '→'
                            }).on('page', function(event, num){
                                window.location.href = '?pageCnt=' + num + '<?php echo $genreQuery; echo $ltrQuery; echo $srchQuery; ?>';
                            });
                        });
                    });
                } else if (params.srchTxt) {
                    $http.post('https://www.tg4tech.com/api/programmes/_search?q='+params.srchTxt+'&fields=prog_title,prog_production_company,prog_production_year,prog_coord,prog_press_desc_short_eng,prog_press_desc_short_gae,mat_id,prog_dynamic_branding_title,prog_first_tx_date&limit=50&orderon=prog_production_year&orderby=asc&page='+params.pageCnt).then(function(d){
                        $scope.allDataLoaded(d.data); 
                        $scope.total = d.data.total_count;

                        $scope.pageTotal = Math.ceil($scope.total/20);
                        console.log("Total Pages ", $scope.pageTotal);

                        $(document).ready(function () {
                            $('#pagination-here').bootpag({
                                total: $scope.pageTotal,
                                page: <?php echo $pageCnt; ?>,
                                maxVisible: 20,
                                leaps: false,
                                //href: "?pageCnt={{number}}<?php echo $sortQuery; ?>",
                                firstLastUse: true,
                                first: '←',
                                last: '→'
                            }).on('page', function(event, num){
                                window.location.href = '?pageCnt=' + num + '<?php echo $genreQuery; echo $ltrQuery; echo $srchQuery; ?>';
                            });
                        });
                    });
                } else if (params.srchDate) {
                    function addZero(number) {
                        console.log(number.length);
                        if (number.length == 1) {
                            if (number < 10) {
                                return "0" + number;
                            }
                        } else {
                            return number;
                        }
                    }

                    //console.log('- ', params.srchDate.indexOf('-'), '/ ', params.srchDate.indexOf('/'), '. ', params.srchDate.indexOf('.'));

                    if (params.srchDate.indexOf('/') !== -1) {
                        var strSplitDate = params.srchDate.split('/');
                    } else if (params.srchDate.indexOf('.') !== -1) {
                        var strSplitDate = params.srchDate.split('.');
                    } else if (params.srchDate.indexOf('-') !== -1) {
                        var strSplitDate = params.srchDate.split('-');
                    }

                    //console.log('POC', strSplitDate[2]);

                    var dbDate = strSplitDate[2] + '-' + addZero(strSplitDate[1]) + '-' + addZero(strSplitDate[0]);
                    //console.log(dbDate);

                    $http.post('https://www.tg4tech.com/api/programmes/archiveTextDateSearch?q='+params.srchTxt+'&date='+dbDate+'&fields=prog_thumbnail_src,prog_poster_src,prog_title,prog_vod_category,prog_production_company,prog_production_year,prog_press_desc_short_eng,prog_press_desc_short_gae,mat_id,prog_dynamic_branding_title,prog_first_tx_date,cue_points&limit=20&searchfield=prog_dynamic_branding_title&orderon=prog_dynamic_branding_title&page='+params.pageCnt).then(function(d){
                        $scope.allDataLoaded(d.data);
                        $scope.total = d.data.total_count;

                        $scope.pageTotal = Math.ceil($scope.total/20);
                        console.log("Total Pages ", $scope.pageTotal);

                        $(document).ready(function () {
                            $('#pagination-here').bootpag({
                                total: $scope.pageTotal,
                                page: <?php echo $pageCnt; ?>,
                                maxVisible: 20,
                                leaps: false,
                                //href: "?pageCnt={{number}}<?php echo $sortQuery; ?>",
                                firstLastUse: true,
                                first: '←',
                                last: '→'
                            }).on('page', function(event, num){
                                window.location.href = '?pageCnt=' + num + '<?php echo $genreQuery; echo $ltrQuery; echo $srchQuery; ?>';
                            });
                        });
                    });
                } else {
                    $http.post('https://www.tg4tech.com/api/programmes/basicSearch?q=^A&searchfield=prog_dynamic_branding_title&orderon=prog_dynamic_branding_title&orderby=asc&page='+params.pageCnt).then(function(d){ 
                        $scope.allDataLoaded(d.data); 
                        $scope.total = d.data.total_count; 

                        $scope.pageTotal = Math.ceil($scope.total/20);
                        console.log("Total ", $scope.total, "Pages ", $scope.pageTotal);

                        $(document).ready(function () {
                            $('#pagination-here').bootpag({
                                total: $scope.pageTotal,
                                page: <?php echo $pageCnt; ?>,
                                maxVisible: 20,
                                leaps: false,
                                //href: "?pageCnt={{number}}<?php echo $sortQuery; ?>",
                                firstLastUse: true,
                                first: '←',
                                last: '→'
                            }).on('page', function(event, num){
                                window.location.href = '?pageCnt=' + num + '<?php echo $genreQuery; echo $ltrQuery; echo $srchQuery; ?>';
                            });
                        });
                    });
                }

                $scope.allDataLoaded = function(da){
                    $scope.pAll=da.results;
                    console.log('RESULTS... ', $scope.pAll);
                }
            });
        </script>
    </div>
</section>

<?php get_footer(); ?>