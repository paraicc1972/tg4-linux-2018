<?php /* Template Name: Foghlaim - Play Video */ ?>

<?php include(locate_template('/header-player.php')); ?>

<div class="section-header">
	<h1 class="section-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Foghlaim: Seinnteoir' : 'Foghlaim: Player'); ?></h1>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
    <div class="section-submenu-wrap">
        <ul class="section-submenu-list-foghlaim">
            <?php wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=1&exclude=32582,93940,93943'); ?>
            &nbsp;
        </ul>
    </div>
</div>

<?php
$progID = ctype_digit($_GET['pid']) ? $_GET['pid'] : '';
$daysLeft = ctype_digit($_GET['dlft']) ? $_GET['dlft'] : '';
$pageCnt = ctype_digit($_GET['pageCnt']) ? $_GET['pageCnt'] : 1;
$pageOffset = ($pageCnt-1)*20;
$progSort = $_GET['progSort'];
$srchQuery = $_GET['srchTxt'];

//Check for the programme sort in querystring
if (isset($progSort)) {
    $sortQuery = "&progSort=" . $progSort;
}

if (isset($_GET['srchTxt'])) {
    $srchTxt = $_GET['srchTxt'];
    $srchQuery = "&srchTxt=" . $_GET['srchTxt'];
}
?>
<section class="plyr-feat-section">
    <div class="section-panel-dark-3">
        <!-- Load Player -->
        <div class="plyr-feat center-panel">
            <div class="plyr-feat-wrap">
                <?php if (($Econ=="AD_SUPPORTED") && ($geoCode == "IE")) { ?>
                    <!-- SSAI Enabled Player - 2xPreRolls | 1x Bumper -->
                    <video id="Subtitles" class="video-js vjs-big-play-centered"
                        controls preload="auto" width="100%" height="auto"
                        data-account="1555966122001"
                        data-player="rkPVmp4yM"
                        data-embed="default"
                        data-video-id="<?php echo $progID ?>">
                        <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p> 
                    </video>
                    <script src="https://players.brightcove.net/1555966122001/rkPVmp4yM_default/index.min.js"></script>
                <?php } else { ?>
                    <!-- No Ads Player -->
                    <video id="Subtitles" class="video-js vjs-big-play-centered"
                        controls preload="auto" width="100%" height="auto"
                        data-account="1555966122001"
                        data-player="HJlOg6KUz"
                        data-embed="default"
                        data-video-id="<?php echo $progID ?>">
                        <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p> 
                    </video>
                    <script src="https://players.brightcove.net/1555966122001/HJlOg6KUz_default/index.min.js"></script>
                <?php } ?>

                <!-- google analytics plugin -->
                <script src="https://d1og0s8nlbd0hm.cloudfront.net/js/min/videojs.ga.videocloud.min.js"></script>
                <!-- video.js BC Player Controls -->
                <script>
                    var myPlayer = videojs("Subtitles");
                    var tg4Player = videojs("Subtitles");
                    var part=0;

                    videojs('Subtitles').one('bc-catalog-error', function() {
                        var myPlayer = this,
                        specificError;
                        myPlayer.errors({
                        'errors': {
                          '-3': {
                            'headline': '<h1>Geo-Restricted!</h1>Níl an clár seo le fáil i do Réigiúnsa.<br />Breis eolas le fáil san leathanach <a href="https://www.tg4.ie/ga/faq/frequently-asked-questions/">Ceisteanna Coitianta</a>.<br /><br />This programme is not avaialble in your region.<br />Further information from our <a href="https://www.tg4.ie/en/faq/frequently-asked-questions/">FAQ</a> page.',
                            'type': 'CLIENT_GEO'
                                }
                            }
                        });
                        if (typeof(myPlayer.catalog.error) !== 'undefined') {
                            specificError = myPlayer.catalog.error.data[0];
                            if (specificError !== 'undefined' & specificError.error_subcode == "CLIENT_GEO") {
                                myPlayer.error({code:'-3'});
                            };
                        };
                    });

                    // GA Custom Script
                    tg4Player.on('loadstart',function() {
                        tg4Player = this;
                        /* console.log("catalog object: ",this.catalog.load()); 
                        console.log("mediainfo object: ", this.mediainfo); */
                        tg4Player.ga();
                    });

                    myPlayer.ready(function() {
                        // get a reference to the player
                        myPlayer = this;

                        myPlayer.on("play", function (evt) {
                            console.log('Asset Play');
                        });

                        myPlayer.on("pause", function (evt) {
                            console.log('Asset Paused');
                        });

                        myPlayer.on("ended", function (evt) {
                            console.log('Asset Ended');
                        });

                    });
                </script>
            </div>
            <div class="prog-feat-ad mod-ad">
                <h2 class="mod-ad-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Fógraíocht' : 'Advertisement'); ?></h2>
                <div class="ad-wrapper">
                    <iframe src="https://www.tg4.ie/wp-content/themes/tg4-starter/mpu-banner.htm" height="250" width="300" scrolling="no" frameborder="0"></iframe>
                    <!-- <iframe src="<?php //echo site_url() . '/wp-content/themes/tg4-starter/'; ?>mpu-banner.htm" height="250" width="300" scrolling="no" frameborder="0"></iframe> -->
                </div>
            </div>
        </div>
    </div>
    <div class="plyr-feat-intro-wrap">
        <div class="center-panel">
            <div class="prog-feat-intro">
                <div class="prog-feat-toolbar">
                    <div class="prog-remind">
                        <div class="prog-remind-txt"><?php echo $progTitle . ' ' . $progSeries . '-' . $progEpisode; ?></div>
                        <?php if ($daysLeft < 35) { ?>
                            <div class="prog-remind-count"><span><?php echo $daysLeft; ?></span><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Lá Fagtha' : 'Days Left'); ?></div>
                        <?php } ?>
                    </div>
                    <div class="prog-share">
                        <!-- Facebook -->
                        <a href="#" onClick="window.open('https://www.facebook.com/sharer.php?u=<?php echo 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>','TG4/Facebook Share','resizable,height=350,width=500'); return false;" class="facebook-share">Facebook</a>
                        <noscript><a href="https://www.facebook.com/sharer.php?u=<?php echo 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" target="_blank" class="facebook-share">Facebook</a></noscript>
                        <!-- Twitter -->    
                        <a href="#" onClick="window.open('https://twitter.com/share?text=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Féach seo...' : 'Féach seo...'); ?>&amp;url=<?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>&amp;hashtags=TG4','TG4/Twitter Share','resizable,height=350,width=500'); return false;" class="twitter-share">Twitter</a>
                        <noscript><a href="https://twitter.com/share?text=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Féach seo...' : 'Féach seo...'); ?>&amp;url=<?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>&amp;hashtags=TG4" target="_blank" class="twitter-share">Twitter</a></noscript>
                        <!-- Email -->
                        <a href="mailto:?Subject=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Suíomh TG4 - https://www.tg4.ie' : 'TG4 Website - https://www.tg4.ie'); ?>&amp;Body=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'A%20Chara,%0A%0AB\'fhéidir%20go%20mbeadh%20suim%20agat%20sa%20chlár%20seo%20ar%20suíomh%20idirlín%20TG4!%0A%0A' : 'A%20Chara,%0A%0AI%20saw%20this%20on%20the%20TG4%20website%20and%20thought%20you%20might%20be%20interested!%0A%0A'); ?><?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" class="email-share">Email</a>
                    </div>
                </div>
                <div class="prog-feat-txt">
                    <p class="bold"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Rátáil Tuismitheora: ' . $progPG : 'Parental Rating: ' . $progPG); ?></p>
                    <p><?php echo (ICL_LANGUAGE_CODE == "ga" ? $progGaeDesc : $progEngDesc); ?></p>
                    <p class="bold"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Chéad Taispeántas: ' . date("j ", strtotime($showStartDate)) . schedule_daily_abbr(date("M", strtotime($showStartDate))) : 'First Shown: ' . $showStartDate); ?></p>
                    <p><a href="<?php echo $linkURL; ?>" target="_New"><?php echo $linkText; ?></a></p>
                </div>
                <?php 
                $noSubs = array('Nuacht TG4','GAA Beo','GAA 2017','Rugbaí Beo','7 Lá','Peil na mBan Beo');
                if (!in_array($progSeriesTitle, $noSubs)) { ?>
                    <div class="prog-feat-keyword" style="background-color:#ffffff; border: 1px solid #043244; padding:8px; margin: 5px 0px 5px 0px;">
                        <p class="bold"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Rogha Fotheidil' : 'Optional Subtitles'); ?></p>
                        <p style="font-size:small;"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Le fotheidil a roghnú, brú [CC][<img src="https://s3.amazonaws.com/tg4-docs/images/player-subs.png">] ag bun an scáileáin.' : 'To choose subtitles, press [CC][<img src="https://s3.amazonaws.com/tg4-docs/images/player-subs.png">] in the player controls.'); ?></p>
                        <p class="bold"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'An Seinnteoir ar IOS' : 'Player on IOS devices'); ?></p>
                        <p style="font-size:small;"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Le breathnú ar an Seinnteoir ar IOS, íoslódáil Aip TG4 anseo... <a href="https://itunes.apple.com/ie/app/tg4-player/id598591024?mt=8" target="_New">https://itunes.apple.com/ie/app/tg4-player/id598591024?mt=8</a>' : 'To access the player on an IOS device, you need to download the TG4 Player App. Free to download here... <a href="https://itunes.apple.com/ie/app/tg4-player/id598591024?mt=8" target="_New">https://itunes.apple.com/ie/app/tg4-player/id598591024?mt=8</a>'); ?></p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- / Schema.org Microdata -->
    <meta itemprop="video" itemscope itemtype="https://schema.org/VideoObject"/>
    <meta itemprop="name" content="<?php echo $progName; ?>" />
    <meta itemprop="description" content="<?php echo $progEngDesc; ?>" />
    <meta itemprop="contentURL" content="<?php echo 'https://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>" />
    <meta itemprop="width" content="960" />
    <meta itemprop="height" content="540" />
    <meta itemprop="thumbnail" content="<?php echo $progBCImage; ?>" />
    <meta itemprop="inLanguage" content="EN" />
    <meta itemprop="isFamilyFriendly" content="<?php echo $famFriendly; ?>" />
    <meta itemprop="contentLocation" content="Ireland" />
    <meta itemprop="encodingFormat" content="MP4" />
    <meta itemprop="videoFrameSize" content="960x540" /> 
    <meta itemprop="about" content="<?php echo (ICL_LANGUAGE_CODE == "ga" ? $progGaeDesc : $progEngDesc); ?>" />
    <meta itemprop="genre" content="<?php echo $progCategory; ?>" />
    <meta itemprop="author" content="TG4" /> 
    <meta itemprop="publisher" content="tg4.ie" />
</section>

<section class="section-panel-dark-2">
    <div class="title-tab-wrap">
        <h2 class="title-tab-dark-2"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Físeáin sa tSraith' : 'Series Videos'); ?></h2>
    </div>
    <?php include(locate_template('/includes/featured-videos-series-api.php')); ?>
</section>

<section class="section-panel-white">
    <div class="title-tab-wrap">
        <h2 class="title-tab-white"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Físeáin Faoi Thrácht' : 'Featured Videos'); ?></h2>
    </div>
    <?php include(locate_template('/includes/featured-videos-feature-api.php')); ?>
</section>

<!-- Angular -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular-touch.min.js"></script>
<script src='https://d1og0s8nlbd0hm.cloudfront.net/js/angular-slick.js'></script>
<script src="https://d1og0s8nlbd0hm.cloudfront.net/js/app-slick-v1.js"></script>
<!-- Angular -->

<?php get_footer(); ?>