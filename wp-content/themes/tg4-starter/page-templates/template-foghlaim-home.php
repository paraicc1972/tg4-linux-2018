<?php /* Template Name: Foghlaim - Home */ ?>

<?php get_header(); ?>

<div class="section-header">
	<h1 class="section-title"><?php echo get_the_title($post->post_parent); ?></h1>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
	<div class="section-submenu-wrap">
		<ul class="section-submenu-list-foghlaim">
            <?php
            wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->ID . '&depth=1&exclude=32582,35640,93940,93943');
            /* if(SwpmMemberUtils::is_member_logged_in()) {
            	wp_list_pages('include=35640&title_li=');
            } */
            ?>&nbsp;
        </ul>
	</div>
</div>

<?php
if (ICL_LANGUAGE_CODE == "ga") {
    $posts = get_posts(array(
    'numberposts' => -1,
    'post_type' => 'Page',
    'meta_key' => 'homepage_foghlaim_feature',
    'orderby' => 'modified',
	'order' => 'DESC',
	'post_status' => 'publish',
    'meta_value' => '1',
    'exclude' => 39558
	));
} else {
    $posts = get_posts(array(
    'numberposts' => -1,
    'post_type' => 'Page',
    'meta_key' => 'homepage_foghlaim_feature',
    'orderby' => 'modified',
	'order' => 'DESC',
	'post_status' => 'publish',
    'meta_value' => 'yes'
	));
}

if($posts)
{ ?>
	<section class="slider-wrapper" style="margin-top: -40px;">
		<div id="full-width-slider" class="royalSlider heroSlider rsMinW">
		    <?php foreach($posts as $post)
		    { ?>
			    <section class="rsContent">
			        <div class="blockContainer">
						<h2 class="title-block rsABlock" data-fade-effect="true" data-move-effect="left" data-move-offset="50" data-speed="400" data-easing="easeOutSine"><?php echo get_field("intro_title") ?></h2>
				        <?php if (get_field("intro_sub_title")) { ?>
				        	<div class="content-block-foghlaim rsABlock" data-fade-effect="true" data-move-effect="left" data-move-offset="50" data-speed="400" data-easing="easeOutSine">
				            	<h3 class="content-subtitle"><?php echo get_field("intro_sub_title") ?></h3>
				            	<?php if (get_field("intro_text")) { ?>
				            		<p><?php echo get_field("intro_text") ?></p>
				            	<?php } ?>
				        	</div>
				        <?php } ?>
				        <div class="rsABlock" data-fade-effect="true" data-move-effect="left" data-move-offset="50" data-speed="400" data-easing="easeOutSine">
				        	<a href="<?php echo get_permalink($post->ID); ?>" class="btn-header">Breis Eolais<span></span></a>
				        </div>
			        </div>
			        <img class="rsImg" src="<?php echo(wp_get_attachment_url(get_post_thumbnail_id($post->ID)));?>" alt="<?php echo get_field("intro_title") ?>" title="<?php echo get_field("intro_title") ?>" width="1920" height="1080">
			    </section> 
			<?php } ?>
		</div>
	</section>
<?php } ?>

<section class="player-mods">
    <h2 class="visuallyhidden">Ceachtanna</h2>
    <div class="plyrmods-wrapper">
        <?php
		global $id;		
        if (ICL_LANGUAGE_CODE == "ga") {
			$id = 30206;
		} else {
			$id = 40224;
		}
		//echo $id;
		$pages = get_pages('title_li=&child_of='.$id.'&sort_column=post_modified&sort_order=desc&echo=0&parent='.$id.'&exclude=35683');
		foreach($pages as $page) {
        	$feat_image = wp_get_attachment_url(get_post_thumbnail_id($page->ID));
			$content = $page->post_content;
		    $content = apply_filters('the_content', $content);
		?>
	        <section class="mod-1">
	            <div class="player-mod-notice">
	                <a href="<?php echo get_page_link($page->ID) ?>" class="prog-panel">
	                    <h3 class="player-title"><?php echo get_the_title($page->ID); ?></h3>
	                    <img src="<?php echo $feat_image; ?>" alt="<?php echo get_the_title($page->ID); ?>" class="prog-img">
	                    <div class="prog-footer">
	                        <h4 class="prog-episode-title"><?php the_field('intro_sub_title', $page->ID); ?></h4>
	                        <!-- <div class="prog-season"></div>
	                        <div class="prog-episode"></div> -->
	                        <div class="arrow-box"></div>
	                    </div>
	                    <div class="prog-desc"><span><?php echo $content; ?></span></div>
	                </a>
	            </div>
	        </section>
        <?php } ?>
    </div>
</section>

<section class="prog-feat-section">
	<div class="section-panel-white">
	    <div class="prog-feat center-panel">
	    	<div class="prog-feat-wrap"><a href="http://www.nuigalway.ie/" target="_New"><img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2016/10/Logo-OEG.gif" border="0"></a><br />
	    		Is comhthionscnamh de chuid TG4 agus Acadamh na hOllscolaíochta Gaeilge, Ollscoil na hÉireann, Gaillimh é TG4 Foghlaim.<p><br />Féach thíos roinnt rogha clár ó Sheinnteoir TG4.  Tuilleadh ar fáil ag <a href="https://www.tg4.tv">www.tg4.tv</a>.</p></div>
	    </div>
	</div>
</section>

<?php get_footer(); ?>