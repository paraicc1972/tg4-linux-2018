<?php /* Template Name: Foghlaim - Listing */ ?>

<?php get_header(); ?>

<div class="section-header">
	<h1 class="section-title"><?php echo get_the_title($post->post_parent); ?></h1>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
	<div class="section-submenu-wrap">
		<ul class="section-submenu-list-foghlaim">
            <?php
			wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=1&exclude=32582,93940,93943');
            /* if(SwpmMemberUtils::is_member_logged_in()) {
            	wp_list_pages('include=35640&title_li=');
            } */
            ?>&nbsp;
        </ul>
	</div>
</div>

<?php
$feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));

if ($feat_image != '') {
	if (get_field("centre_feature_image") == '0' || get_field("centre_feature_image") == 'no') {
		$feat_Class = "prog-head";
	} elseif (get_field("centre_feature_image") == '1' || get_field("centre_feature_image") == 'yes')  {
		$feat_Class = "prog-headcenter";
	} else {
		$feat_Class = "prog-head";
	}
?>
	<section class="<?php echo $feat_Class; ?>" style="background-image: url(<?php echo $feat_image; ?>);">
		<div class="blockContainer">
			<?php 
			if (get_field("intro_text")) { ?>
			<div class="content-block">
				<h2 class="content-subtitle"><?php echo get_field("intro_sub_title"); ?></h2>
				<p><?php echo get_field("intro_text"); ?></p>
			</div>
			<?php } ?>
		</div>
	</section>
<?php } 

if ($post->ID == 72457) { ?>
	<section class="constituency-mods">
	    <div class="constituencymods-wrapper">
    	<div class="constituency-mod-wrap">
<?php } else if ($post->ID == 72866) { ?>
	<section class="governance-mods">
	    <div class="governancemods-wrapper">
    	<div>
<?php } else { ?>
	<section class="player-mods">
	    <div class="plyrmods-wrapper">
    	<div class="governance-mod-wrap">
		<?php }
		global $id;
		$pages = get_pages('title_li=&child_of='. $id .'&sort_column=menu_order&sort_order=desc&echo=0&parent='. $id .'&exclude=35683');
		foreach($pages as $page) {
        	$feat_image = wp_get_attachment_url(get_post_thumbnail_id($page->ID));
			$content = $page->post_content;
		    $content = apply_filters('the_content', $content);
		    if ($post->ID == 72457) { ?>
			    <section class="mod-1">
		            <div class="constituency-mod-notice">
		                <a href="<?php echo get_page_link($page->ID) ?>" class="prog-panel">
		                    <h5 class="vote-constituency"><?php echo get_the_title($page->ID); ?></h3>
		                </a>
		            </div>
		        </section>
			<?php } else if ($post->ID == 72866) { ?>
			    <section class="mod-1">
		            <div class="governance-mod-notice">
		            	<div class="file-btn"><a href="<?php echo get_page_link($page->ID) ?>" class="mod-file"><div class="file-btnImg"></div></a></div>
			            <div class="file-title"><a href="<?php echo get_page_link($page->ID) ?>" class="governance-title"><?php echo get_the_title($page->ID); ?></a></div>
		            </div>
		        </section>
			<?php } else { ?>
	        <section class="mod-1">
	            <div class="player-mod-notice">
	                <a href="<?php echo get_page_link($page->ID) ?>" class="prog-panel">
	                    <h3 class="player-title"><?php echo get_the_title($page->ID); ?></h3>
	                    <img src="<?php echo $feat_image; ?>" alt="<?php echo get_the_title($page->ID); ?>" class="prog-img">
	                    <div class="prog-footer">
	                        <h4 class="prog-episode-title"><?php the_field('intro_sub_title', $page->ID); ?></h4>
	                        <!-- <div class="prog-season"></div>
	                        <div class="prog-episode"></div> -->
	                        <div class="arrow-box"></div>
	                    </div>
	                    <div class="prog-desc"><span><?php echo $content; ?></span></div>
	                </a>
	            </div>
	        </section>
        <?php }} ?>
    	</div>
    </div>
</section>

<section class="prog-feat-section">
	<div class="section-panel-white">
	    <div class="prog-feat center-panel">
	    	<div class="prog-feat-wrap">
	    		<?php echo apply_filters('the_content', get_post_field('post_content', $post_id)); ?>
				<p><a href="http://www.nuigalway.ie/" target="_New"><img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2016/10/Logo-OEG.gif" border="0"></a><br />Is comhthionscnamh de chuid TG4 agus Acadamh na hOllscolaíochta Gaeilge, Ollscoil na hÉireann, Gaillimh é TG4 Foghlaim.</p>
				<p><br />Féach thíos roinnt rogha clár ó Sheinnteoir TG4.  Tuilleadh ar fáil ag <a href="https://www.tg4.tv">www.tg4.tv</a>.</p>
	    	</div>
			<div class="prog-feat-file mod-file">
		    <?php 
		    if(have_rows('file_name')):
		    	while(have_rows('file_name')): the_row();
					if (get_sub_field('file_title')) { ?>
				    	<div class="file-content-wrap">
	                        <div class="file-btn"><a href="<?php the_sub_field('upload_file') ?>" class="mod-file" target="_blank"><div class="file-btnImg"></div></a></div>
	                        <div class="file-content">
	                            <a href="<?php the_sub_field('upload_file') ?>" class="mod-file" target="_blank"><?php the_sub_field('file_title'); ?></a>
	                         </div>
	                    </div>
		    <?php 	}
		    	endwhile;
		    endif;
		    ?>
        	</div>
	    </div>
	</div>
</section>

<?php get_footer(); ?>