<?php /* Template Name: NoVideo-Programme */ ?>

<?php include(locate_template('/header-prog.php'));

if (!post_password_required()) { ?>
	<div class="section-header">
		<h1 class="section-title">
			<?php
			if (get_the_title($post->post_parent) == "Programmes" || get_the_title($post->post_parent) == "Cláir" || get_the_title($post->post_parent) == "Sport" || get_the_title($post->post_parent) == "Spórt") {
				echo get_the_title($post->ID);
			} else {
				echo get_the_title($post->post_parent);
			}
			?>
		</h1>
		<div class="showing-times">
			<?php 
			$broadcast_day = get_field("broadcast_day");
			$broadcast_time = get_field("broadcast_time");
			if($broadcast_day) { ?>
			<div class="showing-box">
				<div class="showing-day"><?php echo (ICL_LANGUAGE_CODE == "ga" ? substr($broadcast_day, 0, 4) : substr($broadcast_day, 0, 3)); ?></div>
				<div class="showing-time"><?php echo $broadcast_time; ?></div>
			</div>
			<?php }
			$repeat_day = get_field("repeat_day");
			$repeat_time = get_field("repeat_time");
			if($repeat_day) { ?>
			<div class="showing-box">
				<div class="showing-day"><?php echo (ICL_LANGUAGE_CODE == "ga" ? substr($repeat_day, 0, 4) : substr($repeat_day, 0, 3)); ?></div>
				<div class="showing-time"><?php echo $repeat_time; ?></div>
			</div>
			<?php } ?>
		</div>
	</div>

	<!-- Sub-navigation -->
	<div class="section-submenu">
		<div class="section-submenu-wrap">
			<ul class="section-submenu-list">
				<?php
				$ancestors = get_post_ancestors($post->ID);
				//echo $post->ID . " - " . $post->post_parent;
				//echo $ancestors[2];
				if ($ancestors[2] == 137 || $ancestors[2] == 140) {
					wp_list_pages('sort_column=menu_order&title_li=&child_of='. $ancestors[1] . '&depth=0');
				} else {
					if ($post->post_parent != 137 && $post->post_parent != 140 && $post->post_parent != 390 && $post->post_parent != 393 || ($ancestors[2] == 137 && $ancestors[2] == 140)) {
						wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=0');
					} elseif ($post->ID != 137 && $post->ID != 140) {
						wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->ID . '&depth=0');
					}
				}
				?>
				&nbsp;
			</ul>
		</div>
	</div>

	<?php
	$feat_image = '';
	$imgCode = '';

	if (get_field("Series_Code") != '') {
		$imgCode = get_field("Series_Code");
	} 
	if (get_field("Programme_Code") != '') {
		$imgCode = get_field("Programme_Code");
	} 
	if ($imgCode == '') {
		$feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
	}

	if (get_field("centre_feature_image") == '0' || get_field("centre_feature_image") == 'no') {
		$feat_Class = "prog-head";
	} elseif (get_field("centre_feature_image") == '1' || get_field("centre_feature_image") == 'yes')  {
		$feat_Class = "prog-headcenter";
	} else {
		$feat_Class = "prog-head";
	}
	
	if ($feat_image != '') { ?>
		<section class="<?php echo $feat_Class; ?>" style="background-image: url(<?php echo $feat_image; ?>);">
			<div class="blockContainer">
				<?php 
				if (get_field("intro_text")) { ?>
					<div class="content-block">
						<h2 class="content-subtitle"><?php echo get_field("intro_sub_title"); ?></h2>
						<p><?php echo get_field("intro_text"); ?></p>
					</div>
				<?php } ?>
			</div>
		</section>
	<?php } else { ?>
		<script>
			//window.alert("Your screen resolution is: " + window.innerHeight + 'x' + window.innerWidth);
			if (window.innerWidth <= 900) {
			    document.write('<section class="<?php echo $feat_Class; ?>" style="background-image: url(\'https://res.cloudinary.com/tg4/image/upload/w_700,h_395,f_auto,q_auto/<?php echo $imgCode; ?>.jpg\');">');
			} else if (window.innerWidth > 901 && window.innerWidth <= 1408) {
			    document.write('<section class="<?php echo $feat_Class; ?>" style="background-image: url(\'https://res.cloudinary.com/tg4/image/upload/w_1310,h_737,f_auto,q_auto/<?php echo $imgCode; ?>.jpg\');">');
			} else {
			    document.write('<section class="<?php echo $feat_Class; ?>" style="background-image: url(\'https://res.cloudinary.com/tg4/image/upload/f_auto,q_auto/<?php echo $imgCode; ?>.jpg\');">');
			}
		</script>
			<div class="blockContainer">
				<?php 
				if (get_field("intro_text")) { ?>
					<div class="content-block">
						<h2 class="content-subtitle"><?php echo get_field("intro_sub_title"); ?></h2>
						<p><?php echo get_field("intro_text"); ?></p>
					</div>
				<?php } ?>
			</div>
		</section>
	<?php }

	if (get_post_field("post_content")) { ?>
		<section class="prog-feat-section">
			<div class="section-panel-white">
			    <div class="prog-feat center-panel">
			    	<!-- POFL: 27.2.16 - Add Refresh & Timer for Social Walls -->
			    	<?php if ($post->ID == 13735 || $post->ID == 13733 || $post->ID == 13322 || $post->ID == 13211) { ?>
					    <script>
						function checklength(i) {
						    'use strict';
						    if (i < 10) {
						        i = "0" + i;
						    }
						    return i;
						}

						var minutes, seconds, count, counter, timer;
						count = 600; //seconds
						counter = setInterval(timer, 1000);

						function timer() {
						    'use strict';
						    count = count - 1;
						    minutes = checklength(Math.floor(count / 60));
						    seconds = checklength(count - minutes * 60);
						    if (count < 0) {
						        clearInterval(counter);
						        return;
						    }
						    document.getElementById("timer").innerHTML = 'Athlódáil/Refresh ' + minutes + ':' + seconds + ' ';
						    if (count === 0) {
						        location.reload();
						    }
						}
						</script>
						<span id="timer"></span>
					<?php } ?>	

			    	<div class="prog-feat-wrap">
			    		<?php echo apply_filters('the_content', get_post_field('post_content', $post_id)); ?>
			    		<?php if (get_field("content_blue_box")) { ?>
					    	<div class="prog-feat center-panel">
					    		<div class="content-blue-box-1"><?php echo get_field("content_blue_box"); ?></div>
					    	</div>
					    <?php } ?>
			    	</div>
			    
					<?php if(have_rows('file_name') || have_rows('image_name')) { ?>
					<div class="prog-feat-ad mod-ad">
						<!-- Uploaded Files to Appear here -->
						<div class="prog-feat-file mod-file">
					    <?php while(have_rows('file_name')): the_row();
							if (get_sub_field('file_title')) { ?>
						    	<div class="file-content-wrap">
		                            <div class="file-btn"><a href="<?php the_sub_field('upload_file') ?>" class="mod-file" target="_blank"><div class="file-btnImg"></div></a></div>
		                            <div class="file-content">
		                                <a href="<?php the_sub_field('upload_file') ?>" class="mod-file" target="_blank"><?php the_sub_field('file_title'); ?></a>
		                             </div>
		                        </div>
					    <?php } endwhile; ?>
			        	</div>
						<!-- Uploaded Image to Appear here -->
						<div>
						    <?php
						    while(have_rows('image_name')): the_row();
						    	$sideImage = get_sub_field('upload_image');
						    ?>
		                        <image src="<?php echo $sideImage['url']; ?>" style="border:1px solid #043244; margin-bottom:10px;">
						    <?php endwhile; ?>
			        	</div>
			        </div>
					<?php } ?>
			    </div>
			</div>
		</section>
	<?php } ?>

	<!-- Angular -->
	<script src= "https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular-touch.min.js"></script>
    <!-- Angular -->
	<section class="section-panel-dark-2">
		<div class="title-tab-wrap">
			<h2 class="title-tab-dark-2"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Físeáin ar Fáil' : 'Available Videos'); ?></h2>
		</div>

		<?php include(locate_template('/includes/featured-videos-api.php')); ?>
	</section>

	<?php if (get_post_field("social_stream_tag")) { ?>
		<section class="prog-feat-section">
			<div class="section-panel-pale-2">
			    <div class="prog-feat center-panel">
					<?php echo apply_filters('the_content', get_post_field('social_stream_tag', $post_id)); ?>
			    </div>
			</div>
		</section>
	<?php }
} else { ?>
    <section class="prog-feat-section">
        <div class="prog-feat center-panel">
            <div class="prog-feat-wrap">
                <h2><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Pasfhocal Riachtanach'  : 'Password Required'); ?></h2>
                <p><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Chun an leathanach seo a fheiceáil, cuir isteach na pasfhocal thíos:'  : 'To view this protected page, enter the password below:'); ?></p>
                <?php
                echo my_password_form();
                ?>
            </div>
        </div>    
    </section>
<?php } 
get_footer(); ?>