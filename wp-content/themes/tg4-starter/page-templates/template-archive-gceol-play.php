<?php /* Template Name: Gradam Ceoil Archive - Player */ ?>

<?php include(locate_template('/header-archive.php')); ?>

<div class="section-header">
    <h1 class="section-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Cartlann Ghradam Ceoil' : 'Gradam Ceoil Archive'); ?></h1>
    <p>&nbsp;</p>
</div>

<?php
$MatID = $_GET['mat_id'];
$srchTxt = $_GET['srchTxt'];

//echo "***" . $_SERVER['HTTP_USER_AGENT'] . " - " . strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'iphone') . " - " . strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'ipad');
?>

<!-- Sub-navigation -->
<div class="section-submenu">
    <div class="section-submenu-wrap">
        <ul class="section-submenu-list">
            <?php
            $parent_post = get_post($post->post_parent);
            $grandparent = $parent_post->post_parent;
            wp_list_pages('sort_column=menu_order&title_li=&child_of='. $grandparent . '&depth=0'); ?>
            &nbsp;
        </ul>
    </div>
</div>
<?php if (isValidURL("https://dpohx73n66ge6.cloudfront.net/". $MatID . "/" . $MatID . ".mpd")) { ?>
    <section class="plyr-feat-section" ng-app="myApp" ng-controller="myCtrl">
<?php } else { ?>
    <section class="plyr-feat-section">
<?php } ?>
    <div class="srch-feat-plyr-wrap">
        <div class="center-panel">
            <div class="srch-feat-intro">
                <div class="arcform-wrap"><input class="arcform-textbox" name="sbox" id="sbox" placeholder="<?php echo (ICL_LANGUAGE_CODE == "ga" ? " Cuardach..." : " Search..."); ?>" /><a href="#" id="srchBtn" class="arcform-btn" onclick="this.href='../?srchTxt=' + document.getElementById('sbox').value">>></a><a class="arcform-hint" title="<?php echo (ICL_LANGUAGE_CODE == "ga" ? "Is féidir cuardach a dhéanamh ar théarma (faoi bhliain ar leith) anseo e.g. Gaillimh 2001" : "You can search by a certain term (within a year) here e.g. Galway 2001"); ?>">?</a><br /><span class="arcform-text"><?php echo (ICL_LANGUAGE_CODE == "ga" ? "Cuardach: Cuimsitheach" : "Advanced Search"); ?></span></div>
                <div class="arcform-wrap"><input class="arcform-textbox" name="mbox" id="mbox" placeholder="<?php echo (ICL_LANGUAGE_CODE == "ga" ? " Ceol..." : " Music..."); ?>" /><a href="#" id="srchMusicBtn" class="arcform-btn" onclick="this.href='../?srchMusic=' + document.getElementById('mbox').value">>></a><br /><span class="arcform-text"><?php echo (ICL_LANGUAGE_CODE == "ga" ? "Cuardach: Ceol" : "Search: Music Title"); ?></span></div>
                <div class="arcform-wrap"><input class="arcform-textbox" name="pbox" id="pbox" placeholder="<?php echo (ICL_LANGUAGE_CODE == "ga" ? " Rannpháirtí..." : " Participants..."); ?>" /><a href="#" id="srchPartBtn" class="arcform-btn" onclick="this.href='../?srchPart=' + document.getElementById('pbox').value">>></a><br /><span class="arcform-text"><?php echo (ICL_LANGUAGE_CODE == "ga" ? "Cuardach: Rannpháirtí" : "Search: Participants"); ?></span></div>
            </div>
            <!-- <div class="arcform-wrap">
                <select ng-model="yearSel" style="margin-right:10px; width:148px; height:27px;" onchange="document.location.href=this.options[this.selectedIndex].value;">
                    <option value=""><?php //echo (ICL_LANGUAGE_CODE == "ga" ? " Gach Bliain" : " All Years"); ?></option>
                    <option value="../?srchYear=2017">2017</option>
                    <option value="../?srchYear=2016">2016</option>
                    <option value="../?srchYear=2015">2015</option>>
                    <option value="../?srchYear=2014">2014</option>
                    <option value="../?srchYear=2013">2013</option>>
                    <option value="../?srchYear=2012">2012</option>
                    <option value="../?srchYear=2011">2011</option>>
                    <option value="../?srchYear=2010">2010</option>
                    <option value="../?srchYear=2009">2009</option>
                    <option value="../?srchYear=2008">2008</option>
                    <option value="../?srchYear=2007">2007</option>
                    <option value="../?srchYear=2006">2006</option>
                    <option value="../?srchYear=2005">2005</option>
                    <option value="../?srchYear=2004">2004</option>
                    <option value="../?srchYear=2003">2003</option>
                    <option value="../?srchYear=2002">2002</option>
                    <option value="../?srchYear=2001">2001</option>
                    <option value="../?srchYear=2000">2000</option>
                    <option value="../?srchYear=1999">1999</option>
                    <option value="../?srchYear=1998">1998</option>
                </select>
            </div> -->

            <script>
            var input1 = document.getElementById("sbox");
            input1.addEventListener("keyup", function(event) {
                event.preventDefault();
                if (event.keyCode === 13) {
                    document.getElementById("srchBtn").click();
                }
            });
            var input2 = document.getElementById("mbox");
            input2.addEventListener("keyup", function(event) {
                event.preventDefault();
                if (event.keyCode === 13) {
                    document.getElementById("srchMusicBtn").click();
                }
            });
            var input3 = document.getElementById("pbox");
            input3.addEventListener("keyup", function(event) {
                event.preventDefault();
                if (event.keyCode === 13) {
                    document.getElementById("srchPartBtn").click();
                }
            });
            </script>
        </div>
    </div>
    <div class="section-panel-dark-3">
    <?php
    $tablet_browser = 0;
    $mobile_browser = 0;

    if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
        $tablet_browser++;
    }

    if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
        $mobile_browser++;
    }

    if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
        $mobile_browser++;
    }

    $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
    $mobile_agents = array('w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac','blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno','ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-','maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-','newt','noki','palm','pana','pant','phil','play','port','prox','qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar','sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-','tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp','wapr','webc','winw','winw','xda ','xda-');

    if (in_array($mobile_ua,$mobile_agents)) {
        $mobile_browser++;
    }

    if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
        $mobile_browser++;
        //Check for tablets on opera mini alternative headers
        $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
        if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
            $tablet_browser++;
        }
    }

    // Load Player - 1st check to see if rendition exists
    function isValidURL($url) {
        $file_headers = @get_headers($url);
        if (strpos($file_headers[0], "200 OK") > 0) {
            return true;
        } else {
            return false;
        }
    }

    if (isValidURL("https://dpohx73n66ge6.cloudfront.net/". $MatID . "/" . $MatID . ".mpd")) {
    ?>
        <div class="plyr-feat center-panel">
            <div class="arcplyr-feat-wrap" >
                <video id="my-video" class="video-js vjs-big-play-centered" style="position:relative; z-index:0; height: 100%" webkit-playsinline controls autoplay preload="auto" data-setup='{}'>
                    <?php if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'ipad') <= 0 || strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'iphone') <= 0) { ?>
                        <!-- MPEG Dash - Cached -->
                        <source src="https://dpohx73n66ge6.cloudfront.net/<?php echo $MatID; ?>/<?php echo $MatID; ?>.mpd" type="application/mpd+xml">
                    <?php } if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'ipad') > 0 || strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'iphone') > 0) { ?>
                        <!-- HLS (for IOS) -->
                        <source src="http://52.51.94.213:8081/hls/smil:<?php echo $MatID; ?>.smil/playlist.m3u8" type="application/x-mpegURL">
                    <?php } ?>
                    <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
                    <!--track kind="captions" src="subs/<?php //echo $MatID; ?>_eng.vtt" srclang="en" label="English"-->
                </video>

                <!-- video.js BC Player Controls -->
                <script>
                    var myPlayer = videojs("my-video");
                    var titleOverlay = document.getElementsByClassName("vjs-modal-dialog-content")[0]; 
                    var description = document.getElementsByClassName("vjs-modal-dialog-description")[0]; 
                    //var titleOverlay = document.getElementsByClassName("vjs-modal-dialog")[0];
                  
                    //dockSelf.style.widheight = "60px";
                    //dockSelf.style.height = "60px";

                    description.style.height = "60px";
                    //dockSelf.style.backgroundColor = '#FF0000';
                    //dockSelf.style.opacity = 0.72;
                    var cuePoints = [];
                    var restrictions_start = [];
                    var restrictions_end = [];
                    var scene_start = [];
                    var scene_end = [];
                    var scene_metadata = [];
                    var restriction = false;

                    myPlayer.ready(function() {
                        myPlayer.on('loadstart',function() {
                            myPlayer.muted(false);
                            cuePoints = getCuePoints("<?php echo $MatID; ?>");
                        });
                    
                        myPlayer.on("loadedmetadata",function() {
                            // myPlayer.currentTime(71);
                            myPlayer.play();
                        }); //end loadedmetadata
                    
                        myPlayer.on("mouseover",function() {
                            //titleOverlay.style.height = (this.videoHeight() - 50) + "px"
                        });
                    
                        myPlayer.on("mouseout",function() {
                            //titleOverlay.style.height = (this.videoHeight()) + "px"
                        });
                    });
                  
                    /* myPlayer.on('timeupdate', function (e) {
                        var restriction = false;
                        var time_now = "";
                        var scene_metadata[0] = "";
                    
                        for (var i = 0; i < restrictions_start.length; i++) {
                            time_now = Math.floor(this.currentTime())
                            if ((time_now >= restrictions_start[i]) && (time_now <= restrictions_end[i])) {
                                myPlayer.muted(false);
                                titleOverlay.style.opacity = 0.98;
                                titleOverlay.style.height = "100%"
                                titleOverlay.style.backgroundColor = '#000000';
                                titleOverlay.innerHTML= "<h2>... Tá an ábhar seo srianta | This content is restricted ...</h2>";
                                restriction = true;
                            } 
                            if (!restriction) {
                                myPlayer.muted(false);
                                titleOverlay.style.opacity = 0.0;
                                description.innerHTML= "";
                            }
                        }
                        for (var i = 0; i < scene_start.length; i++) {
                            if ((time_now >= scene_start[i]) && (time_now <= scene_end[i])) {
                                description.innerHTML = scene_metadata[i].slice(scene_metadata[i]);
                            }
                        }
                    }); */
                  
                    function getCuePoints(mat_id) {
                        var url = "https://www.tg4tech.com/api/findbyid?id="+mat_id+"&fields=prog_vod_category,prog_title,prog_production_company,prog_production_year,prog_coord,prog_press_desc_short_eng,prog_press_desc_short_gae,mat_id,prog_dynamic_branding_title,prog_first_tx_date,cue_points&limit=1"
                        var method = "POST";
                        var postData = "";
                        var async = true;
                        var request = new XMLHttpRequest();
                    
                        request.onload = function () {
                            var status = request.status;
                            var data = request.responseText; 
                            var json = JSON.parse(data);
                            var i;

                            console.log("Cuepoint Data", data);
                  
                            cuePoints = json.cue_points
                       
                            for (i in cuePoints) {
                                if ((cuePoints[i].name == "restriction_start")) {
                                    restrictions_start.push(cuePoints[i].time)
                                }
                                if ((cuePoints[i].name == "restriction_end")) {
                                    restrictions_end.push(cuePoints[i].time)
                                }
                                if ((cuePoints[i].name == "scene_start")) {
                                    scene_start.push(cuePoints[i].time)
                                    scene_metadata.push(cuePoints[i].metadata)
                                }
                                if ((cuePoints[i].name == "scene_end")) {
                                    scene_end.push(cuePoints[i].time)
                                }
                            }
                        }
                    
                        request.open(method, url, async);
                        request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                        request.send();
                    }
                  
                    function detectIphone() {
                        var uagent = navigator.userAgent.toLowerCase();
                        console.log(uagent)
                        if (uagent.search("iphone") > -1)
                            console.log("You have an iPhone!")
                        else
                            console.log("You Don't have an iPhone!")
                    }
                </script> 
            </div>
            <div class="prog-feat-disc mod-disc">
                <div style="overflow-y:scroll; height:400px; margin-left:-4px; padding-right:4px;">
                    <ol class="prog-cuepoint">
                        <li ng-repeat="p in pAllScene"> <!--track by p.id"-->
                        <!-- <a href="#" ng-click="cue_log_track(p.incode,p.duration,0)">{{timecode_to_seconds(p.incode,25)}}</a>
                        <a class="arcLink" ng-href="javascript:myPlayer.currentTime('{{(p.incode | limitTo: 8) | timeToSeconds}}'); myPlayer.play();"> -->
                        <a href="#" ng-click="cue_log_track(p.incode,p.duration,0)" class="arcLink" style="cursor:pointer;">{{(p.incode | limitTo: 8)}}: <strong><span ng-if="p.title!=''" ng-bind-html="highlight(p.title, '<?php echo $srchTxt; ?>')"></span></strong></a><span ng-if="p.description!=''"><br /><span ng-bind-html="highlight(p.description, '<?php echo $srchTxt; ?>')"></span></span> (Fad: <span ng-if="p.duration!=''"  ng-bind="(p.duration | limitTo: 8)"></span>)<!-- <span ng-if="p.participants!=''"><br /><span ng-bind="p.participants"></span></span><span ng-if="p.incode!=''"><br /><span ng-bind="[p.incode]"></span></span> <span ng-if="p.trackTitle!=''"><br /><span ng-bind="p.trackTitle"></span></span> --></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="plyr-feat-intro-wrap">
        <div class="center-panel">
            <div class="prog-feat-intro">
                <div class="prog-feat-toolbar">
                    <div class="prog-remind">
                        <div class="prog-remind-txt">
                            <?php 
                            // echo "** " . strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'ipad') . " - " . $_SERVER['HTTP_USER_AGENT'] . " **";
                            /* if (isset($_GET['teideal']) && $_GET['teideal'] != '') {
                                    $progName = $_GET['teideal'];
                                    echo $progName;
                            } */
                            ?>
                            <span ng-bind="pAll.prog_dynamic_branding_title"></span> <span ng-bind="pAll.prog_first_tx_date | date: 'dd.MM.yyyy'"></span>
                            </div>
                            <script>
                                var app = angular.module('myApp', []);
                                /* app.config([
                                    '$compileProvider',
                                    function($compileProvider)
                                    {   
                                        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|javascript|ftp|mailto|chrome-extension):/);
                                        // Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)
                                    }
                                ]); */

                                app.filter('returnInt', function () {
                                    return function (number) {
                                        return parseInt(number, 10);
                                    };
                                })

                                app.filter('timeToSeconds', [
                                    'returnIntFilter',
                                    function (returnIntFilter) {
                                        return function (time) {
                                            return time.split(':')
                                            .reverse()
                                            .map(returnIntFilter)
                                            .reduce(function (pUnit, cUnit, index) {
                                                return pUnit + cUnit * Math.pow(60, index);
                                            });
                                        };
                                    }
                                ]);

                                app.controller('myCtrl', function($scope, $http, $sce) {
                                    <?php if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'ipad') > 0) { ?>
                                        $http.post("https://www.tg4tech.com/api/hls?id=<?php echo $MatID; ?>")
                                        .then(function(response) {
                                            $scope.pVideo = response.data;
                                            //console.log("POC ", $scope.pVideo);
                                            $scope.status = response.status;
                                            //console.log('STATUS: ', $scope.status);
                                        }, function(error) {
                                            $scope.status = error.status;
                                            console.log('ERROR STATUS: ', $scope.status);
                                        });
                                    <?php } ?>

                                    $http.post("https://www.tg4tech.com/api/findbyid?id=<?php echo $MatID; ?>&fields=prog_vod_category,prog_title,prog_production_company,prog_production_year,prog_coord,prog_press_desc_short_eng,prog_press_desc_short_gae,mat_id,prog_dynamic_branding_title,prog_first_tx_date,cue_points&limit=1")
                                    .then(function(response) {
                                        $scope.timecode_to_seconds = function(timeString, framerate) {
                                            var timeArray = timeString.split(":");
                                            var hours   = timeArray[0] * 60 * 60;
                                            var minutes = timeArray[1] * 60;
                                            var seconds = timeArray[2];
                                            var frames  = timeArray[3]*(1/framerate);
                                          
                                            var totalTime = parseFloat(hours) + parseFloat(minutes) + parseFloat(seconds) + parseFloat(frames) ;
                                            
                                            //console.log(timeString + " = " + totalTime);
                                            //console.log("Totaltime", totalTime);
                                            
                                            return totalTime;
                                        }

                                        $scope.cue_log_track = function(incode, duration, index){
                                            $scope.vidsrc = "https://dpohx73n66ge6.cloudfront.net/<?php echo $MatID; ?>/<?php echo $MatID; ?>.mpd";

                                            var video = document.querySelector('#my-video_html5_api');

                                            $scope.mark_in_tc = $scope.timecode_to_seconds(incode, 25);
                                            $scope.mark_out_tc =$scope.timecode_to_seconds(incode, 25) + parseFloat($scope.timecode_to_seconds(duration, 25));

                                            console.log("Jump to Video time", video, $scope.timecode_to_seconds(incode, 25));
                                                      
                                            video.currentTime = $scope.timecode_to_seconds(incode, 25);
                                            video.play();
                                        }

                                        $scope.pAll = response.data;
                                        console.log("All Data", $scope.pAll);
                                        //console.log("All Scene Tracks", $scope.pAll.scene_track);
                                        $scope.pAllScene = $scope.pAll.scene_track;
                                        $scopeCueSec = $scope.timecode_to_seconds($scope.pAll.scene_track[1].incode, 25);
                                        console.log('POC: ', $scope.pAll.mat_id, $scope.pAll.prog_title, $scope.pAll.scene_track.length, $scope.pAll.video_mpd_url);
                                        //for(var intCnt=0;intCnt<$scope.pAll.scene_track.length; intCnt++) {
                                            //$scopeCueSecs = $scope.timecode_to_seconds($scope.pAll.scene_track[intCnt].incode, 25);
                                            //console.log("Scene Track Info", $scope.pAll.scene_track[intCnt], $scope.pAll.scene_track[intCnt].participants, $scope.timecode_to_seconds($scope.pAll.scene_track[intCnt].incode, 25), $scopeCueSecs);
                                        //}

                                        $scope.highlight = function(haystack, needle) {
                                            if(!needle) {
                                                return $sce.trustAsHtml(haystack);
                                            }
                                            return $sce.trustAsHtml(haystack.replace(new RegExp(needle, "gi"), function(match) {
                                                return '<span class="highlightedText">' + match + '</span>';
                                            })
                                            );
                                        };
                                    });
                                });
                                </script>
                        </div>
                    </div>
                    <div>
                        <div>
                            <h2 class="mod-disc-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'EOLAS' : 'INFORMATION'); ?></h2>
                            <div class="disc-wrapper">
                                <?php if (ICL_LANGUAGE_CODE == "ga") { ?>
                                    <div><span ng-bind-html="highlight(pAll.prog_press_desc_short_gae, '<?php echo $srchTxt; ?>')"></span></div>
                                    <p><br /><strong>Is de thoradh comhthionscadail idir TG4 agus Taisce Cheol Dúchais Éireann an suíomh seo.</strong></p>
                                <?php } else { ?>
                                    <div><span ng-bind-html="highlight(pAll.prog_press_desc_short_eng, '<?php echo $srchTxt; ?>')"></span></div>
                                    <p><br /><strong>This site is the result of a collaborative project between TG4 and The Irish Traditional Music Archive.</strong></p>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div style="width:50%; float:left;"><img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2018/07/TG4-Logo-2.png" alt="TG4 Logo"></a></div>
                    <div style="width:50%; float:right;"><a href="http://www.itma.ie" target="_blank"><img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2018/07/ITMA-Logo-1.png" alt="ITMA Logo"></a></div>
                </div>
            </div>
        </div>
        <!-- / Schema.org Microdata -->
        <meta itemprop="video" itemscope itemtype="https://schema.org/VideoObject"/>
        <meta itemprop="name" content="<?php echo $progName; ?>" />
        <meta itemprop="description" content="<?php echo $progEngDesc; ?>" />
        <meta itemprop="contentURL" content="<?php echo 'http://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>" />
        <meta itemprop="width" content="960" />
        <meta itemprop="height" content="540" />
        <meta itemprop="thumbnail" content="<?php echo $progVideoStill; ?>" />
        <meta itemprop="inLanguage" content="EN" />
        <meta itemprop="isFamilyFriendly" content="<?php echo $famFriend; ?>" />
        <meta itemprop="contentLocation" content="Ireland" />
        <meta itemprop="encodingFormat" content="MP4" />
        <meta itemprop="videoFrameSize" content="960x540" /> 
        <meta itemprop="about" content="<?php echo (ICL_LANGUAGE_CODE == "ga" ? $progGaeDesc : $progEngDesc); ?>" />
        <meta itemprop="genre" content="<?php echo $progCat; ?>" />
        <meta itemprop="author" content="TG4" /> 
        <meta itemprop="publisher" content="tg4.ie" />
    </div>
    <?php } else { ?>
        <div class="plyr-feat center-panel">
            <div class="plyr-feat-wrap" >
                <div><?php echo (ICL_LANGUAGE_CODE == "ga" ? '<h1>Níl an clár seo ar fáil fós.</h1>':'<h1>This programme is not available yet.</h1>'); ?></div>
            </div>
            <div class="prog-feat-disc mod-disc">
                <h2 class="mod-disc-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'EOLAS' : 'INFORMATION'); ?></h2>
                <div class="disc-wrapper">
                    <?php if (ICL_LANGUAGE_CODE == "ga") { ?>
                        <p><strong>Is de thoradh comhthionscadail idir TG4 agus Taisce Cheol Dúchais Éireann an suíomh seo.</strong></p>
                    <?php } else { ?>
                        <p><strong>This site is the result of a collaborative project between TG4 and The Irish Traditional Music Archive.</strong></p>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</section>

<?php get_footer(); ?>