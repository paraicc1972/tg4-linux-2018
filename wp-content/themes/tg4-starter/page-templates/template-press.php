<?php /* Template Name: Press */ ?>

<?php get_header(); ?>

<div class="section-header">
	<h1 class="section-title">
		<?php echo get_the_title($post->post_parent); ?>
	</h1>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
    <div class="section-submenu-wrap">
        <ul class="section-submenu-list">
            <?php wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=1'); ?>
            &nbsp;
        </ul>
    </div>
</div>

<section class="player-mods">
	<h2 class="visuallyhidden">More on TG4</h2>
	<div class="pressmods-wrapper">
		<div class="press-mod-wrap">
			<section class="mod-1">
			    <div class="player-mod-notice">
			        <a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/corporate/preas/press-releases/' : '/corporate/press/press-releases/'); ?>" class="prog-panel">
			            <h3 class="press-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Nuacht Ráiteas' : 'Press Releases'); ?></h3>
			            <img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2015/08/press-panel-1.jpg" alt="Press Releases" class="prog-img">
			            <div class="press-desc"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Féach anseo na Nuacht Ráitis is déanaí ó TG4...' : 'See the latest news releases from TG4 here...'); ?></div>
						<div class="press-footer">
			                <div class="arrow-box"></div>
			            </div>
			        </a>
			    </div>
			</section>
			<section class="mod-1">
			    <div class="player-mod-notice">
			        <a href="https://www.flickr.com/photos/tg4" target="_New" class="prog-panel">
			            <h3 class="press-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Íomhánna' : 'Images'); ?></h3>
			            <img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2015/08/press-panel-2.jpg" alt="Images" class="prog-img">
			           	<div class="press-desc"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Nasc chuig íomhánna poiblíochta TG4 anseo...' : 'Link to TG4 press images here...'); ?></div>
			           	<div class="press-footer">
			                <div class="arrow-box"></div>
			            </div>
			        </a>
			    </div>
			</section>
			<section class="mod-1">
			    <div class="player-mod-notice">
			        <a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/corporate/preas/weekly-highlights/' : '/corporate/press/weekly-highlights/'); ?>" class="prog-panel">
			            <h3 class="press-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Buaicphointí Seachtainiúla' : 'Weekly Highlights'); ?></h3>
			            <img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2015/08/press-panel-3.jpg" alt="Weekly Highlights" class="prog-img">
			            <div class="press-desc"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Féach anseo buaicphointí clár seachtainiúla...' : 'See weekly programme highlights here...'); ?></div>
			            <div class="press-footer">
			                <div class="arrow-box"></div>
			            </div>
			        </a>
			    </div>
			</section>
			<section class="mod-1">
			    <div class="player-mod-notice">
			        <a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/corporate/preas/prog-preview/' : '/corporate/press/prog-preview/'); ?>" class="prog-panel">
			            <h3 class="press-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Réamhbhreathnú Cláir' : 'Programme Previews'); ?></h3>
			            <img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2015/08/press-panel-4.jpg" alt="Programme Previews" class="prog-img">
			            <div class="press-desc"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Seinnteoir chun réamhbhreathnú ar cláir...' : 'Programme Preview Player...'); ?></div>
			            <div class="press-footer">
			                <div class="arrow-box"></div>
			            </div>
			        </a>
			    </div>
			</section>
			<section class="mod-1">
			    <div class="player-mod-notice">
			        <a href="<?php echo site_url(); ?>/press/" target="_New" class="prog-panel">
			            <h3 class="press-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Seoladh Sceideal' : 'Launch Schedule'); ?></h3>
			            <img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2015/08/press-panel-5.jpg" alt="Launch Schedule" class="prog-img">
			            <div class="press-desc"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Scéala ó sheoladh an sceidil is déanaí anseo...' : 'Details of our most recent schedule launch here...'); ?></div>
			            <div class="press-footer">
			                <div class="arrow-box"></div>
			            </div>
			        </a>
			    </div>
			</section>
			<section class="mod-1">
			    <div class="player-mod-notice">
			        <a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/sceideal/baile/' : '/en/schedule/home/'); ?>" class="prog-panel">
			            <h3 class="press-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Liostaí Teilifíse' : 'TV Listings'); ?></h3>
			            <img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2015/08/press-panel-6.jpg" alt="TG4 TV Listings" class="prog-img">
			            <div class="press-desc"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Féach anseo liosta iomlán an sceidil...' : 'See TG4 TV listings here...'); ?></div>
			            <div class="press-footer">
			                <div class="arrow-box"></div>
			            </div>
			        </a>
			    </div>
			</section>
		</div>
	</div>
</section>

<?php get_footer(); ?>