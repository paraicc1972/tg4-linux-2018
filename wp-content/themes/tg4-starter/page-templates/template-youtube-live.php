<?php /* Template Name: YouTube Live */ ?>

<?php include(locate_template('/header-ytlive.php')); ?>

<div class="section-header">
    <h1 class="section-title">
        <?php
        if (get_the_title($post->post_parent) == "Programmes" || get_the_title($post->post_parent) == "Cláir" || get_the_title($post->post_parent) == "Sport" || get_the_title($post->post_parent) == "Spórt") {
            echo get_the_title($post->ID);
        } else {
            echo get_the_title($post->post_parent);
        }
        ?>
    </h1>
</div>

<!-- Sub-navigation -->
<div class="section-submenu">
    <div class="section-submenu-wrap">
        <ul class="section-submenu-list">
            <?php wp_list_pages('sort_column=menu_order&title_li=&child_of='. $post->post_parent . '&depth=1&exclude=28148'); ?>
            &nbsp;
        </ul>
    </div>
</div>

<section class="plyr-feat-section">
    <div class="plyr-feat-intro-wrap">
        <div class="center-panel">
            <div class="prog-feat-ad mod-ad" style="margin-top: 120px;">
                <h2 class="mod-ad-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Fógraíocht' : 'Advertisement'); ?></h2>
                <div class="ad-wrapper">
                    <?php
                    /* Different Leaderboard fot YouTube Live */
                    /*if (is_page('live/u21-ulster/') || is_page('beo/uladh-f21/')) { ?> */
                    if (is_page('48337') || is_page('48355')) { ?>
                        <iframe src="<?php echo get_template_directory_uri(); ?>/mpu-banner-yp.htm" height="250" width="300" scrolling="no" frameborder="0"></iframe>
                    <?php } else { ?>
                        <iframe src="<?php echo get_template_directory_uri(); ?>/mpu-banner.htm" height="250" width="300" scrolling="no" frameborder="0"></iframe>
                    <?php } ?>
                </div>
            </div>
            <div class="prog-feat-intro">
                <?php if (get_post_field("post_content")) { ?>
                    <div class="c2-feat-txt">
                        <?php echo apply_filters('the_content', get_post_field('post_content', $post_id));
                        if (get_field("content_blue_box")) { ?>
                            <div class="content-blue-box-1" style="width:100%;"><?php echo get_field("content_blue_box"); ?></div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>

<!-- Angular -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular-touch.min.js"></script>
<!-- Angular -->
<section class="section-panel-dark-2">
    <div class="title-tab-wrap">
        <h2 class="title-tab-dark-2"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Físeáin ar Fáil' : 'Available Videos'); ?></h2>
    </div>
    <?php
    //$seriesTag="GAA Beo";
    include(locate_template('/includes/featured-videos-api.php'));
    ?>
</section>

<?php get_footer(); ?>