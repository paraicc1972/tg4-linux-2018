			</main>
			<footer class="footer" role="contentinfo">
				<h2 class="visuallyhidden">Footer Links</h2>		
				<?php //if (get_field("floodlight_tags")) { echo get_field("floodlight_tags"); } ?>
				<div class="footer-main">
					<div class="footer-wrapper">
						<div class="footer-programmes">
							<section class="footer-prog-block">
								<h2><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Cláir Reatha' : 'Current Programmes'); ?></h2>
								<?php
								if (ICL_LANGUAGE_CODE == "ga") {
								    $posts = get_posts(array(
								    'numberposts' => -1,
								    'post_type' => 'Page',
									'orderby'=> 'post_title',
									'order'=>'ASC',
								    'meta_key' => 'current_programme',
								    'meta_value' => '1'
									));

								    $newprogs = get_posts(array(
								    'numberposts' => -1,
								    'post_type' => 'Page',
									'orderby'=> 'post_title',
									'order'=>'ASC',
								    'meta_key' => 'upcoming_programme',
								    'meta_value' => '1'
									));
								} else {
								    $posts = get_posts(array(
								    'numberposts' => -1,
								    'post_type' => 'Page',
									'orderby'=> 'post_title',
									'order'=>'ASC',
								    'meta_key' => 'current_programme',
								    'meta_value' => 'yes'
									));

								    $newprogs = get_posts(array(
								    'numberposts' => -1,
								    'post_type' => 'Page',
									'orderby'=> 'post_title',
									'order'=>'ASC',
								    'meta_key' => 'upcoming_programme',
								    'meta_value' => 'yes'
									));
								}

								if($posts) 
								{
									echo '<ul>';
									$curNine = 0;
		    						foreach($posts as $post) { 
		    							if ($curNine < 12){
		    							?>
		    							<li><a href="<?php echo get_permalink($post->ID); ?>"><?php echo get_the_title($post->ID); ?></a></li>
								<?php }	$curNine++; } 
								if ($curNine > 12) { ?>
	   								<li><a href="#"><?php echo (ICL_LANGUAGE_CODE == "ga" ? '+ Níos Mó' : '+ More'); ?></a></li>
								<?php } echo '</ul>'; } ?>
							</section>
							<section class="footer-prog-block">
								<h2><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Cláir le Theacht' : 'Coming Programmes'); ?></h2>
								<?php if($newprogs)
								{
									echo '<ul>';
		    						$comNine = 0;
		    						foreach($newprogs as $newprog) { 
		    							if ($comNine < 12){
		    							?>
		    							<li><a href="<?php echo get_permalink($newprog->ID); ?>"><?php echo get_the_title($newprog->ID); ?></a></li>
								<?php } $comNine++; } 
								if ($comNine > 12) { ?>
	   								<li><a href="#"><?php echo (ICL_LANGUAGE_CODE == "ga" ? '+ Níos Mó' : '+ More'); ?></a></li>
								<?php } echo '</ul>'; } ?>
							</section>
							<section class="footer-prog-block">
								<h2><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Bailiúcháin' : 'Collections'); ?></h2>
								<ul>
									<li><a href="<?php echo site_url() . "/ga/foghlaim/ceachtanna" ?>">Foghlaim</a></li>
									<li><a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/player/boxset/?st=1916 Seachtar na Cásca' : '/en/player/boxset/?st=1916 Seachtar na Cásca'); ?>">Seachtar na Cásca</a></li>
									<li><a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/player/boxset/?st=An Klondike' : '/en/player/boxset/?st=An Klondike'); ?>">An Klondike</a></li>
									<li><a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/player/boxset/?st=Ar an Aer' : '/en/player/boxset/?st=Ar an Aer'); ?>">Ar an Aer</a></li>
									<li><a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/player/boxset/?st=Bliain in Inis Mór' : '/en/player/boxset/?st=Bliain in Inis Mór'); ?>">Bliain in Inis Mór</a></li>
									<li><a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/player/boxset/?st=Bus Ghlaschú' : '/en/player/boxset/?st=Bus Ghlaschú'); ?>">Bus Ghlaschú</a></li>
									<li><a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/player/boxset/?st=CU Burn 1' : '/en/player/boxset/?st=CU Burn 1'); ?>">CU Burn</a></li>
									<li><a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/player/boxset/?st=Cuimhní ón mBlascaod' : '/en/player/boxset/?st=Cuimhní ón mBlascaod'); ?>">Cuimhní ón mBlascaod</a></li>
									<li><a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/player/boxset/?st=Údar' : '/en/player/boxset/?st=Údar'); ?>">Údar</a></li>
									<!-- For use when more than nine Boxset are available -->
									<li><a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/player/boxset' : '/en/player/boxset'); ?>"><?php echo (ICL_LANGUAGE_CODE == "ga" ? '+ Níos Mó' : '+ More'); ?></a></li>
								</ul>
							</section>
						</div>
						<section class="footer-catchup">
							<div class="catchup-wrapper">
								<h2><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Athdheis' : 'Catch Up'); ?></h2>
								<ul>
									<li><a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/player/baile' : '/en/player/home'); ?>"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/CatchUp-Player.jpg" alt="<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Cláir TG4' : 'TG4 Programmes'); ?>" title="<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Cláir TG4' : 'TG4 Programmes'); ?>" width="174" height="72"></a></li>
									<li><a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/player/gasuir' : '/en/player/kids'); ?>"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/CatchUp-Cula4.jpg" alt="<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Cláir Cúla4' : 'Cúla4 Programmes'); ?>" title="<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Cláir Cúla4' : 'Cúla4 Programmes'); ?>" width="174" height="72"></a></li>
									<li><a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/player/news' : '/en/player/news'); ?>"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/CatchUp-Nuacht.jpg" alt="<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Cláir Nuacht TG4' : 'Nuacht TG4 Programmes'); ?>" title="<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Cláir Nuacht TG4' : 'Nuacht TG4 Programmes'); ?>" width="174" height="72"></a></li>
								</ul>
							</div>
						</section>
						<div class="footer-social">
							<div class="watch-follow-wrap">
								<section class="footer-watch">
									<h2><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Breathnaigh' : 'Watch'); ?></h2>
									<ul class="watch-list">
										<li class="watch-item"><span class="watch-tel" title="TV"></span><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Teilifís' : 'TV'); ?></li>
										<li class="watch-item"><span class="watch-lap" title="PC"></span><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Ríomhaire' : 'PC'); ?></li>
										<li class="watch-item"><span class="watch-tab" title="Tablet"></span><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Táibléad' : 'Tablet'); ?></li>
										<li class="watch-item"><span class="watch-mob" title="Mobile"></span><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Fón Póca' : 'Mobile'); ?></li>
										<li class="watch-item"><span class="watch-and" title="Android"></span><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Andróid' : 'Android'); ?></li>
									</ul>
								</section>
								<section class="footer-follow">
									<h2><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Lean muid' : 'Follow Us'); ?></h2>
									<ul class="follow-list">
										<li class="follow-item"><a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/soisialta/' : '/en/social-media/'); ?>"><span class="wall" title="Social Wall"></span><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Balla Sóisialta' : 'Social Wall'); ?></a></li>
										<li class="follow-item"><a href="https://www.facebook.com/TG4TV" target="_blank"><span class="facebook" title="Facebook"></span>Facebook</a></li>
										<li class="follow-item"><a href="https://twitter.com/TG4TV" target="_blank"><span class="twitter" title="Twitter"></span>Twitter</a></li> 
										<li class="follow-item"><a href="https://www.youtube.com/user/TG4?sub_confirmation=1" target="_blank"><span class="youtube" title="Youtube"></span>Youtube</a></li>
										<li class="follow-item"><a href="https://www.flickr.com/photos/tg4" target="_blank"><span class="flickr" title="Flickr"></span>Flickr</a></li>
										<li class="follow-item"><a href="https://www.instagram.com/tg4tv/" target="_blank"><span class="instagram" title="Instagram"></span>Instagram</a></li>
										<li class="follow-item"><a href="https://www.snapchat.com/add/tg4tv/" target="_blank"><span class="snapchat" title="Snapchat"></span>Snapchat</a></li>
									</ul>
								</section>
							</div>
							<section class="footer-share">
								<h2><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Roinn' : 'Share'); ?></h2>
								<ul class="share-list">
			                        <!-- Facebook -->
			                        <li class="share-item"><a href="#" onClick="window.open('https://www.facebook.com/sharer.php?u=<?php echo 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>','TG4/Facebook Share','resizable,height=350,width=500'); return false;"><span class="share-facebook" title="Facebook"></span>Facebook</a></li>
			                        <noscript><li class="share-item"><a href="https://www.facebook.com/sharer.php?u=<?php echo 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" target="_blank"><span class="share-facebook" title="Facebook"></span>Facebook</a></li></noscript>
			                        <!-- Twitter -->    
			                        <li class="share-item"><a href="#" onClick="window.open('https://twitter.com/share?text=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Féach seo...' : 'Féach seo...'); ?>&amp;url=<?php echo 'https://'. $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>&amp;hashtags=TG4','TG4/Twitter Share','resizable,height=350,width=500'); return false;"><span class="share-twitter" title="Twitter"></span>Twitter</a></li>
			                        <noscript><li class="share-item"><a href="https://twitter.com/share?text=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Féach seo...' : 'Féach seo...'); ?>&amp;url=<?php echo 'https://'. $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>&amp;hashtags=TG4" target="_blank"><span class="share-twitter"></span title="Twitter">Twitter</a></li></noscript>
			                        <!-- Email -->
									<li class="share-item"><a href="mailto:?Subject=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Suíomh TG4 - ' . site_url() . '' : 'TG4 Website - ' . site_url() . ''); ?>&amp;Body=<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'A%20Chara,%0A%0AB\'fhéidir%20go%20mbeadh%20suim%20agat%20sa%20leathanach%20seo%20ar%20suíomh%20idirlín%20TG4!%0A%0A' : 'A%20Chara,%0A%0AI%20saw%20this%20on%20the%20TG4%20website%20and%20thought%20you%20might%20be%20interested!%0A%0A'); ?><?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>"><span class="share-email" title="Email"></span>Email</a></li>
								</ul>
							</section>
						</div>
					</div>
					<?php if (get_field("floodlight_tags")) { echo get_field("floodlight_tags"); } ?>
				</div>
				<section class="footer-links">
					<h3 class="visuallyhidden">Other Links</h3>
					<div class="footer-wrapper">
						<?php wp_nav_menu(array('menu' => 'Bottom Menu')); ?>
						<p class="copyright">
							&copy; <?php echo date('Y'); echo (ICL_LANGUAGE_CODE == "ga" ? ' Cóipcheart ' : ' Copyright '); bloginfo('name'); ?>
						</p>
					</div>
				</section>
			</footer>
		</div>

        <!-- Facebook Pixel Code - 26.07.2018 -->
        <script>
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window, document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '204101163593179');
          fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
          src="https://www.facebook.com/tr?id=204101163593179&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->

		<!-- /wrapper -->

		<!-- ============= OUTDATED BROWSER ============= -->
        <div id="outdated"></div>
		
		<?php wp_footer(); ?>
    </body>
</html>