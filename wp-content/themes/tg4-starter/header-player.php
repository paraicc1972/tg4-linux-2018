<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8,IE=9,IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="dns-prefetch" href="https://www.google-analytics.com">
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
        <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png">

        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/main-min.css">
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="https://d1og0s8nlbd0hm.cloudfront.net/js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
        <script src="https://d1og0s8nlbd0hm.cloudfront.net/js/min/main-min.js"></script>

        <!-- Core Media / CoreCast Pixel to Define Audience -->
        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');

            fbq('init', '961763507266245');
            fbq('track', "PageView");
        </script>
        <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=961763507266245&ev=PageView&noscript=1" /></noscript>
        <!-- End Facebook Pixel Code -->
        <!-- Start Alexa Certify Javascript -->
        <script type="text/javascript">
        _atrk_opts = { atrk_acct:"B8tfs1zDGU20kU", domain:"tg4.ie",dynamic: true};
        (function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://certify-js.alexametrics.com/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
        </script>
        <noscript><img src="https://certify.alexametrics.com/atrk.gif?account=B8tfs1zDGU20kU" style="display:none" height="1" width="1" alt="" /></noscript>
        <!-- End Alexa Certify Javascript -->
        <?php
        $progID = ctype_digit($_GET['pid']) ? $_GET['pid'] : file_get_contents(get_template_directory_uri() . '/assets/php/tg4-proxy-1.php?seriesTag=' . $seriesTag);

        $clientIP = $_SERVER['HTTP_X_FORWARDED_FOR']?: $_SERVER['HTTP_CLIENT_IP']?: $_SERVER['REMOTE_ADDR'];

        if ($clientIP == "::1") {
            $clientIP = "77.75.98.34"; //IE
            //$clientIP = "185.86.151.11"; //LONDON
            //$clientIP = "94.0.69.150"; //NIR
            //$clientIP = "72.229.28.185"; //USA
        }

        $request       = "https://www.tg4tech.com/api/programmes/countryCode?id=" . $clientIP;
        $ch            = curl_init($request);
        curl_setopt_array($ch, array(
            CURLOPT_POST           => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_SSL_VERIFYHOST => FALSE,
            CURLOPT_SSL_VERIFYPEER => FALSE,
            CURLOPT_HTTPHEADER     => array('Content-type: application/x-www-form-urlencoded'),
        ));
        $response = curl_exec($ch);

        // Check for errors
        if ($response === FALSE) {
            die(curl_error($ch));
        }
        curl_close($ch);

        $responseData = json_decode($response, TRUE);
        $geoCode = $responseData["country"];

        //echo "GEO - " . $geoCode . " - " . $clientIP;

        if ($progID) {
            $policy_key = "BCpkADawqM2kgtI3JAwTMh6q1YGqnxi3ymI39Eh1cUZnCV5KwmoP81aD2ZETWwWd0LJWahHL8LXPfFrwLgMlJDUvGk2HTJMyG0vLX60Mv09bCbQg7t08jTMiQu3B-3biJZ4t8-WYpgU0M0zb";
            $request = "https://edge.api.brightcove.com/playback/v1/accounts/1555966122001/videos/" . $progID;
            $ch = curl_init($request);
            curl_setopt_array($ch, array(
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER     => array('Accept: application/json;pk=' . $policy_key)
            ));
            $response = curl_exec($ch);
            // Check for errors
            if ($response === FALSE) {
                die(curl_error($ch));
            }
            curl_close($ch);

            // Decode the response
            $responseData = json_decode($response, TRUE);
            $referenceID = $responseData["reference_id"];
            $progTitle = $responseData["custom_fields"]["title"];
            $progSeriesTitle = $responseData["custom_fields"]["seriestitle"];
            $progSeries = $responseData["custom_fields"]["series"];
            $progEpisode = $responseData["custom_fields"]["episode"];
            $progPG = $responseData["custom_fields"]["parental_guide"];
            $progGaeDesc = $responseData["custom_fields"]["longdescgaeilge"];
            $progEngDesc = $responseData["description"];
            $progStartDate = $responseData["custom_fields"]["date"];
            $bits = explode('.', $progStartDate);
            $progStartDate = $bits[0] . '.' . $bits[1] . '.20' . $bits[2];
            $showStartDate = date(" j M ", strtotime($progStartDate));
            $progName = $responseData["name"];
            $progBCImage = $responseData["poster"];
            $progCategory = $responseData["custom_fields"]["category_c"];
            $progProdImage = 'http://res.cloudinary.com/tg4/image/upload/w_700,h_395,f_auto,q_auto/' . $responseData["custom_fields"]["p_prodcode"] . '.jpg';
            $progSeriesImage = $responseData["custom_fields"]["seriesimgurl"];
            $Econ = $responseData["economics"];
            
            if (($progPG == '18') || ($progPG == '16')) {
                $famFriendly = 'False';
            } else {
                $famFriendly = 'True';
            }

            //* Remove Pre-Rolls if Traffic from TG4 *//
            if (getenv('REMOTE_ADDR')=='77.75.98.34' || getenv('HTTP_X_FORWARDED_FOR')=='77.75.98.34') {
                //$Econ = 'TG4-NO ADS';
            }

            ?>

            <!-- Page Title -->
            <title><?php echo $progTitle . ' (' . $progSeries . '-' . $progEpisode . ') | ' . (ICL_LANGUAGE_CODE == "ga" ? 'Seinnteoir' : 'Player'); ?> | TG4 | Súil Eile</title>

            <!-- Facebook Open Graph Microdata -->
            <meta property="og:locale" content="en_US" />
            <meta property="og:type" content="video" />
            <meta property="og:title" content="<?php echo $progTitle; ?> | Baile | Seinnteoir | TG4" />
            <meta property="article:publisher" content="https://www.facebook.com/TG4TV" />
            <meta property="og:image" content="<?php echo $progProdImage; ?>" />
            <meta property="og:image" content="<?php echo $progBCImage; ?>" />
            <meta property="og:image" content="<?php echo $progSeriesImage; ?>" />
            <meta property="og:description" content="<?php echo (ICL_LANGUAGE_CODE == "ga" ? $progGaeDesc : $progEngDesc); ?>" />
            <meta property="og:url" content="<?php echo 'https://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>" />
            <meta property="og:video" content="<?php echo 'https://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>" />
            <meta property="og:video:width" content="640" />
            <meta property="og:video:height" content="360" />
            <meta property="og:video:type" content="video.tv_show" />
            <meta property="og:site_name" content="TG4" />
            <meta property="fb:app_id" content="54211732954" /> 

            <!-- Twitter Card Meta Data -->
            <meta name="twitter:card" content="summary_large_image"/>
            <meta name="twitter:title" content="<?php echo $progTitle; ?> | Baile | Seinnteoir | TG4"/>
            <meta name="twitter:site" content="@TG4TV"/>
            <meta name="twitter:domain" content="TG4"/>
            <meta name="twitter:image:src" content="<?php echo $progBCImage; ?>"/>
            <meta name="twitter:image:src" content="<?php echo $progSeriesImage; ?>"/>
            <meta name="twitter:image:src" content="<?php echo $progProdImage; ?>"/>
            <meta name="twitter:creator" content="@TG4TV"/>
        <?php } else { 
            wp_head();
            echo "<title>" . get_post_meta(get_the_ID(), '_yoast_wpseo_title', true) . "</title>";
        } ?>

        <!-- DFP Declare Publisher Tags -->
        <script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>

        <script>
            var googletag = googletag || {};
            googletag.cmd = googletag.cmd || [];
          
            googletag.cmd.push(function() {
                googletag.defineSlot('/172054193/StanLead//1234', [728, 90], 'div-gpt-ad-1520424645901-0').addService(googletag.pubads());
                googletag.defineSlot('/172054193/MPU//Side1234', [300, 250], 'div-gpt-ad-1484321672116-0').addService(googletag.pubads());
                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
            });
        </script>

        <!-- Google Analytics Tracking Code -->
        <script async language="JavaScript" type="text/javascript">
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-4024457-9', 'auto');
            ga('send', 'pageview');
        </script>

        <style>
            .highlightedText {
                background: #F8CA00;
                padding: 1px 3px 1px 3px;
            }
        </style>
    </head>
    <body <?php body_class(); ?> ng-app="tg4App" ng-controller="tg4Ctrl">
        <a href="#maincontent" class="skip-to-main">Skip to main content</a> 
        <div class="wrapper">
            <header class="header-nav">
                <div class="header-wrapper">
                    <div class="top-bar">
                        <a href="<?php echo (ICL_LANGUAGE_CODE == "ga" ? '/ga/' : '/en/'); ?>" class="logo-home"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/TG4.png" alt="TG4 Logo" class="logo" height="550" width="201"></a>
                        <div class="header-banner" aria-hidden="true">
                            <!-- /172054193/StanLead//1234 -->
                            <div id='div-gpt-ad-1520424645901-0' style='height:90px; width:728px;'>
                              <script>
                                googletag.cmd.push(function() { 
                                    googletag.display('div-gpt-ad-1520424645901-0');
                                });
                              </script>
                            </div>
                        </div>
                    </div>
                    <div class="languages">
                        <?php icl_post_languages() ?>
                    </div>
                    <a href="" class="search-btn-toggle"></a>
                    <div class="search-wrapper">
                        <div class="search">
                            <form method="get" id="searchform" class="search-form" action="<?php bloginfo('home'); ?>/" autocomplete="off">
                                <input type="search" class="search-input" name="s" id="s" value="" placeholder="<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Cuardaigh anseo...' : 'Enter your search here...'); ?>">
                                <button type="submit" id="searchsubmit" value="Search" class="search-btn">Search</button>
                            </form>
                        </div>
                    </div>
                    <div class="nav-cover"></div>
                    <a href="#main-menu" class="burger-menu">Menu</a>
                </div>
                <?php include('includes/main-menu.php'); ?>
            </header>
            <main id="maincontent">