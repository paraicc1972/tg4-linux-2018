<?php get_header(); ?>
<div class="section-header">
	<h1 class="section-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Cuardaigh' : 'Search'); ?></h1>
</div>

<?php get_template_part('/includes/schedule-highlights'); ?>

<section class="prog-feat-section">
	<div class="prog-feat center-panel">
	    <div class="prog-feat-wrap">
	    	<main role="main">
				<p><?php //echo (ICL_LANGUAGE_CODE == "ga" ? 'Úsáid an foirm thíos chun an suíomh a chuardach.' : 'To search the website, please use the form below.'); ?></p>

				<?php
				//get_search_form();
				$s=get_search_query();
				$args = array('s' =>$s);
				// The Query
				$the_query = new WP_Query($args);
				if ($the_query->have_posts()) {
			        _e("<h2>Search Results for: ".get_query_var('s')."</h2>");
			        while ( $the_query->have_posts() ) {
			           	$the_query->the_post(); ?>
			                <li>
			                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			                </li>
			    <?php }
			    } else {
				?>
				   	<h2>Nothing Found</h2>
			        <div class="alert alert-info">
			        	<p>Sorry, but nothing matched your search criteria. Please try again with some different keywords.</p>
			        </div>
				<?php } ?>
				<p>&nbsp;</p>
			</main>
		</div>
	</div>
</section>

<?php get_footer(); ?>