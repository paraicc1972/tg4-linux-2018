<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8,IE=9,IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="dns-prefetch" href="https://www.google-analytics.com">
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
        <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png">

        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/main-molsceal.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,700" rel="stylesheet">
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="https://d1og0s8nlbd0hm.cloudfront.net/js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
        <script src='https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js'></script>

        <script>
        // Wait for the DOM to be ready
        $(function() {
            // Initialize form validation on the search form.
            // It has the name attribute "searchform"
            $("form[name='searchform']").validate({
                // Specify validation rules
                rules: {
                    // The key name on the left side is the name attribute
                    // of an input field. Validation rules are defined on the right side
                    search: "required"
                },
                // Specify validation error messages
                messages: {
                    search: "Téarma ag teastail"
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });
        </script>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-4024457-16"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-4024457-16');
        </script>

        <?php
        if(isset($_GET['CID']) && $_GET['CID'] != '') {
            $videoID = ctype_digit($_GET['CID']) ? $_GET['CID'] : '';
        }

        $policy_key = "BCpkADawqM3NldaK46lJCpDvZHF4oJAikn_67MO2s3FacOBR2qBakkoIpKGFayXcSJBAhjTM8zoluB8TYwSTBftfCcKEY5qEZQFyO4XRmeXyxCmn-jF2IcS0X5HrweheUB-wUzBMcOZobiXR";
        $request = "https://edge.api.brightcove.com/playback/v1/accounts/5561472261001/videos/" . $videoID;
        $ch = curl_init($request);
        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER     => array('Accept: application/json;pk=' . $policy_key)
        ));
        $response = curl_exec($ch);
        // Check for errors
        if ($response === FALSE) {
            die(curl_error($ch));
        }
        curl_close($ch);

        // Decode the response
        $responseData = json_decode($response, TRUE);

        /* var_dump($responseData);
        echo "POC - " . $responseData["poster"]; */

        $molRefID = $responseData["reference_id"];
        $molTitle = $responseData["custom_fields"]["teideal"];
        $molRegion = $responseData["custom_fields"]["reigiun"];
        $molCounty = $responseData["custom_fields"]["contae"];
        $molGenre = $responseData["custom_fields"]["genre"];
        $molDesc = $responseData["custom_fields"]["eolasgaeilge"];
        $molDate = $responseData["custom_fields"]["date"];
        $bits = explode('.', $molDate);
        $molStartDate = $bits[0] . '.' . $bits[1] . '.20' . $bits[2];
        $showStartDate = date(" j M ", strtotime($molStartDate));
        $molTWLink = $responseData["custom_fields"]["twlink"];
        $molFBLink = $responseData["custom_fields"]["fblink"];
        $molImage = $responseData["poster"];
        ?>

        <!-- Facebook Open Graph Microdata -->
        <meta property="og:locale" content="en_US" />
        <meta prefix="og: http://ogp.me/ns#" property="og:type" content="video" />
        <meta prefix="og: http://ogp.me/ns#" property="og:title" content="<?php echo $molTitle; ?> | Molscéal" />
        <meta prefix="og: http://ogp.me/ns#" property="og:image" content="<?php echo $molImage; ?>" />
        <meta property="og:description" content="<?php echo $molDesc; ?>" />
        <meta prefix="og: http://ogp.me/ns#" property="og:url" content="<?php echo 'https://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>" />
        <meta property="og:video" content="<?php echo 'https://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>" />
        <meta property="og:video:width" content="640" />
        <meta property="og:video:height" content="360" />
        <meta property="og:video:type" content="video.tv_show" />
        <meta property="og:site_name" content="Molscéal" />

        <!-- Twitter Card Meta Data -->
        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:title" content="<?php echo $molTitle; ?> | Molscéal"/>
        <meta name="twitter:site" content="@MOLSCEAL"/>
        <meta name="twitter:domain" content="Molscéal"/>
        <meta name="twitter:image:src" content="<?php echo $molImage; ?>"/>
        <meta name="twitter:creator" content="@MOLSCEAL"/>

        <?php
        //wp_head();
        $ancestors = get_post_ancestors($post->ID);
        echo "<title>" . $molTitle . ' | ' . get_the_title($ancestors[0]) . ' | ' . "Molscéal</title>";
        ?>
    </head>
    <body <?php body_class(); ?>>
        <a href="#maincontent" class="skip-to-main">Skip to main content</a> 
        <div class="wrapper" ng-app="molscealApp" ng-controller="molscealCtrl">
            <div class="header-wrapper">
                <div class="top-bar-molsceal">
                    <div class="top-bar-wrapper">
                        <div class="region">
                            <a class="regiontoggle">Mo Réigiún</a>&nbsp;
                            <a class="regiontoggle"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/MolSceal-Region.png" alt="MolScéal: Réigiún"></a>
                        </div>
                        <div class="search">
                            <form name="searchform" method="get" id="searchform" class="search-form" action="<?php echo site_url(); ?>/toradh" autocomplete="off">
                                <input required type="search" class="search-input" name="search" id="search" value="">
                                <button type="submit" id="searchsubmit" value="Search" class="search-btn"></button>
                            </form>
                        </div>
                        <div id="regiondd">
                            <ul>
                                <li class="regLnk"><a href="<?php echo site_url(); ?>/connachta">Connachta</a></li>
                                <li class="regLnk"><a href="<?php echo site_url(); ?>/an-mhumhain">An Mhumhain</a></li>
                                <li class="regLnk"><a href="<?php echo site_url(); ?>/laighin">Laighin</a></li>
                                <li class="regLnk"><a href="<?php echo site_url(); ?>/ulaidh">Ulaidh</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="title-bar-molsceal"><a href="<?php echo site_url(); ?>/"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/MolSceal-Logo.png" alt="Molscéal Logo"></a></div>
                </div>
            </div>
            <main id="maincontent">