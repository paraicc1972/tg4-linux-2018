<?php get_header(); ?>
<div class="section-header">
	<h1 class="section-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Leathanach ar iarraidh' : 'Page not found'); ?></h1>
</div>

<?php get_template_part('/includes/schedule-highlights'); ?>

<section class="prog-feat-section">
	<div class="prog-feat center-panel">
	    <div class="prog-feat-wrap">
	    	<main role="main">
				<h2><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Níl an leathanach sin le fáil anois. Seo iad na leathanaigh uilig atá ar an suíomh.' : 'This page does not exist. Below is a listing of the pages on the site.'); ?></h2>
		
				<ul><?php wp_list_pages('sort_column=post_title&title_li=&depth=3&post_status=publish'); ?></ul>
			</main>
		</div>
	</div>
</section>

<?php get_footer(); ?>