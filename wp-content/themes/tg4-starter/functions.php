<?php
/* Homepage Redirect - Returns visitor to the Cookie Recorded Language */
function redirect_homepage() {
    /* Get Current URL */
    $url = 'https://'. $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    /* Check if Current URL is not-equal to www.tg4.ie */
    if ($url != "https://www.tg4.ie/") // Root page ID
    return;
    /* check to see if Cookie variable is populated */
    if ($_COOKIE['_icl_current_language'] == '') {
        // Website Base-Language - Should get this info from WPML
        $langCode = 'ga';
    } else {
        $langCode = $_COOKIE['_icl_current_language'];
    }
    /* Build redirect url to include language code */
    $request = 'https://www.tg4.ie/'. $langCode .'/';
    /* Issue Redirect Command */ 
    wp_redirect($request, 301);
    exit;
}
add_action('template_redirect', 'redirect_homepage');

// Incorporating elements from http://html5blank.com/

// add custom main nav build
    //include 'includes/walker_main_menu.php';

/*  ==========================================================================
    Theme support
    ========================================================================== */

if (function_exists('add_theme_support'))
{
    // Add Menu Support 
    add_theme_support('menus');
    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');
}

/*  ==========================================================================
    Functions
    ========================================================================== */

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions($html)
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// add mobile class to body if mobile detected
function my_body_classes($classes) {
    if (wp_is_mobile()){ 
        $classes[] = 'is-mobile'; 
    }    
    return $classes;
}

// register/deregister js
function LoadMainJS() {
    if(!is_admin()) {
        wp_deregister_script('jquery');
        //wp_register_script('jquery', get_template_directory_uri().'/assets/js/min/main-min.js', false, null);
        //wp_enqueue_script('jquery');
    }
}

// Remove Contact Form 7 Links from dashboard menu items if not admin
function remove_wpcf7() {
    if (!(current_user_can('administrator'))) {
        remove_menu_page( 'wpcf7' ); 
    }
}

// Remove admin menu items
function remove_menus() {
    remove_menu_page('index.php');                      //Dashboard
    remove_menu_page('edit.php');                       //Posts
    remove_menu_page('upload.php');                     //Media
    //remove_menu_page('edit.php?post_type=page');      //Pages
    remove_menu_page('edit-comments.php');              //Comments
    //remove_menu_page('themes.php');                   //Appearance
    //remove_menu_page('plugins.php');                  //Plugins
    //remove_menu_page('users.php');                    //Users
    //remove_menu_page('tools.php');                    //Tools
    //remove_menu_page('options-general.php');          //Settings
}

// Disable pingbacks only, ref: http://wptavern.com/how-to-prevent-wordpress-from-participating-in-pingback-denial-of-service-attacks
function remove_xmlrpc_pingback_ping($methods) {
    unset($methods['pingback.ping']);
    return $methods;
};

// add an ACF options page
if(function_exists('acf_add_options_page')) {
    acf_add_options_page();
}

// add filter to stop tinymce removing other elements e.g. <span>
function myextensionTinyMCE($init) {
    // Command separated string of extended elements
    $ext = 'span[id|name|class|style]';
    // Add to extended_valid_elements if it alreay exists
    if (isset( $init['extended_valid_elements'])) {
        $init['extended_valid_elements'] .= ',' . $ext;
    } else {
        $init['extended_valid_elements'] = $ext;
    }
    // Super important: return $init!
    return $init;
}

// Add block format elements you want to show in dropdown
function customformatTinyMCE($init) {
    $init['block_formats'] = "Paragraph=p; Heading 2=h2; Heading 3=h3; Heading 4=h4";
    return $init;
}

// limit the excerpt length in characters
function get_excerpt() {
    $excerpt = get_the_content();
    $excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, 120);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
    $excerpt = '<p>'.$excerpt.'...</p>';
    return $excerpt;
}

// Numbered Pagination
if (!function_exists('printPagination')) {
    function printPagination() {
        global $wp_query;
        $total = $wp_query->max_num_pages;
        $big = 999999999; // need an unlikely integer
        if($total > 1)  {
             if(!$current_page = get_query_var('paged'))
                 $current_page = 1;
             if(get_option('permalink_structure')) {
                 $format = 'page/%#%/';
             } else {
                 $format = '&paged=%#%';
             }
            $paginate_links =  paginate_links(array(
                'base'          => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                'format'        => $format,
                'current'       => max(1, get_query_var('paged')),
                'total'         => $total,
                'mid_size'      => 3,
                'prev_next'     => true,
                'type'          => 'list',
                'prev_text'     => 'Prev',
                'next_text'     => 'Next',
             ));

            if ($paginate_links) {
                echo $paginate_links;
            }
        }
    }
}

// check if there are prev or next pages available for the post list
function has_previous_post() {
    ob_start();
    previous_post_link();
    $result = strlen(ob_get_contents());
    ob_end_clean();
    return $result; 
}

function has_next_post() {
    ob_start();
    next_post_link();
    $result = strlen(ob_get_contents());
    ob_end_clean();
    return $result;
}

// add my custom image sizes to the admin list
function my_custom_sizes($sizes) {
    return array_merge($sizes, array(
        'article-main' => __('Featured srticle image'),
        'property' => __('Large image (full width)'),
        'medium-image' => __('Medium image'),
        'small-image' => __('Small image'),
        //'article-medium' => __('Medium thumbail'),
        //'article-small' => __('Small thumbnail'),
        'profile-small' => __('Small profile image'),
        'profile-large' => __('Large profile image')
    ));
}

// remove the WP default image sizes
function remove_default_image_sizes($sizes) {
    unset($sizes['thumbnail']);
    unset($sizes['medium']);
    unset($sizes['large']);
    return $sizes;
}

/* function fix_35012_wp_old_slug() {
    global $wp_query;
 
    if ($wp_query->post_count > 0) {
        remove_action('template_redirect', 'wp_old_slug_redirect');
    }
}
add_action('template_redirect', 'fix_35012_wp_old_slug', 5); */

/*  ==========================================================================
    Actions, Filters, andd Shortcodes
    ========================================================================== */

// Add Actions
add_action('wp_enqueue_scripts', 'LoadMainJS', 100); // register/deregister js
add_action('admin_menu', 'remove_menus'); // remove admin menu items

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
//add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images
add_filter('body_class','my_body_classes'); // add mobile class to body
add_filter('xmlrpc_methods', 'remove_xmlrpc_pingback_ping'); // Disable pingbacks only, ref: http://wptavern.com/how-to-prevent-wordpress-from-participating-in-pingback-denial-of-service-attacks
add_filter('tiny_mce_before_init', 'myextensionTinyMCE'); // add filter to stop tinymce removing other elements e.g. <span>
add_filter('tiny_mce_before_init', 'customformatTinyMCE'); // Modify Tiny_MCE init
add_filter('image_size_names_choose', 'my_custom_sizes'); // add my custom image sizes to the admin list
add_filter('wpseo_canonical', '__return_false'); //Switch off Canonical URLs
//add_filter('xmlrpc_enabled', '__return_false'); //Disable XML-RPC
//add_filter('intermediate_image_sizes_advanced', 'remove_default_image_sizes'); // remove the WP default image sizes

// Remove Filters

// Shortcodes

/*  ==========================================================================
    Custom Post Types
    ========================================================================== */

// Create 1 Custom Post type for a Demo, called HTML5-Blank
// function create_post_type_html5() {
//     register_taxonomy_for_object_type('category', 'html5-blank'); // Register Taxonomies for Category
//     register_taxonomy_for_object_type('post_tag', 'html5-blank');
//     register_post_type('html5-blank', // Register Custom Post Type
//         array(
//         'labels' => array(
//             'name' => __('HTML5 Blank Custom Post', 'html5blank'), // Rename these to suit
//             'singular_name' => __('HTML5 Blank Custom Post', 'html5blank'),
//             'add_new' => __('Add New', 'html5blank'),
//             'add_new_item' => __('Add New HTML5 Blank Custom Post', 'html5blank'),
//             'edit' => __('Edit', 'html5blank'),
//             'edit_item' => __('Edit HTML5 Blank Custom Post', 'html5blank'),
//             'new_item' => __('New HTML5 Blank Custom Post', 'html5blank'),
//             'view' => __('View HTML5 Blank Custom Post', 'html5blank'),
//             'view_item' => __('View HTML5 Blank Custom Post', 'html5blank'),
//             'search_items' => __('Search HTML5 Blank Custom Post', 'html5blank'),
//             'not_found' => __('No HTML5 Blank Custom Posts found', 'html5blank'),
//             'not_found_in_trash' => __('No HTML5 Blank Custom Posts found in Trash', 'html5blank')
//         ),
//         'public' => true,
//         'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
//         'has_archive' => true,
//         'supports' => array(
//             'title',
//             'editor',
//             'excerpt',
//             'thumbnail'
//         ), // Go to Dashboard Custom HTML5 Blank post for supports
//         'can_export' => true, // Allows export in Tools > Export
//         'taxonomies' => array(
//             'post_tag',
//             'category'
//         ) // Add Category and Post Tags support
//     ));
// }

/*  ==========================================================================
    Shortcode Functions
    ========================================================================== */

/**
 * Daily Schedule - alter times greater than 24 hours
 * POC - 11/06/15
 */
function tg_alterTime($var) {
    if ((substr($var, 0, 2)) >= 24) {
        $newTime = ((substr($var, 0, 2)) - 24) . substr($var, 2, 3) . "AM";
    }
    else if ((substr($var, 0, 2)) >= 13) {
        $newTime = ((substr($var, 0, 2)) - 12) . substr($var, 2, 3) . "PM";
    }
    else if ((substr($var, 0, 2)) == 12) {
        $newTime = substr($var, 0, 5) . "PM";
    }
    else if ((substr($var, 0, 2)) < 10) {
        $newTime = substr($var, 1, 4) . "AM";
    } else {
        $newTime = substr($var, 0, 5) . "AM";
    }
    return $newTime;
}

/**
 * Daily Schedule Geo-targetting Icons - display correct css icon per programme
 * POC - 11/06/15
 */
function tg_geoIcon($var1, $var2) {
    if (($var1 == 0) && ($var2 == 0)) {
        $geoIconCSS = " no-player";
    } else if (($var1 == 1) && ($var2 == 0)) {
        $geoIconCSS = " ireland-only";
    } else {
        $geoIconCSS = "";
    }
    return $geoIconCSS;
}

/**
 * Daily Schedule Geo-targetting Icons - display correct css icon per programme
 * POC - 11/06/15
 */
function tg_liveProg($var1) {
    if ($var1 == 1) {
        $liveIconCSS = " live-now";
    } else {
        $liveIconCSS = "";
    }
    return $liveIconCSS;
}

/**
 * Add language functionality to tabs in Menu bar
 * POC - 11/06/15
 */
function icl_post_languages() {
  $languages = icl_get_languages('skip_missing=1');
  if (!empty($languages)) {
    //echo __('This post is also available in: ');
    foreach ($languages as $l) {
      if ($l['active']) $langs[] = '<a href="'.$l['url'].'" class="lang-btn lang-selected">'.$l['native_name'].'</a>';
      if (!$l['active']) $langs[] = '<a href="'.$l['url'].'" class="lang-btn">'.$l['native_name'].'</a>';
    }
    //echo join(', ', $langs);
    echo join($langs);
  }
}

/**
 * WPML Code to handle querystrings being passed between languages
 */
add_filter('icl_ls_languages', 'wpml_ls_filter');

/* Schedule listing - abbreviate days and months and convert to Irish */
function schedule_daily_abbr($scheduleTitle) {
    switch (trim($scheduleTitle)) {
        case "Sat":
            $schedule_abbr = "Sat";
            break;
        case "Sun":
            $schedule_abbr = "Dom";
            break;
        case "Mon":
            $schedule_abbr = "Lua";
            break;
        case "Tue":
            $schedule_abbr = "Mai";
            break;
        case "Wed":
            $schedule_abbr = "Cea";
            break;
        case "Thu":
            $schedule_abbr = "Dea";
            break;
       case "Fri":
            $schedule_abbr = "Aoi";
            break;
        case "Jan":
            $schedule_abbr= "Ean";
            break;
        case "Feb":
            $schedule_abbr= "Fea";
            break;
        case "Mar":
            $schedule_abbr= "Mar";
            break;
        case "Apr":
            $schedule_abbr= "Aib";
            break;
        case "May":
            $schedule_abbr= "Bea";
            break;
        case "Jun":
            $schedule_abbr= "Mei";
            break;
        case "Jul":
            $schedule_abbr= "Iui";
            break;
        case "Aug":
            $schedule_abbr= "Lun";
            break;
        case "Sep":
            $schedule_abbr= "Mfo";
            break;
        case "Oct":
            $schedule_abbr= "Dfo";
            break;
        case "Nov":
            $schedule_abbr= "Sam";
            break;
        case "Dec":
            $schedule_abbr= "Nol";
            break;
        }
    return $schedule_abbr;
}

function wpml_ls_filter($languages) {
    global $sitepress;
    if($_SERVER["QUERY_STRING"]) {
        if(strpos(basename($_SERVER['REQUEST_URI']), $_SERVER["QUERY_STRING"]) !== false) {
            foreach($languages as $lang_code => $language) {    
                if (stripos($languages[$lang_code]['url'],"?") == null) {
                    $languages[$lang_code]['url'] = $languages[$lang_code]['url']. '?'.$_SERVER["QUERY_STRING"];
                }
            }
        }
    }
    return $languages;
}

function exclude_draft_nav_items($items, $menu, $args) {
    global $wpdb;
    //add your custom posttypes to this array
    $allowed_posttypes = array('post', 'page');
    $sql = "SELECT ID FROM {$wpdb->prefix}posts WHERE (post_status='draft' OR post_status='pending') AND ID=%d && post_type=%s";
    foreach ($items as $k => $item) {
        if(in_array($item->object, $allowed_posttypes)) {
            $query = $wpdb->prepare($sql, $item->object_id, $item->object);
            $result = $wpdb->get_var($query);
            if($result) unset($items[$k]);
        }
    }
    return $items;
}
add_filter('wp_get_nav_menu_items', 'exclude_draft_nav_items', 10, 3);

/* Remove Canonical Tagging from Player Play Pages English and Irish */
function wpseo_canonical_exclude($canonical) {
    global $post;
    if(is_single(array(236, 239))) {
        $canonical = false;
    }
    return $canonical;
}

/* Customized Password Entry Form for Protected Posts */
function my_password_form() {
    global $post;
    $label = 'pwbox-'.(empty($post->ID) ? rand() : $post->ID);
    $o = '
    <form action="' . esc_url(site_url('wp-login.php?action=postpass', 'login_post')) . '" method="post">
        <p><label for="' . $label . '">' . __("Pasfhocal/Password:") . ' </label><input style="width:150px;" name="post_password" id="' . $label . '" type="password" size="20" maxlength="20" /></p>
        <input style="margin: 20px 20px 50px 0px; background-color: #d7e2ea;; border: 1px solid #3e8eb4;; min-width: 100px; min-height: 30px;" type="submit" name="Submit" value="' . esc_attr__("Seol/Submit") . '" />
    </form>';
    return $o;
}
add_filter('the_password_form', 'my_password_form');

/* Check if it is page or subpage */
function is_tree($pid) {      // $pid = The ID of the page we're looking for pages underneath
    global $post;               // load details about this page
    if (is_page($pid))
        return true;            // we're at the page or at a sub page
    $anc = get_post_ancestors($post->ID);
    foreach ($anc as $ancestor) {
        if(is_page() && $ancestor == $pid) {
            return true;
        }
    }
    return false;  // we aren't at the page, and the page is not an ancestor
}

/* Allow draft pages to be parents in Admin section  */
add_filter('page_attributes_dropdown_pages_args', 'so_3538267_enable_drafts_parents');
add_filter('quick_edit_dropdown_pages_args', 'so_3538267_enable_drafts_parents');

function so_3538267_enable_drafts_parents($args)
{
    $args['post_status'] = 'draft,publish,pending';
    return $args;
}

# Remove customizer options.
function emersonthis_remove_customizer_options($wp_customize) {
    $wp_customize->remove_section('static_front_page');
    $wp_customize->remove_section('title_tagline');
    $wp_customize->remove_section('colors');
    $wp_customize->remove_section('header_image');
    $wp_customize->remove_section('background_image');
    $wp_customize->remove_section('nav');
    $wp_customize->remove_section('themes');
    $wp_customize->remove_section('featured_content');
    $wp_customize->remove_panel('widgets');
}
add_action('customize_register', 'emersonthis_remove_customizer_options', 30);

function my_login_logo_one() { 
?> 
    <style type="text/css"> 
        body.login div#login h1 a {
        background-image: url(https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2016/01/TG4-admin.png);
        padding-bottom: 30px;
        width: 192px;
        -webkit-background-size: inherit;
        background-size: inherit;
    } 
    </style>
<?php 
} add_action('login_enqueue_scripts', 'my_login_logo_one');

# Finding handle for your plugins 
/* function display_script_handles() {
    global $wp_scripts;
    if(current_user_can('manage_options') && !is_admin()) { # Only load when user is admin
        echo '<ol>';
        foreach($wp_scripts->queue as $handle) :
            $obj = $wp_scripts->registered [$handle];
            echo "<li>" . $filename = $obj->src;
            echo ' : Handle for this script is:</b> <span style="color:#ff0000">' . $handle . '</span></li>';
        endforeach;
        echo '</ol>';
    }   
}
add_action('wp_print_scripts', 'display_script_handles'); */

function conditionally_load_plugin_js_css() {
    /* Social Wall */
    if(!is_page(array(883,924,928,1736,1739,1806,1809,2235,2238,4277,4285,12735,12739,13211,13322,13733,13735,70124,70126))) {  # Load CSS and JS only on Pages with ID 4 and 12
        wp_dequeue_script('dcwss-wall'); # Restrict scripts.
        wp_dequeue_style('dcwss-wall'); # Restrict css.      
        wp_dequeue_script('dcwss');
        wp_dequeue_style('dcwss');     
    }
    /* Contact Form 7 */
    if(!is_page(array(721,724,2849,2868,3762,3777,13199,13202,16670,16673,28483,28487,57659,57677,93120,93134))) {
        wp_dequeue_script('contact-form-7');
        wp_dequeue_style('contact-form-7');
        wp_dequeue_script('google-recaptcha');    
    }
    /* Simple Membership & SWPM Form Builder */
    if(!is_page(array(17053,17054,17056,17057,17122,17128,17132,17136,30230,30250))) {
        wp_dequeue_script('jquery-form-validation');
        wp_dequeue_style('swpm-form-builder-css');
        wp_dequeue_script('swpm-form-builder-validation');
        wp_dequeue_style('swpm-jqueryui-css');
        wp_dequeue_script('swpm-form-builder-metadata');
        wp_dequeue_style('swpm.common');
        wp_dequeue_script('swpm-validation-i18n');
        wp_dequeue_style('validationEngine.jquery');
    }
    /* Wonderplugin Audio & Tabs */
    if ((!is_tree(30206)) && (!is_tree(30397)) && (!is_tree(72457)) && (!is_tree(72866))) {
        wp_dequeue_style('h5p-plugin-styles');
        wp_dequeue_script('wonderplugin-audio-skins-script');
        wp_dequeue_script('wonderplugin-audio-script');
        wp_dequeue_script('wonderplugin-tabs-engine-script');
        wp_dequeue_style('font-awesome');
        wp_dequeue_style('wonderplugin-tabs-engine-css');
        wp_dequeue_style('arconix-shortcodes');
    }
    /* Wonderplugin Slider */
    if ((!is_tree(22890)) && (!is_tree(22893))) {
        wp_dequeue_script('wonderplugin-slider-skins-script');
        wp_dequeue_style('wonderplugin-slider-css');
        wp_dequeue_script('wonderplugin-slider-script');
    }
}   
add_action('wp_enqueue_scripts', 'conditionally_load_plugin_js_css');

add_filter('ts_sendy_list_count','sendy_show_checkbox',10,1);

function sendy_show_checkbox($count) {
    return 2;
}

/* Order Country List on Contact Form 7 usinmg Listo plugin */
function wpcf7_listo_ordered($data) {
    //setlocale(LC_ALL, 'fr_FR.utf8');
    sort($data, SORT_LOCALE_STRING);
    return $data;
}
add_filter('wpcf7_form_tag_data_option', 'wpcf7_listo_ordered', 11, 1);

/* add_action('wpcf7_init', 'custom_add_form_tag_clock');
 
function custom_add_form_tag_clock() {
    wpcf7_add_form_tag('ts_subscriptiondate', 'custom_clock_form_tag_handler'); // "clock" is the type of the form-tag
}
 
function custom_clock_form_tag_handler($tag) {
    return current_time(date('Y-m-d'));
    //return date_i18n(get_option('time_format'));
} */

/* function cdn_url() {
    return 'https://d22htqtqwj3xpd.cloudfront.net/wp-content/uploads';
}

add_filter ('pre_option_upload_url_path', 'cdn_url'); */

// Add time to querystring - cachebuster
/* function wpd_append_query_string($url, $id) {
    if ((53 == $id) || (128 == $id) || (941 == $id) || (948 == $id) || (6189 == $id) || (6282 == $id) || (6413 == $id) || (6415 == $id) || (912 == $id) || (916 == $id) || (20888 == $id) || (20891 == $id) || (2849 == $id) || (2868 == $id) || (53803 == $id) || (53824 == $id)) {
        if (stripos($url,"dTime") == null) {
            $url = add_query_arg('dTime', time(), $url);
        }
    }
    return $url;
}
add_filter('page_link', 'wpd_append_query_string', 10, 2); */

function tg_geoLoc() {
    if (!isset($geoCode)) {
        global $clientIP, $geoCode;

        $clientIP = $_SERVER['HTTP_X_FORWARDED_FOR']?: $_SERVER['HTTP_CLIENT_IP']?: $_SERVER['REMOTE_ADDR'];

        if ($clientIP == "::1") {
            $clientIP = "77.75.98.34"; //IE
            /* $clientIP = "194.32.31.1"; //GB
            $clientIP = "94.0.69.150"; //NIR
            $clientIP = "217.91.35.16"; //GER
            $clientIP = "176.164.135.240"; //FRA */
        }

        $request = "https://www.tg4tech.com/api/programmes/countryCode?id=" . $clientIP;
        $ch = curl_init($request);
        curl_setopt_array($ch, array(
            CURLOPT_POST           => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_SSL_VERIFYHOST => FALSE,
            CURLOPT_SSL_VERIFYPEER => FALSE,
            CURLOPT_HTTPHEADER     => array('Content-type: application/x-www-form-urlencoded'),
        ));
        $response = curl_exec($ch);

        // Check for errors
        if ($response === FALSE) {
            die(curl_error($ch));
        }
        curl_close($ch);

        $responseData = json_decode($response, TRUE);
        $geoCode = $responseData["country"];

        //return $geoCode;
    }
}
?>