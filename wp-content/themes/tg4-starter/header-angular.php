<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
		<link rel="dns-prefetch" href="https://www.google-analytics.com">
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
        <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png">

        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/main-min.css">
        <link rel="stylesheet" href="https://players.brightcove.net/videojs-ima3/videojs.ima3.min.css">
        <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script async src="https://d1og0s8nlbd0hm.cloudfront.net/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        <script async src="https://d1og0s8nlbd0hm.cloudfront.net/js/min/main-min.js"></script>

        <!-- Core Media / CoreCast Pixel to Define Audience -->
        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');

            fbq('init', '961763507266245');
            fbq('track', "PageView");
        </script>
        <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=961763507266245&ev=PageView&noscript=1" /></noscript>
        <!-- End Facebook Pixel Code -->

        <?php
        $clientIP = $_SERVER['HTTP_X_FORWARDED_FOR']?: $_SERVER['HTTP_CLIENT_IP']?: $_SERVER['REMOTE_ADDR'];

        if ($clientIP == "::1") {
            $clientIP = "77.75.98.34"; //IE
            //$clientIP = "194.32.31.1"; //GB
            //$clientIP = "94.0.69.150"; //NIR
            //$clientIP = "217.91.35.16"; //GER
        }

        $request       = "https://www.tg4tech.com/api/programmes/countryCode?id=" . $clientIP;
        $ch            = curl_init($request);
        curl_setopt_array($ch, array(
                CURLOPT_POST           => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER     => array('Content-type: application/x-www-form-urlencoded'),
            ));
        $response = curl_exec($ch);

        // Check for errors
        if ($response === FALSE) {
            die(curl_error($ch));
        }
        curl_close($ch);

        $responseData = json_decode($response, TRUE);
        $geoCode = $responseData["country"];

        //echo "GEO - " . $geoCode . " - " . $clientIP . " - " . $progID;
        
        wp_head();
        $ancestors = get_post_ancestors($post->ID);
        echo "<title>";
        echo empty($post->post_parent) ? get_the_title($post->ID) : get_the_title($post->ID) . ' | ' . get_the_title($post->post_parent) . ' | ';
        echo empty($ancestors[1]) ? '' : get_the_title($ancestors[1]) . ' | ';
        echo empty($ancestors[2]) ? '' : get_the_title($ancestors[2]) . ' | ';
        echo "TG4 | Súil Eile</title>"; ?>

        <!-- DFP Declare Publisher Tags -->
        <script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>

        <script>
            var googletag = googletag || {};
            googletag.cmd = googletag.cmd || [];
          
            googletag.cmd.push(function() {
                googletag.defineSlot('/172054193/StanLead//1234', [728, 90], 'div-gpt-ad-1520424645901-0').addService(googletag.pubads());
                googletag.defineSlot('/172054193/MPU//Side1234', [300, 250], 'div-gpt-ad-1484321672116-0').addService(googletag.pubads());
                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
            });
        </script>

        <!-- Google Analytics Tracking Code -->
        <script async language="JavaScript" type="text/javascript">
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-4024457-9', 'auto');
            ga('send', 'pageview');
        </script>

        <style>
            .highlightedText {
                background: #F8CA00;
                padding: 1px 3px 1px 3px;
            }
        </style>
    </head>
    <body <?php body_class(); ?> ng-app="tg4App" ng-controller="tg4Ctrl">
        <a href="#maincontent" class="skip-to-main">Skip to main content</a>
        <div class="wrapper">
            <header class="header-nav">
                <div class="header-wrapper">
                    <div class="top-bar">
                        <a href="<?php echo (ICL_LANGUAGE_CODE == "ga" ? '/ga/' : '/en/'); ?>" class="logo-home"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/TG4.png" alt="TG4 Logo" class="logo"></a>
                        <div class="header-banner" aria-hidden="true">
                            <!-- /172054193/StanLead//1234 -->
                            <div id='div-gpt-ad-1520424645901-0' style='height:90px; width:728px;'>
                              <script>
                                googletag.cmd.push(function() { 
                                    googletag.display('div-gpt-ad-1520424645901-0');
                                });
                              </script>
                            </div>
                        </div>
                    </div>
                    <div class="languages">
                        <?php icl_post_languages() ?>
                    </div>
                    <a href="" class="search-btn-toggle"></a>
                    <div class="search-wrapper">
                        <div class="search">
                            <form method="get" id="searchform" class="search-form" action="<?php bloginfo('home'); ?>/" autocomplete="off">
                                <input type="search" class="search-input" name="s" id="s" value="" placeholder="<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Cuardaigh anseo...' : 'Enter your search here...'); ?>">
                                <button type="submit" id="searchsubmit" value="Search" class="search-btn">Search</button>
                            </form>
                        </div>
                    </div>
                    <div class="nav-cover"></div>
                    <a href="#main-menu" class="burger-menu">Menu</a>
                </div>
                <?php include('includes/main-menu.php'); ?>
            </header>
            <main id="maincontent">