<?php get_header(); ?>

<h1 class="visuallyhidden">TG4</h1>

<?php include('includes/home-slider.php'); ?>

<a name="inniu"></a>
<section class="section-panel-blue">
    <div class="title-tab-wrap">
        <h2 class="title-tab-blue"><a href="#inniu" style="color:white"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Inniu ar TG4' : 'Today on TG4'); ?></a></h2>
    </div>
    <?php get_template_part('/includes/tonight-slider'); ?>
    <a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/sceideal/?dTime='.time() : '/en/irish-tv-schedule/?dTime='.time()); ?>" class="view-full"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Sceideal Iomlán' : 'View full schedule'); ?></a>
</section>

<section class="home-mods">
    <h2 class="visuallyhidden">More on TG4</h2>
    <div class="mods-wrapper">
        <div class="home-mod-wrap">
            <section class="mod-1">
                <div class="mod-livesport">
                    <div class="livesport-item-wrap">
                        <div class="livesport-head"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Spórt Beo ar TG4' : 'Live Sport on TG4'); ?></div>
                        <?php
                        if (ICL_LANGUAGE_CODE=="ga") {
                            $post_id = 4413;
                        } else {
                            $post_id = 4412;
                        }
                        if(get_field('tg4_sport', $post_id)) {
                            while(the_repeater_field('tg4_sport', $post_id))
                            {
                                echo "<div class='livesport-content-wrap'>";
                                echo "<div class='livesport-img'><span class='live-" . get_sub_field('sport_icon') . "'></span></div>";
                                echo "<div class='livesport-content'>";
                                echo "<p class='livesport-fixture'>" . get_sub_field('sport_title') . "</p>";
                                echo "<p class='livesport-date'>" . get_sub_field('sport_day') . "</p>";
                                echo "</div>";
                                echo "</div>";
                           }
                        } ?>
                    </div>
                </div>
            </section>
            <section class="mod-2">
                <div class="mod-notice">
                    <?php
                    if (ICL_LANGUAGE_CODE == "ga") {
                        $posts = get_posts(array(
                        'numberposts' => -1,
                        'post_type' => 'Page',
                        'meta_key' => 'mini_feature',
                        'meta_value' => '1'
                        ));
                    } else {
                        $posts = get_posts(array(
                        'numberposts' => -1,
                        'post_type' => 'Page',
                        'meta_key' => 'mini_feature',
                        'meta_value' => 'yes'
                        ));
                    }

                    if($posts)
                    { ?>
                    <section class="mini-slider-wrapper">
                        <div id="mini-width-slider" class="royalminiSlider rsMinW">
                        <?php foreach($posts as $post)
                        { ?>
                            <section class="rsContent">
                                <div class="blockContainer">
                                    <h2 class="mini-title-block rsABlock" data-fade-effect="true" data-move-effect="left" data-move-offset="50" data-speed="400" data-easing="easeOutSine"><?php echo get_field("mini_feature_title") ?></h2>
                                    <!-- Remove because Slider hardcodes style that will not work on mobile...
                                    <div class="mini-content-block rsABlock" data-fade-effect="true" data-move-effect="left" data-move-offset="50" data-speed="400" data-easing="easeOutSine">
                                        <h3 class="mini-content-subtitle"><?php //echo get_field("mini_feature_sub_title") ?></h3>
                                    </div>
                                    -->
                                    <?php if (ICL_LANGUAGE_CODE == "ga") { ?>
                                        <div class="rsABlock" data-fade-effect="true" data-move-effect="left" data-move-offset="50" data-speed="400" data-easing="easeOutSine">
                                            <a href="<?php echo get_permalink($post->ID); ?>" class="btn-header">Féach Leat...<span></span></a>
                                        </div>
                                    <?php } else { ?>
                                        <div class="rsABlock" data-fade-effect="true" data-move-effect="left" data-move-offset="50" data-speed="400" data-easing="easeOutSine">
                                            <a href="<?php echo get_permalink($post->ID); ?>" class="btn-header">See More...<span></span></a>
                                        </div>
                                    <?php } ?>
                                </div>
                                <img class="rsImg" src="<?php echo(get_field("mini_feature_image"));?>" alt="<?php echo get_field("mini_feature_title") ?>" title="<?php echo get_field("mini_feature_title") ?>" width="582" height="360">
                            </section>
                        <?php } ?>
                        </div>
                    </section>
                    <?php } ?>
                </div>
            </section>
        </div>
        <section class="mod-ad">
            <h3 class="mod-ad-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Fógraíocht' : 'Advertisement'); ?></h3>
            <div class="ad-wrapper">
                <!-- /172054193/MPU//Side1234 -->
                <div id='div-gpt-ad-1484321672116-0' style='height:250px; width:300px;'>
                    <script>
                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1484321672116-0'); });
                    </script>
                </div>
            </div>
        </section>
    </div>
</section>

<section class="section-panel-dark online-section">
    <div class="title-tab-wrap">
        <h2 class="title-tab-dark"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Ar líne anois' : 'Online Now'); ?></h2>
    </div>
    <?php include(locate_template('/includes/online-now.php')); ?>
    <?php //include(locate_template('/includes/online-now-channel-2.php')); ?>
</section>

<!-- Panel don tOireachtais -->
<!--section class="section-panel-dark-2 featv-section">
    <div class="title-tab-wrap">
        <h2 class="title-tab-dark-2">An tOireachtas</h2>
    </div>
    <?php //include(locate_template('/includes/featured-oireachtas.php')); ?>
</section-->

<!-- Angular -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular-touch.min.js"></script>
<!-- Angular -->
<section class="section-panel-dark-3 featv-section">
    <div class="title-tab-wrap">
        <h2 class="title-tab-dark-3"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Físeáin (rogha reatha)' : 'Featured Videos'); ?></h2>
    </div>
    <?php include(locate_template('/includes/featured-videos-api.php')); ?>
</section>

get_footer(); ?>