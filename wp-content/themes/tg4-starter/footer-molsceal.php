			</main>
			<footer class="footer">
				<section class="footer-links">
					<div class="footer-wrapper">
						<p class="sendmail"><a href="mailto:molsceal@tg4.ie?subject=Mol Scéal dúinn"><span class="fa fa-envelope"></span> Mol Scéal dúinn</a></p>
					</div>
				</section>
			</footer>
			<footer class="footer">
				<section class="social-links">
					<div class="footer-wrapper">
						<div style="float:left;"><a href="https://www.tg4.ie/ga/" target="_New" class="logo-home"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/TG4.png" alt="TG4 Logo" class="logo" height="274" width="100" style="padding-left:10px;"></a></div>
						<div>
							<p class="socialicons">
								<a href="https://www.facebook.com/MOLSCEAL/" target="_New"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/Mol-Facebook.svg" alt="Molscéal: Facebook" title="Molscéal: Facebook" class="social-logo" height="90" width="45"></a>
								<a href="https://twitter.com/molsceal/" target="_New"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/Mol-Twitter.svg" alt="Molscéal: Twitter" title="Molscéal: Twitter" class="social-logo" height="34" width="42"></a>
								<a href="https://www.instagram.com/molsceal/" target="_New"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/Mol-Instagram.svg" alt="Molscéal: Instagram" title="Molscéal: Share" class="social-logo" height="50" width="45"></a>
							</p>
						</div>
					</div>
				</section>
			</footer>
		</div>

		<script>
			$('.regiontoggle').click(function() {
				$('#regiondd').toggle('slow');
			});
			$('.sharetoggle').click(function() {
				$('#sharedd').toggle('slow');
			});
			$('.closetoggle').click(function() {
				$('#regiondd').toggle('slow');
			});
		</script>

		<!-- Twitter Analytics Tags -->
		<script src="https://static.ads-twitter.com/oct.js" type="text/javascript"></script>
		<script async type="text/javascript">twttr.conversion.trackPid('l4gjq', { tw_sale_amount: 0, tw_order_quantity: 0 });</script>
		<noscript>
			<img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=l4gjq&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
			<img height="1" width="1" style="display:none;" alt="" src="https://t.co/i/adsct?txn_id=l4gjq&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
		</noscript>
		<!-- Twitter Analytics Tags -->

		<!-- /wrapper -->

		<!-- ============= OUTDATED BROWSER ============= -->
        <div id="outdated"></div>
		
		<?php wp_footer(); ?>
    </body>
</html>