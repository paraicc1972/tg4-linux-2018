<section class="sched-highlights-player">
    <h2 class="visuallyhidden">Programme Highlights</h2>
    <div class="high-slider visuallyhidden">
        <?php
        if (ICL_LANGUAGE_CODE == "ga") {
            $posts = get_posts(array(
            'numberposts' => -1,
            'post_type' => 'Page',
            'meta_value' => '1'
            ));
        } else {
            $posts = get_posts(array(
            'numberposts' => -1,
            'post_type' => 'Page',
            'meta_key' => 'homepage_feature',
            'meta_value' => 'yes'
            ));
        }

        if($posts)
        {
            foreach($posts as $post)
            { ?>
                <article class="high-module">
                    <!--<a href="" class="high-link">-->
                    <a href="" class="high-link" style="background-image: url(<?php echo(wp_get_attachment_url(get_post_thumbnail_id($post->ID)));?>)">
                        <div class="high-details">
                            <h3 class="high-title"><?php echo get_the_title($post->ID) ?></h3>
                            <p class="high-date"><?php echo get_field("intro_title") ?></p>
                        </div>
                        <?php 
                        if (get_field("series_tag")) { 
                            if (ICL_LANGUAGE_CODE == "ga") {
                        ?>
                            <div class="btn-high">Féach ar na Cláir Uile<span></span></div>
                        <?php 
                            } else { 
                        ?>
                            <div class="btn-high">Watch Full Episodes<span></span></div>
                        <?php } } ?>
                    </a>
                </article>
        <?php } } ?>
    </div>
</section>