<?php
class walker_main_menu extends Walker_Nav_Menu {
    function start_lvl(&$output, $depth, $args) {
        parent::start_lvl($output, $depth,$args);
    }

    function end_lvl( &$output, $depth, $args ) {
        parent::end_lvl( $output, $depth = 0, $args);
    }

    function start_el( &$output, $item, $depth, $args, $id ) {
        parent::start_el( $output, $item, $depth = 0, $args, $id );
    }

    function end_el( &$output, $item, $depth, $args = array() ) {
        parent::end_el( $output, $item, $depth, $args);
    }

    function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {
        parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }
}
?>