<div class="featv-wrap featv-slider visuallyhidden">
    <section class="prog-module online-tg4">
        <div class="prog-slide-wrap">
            <div class="online-logo">
                <img src="https://d1og0s8nlbd0hm.cloudfront.net/images/tg4-logo.svg" alt="TG4 logo" title="TG4 logo" class="online-tg4-logo" height="379" width="139">
            </div>
            <div class="tonight-live">Live</div>
            <a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/beo/' : '/live/'); ?>" class="prog-panel">
                <h3 class="prog-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'TG4 Beo' : 'TG4 Live'); ?></h3>
                <img src="https://d1og0s8nlbd0hm.cloudfront.net/images/TG4-Beo.jpg" alt="" class="prog-img">
                <div class="prog-footer">
                    <h4 class="prog-episode-title">Féach aon áit, aon uair.</h4>
                    <div class="arrow-box"></div>
                </div>
            </a>
        </div>
    </section>
    <section class="prog-module online-playlist">
        <div class="prog-slide-wrap">
            <div class="online-logo"><span class="online-logo-playlist">An tOireachtas</span></div>
            <div class="tonight-live">Live</div>
            <a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/beo/caineal-2/' : '/en/live/chl-2/'); ?>" class="prog-panel">
                <h3 class="prog-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Corn Uí Riada Beo' : 'Live Corn Uí Riada'); ?></h3>
                <img src="https://d1og0s8nlbd0hm.cloudfront.net/tg4-redesign-2015/wp-content/uploads/2015/08/CornRiada2016.jpg" alt="" class="prog-img">
                <div class="prog-footer">
                    <h4 class="prog-episode-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Dé Sathairn @ 7pm' : 'Saturday @ 7pm'); ?></h4>
                    <p class="prog-episode-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Oireachas na Samhna ' : 'Oireachas na Samhna'); ?></p>
                    <div class="arrow-box"></div>
                </div>
            </a>
        </div>
    </section>
    <section class="prog-module online-cula">
        <div class="prog-slide-wrap">
            <div class="online-logo"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/cula-logo.svg" alt="Cúla4 logo" title="Cúla4 logo" class="online-cula-logo" height="906" width="407"></div>
            <a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/beo/cula4/' : '/live/cula4/'); ?>" class="prog-panel">
                <h3 class="prog-title">Cúla4</h3>
                <img src="https://d1og0s8nlbd0hm.cloudfront.net/images/girls-small.jpg" alt="" class="prog-img">
                <div class="prog-footer">
                    <h4 class="prog-episode-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Seinnliosta Cúla4' : 'Cúla4 Playlist'); ?></h4>
                    <div class="arrow-box"></div>
                </div>
            </a>
        </div>
    </section>
    <section class="prog-module online-playlist">
        <div class="prog-slide-wrap">
            <div class="online-logo"><span class="online-logo-playlist">Nuacht TG4</span></div>
            <a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/nuacht/' : '/news/'); ?>" class="prog-panel">
                <h3 class="prog-title">Nuacht an lae...</h3>
                <img src="https://d1og0s8nlbd0hm.cloudfront.net/images/NuachtTG4.jpg" alt="" class="prog-img">
                <div class="prog-footer">
                    <h4 class="prog-episode-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Na scéalta is déanaí...' : 'Latest News...'); ?></h4>
                    <div class="arrow-box"></div>
                </div>
            </a>
        </div>
    </section>
</div>