<section class="sched-highlights" style="margin-top:-40px;">
    <h2 class="visuallyhidden">Programme Highlights</h2>
	<div class="high-slider visuallyhidden">
		<?php
        if (ICL_LANGUAGE_CODE == "ga") {
            $posts = get_posts(array(
            'numberposts' => -1,
            'post_type' => 'Page',
            'meta_key' => 'sports_homepage',
            'meta_value' => '1'
            ));
        } else {
            $posts = get_posts(array(
            'numberposts' => -1,
            'post_type' => 'Page',
            'meta_key' => 'sports_homepage',
            'meta_value' => 'yes'
            ));
        }

        if($posts)
        {
            foreach($posts as $post)
            { ?>
        		<article class="high-module">
                    <a href="<?php echo get_permalink($post->ID); ?>" class="high-link" style="background-image: url(<?php echo(wp_get_attachment_url(get_post_thumbnail_id($post->ID)));?>)">
                    	<div class="high-details">
                        	<h3 class="high-title"><?php echo get_field("intro_title") ?></h3>
                            <?php 
                            if (get_field("intro_sub_title")) { ?>
                            	<p class="high-date"><?php echo get_field("intro_sub_title") ?></p>
                            <?php } ?>
                        </div>
                        <?php 
                            if (ICL_LANGUAGE_CODE == "ga") {
                        ?>
                            <div class="btn-high">Breis Eolais<span></span></div>
                        <?php 
                            } else { 
                        ?>
                            <div class="btn-high">More Information<span></span></div>
                        <?php } ?>
                    </a>
        	    </article>
        <?php } } ?>
	</div>
</section>