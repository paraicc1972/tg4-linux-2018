<section class="sched-highlights">
    <h2 class="visuallyhidden">Programme Highlights</h2>
	<div class="high-slider visuallyhidden">
		<?php
        if (ICL_LANGUAGE_CODE == "ga") {
            $posts = get_posts(array(
            'numberposts' => -1,
            'post_type' => 'Page',
            'meta_key' => 'homepage_feature',
            'meta_value' => '1'
            ));
        } else {
            $posts = get_posts(array(
            'numberposts' => -1,
            'post_type' => 'Page',
            'meta_key' => 'homepage_feature',
            'meta_value' => 'yes'
            ));
        }

        if($posts)
        {
            foreach($posts as $post)
            { ?>
        		<article class="high-module">
                    <?php 
                    $imgCode = '';  
                    if (get_field("Series_Code") != '') {
                        $imgCode = get_field("Series_Code");
                    } 
                    if (get_field("Programme_Code") != '') {
                        $imgCode = get_field("Programme_Code");
                    }
                    if ($imgCode == '') { ?>
                        <a href="<?php echo get_permalink($post->ID); ?>" class="high-link" style="background-image: url(<?php echo(wp_get_attachment_url(get_post_thumbnail_id($post->ID)));?>)">
                    <?php } else { ?>
                        <a href="<?php echo get_permalink($post->ID); ?>" class="high-link" style="background-image: url(https://res.cloudinary.com/tg4/image/upload/w_700,h_395,f_auto,q_auto/<?php echo $imgCode; ?>.jpg)">
                    <?php } ?>
                    <!--a href="<?php //echo get_permalink($post->ID); ?>" class="high-link" style="background-image: url(<?php //echo(wp_get_attachment_url(get_post_thumbnail_id($post->ID)));?>)"-->
                    	<div class="high-details">
                        	<h3 class="high-title"><?php echo get_field("intro_title") ?></h3>
                            <?php 
                            if (get_field("intro_sub_title")) { ?>
                            	<p class="high-date"><?php echo get_field("intro_sub_title") ?></p>
                            <?php } ?>
                        </div>
                        <?php 
                            if (ICL_LANGUAGE_CODE == "ga") {
                        ?>
                            <div class="btn-high">Breis Eolais<span></span></div>
                        <?php 
                            } else { 
                        ?>
                            <div class="btn-high">More Information<span></span></div>
                        <?php } ?>
                    </a>
        	    </article>
        <?php } } ?>
	</div>
</section>