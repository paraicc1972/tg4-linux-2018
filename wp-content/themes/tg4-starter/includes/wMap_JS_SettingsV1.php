<?php
// INSTRUCTIONS: Full instructions are contained at the bottom of this script.

// START - Editable
$imagesPath = "https://d2dlsfk5bqexp7.cloudfront.net/images/"; //the root of weather images
$intervalStart_Default = 0; // Start interval of each day. Numeric value in 'minutes': 1am = 60, 1.20pm = (13 * 60) + 20
$intervalGap_Default = 180; // Interval increments, numeric value in 'minutes'. Default 'tick mark' on the slider is in 6hr gaps.
$startTab = 0; // the initial 'Tab' to be loaded/selected

$settings = array('Cloud &amp; Rain' => array(
                'B&aacute;isteach' // Position [0] in this array is for Irish tab-name
                ,-1 // Position [1] in this array is for overriding [$intervalStart_Default] for this tab, [-1] means use default
                ,-1 // Position [2] in this array is for overriding [$intervalGap_Default] for this tab, [-1] means use default
                ,0 // Position [3] in this array stores an offset, in days from todays date, for the starting day label for the tab. [0] means use todays date
                ,'CloudAndRain-' // Position [4] in this array stores the image prefix format: [prefix-YYMMDDHH]
                ,'jpg' // Position [5] in this array stores the image filetype
                , array('step'=> 0, 'labeltype'=>'d', 'intervalgap'=>0) // All remaining elements here relate to the 'slider' tick/steps
                , array('step'=> 1, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 2, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 3, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 4, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 5, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 6, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 7, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 8, 'labeltype'=>'d', 'intervalgap'=>0) 
                , array('step'=> 9, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 10, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 11, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 12, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 13, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 14, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 15, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 16, 'labeltype'=>'d', 'intervalgap'=>0) 
                , array('step'=> 17, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 18, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 19, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 20, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 21, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 22, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 23, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 24, 'labeltype'=>'d', 'intervalgap'=>0) 
                , array('step'=> 25, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 26, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 27, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 28, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 29, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 30, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 31, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 32, 'labeltype'=>'d', 'intervalgap'=>0) // All remaining elements here relate to the 'slider' tick/steps
            )
            ,'Wind &amp; Temp' => array(
                'Gaoth &amp; Teocht' // Position [0] in this array is for Irish tab-name
                ,-1 // Position [1] in this array is for overriding [$intervalStart_Default] for this tab, [-1] means use default
                ,-1 // Position [2] in this array is for overriding [$intervalGap_Default] for this tab, [-1] means use default
                ,0 // Position [3] in this array stores an offset, in days from t0days date, for the starting day label for the tab. [0] means use todays date
                ,'TempAndWind-' // Position [4] in this array stores the image prefix format: [prefix-YYMMDDHH]
                ,'jpg' // Position [5] in this array stores the image filetype
                , array('step'=> 0, 'labeltype'=>'d', 'intervalgap'=>0) // All remaining elements here relate to the 'slider' tick/steps
                , array('step'=> 1, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 2, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 3, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 4, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 5, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 6, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 7, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 8, 'labeltype'=>'d', 'intervalgap'=>0) 
                , array('step'=> 9, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 10, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 11, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 12, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 13, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 14, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 15, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 16, 'labeltype'=>'d', 'intervalgap'=>0) 
                , array('step'=> 17, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 18, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 19, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 20, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 21, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 22, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 23, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 24, 'labeltype'=>'d', 'intervalgap'=>0) 
                , array('step'=> 25, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 26, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 27, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 28, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 29, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 30, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 31, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 32, 'labeltype'=>'d', 'intervalgap'=>0) 
            )
            ,'Air Pressure' => array(
                'Aerbhr&uacute;' // Position [0] in this array is for Irish tab-name
                ,-1 // Position [1] in this array is for overriding [$intervalStart_Default] for this tab, [-1] means use default
                ,-1 // Position [2] in this array is for overriding [$intervalGap_Default] for this tab, [-1] means use default
                ,0 // Position [3] in this array stores an offset, in days from t0days date, for the starting day label for the tab. [0] means use todays date
                ,'Pressure-' // Position [4] in this array stores the image prefix format: [prefix-YYMMDDHH]
                ,'jpg' // Position [5] in this array stores the image filetype
                , array('step'=> 0, 'labeltype'=>'d', 'intervalgap'=>0) // All remaining elements here relate to the 'slider' tick/steps
                , array('step'=> 1, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 2, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 3, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 4, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 5, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 6, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 7, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 8, 'labeltype'=>'d', 'intervalgap'=>0) 
                , array('step'=> 9, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 10, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 11, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 12, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 13, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 14, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 15, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 16, 'labeltype'=>'d', 'intervalgap'=>0) 
                , array('step'=> 17, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 18, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 19, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 20, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 21, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 22, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 23, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 24, 'labeltype'=>'d', 'intervalgap'=>0) 
                , array('step'=> 25, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 26, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 27, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 28, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 29, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 30, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 31, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 32, 'labeltype'=>'d', 'intervalgap'=>0)
            )
            ,'High Tides' => array(
                'L&aacute;n Mara' // Position [0] in this array is for Irish tab-name
                ,-1 // Position [1] in this array is for overriding [$intervalStart_Default] for this tab, [-1] means use default
                ,-1 // Position [2] in this array is for overriding [$intervalGap_Default] for this tab, [-1] means use default
                ,0 // Position [3] in this array stores an offset, in days from t0days date, for the starting day label for the tab. [0] means use todays date
                ,'NextHighTide-' // Position [4] in this array stores the image prefix format: [prefix-YYMMDDHH]
                ,'jpg' // Position [5] in this array stores the image filetype
                , array('step'=> 0, 'labeltype'=>'d', 'intervalgap'=>0) // All remaining elements here relate to the 'slider' tick/steps
                , array('step'=> 1, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 2, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 3, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 4, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 5, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 6, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 7, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 8, 'labeltype'=>'d', 'intervalgap'=>0) 
                , array('step'=> 9, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 10, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 11, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 12, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 13, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 14, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 15, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 16, 'labeltype'=>'d', 'intervalgap'=>0) 
                , array('step'=> 17, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 18, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 19, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 20, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 21, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 22, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 23, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 24, 'labeltype'=>'d', 'intervalgap'=>0) 
                , array('step'=> 25, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 26, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 27, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 28, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 29, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 30, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 31, 'labeltype'=>'i', 'intervalgap'=>1)
                , array('step'=> 32, 'labeltype'=>'d', 'intervalgap'=>0) 
            )
        );
// End - Editable

/*
PREPARE OUTPUT FROM THIS POINT:
As xMod Snippets don't allow embedded functions, the processing of this script is designed to
run in a linear fashion (from top to bottom) in order to produce the required output.
*/

if (ICL_LANGUAGE_CODE == "ga") {
    $YAMS_ID = "ie";
} else {
    $YAMS_ID = "en";
}

$timeslotIndexStart = 6; // the details for the 'timeslots' start at [$settings[tab_name][$timeslotIndexStart]]

//$YAMS_ID = isset($YAMS_ID) ? $YAMS_ID: ''; // LIVE: [$YAMS_ID] passed in for lang versions in SNIPPET
//$YAMS_ID = "en"; // TESTING: [$YAMS_ID] hard coded for local testing
$noInfo = array('en'=>'Information will be available shortly.', 'ie'=>'Beidh eolas ar f&aacute;il go luath.'); // shown on screen if no images available for a 'tab'


/* START: Weekday strings
    - Position of weekday text in following arrays needs to match the PHP Date Object numeric representation of the day of the week:
    - 0 (for Sunday) through 6 (for Saturday)
    - Determine which weekday arrays to use based on YAMS_ID
*/
if ($YAMS_ID == "en"){
    $arrShortDays = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
    $arrLongDays = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
    $today = "Today";
    $tomorrow = "Tomorrow";
    $am = "am";
    $pm = "pm";
}else{
    $arrShortDays = array("Dom", "Lua", "M&aacute;i", "C&eacute;a", "D&eacute;a", "Aoi", "Sat");
    $arrLongDays = array("D&eacute; Domhnaigh", "D&eacute; Luain", "D&eacute; M&aacute;irt", "D&eacute; C&eacute;adaoin", "D&eacute;ardaoin", "D&eacute; hAoine", "D&eacute; Sathairn");
    $today = "Inniu";
    $tomorrow = "Am&aacute;rach";
    $am = "rn";
    $pm = "in";
}
// END: Weekday strings

/*
 START: [ $dayPartLabel ] label calculation variables array
 - The [start] and [ end] of daily times are stored as a count of minutes from midnight
*/
$times = array(
        array('start'=> 1, 'end'=> 359, 'ie'=>'Maidin Luath', 'en'=>'Early Morning')
        , array('start'=> 360, 'end'=> 719, 'ie'=>'Maidin', 'en'=>'Morning')
        , array('start'=> 720, 'end'=> 720, 'ie'=>'N&oacute;in', 'en'=>'Noon')
        , array('start'=> 721, 'end'=> 1079, 'ie'=>'Iarn&oacute;in', 'en'=>'Afternoon')
        , array('start'=> 1080, 'end'=> 1199, 'ie'=>'Tr&aacute;thn&oacute;na', 'en'=>'Evening')
        , array('start'=> 1200, 'end'=> 1439, 'ie'=>'O&iacute;che', 'en'=>'Night')
        , array('start'=> 0, 'end'=> 0, 'ie'=>'M&eacute;an O&iacute;che', 'en'=>'Midnight')
    );
// END: [ $dayPartLabel ] label calculation variables array

// START: Set today date/time variables
// Get todays date, will also be referenced for start value of slider
 $nowDate = date("Y-m-d"); //LIVE
//$nowDate = date("2013-09-05"); //TESTING

// Get the numeric value of the current weekday, used to determine weekday labels
$nowWeekDay = date('w', strtotime($nowDate)); // Need to user [strtotime] or PHP throws a 'Notice: A non well formed numeric value encountered' error message

// Need to use [ strtotime("now") ] to set [ $nowMinutes ]  as [ $nowDate ] does not contain minutes
$nowMinutes = ((date('H', strtotime("now")) * 60) + date('i', strtotime("now"))); // the current time in minutes, used to set
// END: Set today date/time variables

/* START - Check if tab link clicked
    - if a 'tab' link was clicked (right-click, open in new window/tab from a users browser)
    - we need to reset [$startTab], the initial 'tab' that will be selected on screen
*/
$clickTab = (isset ($_REQUEST['tab'])) ? $_REQUEST['tab'] : '';
if ($clickTab != ""){
    $tabCounter = 0; // used to keep track of the 'tabs' array index 
    
    foreach ($settings as $key => $value) {
        if ($YAMS_ID == "en"){
            $tabTitle = $key;
        }else{
            $tabTitle = $settings[$key][0];
        }
        if ($clickTab == $tabTitle){
             $startTab = $tabCounter;
             break; // match found, exit the 'foreach' loop
        }
        $tabCounter++;
    }
}
// END - Check if tab link clicked

// START - Prepare URL for 'tab' links (and for Google Analytics in Client browser)
if ($YAMS_ID != ""){ // we need to add the YAMS_ID
    $site = 'https://www.tg4.ie/'; // TESTING
    $url = str_Replace($site, ($site . $YAMS_ID . '/'), $url);
}
$url .= "?"; // Add the '?' to prepare to add querystring below for each individual 'tab'
// END - Prepare URL for 'tab' links

/* START - Compile ALL output
    - The javascript object constuctor expects values to ba passed in this order: 
        [ wmap_tab(tabname, tabSteps, tabLabels, tabImages, tabImageLabels, startPosition) ]
    - WARNING FOR MODX:
        - we are outputting data as an array of arrays to be picked up bt the main JS file
        - make sure not to have it encapsulated as 'var wMap_data = [[ ... ]]' or MODX will look at it as somekind of placeholder.
        - SOLUTION: put spaces between the brackets '[   [ ... ]   ]'
*/

$outputJS = 'var wMap_data = [   '; // [$outputJS] will store all the javascript initialisation settings for the 'tabs' & 'slider'
$outputTabs = ''; // Will hold <li> elements for the tabs
$tabCounter = 0; // used to keep track of the 'tabs' array index. 
                 // Reset to [0] here as it may have been used above if a 'tab' had been opened in a new window, etc.

foreach ($settings as $key => $value) { // Loop and compile output for each 'tab'
    // START: Compile HTML Output for this tab
    if ($YAMS_ID == 'en'){
        $tabTitle = $key;
    }else{
        $tabTitle = $settings[$key][0];
    }
    
    if ($tabCounter == $startTab && $tabCounter == 0){ // the first tab is selected
        $tabStyle = 'selected first';
    }elseif($tabCounter == 0){ // dealing with the first tab, not selected
        $tabStyle = 'first';
    }elseif($tabCounter == $startTab && $tabCounter == (count($settings)-1)){// the last tab is selected
        $tabStyle = 'selected last';
    }elseif($tabCounter == (count($settings)-1)){// dealing with the last tab, not selected
        $tabStyle = 'last';
    }else{// dealing with an unselected tab that is not the first or last one
        $tabStyle = '';
    }
    
    if ($tabStyle == ''){
        $tabClass = '';
    }else{
        $tabClass = ' class="' . $tabStyle . '"';
    }
    
    $outputTabs .= '    <li' . $tabClass . '><div class="wmap-ldr-sml"></div><a href="' . $url . 'tab=' . urlencode($tabTitle) . '#wmap">' . $tabTitle . '</a></li>';
    // END: Compile HTML Output for this 'tab'
    
    
    /* START: Compile Javascript output for this 'tab'
        - IMPORTANT: The javascript object constuctor expects values to ba passed in this order: 
            [wmap_tab(tabname, tabSteps, tabLabels, tabImages, tabImageLabels, startPosition)]
    */
    if ($tabCounter > 0){ //not dealing with first 'tab', add a 'comma' spacer between JS object constructor calls
        $outputJS .= ',';
    }
    $outputJS .= '[\''. $key . '\''; // Start the JS, 'tab', object array
    
    $startStep = -1; // The starting position for each 'tab' 'slider' is flaged here as 'unset' and is  then 'set' in the code below based on current time
    
    $theSteps = ',['; // To store the javascript text for the js [theSteps] array
    $theLabels = ',[';  // To store the javascript text for the js [theLabels] array
    $images = ',[';  // To store the javascript text for the js [images] array
    $imageTags = ',[';  // To store the javascript text for the js [theLabels] array
    
    
    // START: Set the start date & 'timeslot' settings for this tab
    if($settings[$key][1] != -1){ // Check for timeslot start-time override
        $intervalStart = $settings[$key][1];
    }else{
        $intervalStart = $intervalStart_Default;
    }
    $intervalValue = $intervalStart; // Set/updated during loop below.
    if($settings[$key][2] != -1){ // Check for timeslot time-interval override
        $intervalGap = $settings[$key][2];
    }else{
        $intervalGap = $intervalGap_Default;
    }
    if($settings[$key][3] != 0){ // If there is a date offset for this 'tab'
        $tabDate = date('Y-m-d', strtotime('+' . $settings[$key][3] . ' day', strtotime($nowDate)));
        $tabWeekDay = ($nowWeekDay + $settings[$key][3]) % 7; // Set for use when the first 'day' is encountered
        if ($tabWeekDay < 0) {// ensure weekday is correct value from [0] to [6]
            $tabWeekDay = 7 + $tabWeekDay; // Example: [-2] becomes [5]
        }
    }else{ // no date offset
        $tabDate = $nowDate;
        $tabWeekDay = $nowWeekDay; // Set for use when the first 'day' is encountered
    }
    // END: Set the start date & 'timeslot' settings for this tab
    
    // NOTE: remember that elements in the array that relate to settings for the JS object constructor of each 'tab' start at [ $timeslotIndexStart ] 
    $arrlength=count($settings[$key]);
    for ($x=$timeslotIndexStart; $x < $arrlength; $x++){
        //  Add to the js [theSteps] array
        $theSteps .= $settings[$key][$x]['step'];
        
        // Add to the js [theLabels] array
        if ($settings[$key][$x]['labeltype'] == 'd'){ // Calculations when dealing with a weekday
            
            if ($settings[$key][$x]['intervalgap'] == 0){
                $intervalValue = $intervalStart; // reset the time interval for this day. Used to output the 24hr value for the js [imageTags] array
            }else{
                $intervalValue += ($settings[$key][$x]['intervalgap'] * $intervalGap); // CHECK: may need to change this to [  $intervalValue = $intervalStart + ($settings[$key][$x]['intervalgap'] * $intervalGap); ]
                                                                                       //        - Will depend on required functionality as requirements become clearer
            }
            
            if ($x > $timeslotIndexStart) { // we have already shown slots for 1st day on 'slider' => increment day/date variables by 1
                // Set [$tabDate] to next calendar date
                $tabDate = date('Y-m-d', strtotime('+1 day', strtotime($tabDate)));
                
                // Set [$tabWeekDay] to next calendar day
                if ($tabWeekDay == 6){
                    $tabWeekDay = 0; // Go from last day in array (Saturday), to first day in array (Sunday)
                }else{
                    ++$tabWeekDay; // Set to next day
                }
            }
            /*
            - The value that currently identifies days, ['labeltype'=>'d'], means that we output the
              short-dayname to the js array
            - This is currently referenced in the javascripts to display short-dayname along with a
              a 'prominent' tick mark on the 'slider'. 
            */
            $theLabels .=  '\'' . $arrShortDays[$tabWeekDay] . '\'';
            
        }else{ // Calculations when dealing with day-intervals
            // Increment the time interval as required. Used to output the 24hr value for the js [imageTags] array
            $intervalValue += ($settings[$key][$x]['intervalgap'] * $intervalGap);
            
            /*
            - The value that is currently assigned to time-intervals, ['labeltype'=>'i'], means that we output
              [''] to the js [theLabels] array
            - This is currently referenced in the javascripts to show a 'less-prominent' tick mark with no label. 
            */
            $theLabels .=  '\'\'';
        }
        //add to the  js [imageTags] array, to show text overlay in format: [day, date 24hr]
        //$imageTags .=  '\'' . $arrLongDays [$tabWeekDay] . ', ' . date("d/m/Y", strtotime($tabDate)) . ' ' . gmdate("H:i", ($intervalValue * 60)) .  '\''; // Note: [gmdate("H:i", value_in_seconds)] returns [hh:mm]
        
        //AOCU: Work out new text block here
        //Set the day-part label here. 'Morning', 'Noon', etc.
        $dayPartLabel = '';
        $timeslength=count($times);
        for ($intTime=0; $intTime < $timeslength; $intTime++){
            if ($intervalValue >= $times[$intTime]['start'] && $intervalValue <= $times[$intTime]['end']){
                $dayPartLabel = $intTime; //$times[$intTime][$YAMS_ID];
                break; // match found, exit the 'for' loop
            }
        }
        // set the hour/min suffix
        if ($intervalValue >= 720){ // 'pm'
            $hourMinSuffix = $pm; // correct lang version set above
        }else{
            $hourMinSuffix = $am; // correct lang version set above
        }
        // set the day-label: 'Today', 'Tomorrow' or day-name
        if($tabDate == $nowDate){
            $dayNameLabel = 0;//$today;
        }elseif($tabDate == date('Y-m-d', strtotime('+1 day', strtotime($nowDate)))){
            $dayNameLabel = 1;//$tomorrow;
        }else{
            $dayNameLabel = $tabWeekDay + 2;// the JS array this looks-up starts with 'today' & 'tomorrow' :  previously:$arrLongDays [$tabWeekDay];
        }
        //add to the  js [imageTags] array, to show text box details in format: [day-part label => day label => hrs:mins + suffix]
        //$imageTags .=  '\'' . $dayPartLabel . '<br/>' . $dayNameLabel . '<br/>' . gmdate("g:i", ($intervalValue * 60)) . $hourMinSuffix . '\''; // Note: [gmdate("H:i", value_in_seconds)] returns [hh:mm]
        $imageTags .=  '\'' . $dayNameLabel . '.' . $dayPartLabel . '.' . gmdate("g:i", ($intervalValue * 60)) . $hourMinSuffix . '\''; // Note: [gmdate("H:i", value_in_seconds)] returns [hh:mm]
        
        
        //  Add to the js [images] array
                /*
                if ($key === 'Air Pressure'){
                    $imgType = 'png';
                }else{
                    $imgType = 'jpg';
                }
                 * 
                 */
        //$images .= '\'' .  $imagesPath .  $settings[$key][4] . '-' . date("ymd", strtotime($tabDate)). gmdate("H", ($intervalValue * 60)) . '.' . $imgType . '\'';                                                   //        as oppossed to having them had-coded in the [ $settings ] array 
        $images .= '\'' .  $tabCounter . '.' . date('ymd', strtotime($tabDate)). gmdate('H', ($intervalValue * 60)) . '.' . $tabCounter . '\'';                                                //        as oppossed to having them had-coded in the [ $settings ] array 
        
                if ($startStep == -1) { //if starting 'step'/'timeslot' hasn't been set yet
            if ($tabDate > $nowDate){ // if the day value of this 'timeslot' is greater than today use it.
                $startStep = $x - $timeslotIndexStart;
            }elseif ($tabDate == $nowDate && $intervalValue >= $nowMinutes) { // if the day value of this 'timeslot' is equal to today 
                                                                                 // AND the  interval value (in minutes) is greater than current time (in minutes) use it.
                $startStep = $x - $timeslotIndexStart;
            }
        }
        // Check/output javascript array deliminator here, add if there are further elements to be processed
        if ($x < ($arrlength - 1)){
            $theSteps .=  ',';
            $theLabels .=  ',';
            $images .=  ',';
            $imageTags .=  ',';
        }
    }
    if ($startStep == -1){ // not set in loop above because either all 'timeslots' were in the future OR all 'timeslots' were in the past for that tab
        if ($tabDate < $nowDate){ // the last step on the last tab was in the past
            $startStep = ($arrlength - 1) - $timeslotIndexStart; // set to last step for that tab
        }else{ // the first step on the last tab was in the future
            $startStep = 0;
        }
    }
    $theSteps .= ']';
    $images .= ']';
    $theLabels .= ']';
    $imageTags .= ']';
    
    $outputJS.=  $theSteps;
    $outputJS.= $theLabels;
    $outputJS.= $images;
    $outputJS.= $imageTags;
    $outputJS.= ',' . $startStep;
    
    $outputJS.= '   ]'; // close the 'tab' object array
    //END: Compile Javascript output for this 'tab'
    
    $tabCounter ++;
}

$outputJS .= '];';
$outputJS .= 'wmStartTab = ' . $startTab . ';';
$outputJS .= 'wmThisURL = "' . $url . '";';

// output the array holding the timeslots text ('Early Morning','Morning',etc)
$outputJS .= 'var wMap_time = [';
$arrlength=count($times);
for ($x=0; $x < $arrlength; $x++){
    if($x > 0){
        $outputJS .= ',';
    }
    $outputJS .= '"' . $times[$x][$YAMS_ID] . '"';
}
$outputJS .= '];';

// output the array holding the days text ("Today","Tomorrow","Sunday",etc)
$outputJS .= 'var wMap_day = [';
$outputJS .= '"' . $today . '"';
$outputJS .= ',"' . $tomorrow . '"';
$arrlength=count($arrLongDays);
for ($x=0; $x < $arrlength; $x++){
    $outputJS .= ',"' . $arrLongDays[$x]. '"';
}
$outputJS .= '];';

// output the array holding the image prefixes text ('CloudAndRain-','TempAndWind-',etc)
$outputJS .= 'var wMap_imgPre = [';
$arrlength=count($settings);
$intCnt = 0;
foreach ($settings as $key => $value){
    if($intCnt > 0){
        $outputJS .= ',';
    }
    $outputJS .= '"' . $settings[$key][4] . '"';
    $intCnt++;
}
$outputJS .= '];';

// output the array holding the image prefixes text ('CloudAndRain-','TempAndWind-',etc)
$outputJS .= 'var wMap_imgType = [';
$arrlength=count($settings);
$intCnt = 0;
foreach ($settings as $key => $value){
    if($intCnt > 0){
        $outputJS .= ',';
    }
    $outputJS .= '"' . $settings[$key][5] . '"';
    $intCnt++;
}
$outputJS .= '];';

// output the image cache-buster based on Metra generating updated images daily at: 00:00, 06:00, 12:00, 18:00
if($nowMinutes >= 1080){ // 18:00 - 23:59
    $imgCachMins = '1800';
}elseif($nowMinutes >= 720){ // 12:00 - 15:59
    $imgCachMins = '1200';
}elseif($nowMinutes >= 360){ // 06:00 - 11:59
    $imgCachMins = '0600';
}else{ // 00:00 - 05:59
    $imgCachMins = '0000';
}
$outputJS .= 'var wmCacheBuster = "' . $nowDate . $imgCachMins . '";';
$outputJS .= 'wMap_init();';

$outputALL = '<a name="wmap"></a>';
$outputALL .= '<div id="wmap_header">';
$outputALL .= ' <ul>';
$outputALL .= $outputTabs;
$outputALL .= ' </ul>';
$outputALL .= '</div>';
$outputALL .= '<div id="wmap_content">';
$outputALL .= ' <div class="wmap-wrapper">';
$outputALL .= '     <div id = "wmap-image"></div>';
$outputALL .= '     <div id = "wmap-loader"></div>';
$outputALL .= '     <div id = "wmap-noinfo">' . $noInfo[$YAMS_ID] . '</div>';
$outputALL .= '     <div id="wmap-slider-stuff">';
$outputALL .= '         <div id="wmap-slider-text"></div>';
$outputALL .= '         <div id="wmap-button-holder">';
$outputALL .= '             <div id="wmap-next" class="wmap-button"></div>';
$outputALL .= '             <div id="wmap-prev" class="wmap-button"></div>';
$outputALL .= '         </div>';

// output 'slider' DOM elements for each 'tab'
$arrlength=count($settings);
for ($x=0; $x < $arrlength; $x++){
    $outputALL .= '         <div id="wmap-slider-box' . $x . '">';
    $outputALL .= '             <div id="wmap-slider' . $x . '"></div>';
    $outputALL .= '         </div>';
}
$outputALL .= '     </div>';
$outputALL .= ' </div>';
$outputALL .= '</div>';
$outputALL .= '<script language="JavaScript" type="text/javascript">';
$outputALL .= $outputJS;
$outputALL .= '</script>';
// END - Compile ALL output

//echo $outputALL; // TESTING
echo $outputALL; // LIVE

/*
INSTRUCTIONS:

There are a number of system/default variables that can be edited, detailed in section B) below,
as well as the main [ $settings ] 3D array that contains all settings for each 'tab' and its associated 'slider'
, detailed in section C) below.

These are all contained within the [ // START - Editable ] and [ // End - Editable ] comment tags.
Unless changing the underlying logic/structure nothing outside of these comment tags should be changed.

The actual code block/logic may seem overly complex and verbose but this was due to the constraint of having it 
be contained within one 'snippet'. This meant it had to execute in a linear fashion (from top to bottom) and code 
could not be encapsulated within seperate functions.

A) Output from this script:
    A.1) It is assumed that the page that ouput from this script is returned to will already have 
        - javascript tag linking to the main jQuery library, this needs the latest version which has dealt with IE AJAX issues.
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
            - javascript tag linking to the 'wmap' client-side functionality, in this order:
                <script language="JavaScript" type="text/javascript"  src="https://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
                <script language="JavaScript" type="text/javascript" src="https://d1og0s8nlbd0hm.cloudfront.net/js/wMap/wmap_init_functionsV2.js"></script>
    A.2) This script will output the following:
        A.2.1) Links to stylesheet  for the 'tabs' & 'slider'
        A.2.2) The HTML/DOM elements for the 'tabs' and 'slider'
            - The same 'slider' is reinitalised for each 'tab'
            - The 'tabs' and a loading icon are shown until preloading of images has been done
            - Once the very first image has been preloaded this is shown, overlaid with the loading image.
            - When all images for the default tab are preloaded, the 'slider' is shown and the
              loading image removed
            - the other 'tabs' become active when their images have been fully preloaded
        A.2.3) Links to javascripts for the 'tabs' & 'slider'
        A.2.4) Javascript code within script tags:
            - This contains all initalisation data
            - It contains the function call to start things rockin!
    
B) System/default variables:
    B.1) [ $imagesPath ]
        - The filepath to all images
    B.2) [ $intervalStart_Default ]
        - The starting time, in minutes from midnight, for each Day on the 'slider'
        - this can be overridden fro each individual 'tab'. Ref. C.2.2)
    B.3) [ $intervalGap_Default ]
        - The time interval between steps/ticks on the 'slider', in minutes
        - this can be overridden fro each individual 'tab'. Ref. C.2.3)
    B.4) [ $startTab ]
        - The index position of the default tab to be activated when this script is deployed
        - Example:
            [  $settings = array('Cloud &amp; Rain'......
                                ,'Wind &amp; Temperature' .....
                                ,'Air Pressure'.....
                                ,'Sea Levels'.....
                ); ]
            
            Here [ $startTab = 0 ] would refer to the 'tab' for 'Cloud &amp; Rain'. [ 2 ] would refer to the 'tab' for 'Air Pressure'
    
C) The [ $settings ] array:
    - This Controls all 'tabs' and their associated 'sliders'
    - This is a 3D array:
    C.1) The first dimension holds the tab-name : 
        - [ $settings['the_tab_name_here'].... ]
    C.2) The 2nd dimension contains another array with all settings for that particular tab.
        - This array is contructed as follows:
        C.2.1) [ $settings['the_tab_name_here'][0] ]:
            - This holds the tab-name in irish
        C.2.2) [ $settings['the_tab_name_here'][1] ]:
            - This gives the option of overriding [ $intervalStart_Default ] for this tab
            - A value of [ -1 ] means use the default.
            - If overriding the value must be a whole/positive number in minutes, from midnight
        C.2.3) [ $settings['the_tab_name_here'][2] ]:
            - This gives the option of overriding [ $intervalGap_Default ] for this tab
            - A value of [ -1 ] means use the default.
            - If overriding the value must be a whole/positive number in minutes
        C.2.4) [ $settings['the_tab_name_here'][3] ]:
            - This gives the option of setting an offset, in days, from the current/start-date for this tab
            - A value of [0] means use todays date.
            - A positive value will result in the 'slider' etc for this 'tab' starting on the number of days specified in the future
            - A negative value will result in the 'slider' etc for this 'tab' starting on the number of days specified in the past
        C.2.5) All remaining positions in this array, [ $settings['the_tab_name_here'][$timeslotIndexStart] ] up 'till the end of the array
               each store another array holding details for each 'step'/'tick' on the 'slider' for that 'tab'.
            - This array, the 3rd dimension of [ $settings ], is detailed in section D) below.
    
      
*/
?>