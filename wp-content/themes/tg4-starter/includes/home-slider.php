<section class="slider-wrapper">
	<div id="full-width-slider" class="royalSlider heroSlider rsMinW">
		<?php
		if (ICL_LANGUAGE_CODE == "ga") {
		    $posts = get_posts(array(
		    'numberposts' => -1,
		    'post_type' => 'Page',
		    'meta_key' => 'homepage_feature',
		    'orderby' => 'modified',
			'order' => 'DESC',
			'post_status' => 'publish',
		    'meta_value' => '1'
			));
		} else {
		    $posts = get_posts(array(
		    'numberposts' => -1,
		    'post_type' => 'Page',
		    'meta_key' => 'homepage_feature',
		    'orderby' => 'modified',
			'order' => 'DESC',
			'post_status' => 'publish',
		    'meta_value' => 'yes'
			));
		}

		if($posts)
		{
		    foreach($posts as $post)
		    { ?>
			    <section class="rsContent">
			        <div class="blockContainer">
						<a href="<?php echo get_permalink($post->ID);?>"><h2 class="title-block rsABlock" data-fade-effect="true" data-move-effect="left" data-move-offset="50" data-speed="400" data-easing="easeOutSine"><?php echo get_field("intro_title") ?></h2></a>
						<?php if ((get_field('intro_sub_title')) || (get_field('intro_text'))) { ?>
					        <div class="content-block rsABlock" data-fade-effect="true" data-move-effect="left" data-move-offset="50" data-speed="400" data-easing="easeOutSine">
					            <h3 class="content-subtitle"><?php echo get_field("intro_sub_title") ?></h3>
					            <p><?php echo get_field("intro_text") ?></p>
					        </div>
				        <?php }
				        if (ICL_LANGUAGE_CODE == "ga") { ?>
				        	<div class="rsABlock" data-fade-effect="true" data-move-effect="left" data-move-offset="50" data-speed="400" data-easing="easeOutSine">
				        		<a href="<?php echo get_permalink($post->ID);?>" class="btn-header">Breis Eolais<span></span></a>
				        	</div>
				        <?php } else { ?>
				        	<div class="rsABlock" data-fade-effect="true" data-move-effect="left" data-move-offset="50" data-speed="400" data-easing="easeOutSine">
				        		<a href="<?php echo get_permalink($post->ID);?>" class="btn-header">More Information<span></span></a>
				        	</div>
				        <?php } ?>
			        </div>
			        <?php 
			        $imgCode = '';
					if (get_field("Series_Code") != '') {
						$imgCode = get_field("Series_Code");
					} 
					if (get_field("Programme_Code") != '') {
						$imgCode = get_field("Programme_Code");
					}
					if ($imgCode == '') { ?>
						<img class="rsImg" src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID))?>" alt="<?php echo get_field("intro_title") ?>" title="<?php echo get_field("intro_title") ?>" width="1920" height="1080">
					<?php } else {
					?>
						<script>
							if (window.innerWidth <= 900) {
							    document.write('<img class="rsImg" src="https://res.cloudinary.com/tg4/image/upload/w_700,h_395,f_auto,q_auto/<?php echo $imgCode; ?>.jpg" alt="<?php echo get_field("intro_title") ?>" title="<?php echo get_field("intro_title") ?>">');
							} else if (window.innerWidth > 901 && window.innerWidth <= 1408) {
							    document.write('<img class="rsImg" src="https://res.cloudinary.com/tg4/image/upload/w_1310,h_737,f_auto,q_auto/<?php echo $imgCode; ?>.jpg" alt="<?php echo get_field("intro_title") ?>" title="<?php echo get_field("intro_title") ?>">');
							} else {
							    document.write('<img class="rsImg" src="https://res.cloudinary.com/tg4/image/upload/f_auto,q_auto/<?php echo $imgCode; ?>.jpg" alt="<?php echo get_field("intro_title") ?>" title="<?php echo get_field("intro_title") ?>">');
							}
						</script>
					<?php } ?>
			    </section> 
			<?php } } ?>
	</div>
</section>