<nav id="main-menu-nd" class="main-menu-nd" role="navigation">
    <?php 
    $defaults = array(
        'theme_location'  => '',
        'menu'            => 'Main Menu',
        'container'       => '',
        'container_class' => '',
        'container_id'    => '',
        'menu_class'      => '',
        'menu_id'         => '',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<ul class="main-menu-list">%3$s</ul>',
        'depth'           => 2
    );
    wp_nav_menu($defaults);
    ?>
</nav>