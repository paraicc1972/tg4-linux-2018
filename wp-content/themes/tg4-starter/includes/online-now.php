<div class="online-wrap online-slider visuallyhidden">
    <section class="prog-module online-tg4">
        <div class="prog-slide-wrap">
            <!--div class="online-logo"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/tg4-logo.svg" alt="TG4 logo" title="TG4 logo" class="online-tg4-logo" height="379" width="139"></div-->
            <div class="tonight-live">Live</div>
            <a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/beo/' : '/en/live/'); ?>" class="prog-panel">
                <h3 class="prog-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'TG4 Beo' : 'TG4 Live'); ?></h3>
                <img src="https://d1og0s8nlbd0hm.cloudfront.net/images/TG4-Beo.jpg" alt="TG4 Beo" title="TG4 Beo" class="prog-img" width="378" height="213">
                <div class="prog-footer">
                    <h4 class="prog-episode-title">Féach aon áit, aon uair.</h4>
                    <div class="arrow-box"></div>
                </div>
            </a>
        </div>
    </section>
    <section class="prog-module online-cula">
        <div class="prog-slide-wrap">
            <!--div class="online-logo"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/cula-logo.svg" alt="Cúla4 logo" title="Cúla4 logo" class="online-cula-logo" height="906" width="407"></div-->
            <a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/beo/cula4/' : '/en/live/cula4/'); ?>" class="prog-panel">
                <h3 class="prog-title">Cúla4</h3>
                <img src="https://d1og0s8nlbd0hm.cloudfront.net/images/girls-small.jpg" alt="Cúla4" title="Cúla4" class="prog-img" width="378" height="213">
                <div class="prog-footer">
                    <h4 class="prog-episode-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Seinnliosta Cúla4' : 'Cúla4 Playlist'); ?></h4>
                    <div class="arrow-box"></div>
                </div>
            </a>
        </div>
    </section>
    <section class="prog-module online-playlist">
        <div class="prog-slide-wrap">
            <!--div class="online-logo"><span class="online-logo-playlist">Nuacht TG4</span></div-->
            <a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/beo/nuacht-tg4/' : '/en/live/nuacht-tg4/'); ?>" class="prog-panel">
                <h3 class="prog-title">Nuacht an lae...</h3>
                <img src="https://d1og0s8nlbd0hm.cloudfront.net/images/NuachtTG4.jpg" alt="Nuacht an Lae" title="Nuacht an Lae" class="prog-img" width="378" height="213">
                <div class="prog-footer">
                    <h4 class="prog-episode-title"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Na scéalta is déanaí...' : 'Latest News...'); ?></h4>
                    <!-- <div class="prog-season">S<span>1</span></div>
                    <div class="prog-episode">E<span>1</span></div> -->
                    <div class="arrow-box"></div>
                </div>
            </a>
        </div>
    </section>
</div>