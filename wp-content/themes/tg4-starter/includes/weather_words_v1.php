<?php
$imageFolder = 'https://d1og0s8nlbd0hm.cloudfront.net/images/Aimsir/focail-aimsire/';
$audioFolder = 'https://d1og0s8nlbd0hm.cloudfront.net/audio/Tearmai_Aimsire/';
$mainTabs = array( 
                array('title_ie' => 'Focail'
                      , 'title_en' => 'Words'
                      , 'linkTo_ie' => 'Focail-Aimsire'
                      , 'linkTo_en' => 'Weather-Words'
                      , 'subTabs' => array(
                                        array(
                                            'title_ie' => 'Connachtach'
                                            , 'title_en' => 'Connacht'
                                            , 'showTitle' => TRUE
                                            , 'contentType' => 'IMAGES'
                                            , 'contents' => array(
                                                                array('image'=>'Scamall.png', 'txt_ie'=>'Scamaill', 'txt_en'=>'Clouds','audio'=>'Tearmai-Connachtach/scamaill.mp3')
                                                                , array('image'=>'Baisteach.png', 'txt_ie'=>'B&aacute;isteach', 'txt_en'=>'Rain','audio'=>'Tearmai-Connachtach/baisteach.mp3')
                                                                , array('image'=>'Ceathanna.png', 'txt_ie'=>'Ceathanna', 'txt_en'=>'Showers','audio'=>'Tearmai-Connachtach/ceathanna.mp3')
                                                                , array('image'=>'Grian.png', 'txt_ie'=>'Grian', 'txt_en'=>'Sun','audio'=>'Tearmai-Connachtach/grian.mp3')
                                                                , array('image'=>'Ceo.png', 'txt_ie'=>'Ceo', 'txt_en'=>'Fog','audio'=>'Tearmai-Connachtach/ceo.mp3')
                                                                , array('image'=>'Flichshneachta.png', 'txt_ie'=>'Flichshneachta', 'txt_en'=>'Hail','audio'=>'Tearmai-Connachtach/flichshneachta.mp3')
                                                                , array('image'=>'1-15.png', 'txt_ie'=>'An Ghealach', 'txt_en'=>'The Moon','audio'=>'Tearmai-Connachtach/gealach.mp3')
                                                                , array('image'=>'Sneachta.png', 'txt_ie'=>'Sneachta', 'txt_en'=>'Snow','audio'=>'Tearmai-Connachtach/sneachta.mp3')
                                                                , array('image'=>'Toirneach.png', 'txt_ie'=>'T&oacute;irneach', 'txt_en'=>'Thunder','audio'=>'Tearmai-Connachtach/toirneach.mp3')
                                                                , array('image'=>'Gaoth.png', 'txt_ie'=>'Gaoth', 'txt_en'=>'Wind','audio'=>'Tearmai-Connachtach/gaoth.mp3')
                                                                , array('image'=>'Ard-Bhru.png', 'txt_ie'=>'Ard Bhr&uacute;', 'txt_en'=>'High Pressure','audio'=>'Tearmai-Connachtach/ardbhru.mp3')
                                                                //, array('image'=>'Lag-Bhru.png', 'txt_ie'=>'Lag Bhr&uacute;', 'txt_en'=>'Low Pressure','audio'=>'Tearmai-Connachtach/.mp3')
                                                            )
                                        )
                                        ,array(
                                            'title_ie' => 'Muimhneach'
                                            , 'title_en' => 'Munster'
                                            , 'showTitle' => TRUE
                                            , 'contentType' => 'IMAGES'
                                            , 'contents' => array(
                                                                array('image'=>'Scamall.png', 'txt_ie'=>'Scamaill', 'txt_en'=>'Clouds','audio'=>'Tearmai-Muimhneach/scamaill.mp3')
                                                                , array('image'=>'Baisteach.png', 'txt_ie'=>'B&aacute;isteach', 'txt_en'=>'Rain','audio'=>'Tearmai-Muimhneach/baisteach.mp3')
                                                                , array('image'=>'Ceathanna.png', 'txt_ie'=>'Ceathanna', 'txt_en'=>'Showers','audio'=>'Tearmai-Muimhneach/ceathanna2.mp3')
                                                                , array('image'=>'Grian.png', 'txt_ie'=>'Grian', 'txt_en'=>'Sun','audio'=>'Tearmai-Muimhneach/grian.mp3')
                                                                , array('image'=>'Ceo.png', 'txt_ie'=>'Ceo', 'txt_en'=>'Fog','audio'=>'Tearmai-Muimhneach/ceo.mp3')
                                                                , array('image'=>'Flichshneachta.png', 'txt_ie'=>'Flichshneachta', 'txt_en'=>'Hail','audio'=>'Tearmai-Muimhneach/flithshneachta.mp3')
                                                                , array('image'=>'1-15.png', 'txt_ie'=>'Gealach', 'txt_en'=>'Moon','audio'=>'Tearmai-Muimhneach/gealach.mp3')
                                                                , array('image'=>'Sneachta.png', 'txt_ie'=>'Sneachta', 'txt_en'=>'Snow','audio'=>'Tearmai-Muimhneach/sneachta.mp3')
                                                                , array('image'=>'Toirneach.png', 'txt_ie'=>'T&oacute;irneach', 'txt_en'=>'Thunder','audio'=>'Tearmai-Muimhneach/toirneach.mp3')
                                                                , array('image'=>'Gaoth.png', 'txt_ie'=>'Gaoth', 'txt_en'=>'Wind','audio'=>'Tearmai-Muimhneach/gaoth.mp3')
                                                                , array('image'=>'Ard-Bhru.png', 'txt_ie'=>'Ard Bhr&uacute;', 'txt_en'=>'High Pressure','audio'=>'Tearmai-Muimhneach/ard.mp3')
                                                                , array('image'=>'Lag-Bhru.png', 'txt_ie'=>'Lag Bhr&uacute;', 'txt_en'=>'Low Pressure','audio'=>'Tearmai-Muimhneach/lag.mp3')
                                                            )
                                        )
                                        ,array(
                                            'title_ie' => 'Ultach'
                                            , 'title_en' => 'Ulster'
                                            , 'showTitle' => TRUE
                                            , 'contentType' => 'IMAGES'
                                            , 'contents' => array(
                                                                array('image'=>'Scamall.png', 'txt_ie'=>'Scamaill', 'txt_en'=>'Clouds','audio'=>'Tearmai-Ultach/scamaill.mp3')
                                                                , array('image'=>'Baisteach.png', 'txt_ie'=>'B&aacute;isteach', 'txt_en'=>'Rain','audio'=>'Tearmai-Ultach/baisteach.mp3')
                                                                , array('image'=>'Ceathanna.png', 'txt_ie'=>'Ceathanna', 'txt_en'=>'Showers','audio'=>'Tearmai-Ultach/Ceathanna.mp3')
                                                                , array('image'=>'Grian.png', 'txt_ie'=>'Grian', 'txt_en'=>'Sun','audio'=>'Tearmai-Ultach/grian.mp3')
                                                                , array('image'=>'Ceo.png', 'txt_ie'=>'Ceo', 'txt_en'=>'Fog','audio'=>'Tearmai-Ultach/ceo.mp3')
                                                                , array('image'=>'Flichshneachta.png', 'txt_ie'=>'Flichshneachta', 'txt_en'=>'Hail','audio'=>'Tearmai-Ultach/flichneachta.mp3')
                                                                , array('image'=>'1-15.png', 'txt_ie'=>'Gealach', 'txt_en'=>'Moon','audio'=>'Tearmai-Ultach/gealach.mp3')
                                                                , array('image'=>'Sneachta.png', 'txt_ie'=>'Sneachta', 'txt_en'=>'Snow','audio'=>'Tearmai-Ultach/sneachta.mp3')
                                                                , array('image'=>'Toirneach.png', 'txt_ie'=>'T&oacute;irneach', 'txt_en'=>'Thunder','audio'=>'Tearmai-Ultach/toirneach.mp3')
                                                                , array('image'=>'Gaoth.png', 'txt_ie'=>'Gaoth', 'txt_en'=>'Wind','audio'=>'Tearmai-Ultach/gaoth.mp3')
                                                                , array('image'=>'Ard-Bhru.png', 'txt_ie'=>'Ard Bhr&uacute;', 'txt_en'=>'High Pressure','audio'=>'Tearmai-Ultach/ardbhru.mp3')
                                                                , array('image'=>'Lag-Bhru.png', 'txt_ie'=>'Lag Bhr&uacute;', 'txt_en'=>'Low Pressure','audio'=>'Tearmai-Ultach/lagbhru.mp3')
                                                            )
                                        )
                                    )
                )
                ,array('title_ie' => 'Nathanna Cainte'
                      , 'title_en' => 'Phrases'
                      , 'linkTo_ie' => 'Nathanna-Cainte-Aimsire'
                      , 'linkTo_en' => 'Weather-Phrases'
                      , 'subTabs' => array(
                                        array(
                                            'title_ie' => 'Connachtach'
                                            , 'title_en' => 'Connacht'
                                            , 'showTitle' => TRUE
                                            , 'contentType' => 'TWO_COLUMN'
                                            , 'contents' => array(
                                                                array('txt_ie'=>'Br&aacute;d&aacute;n b&aacute;ist&iacute;', 'audio'=>'Tearmai-Connachtach/bradain_baisti.mp3')
                                                                , array('txt_ie'=>'Ag d&oacute;irteadh b&aacute;ist&iacute;', 'audio'=>'Tearmai-Connachtach/dortadh_14.mp3')
                                                                , array('txt_ie'=>'Baol seaca', 'audio'=>'Tearmai-Connachtach/baol_seaca.mp3')
                                                                , array('txt_ie'=>'Bhainfeadh an fuacht an craiceann dhuit', 'audio'=>'Tearmai-Connachtach/fuacht_craiceann.mp3')
                                                                , array('txt_ie'=>'Bh&iacute; ceo anuas go dt&iacute; an talamh ann', 'audio'=>'Tearmai-Connachtach/vceo_go_talamh.mp3')
                                                                , array('txt_ie'=>'Brat b&aacute;ist&iacute;', 'audio'=>'Tearmai-Connachtach/brat_baisti.mp3')
                                                                , array('txt_ie'=>'Ce&oacute; tal&uacute;n', 'audio'=>'Tearmai-Connachtach/ceo_talun.mp3')
                                                                , array('txt_ie'=>'Clabhta&iacute;', 'audio'=>'Tearmai-Connachtach/clabhtai.mp3')
                                                                , array('txt_ie'=>'D&eacute;anfaidh s&eacute; sioc go gainnimh rua', 'audio'=>'Tearmai-Connachtach/sioc_gain_rua.mp3')
                                                                , array('txt_ie'=>'Gaoth fheannta', 'audio'=>'Tearmai-Connachtach/gaoth_fheanta.mp3')
                                                                , array('txt_ie'=>'Leac oighir', 'audio'=>'Tearmai-Connachtach/leac_oighir.mp3')
                                                                , array('txt_ie'=>'R&eacute;alta', 'audio'=>'Tearmai-Connachtach/realta.mp3')
                                                                , array('txt_ie'=>'Salachar b&aacute;ist&iacute;', 'audio'=>'Tearmai-Connachtach/salachar.mp3')
                                                                , array('txt_ie'=>'Staid na farraige', 'audio'=>'Tearmai-Connachtach/staid.mp3')
                                                                , array('txt_ie'=>'Stoirm Th&oacute;irn&iacute;', 'audio'=>'Tearmai-Connachtach/stoirm_thoirni.mp3')
                                                                , array('txt_ie'=>'T&aacute; a chos&uacute;lacht air go mairfidh an dea aimsir', 'audio'=>'Tearmai-Connachtach/deaaimsir.mp3')
                                                                , array('txt_ie'=>'Tonnta', 'audio'=>'Tearmai-Connachtach/tonnta.mp3')
                                                                , array('txt_ie'=>'', 'audio'=>'Tearmai-Connachtach/.mp3')
                                                            )
                                        )
                                        ,array(
                                            'title_ie' => 'Muimhneach'
                                            , 'title_en' => 'Munster'
                                            , 'showTitle' => TRUE
                                            , 'contentType' => 'TWO_COLUMN'
                                            , 'contents' => array(
                                                                array('txt_ie'=>'An fharraige ag glas&uacute;', 'audio'=>'Tearmai-Muimhneach/glasu.mp3')
                                                                , array('txt_ie'=>'An ghrian ag scoilteadh na gcloch', 'audio'=>'Tearmai-Muimhneach/scoilteadh.mp3')
                                                                , array('txt_ie'=>'Baol seaca', 'audio'=>'Tearmai-Muimhneach/baol.mp3')
                                                                , array('txt_ie'=>'Brat b&aacute;ist&iacute;', 'audio'=>'Tearmai-Muimhneach/brat.mp3')
                                                                , array('txt_ie'=>'Briseadh idir na scamaill', 'audio'=>'Tearmai-Muimhneach/briseadh.mp3')
                                                                , array('txt_ie'=>'Ceathanna troma', 'audio'=>'Tearmai-Muimhneach/ceathanna.mp3')
                                                                , array('txt_ie'=>'Ceo tal&uacute;n', 'audio'=>'Tearmai-Muimhneach/ceotalun.mp3')
                                                                , array('txt_ie'=>'Chomh fuar le sioc', 'audio'=>'Tearmai-Muimhneach/chomhfuar.mp3')
                                                                , array('txt_ie'=>'Clagarnach bh&aacute;ist&iacute;', 'audio'=>'Tearmai-Muimhneach/clagarnachbhaisti.mp3')
                                                                , array('txt_ie'=>'Farraig&iacute; suaite', 'audio'=>'Tearmai-Muimhneach/farraigisuaite.mp3')
                                                                , array('txt_ie'=>'Gaoth mh&oacute;r', 'audio'=>'Tearmai-Muimhneach/gaothmhor.mp3')
                                                                , array('txt_ie'=>'L&aacute; bre&aacute; brothallach', 'audio'=>'Tearmai-Muimhneach/brothallach.mp3')
                                                                , array('txt_ie'=>'Lagtr&aacute;', 'audio'=>'Tearmai-Muimhneach/lagtra.mp3')
                                                                , array('txt_ie'=>'Leac oighir', 'audio'=>'Tearmai-Muimhneach/leac.mp3')
                                                                , array('txt_ie'=>'Leoithne gaoithe', 'audio'=>'Tearmai-Muimhneach/leoithne.mp3')
                                                                , array('txt_ie'=>'Rabharta', 'audio'=>'Tearmai-Muimhneach/rabharta.mp3')
                                                                , array('txt_ie'=>'Screabhanna', 'audio'=>'Tearmai-Muimhneach/screabha.mp3')
                                                                , array('txt_ie'=>'Sioc cruaidh', 'audio'=>'Tearmai-Muimhneach/sioccruaidh.mp3')
                                                                , array('txt_ie'=>'Sioc liath', 'audio'=>'Tearmai-Muimhneach/siocliath.mp3')
                                                                , array('txt_ie'=>'S&iacute;on', 'audio'=>'Tearmai-Muimhneach/sion.mp3')
                                                                , array('txt_ie'=>'Sp&eacute;ir ghlan', 'audio'=>'Tearmai-Muimhneach/speir.mp3')
                                                                , array('txt_ie'=>'Scr&iacute;b', 'audio'=>'Tearmai-Muimhneach/scrib.mp3')
                                                                , array('txt_ie'=>'Splancanna', 'audio'=>'Tearmai-Muimhneach/splanc.mp3')
                                                                , array('txt_ie'=>'Stoirm thintr&iacute;', 'audio'=>'Tearmai-Muimhneach/stoirm.mp3')
                                                                , array('txt_ie'=>'', 'audio'=>'Tearmai-Muimhneach/.mp3')
                                                                , array('txt_ie'=>'', 'audio'=>'Tearmai-Muimhneach/.mp3')
                                                                , array('txt_ie'=>'', 'audio'=>'Tearmai-Muimhneach/.mp3')
                                                                , array('txt_ie'=>'', 'audio'=>'Tearmai-Muimhneach/.mp3')
                                                                , array('txt_ie'=>'', 'audio'=>'Tearmai-Muimhneach/.mp3')
                                                                , array('txt_ie'=>'', 'audio'=>'Tearmai-Muimhneach/.mp3')
                                                            )
                                        )
                                        ,array(
                                            'title_ie' => 'Ultach'
                                            , 'title_en' => 'Ulster'
                                            , 'showTitle' => TRUE
                                            , 'contentType' => 'TWO_COLUMN'
                                            , 'contents' => array(
                                                                array('txt_ie'=>'Ag pl&uacute;chadh sneachta', 'audio'=>'Tearmai-Ultach/ag_pluchadh_sneachta.mp3')
                                                                , array('txt_ie'=>'Art na gaoithe', 'audio'=>'Tearmai-Ultach/art_na_gaoithe.mp3')
                                                                , array('txt_ie'=>'Beidh b&aacute;is&iacute;n maith fearthainne ar an l&aacute; inniu', 'audio'=>'Tearmai-Ultach/baisin_fearthaine.mp3')
                                                                , array('txt_ie'=>'Beidh l&aacute; leath bhrocach ann', 'audio'=>'Tearmai-Ultach/la_brocach.mp3')
                                                                , array('txt_ie'=>'Beidh l&aacute; stabhlaigh ann', 'audio'=>'Tearmai-Ultach/la_stabhlaigh.mp3')
                                                                , array('txt_ie'=>'Beidh s&eacute; ceathaideach', 'audio'=>'Tearmai-Ultach/Ceathaideach.mp3')
                                                                , array('txt_ie'=>'Beim&iacute;d b&aacute;ite', 'audio'=>'Tearmai-Ultach/baite.mp3')
                                                                , array('txt_ie'=>'Beim&iacute;d in&aacute;r mb&aacute;d&aacute;in', 'audio'=>'Tearmai-Ultach/badain.mp3')
                                                                , array('txt_ie'=>'Clab&aacute;nach', 'audio'=>'Tearmai-Ultach/clabanach.mp3')
                                                                , array('txt_ie'=>'D&eacute;anfaidh s&eacute; sioc', 'audio'=>'Tearmai-Ultach/deanfaidh_sioc.mp3')
                                                                , array('txt_ie'=>'Farraige suaite', 'audio'=>'Tearmai-Ultach/Farraige_suaite.mp3')
                                                                , array('txt_ie'=>'Farraige trom', 'audio'=>'Tearmai-Ultach/Farraige_trom.mp3')
                                                                , array('txt_ie'=>'Gaoth mh&oacute;r', 'audio'=>'Tearmai-Ultach/Gaoth_mhor.mp3')
                                                                , array('txt_ie'=>'Gaoth thirim &uacute;r', 'audio'=>'Tearmai-Ultach/Gaoth_thirim.mp3')
                                                                , array('txt_ie'=>'Iontach doinneanta', 'audio'=>'Tearmai-Ultach/doineanta.mp3')
                                                                , array('txt_ie'=>'L&aacute; brothallach', 'audio'=>'Tearmai-Ultach/la_brothal.mp3')
                                                                , array('txt_ie'=>'L&aacute; iontach gaofar', 'audio'=>'Tearmai-Ultach/la_gaofar.mp3')
                                                                , array('txt_ie'=>'L&aacute; meiribh', 'audio'=>'Tearmai-Ultach/la_meirbh.mp3')
                                                                , array('txt_ie'=>'L&aacute; na b&oacute; duibhe', 'audio'=>'Tearmai-Ultach/la_na_bo_duibhe.mp3')
                                                                , array('txt_ie'=>'Leac oighir', 'audio'=>'Tearmai-Ultach/leacoighear.mp3')
                                                                , array('txt_ie'=>'Milteannach te', 'audio'=>'Tearmai-Ultach/milteanach_te.mp3')
                                                                , array('txt_ie'=>'N&eacute;alta', 'audio'=>'Tearmai-Ultach/nealta.mp3')
                                                                , array('txt_ie'=>'N&iacute;l cos&uacute;lacht ann go nd&eacute;anfaidh s&eacute; toradh', 'audio'=>'Tearmai-Ultach/toradh.mp3')
                                                                , array('txt_ie'=>'N&iacute;l d&eacute;anamh air go stadfaidh an fhearthainn', 'audio'=>'Tearmai-Ultach/nil_deanamh_air.mp3')
                                                                , array('txt_ie'=>'Rachfaidh an aimsir chun donais', 'audio'=>'Tearmai-Ultach/chun_donais.mp3')
                                                                , array('txt_ie'=>'R&eacute;alta', 'audio'=>'Tearmai-Ultach/realta.mp3')
                                                                , array('txt_ie'=>'Saoist&iacute; m&oacute;ra', 'audio'=>'Tearmai-Ultach/saoisti.mp3')
                                                                , array('txt_ie'=>'S&eacute;ide&aacute;in l&aacute;idre gaoithe', 'audio'=>'Tearmai-Ultach/seidean_gaoithe.mp3')
                                                                , array('txt_ie'=>'Sioc', 'audio'=>'Tearmai-Ultach/sioc.mp3')
                                                                , array('txt_ie'=>'Stoirm', 'audio'=>'Tearmai-Ultach/stoirm.mp3')
                                                                , array('txt_ie'=>'T&aacute; bruithne ar an l&aacute; inniu', 'audio'=>'Tearmai-Ultach/buithne.mp3')
                                                                , array('txt_ie'=>'T&aacute; cos&uacute;lacht ar an dea aimsir go mairfidh s&iacute;', 'audio'=>'Tearmai-Ultach/dea_aimsir.mp3')
                                                                , array('txt_ie'=>'T&aacute; stalcas go leor ar an l&aacute;', 'audio'=>'Tearmai-Ultach/stalcas.mp3')
                                                                , array('txt_ie'=>'Teas', 'audio'=>'Tearmai-Ultach/Teas.mp3')
                                                                , array('txt_ie'=>'Teas madr&uacute;il', 'audio'=>'Tearmai-Ultach/Teas_madruil.mp3')
                                                                , array('txt_ie'=>'Tonnta', 'audio'=>'Tearmai-Ultach/tonta.mp3')
                                                                , array('txt_ie'=>'', 'audio'=>'Tearmai-Ultach/.mp3')
                                                                , array('txt_ie'=>'', 'audio'=>'Tearmai-Ultach/.mp3')
                                                                , array('txt_ie'=>'', 'audio'=>'Tearmai-Ultach/.mp3')
                                                                , array('txt_ie'=>'', 'audio'=>'Tearmai-Ultach/.mp3')
                                                                , array('txt_ie'=>'', 'audio'=>'Tearmai-Ultach/.mp3')
                                                            )
                                        )
                                    )
                )
                ,array('title_ie' => 'Seanfhocail'
                      , 'title_en' => 'Proverbs'
                      , 'linkTo_ie' => 'Seanfhocail-Aimsire'
                      , 'linkTo_en' => 'Weather-Proverbs'
                      , 'subTabs' => array(
                                        array(
                                            'title_ie' => ''
                                            , 'title_en' => ''
                                            , 'showTitle' => False
                                            , 'contentType' => 'FULL_WIDTH'
                                            , 'contents' => array(
                                                                array('txt_ie'=>'An ghaoth aduaidh b&iacute;onn s&iacute; crua is cuireann s&iacute; fuacht ar dhaoine,<br/>An ghaoth aneas b&iacute;onn s&iacute; tais is cuireann s&iacute; rath ar sh&iacute;olta,<br/>An ghaoth aniar b&iacute;onn s&iacute; fial is cuireann s&iacute; iasc i l&iacute;onta,<br/>An ghaoth anoir b&iacute;onn s&iacute; tirim is cuireann s&iacute; sioc san o&iacute;che', 'audio'=>'Tearmai-Muimhneach/anghaothaduaidh.mp3')
                                                                , array('txt_ie'=>'Ci&uacute;n&uacute; na ho&iacute;che, buan&uacute; na s&iacute;ne', 'audio'=>'Tearmai-Muimhneach/ciunu.mp3')
                                                                , array('txt_ie'=>'Comhartha na doininne, an o&iacute;che a bheith soineann', 'audio'=>'Tearmai-Muimhneach/comhartha.mp3')
                                                                , array('txt_ie'=>'Is annamh earrach gan fuacht', 'audio'=>'Tearmai-Muimhneach/isannamh.mp3')
                                                                , array('txt_ie'=>'Is ioma&iacute; athr&uacute; a chuireann l&aacute; M&aacute;rta de', 'audio'=>'Tearmai-Connachtach/is_iomai_athru.mp3')
                                                                , array('txt_ie'=>'N&iacute;l tuile d&aacute; mh&eacute;ad nach dtr&aacute;nn', 'audio'=>'Tearmai-Muimhneach/nituille.mp3')
                                                                , array('txt_ie'=>'N&iacute; h&eacute; l&aacute; na gaoithe l&aacute; na scolb', 'audio'=>'Tearmai-Muimhneach/lanagaoithe.mp3')
                                                                , array('txt_ie'=>'M&aacute;rta crua gaofar, Aibre&aacute;n bog braonach', 'audio'=>'Tearmai-Muimhneach/marthacruaidh.mp3?1')
                                                          )
                                        )
                                    )
                )
            );

/*
 * Main Variables used below:
 * [$YAMS_ID] : string : What language version to display
 *      It will also determine what querystring [key] & [value] pairs we will check for to see if
 *      a client is requesting a specific area to be displayed when page loads
 * [$cntMainTabs] : integer : Will store the count of 'Main Tabs'
 * [$cntSubTabs] : integer : Will store the count of 'Sub Tabs' for the 'Main Tab' being processed
 * [$cntContents] : integer : Will store the count of 'Content Blocks' for the 'Sub Tab' being processed
 * [$linkToKey] : array : Language versions of the HREF querystring key that may have a value passed, 
 *      to select a specific 'Main Tab' to be displayed
 * [$requestedTab] : string : The value that may be passed in the [$linkToKey], in the querystring.
 * [$activeTabIdx] : integer : the array index of the 'Main Tab', [$mainTabs], to be shown when page is rendered
 */

//$YAMS_ID = isset($YAMS_ID) ? $YAMS_ID: ''; // LIVE: [$YAMS_ID] passed in for lang versions in SNIPPET
//$YAMS_ID = "en"; // TESTING: [$YAMS_ID] hard coded for local testing

if (ICL_LANGUAGE_CODE == "ga") {
    $YAMS_ID = "ie";
} else {
    $YAMS_ID = "en";
}

$cntMainTabs = count($mainTabs);
$activeTabIdx = 0; // the 1st 'Main Tab' will be shown by default

$linkToKey = array('en'=>'section', 'ie'=>'roinn');

/*
 * 1) Check if a specific section has been requested:
 */
$requestedTab = isset($_GET[$linkToKey[$YAMS_ID]]) ? $_GET[$linkToKey[$YAMS_ID]]: '';

if ($requestedTab !== ''){ //check for a valid request
    $requestedTab = strtolower($requestedTab); // we will ensure string comparisons are all made in lowercase to avoid any upper/lowercase typos
    for ($loopIdx = 0; $loopIdx < $cntMainTabs; $loopIdx++){
        if ($requestedTab === strtolower($mainTabs[$loopIdx]['linkTo_' . $YAMS_ID])){
            $activeTabIdx = $loopIdx;
            break; // match found, exit the 'for' loop
        }
    }
}

/* 
 * 2) Prepare URL for 'Main Tab' links (and for Google Analytics in Client browser)
 */
//$url = $modx->makeUrl($modx->documentIdentifier,"", "", "full"); // LIVE: this gives the ModX URL of the page
$url  = '/programmes/'; // TESTING
if ($YAMS_ID != ""){ // we need to add the YAMS_ID
    //$site = $modx->config['site_url']; // LIVE
    $site = 'http://dev.tg4.ie/'; // TESTING
    $url = str_Replace($site, ($site . $YAMS_ID . '/'), $url);
}
$url .= "?"; // Add the '?' to prepare to add querystring below for each individual 'tab'

/*
 * 3) Prepare output for the audio containers:
 */
$output = '<!-- START: audio containers  --->';
$output .= '<div id="Audio-MediaContainer"></div>   ';
$output .= '<div id="jquery_jplayer_1" class="jp-jplayer"></div>';
$output .= '<div id="jp_container_1" style="display:none;">';
$output .= '    <div class="jp-gui ui-widget">';
$output .= '        <ul>';
$output .= '            <li class="jp-play focail_button"><a href="javascript:;" class="jp-play" tabindex="1" title="play"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/Aimsir/focail-aimsire/audio-play.png" border="0"/></a></li>';
$output .= '            <li class="jp-pause focail_button"><a href="javascript:;" class="jp-pause" tabindex="1" title="pause"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/Aimsir/focail-aimsire/audio-pause.png" border="0"/></a></li>';
$output .= '            <li class="jp-stop focail_button"><a href="javascript:;" class="jp-stop" tabindex="1" title="stop"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/Aimsir/focail-aimsire/audio-stop.png" border="0"/></a></li>';
$output .= '        </ul>';
$output .= '        <div class="jp-progress-slider"></div>';
$output .= '        <div class="jp-current-time"></div>';
$output .= '        <div class="jp-duration"></div>';
$output .= '        <div class="jp-clearboth"></div>';
$output .= '    </div>';
$output .= '    <div class="jp-no-solution">';
$output .= '        <span>Update Required</span>';
$output .= '        To play the media you will need to either update your browser to a recent version or update your <a href="https://get.adobe.com/flashplayer/" target="_New">Flash plugin</a>.';
$output .= '    </div>';
$output .= '</div>';
$output .= '<!-- END: audio containers  --->';

/*
 * 4) Prepare output for the  'Main Tabs' navbar:
 */
$output .= '<div id="focail_menu">';
$output .= '<ul>';

for ($loopIdx = 0; $loopIdx < $cntMainTabs; $loopIdx++){
    if ($loopIdx === $activeTabIdx){
        $css = ' class="navHere"';
    }else{
        $css = '';
    }
    $output .= '    <li' . $css . '><a href="' . $url . $linkToKey[$YAMS_ID] .'=' . urlencode($mainTabs[$loopIdx]['linkTo_' . $YAMS_ID])
            . '" title="' . str_replace('"', '&quot;', $mainTabs[$loopIdx]['title_' . $YAMS_ID]) . '" onclick="return false;">' . $mainTabs[$loopIdx]['title_' . $YAMS_ID] . '</a></li>';
}

$output .= '</ul>';
$output .= '</div>';
$output .= '<div class="clearfix"></div>';

/*
 * 5) Prepare output for the  contents of each 'Main Tabs':
 */
for ($mainTabIdx = 0; $mainTabIdx < $cntMainTabs; $mainTabIdx++){
    if ($mainTabIdx === $activeTabIdx){
        $css = '';
    }else{
        $css = ' style="display:none;"';
    } 
    $output .= '<div class="focail_box"' . $css . '>'; // open the main container to hold all 'sub tabs' for this 'main tab'
    
    
    $arrSubTab = $mainTabs[$mainTabIdx]['subTabs']; // save 'sub tab' array into new array variable for readability of code (slight trade off in memory usage)
    $cntSubTabs = count($arrSubTab);
    $activeSubTabIdx = 0; // the 1st 'Main Tab' will be shown by default
    /*
    * 5.1) First check if there are sub-headers to be shown:
    *       Show first one found as 'selected'. By right if more than one 'sub tab' they should all have a 'header' 
    *       unless further manipulation being done with client-side scripting.
    */
    $subTabHeader = '';
    for ($subTabIdx = 0; $subTabIdx < $cntSubTabs; $subTabIdx++){
        if ($arrSubTab[$subTabIdx]['showTitle'] === true){
            if ($subTabHeader === ''){ // 
                $css = ' class="navHere"';
                $activeSubTabIdx = $subTabIdx;
            }else{
                $css = '';
            }
            $subTabHeader .= '    <li' . $css . '><a href="' . $url . $linkToKey[$YAMS_ID] .'=' . urlencode($mainTabs[$mainTabIdx]['linkTo_' . $YAMS_ID]) 
                . '&' . $linkToKey[$YAMS_ID] .'2=' . urlencode($arrSubTab[$subTabIdx]['title_' . $YAMS_ID])
                . '" title="' . str_replace('"', '&quot;', $arrSubTab[$subTabIdx]['title_' . $YAMS_ID]) . '" onclick="return false;">' . $arrSubTab[$subTabIdx]['title_' . $YAMS_ID] . '</a></li>';
        }
    }
    if($subTabHeader !== ''){
        $subTabHeader = '<div class="focail_submenu"><ul>'
                . $subTabHeader
                . '</ul><div class="clearfix"></div></div>';
    }
    $output .= $subTabHeader;
    /*
    * 5.2) output each 'sub tab' of this 'main tab':
    */
    for ($subTabIdx = 0; $subTabIdx < $cntSubTabs; $subTabIdx++){
        if ($subTabIdx === $activeSubTabIdx){
            $css = '';
        }else{
            $css = ' style="display:none;"';
        }
        $output .= '<div class="focail_dtls"' . $css . '>'; // Open the main container to hold all 'contents' for this 'sub tab'
        /*
        * 5.3) output the 'contents' for this 'sub tab':
        */
        $contentType = $arrSubTab[$subTabIdx]['contentType'];
        $arrContents = $arrSubTab[$subTabIdx]['contents']; // save 'contents' array into new array variable for readability of code (slight trade off in memory usage)
        $cntContents = count($arrContents);
        
        if ($contentType === 'IMAGES'){
            for ($contentIdx = 0; $contentIdx < $cntContents; $contentIdx++){
                if($arrContents[$contentIdx]['image'] === '' || $arrContents[$contentIdx]['txt_ie'] === '' 
                         || $arrContents[$contentIdx]['txt_en'] === '' || $arrContents[$contentIdx]['audio'] === '' ) continue; // missing some data, skip this item
                
                $output .= '        <div class="audio_img">';
                $output .= '            <a href="' . $audioFolder . $arrContents[$contentIdx]['audio'] . '" onclick="return false;">';
                $output .= '            <div class="img"><img src="' . $imageFolder . $arrContents[$contentIdx]['image'] . '" height="92"/></div>';
                $output .= '            <div class="audio_play"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/Aimsir/focail-aimsire/Play-Speaker.png" height="14"/></div>';
                $output .= '            <div class="audio_stop" style="display:none;"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/Aimsir/focail-aimsire/stopPlay.gif" height="14"/></div>';
                $output .= '            <div class="txt_ie">' . $arrContents[$contentIdx]['txt_ie'] . '</div>';
                $output .= '            <div class="txt_en">' . $arrContents[$contentIdx]['txt_en'] . '</div>';
                $output .= '            </a>';
                $output .= '        </div>';
            }
        }elseif ($contentType === 'TWO_COLUMN'){
            for ($contentIdx = 0; $contentIdx < $cntContents; $contentIdx++){
                if($arrContents[$contentIdx]['txt_ie'] === '' || $arrContents[$contentIdx]['audio'] === '' ) continue; // missing some data, skip this item
                
                $output .= '        <div class="audio_img2">';
                $output .= '            <a href="' . $audioFolder . $arrContents[$contentIdx]['audio'] . '" onclick="return false;">';
                $output .= '            <div class="audio_play"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/Aimsir/focail-aimsire/Play-Speaker.png" height="14"/></div>';
                $output .= '            <div class="audio_stop" style="display:none;"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/Aimsir/focail-aimsire/stopPlay.gif" height="14"/></div>';
                $output .= '            <div class="txt_ie">' . $arrContents[$contentIdx]['txt_ie'] . '</div>';
                $output .= '            </a>';
                $output .= '        </div>';
            }
        } else{
            for ($contentIdx = 0; $contentIdx < $cntContents; $contentIdx++){
                if($arrContents[$contentIdx]['txt_ie'] === '' || $arrContents[$contentIdx]['audio'] === '' ) continue; // missing some data, skip this item
                
                $output .= '        <div class="audio_img3">';
                $output .= '            <a href="' . $audioFolder . $arrContents[$contentIdx]['audio'] . '" onclick="return false;">';
                $output .= '            <div class="audio_play"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/Aimsir/focail-aimsire/Play-Speaker.png" height="14"/></div>';
                $output .= '            <div class="audio_stop" style="display:none;"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/Aimsir/focail-aimsire/stopPlay.gif" height="14"/></div>';
                $output .= '            <div class="txt_ie">' . $arrContents[$contentIdx]['txt_ie'] . '</div>';
                $output .= '            <div class="clearfix"></div>';
                $output .= '            </a>';
                $output .= '        </div>';
            }
        }
        
        $output .= '<div class="clearfix"></div></div>'; // Close the main container to hold all 'contents' for this 'sub tab'
    }
    $output .= '</div>'; // Close the main container to hold all 'sub tabs' for this 'main tab'
}
/*
* 6) Add JAvascript function call to start the magic!:
*/
$output .= '<script>initAimsirAudio();</script>'; // 
/*
 * FINALLY) Return the prepared output:
 */
echo $output; // TESTING
//return $output; // LIVE
?>