<div class="tonight-slider-wrap">
	<?php
	//Needed to determine the current time and where to start the slick carousel from - 29/06/15
	$nowDate = date("Y-m-d");
	$d=strtotime("tomorrow");
	$tomorrowDate = date("Y-m-d", $d);
	$recCount = -1;
	$currTime = current_time('timestamp'); // use the selected date and current hh:mm to scroll the slick carousel - based on localtime
	if ($dailySchedules = $wpdb->get_results("SELECT * FROM tg4_site_schedule WHERE fldDate = '$nowDate' ORDER BY fldTime ASC")) {
		foreach ($dailySchedules as $dailySchedule) 
		{ 
			$DBTime = strtotime((($dailySchedule->fldDate) . " " . ($dailySchedule->fldTime)));
			if ($currTime < $DBTime) {
				if ($StartNum == "") {
					$StartNum = $recCount;
				}
			}
			//Need to revisit as Current time is 1 hour behind actual time
			//echo $StartNum . " - " . $recCount . " - " . $currTime . " - " . $DBTime . " - " . ($dailySchedule->fldTime) . "<br />";
			$recCount++;
		}
	}
	?>
	<div class="tonight-slider visuallyhidden" data-slick='{"initialSlide": <?php echo $StartNum; ?>}'>
		<?php
		echo "<!-- StartNum - " . $StartNum . ", RecCnt - " . $recCount . ", CurrTime - " . $currTime . ", DBTime - " . $DBTime . ", FldTime " . ($dailySchedule->fldTime) . " -->";
		//Populate the slick carousel with the daily schedule - 29/06/15
		if ($dailySchedules = $wpdb->get_results("SELECT * FROM tg4_site_schedule WHERE fldDate >= '$nowDate' AND fldDate <= '$tomorrowDate' ORDER BY fldDate ASC, fldTime ASC")) {
			$recOtherCount = 0;
			foreach ($dailySchedules as $dailySchedule) {
				if ($dailySchedule->fldInternational == '1') {
					if (ICL_LANGUAGE_CODE == "ga") {
						$schTitle = 'Domhanda';
					}else{
						$schTitle = 'Worldwide';
					}
				}elseif ($dailySchedule->fldLive == '1') {
					if (ICL_LANGUAGE_CODE == "ga") {
						$schTitle = 'Éirinn Amháin';
					}else{
						$schTitle = 'Ireland Only';
					}
				}else{
					if (ICL_LANGUAGE_CODE == "ga") {
						$schTitle = 'Gan Cearta';
					}else{
						$schTitle = 'Not Available';
					}
				}

				?>
			<div class="tonight-item">
				<a href="<?php echo site_url() . (ICL_LANGUAGE_CODE == "ga" ? '/ga/beo' : '/live'); ?>" class="tonight-link<?php if ($StartNum === $recOtherCount) { echo tg_liveProg($dailySchedule->fldLive); } ?>">
					<h3 class="tonight-title"><?php echo $dailySchedule->fldTitle; ?></h3>
					<p class="tonight-time<?php echo tg_geoIcon($dailySchedule->fldLive, $dailySchedule->fldInternational); ?>" title="<?php echo $schTitle; ?>"><?php echo tg_alterTime($dailySchedule->fldTime); ?></p>
					<div class="tonight-live"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Beo' : 'Live'); ?></div>
					<div class="img-wrap tonight-thumb">
						<!-- <img src="<?php //echo "https://d2njdm9lid19nh.cloudfront.net/" . $dailySchedule->fldImage; ?>" alt="<?php //echo $dailySchedule->fldTitle; ?>" title="<?php //echo $dailySchedule->fldTitle; ?>"> -->
						<img src="<?php echo "https://res.cloudinary.com/tg4/image/upload/w_251,h_141,f_auto,q_auto/" . ($dailySchedule->fldProgramCode ? $dailySchedule->fldProgramCode : $dailySchedule->fldSeriesCode); ?>.jpg" alt="<?php echo $dailySchedule->fldTitle; ?>" title="<?php echo $dailySchedule->fldTitle; ?>">
					</div>
					<div class="btn-wrapper">
						<div class="btn-watch-small"><?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Breathnaigh ar' : 'Watch Live'); ?></div>
						<!-- <div class="btn-info-small"><?php //echo (ICL_LANGUAGE_CODE == "ga" ? 'Eolas' : 'Programme Info'); ?></div> -->
					</div>
				</a>
			</div>
		<?php 
		$recOtherCount++;
		}} ?>
	</div>
</div>