<?php
if ($_GET["date"]) {
	$sceiDate = strval($_GET['date']);
} else {
	$sceiDate = date("Y-m-d");
}

if(stripos($_SERVER['REQUEST_URI'],"ga")){
	switch (date("l", strtotime($sceiDate))) {
		case "Saturday":
			$day_title= "D&eacute; Sathairn";
			break;
		case "Sunday":
	 		$day_title= "D&eacute; Domhnaigh";
			break;
		case "Monday":
	 		$day_title= "D&eacute; Luain";
			break;
		case "Tuesday":
	 		$day_title= "D&eacute; M&aacute;irt";
			break;
		case "Wednesday":
	 		$day_title= "D&eacute; C&eacute;adaoin";
			break;
		case "Thursday":
	 		$day_title= "D&eacute;ardaoin";
			break;
		case "Friday":
	 		$day_title= "D&eacute; hAoine";
			break;
	}

	switch (date("F", strtotime($sceiDate))) {
		case "January":
	 		$month_title= "Ean&aacute;ir";
			break;
	 	case "February":
	 		$month_title= "Feabhra";
			break;
	 	case "March":
	 		$month_title= "M&aacute;rta";
			break;
	 	case "April":
	 		$month_title= "Aibre&aacute;n";
			break;
	 	case "May":
	 		$month_title= "Bealtaine";
			break;
	 	case "June":
	 		$month_title= "Meitheamh";
			break;
	 	case "July":
	 		$month_title= "I&uacute;il";
			break;
	 	case "August":
	 		$month_title= "L&uacute;nasa";
			break;
	 	case "September":
	 		$month_title= "Me&aacute;n F&oacute;mhair";
			break;
	 	case "October":
	 		$month_title= "Deireadh F&oacute;mhair";
			break;
	 	case "November":
	 		$month_title= "Samhain";
			break;
	 	case "December":
	 		$month_title= "Nollaig";
			break;
	}
}

if ($dailySchedules = $wpdb->get_results("SELECT * FROM tg4_site_schedule WHERE fldDate = '$sceiDate' ORDER BY fldTime ASC")) {  ?>
	<h3 class="sched-list-day"><?php echo (ICL_LANGUAGE_CODE == "ga" ? $day_title . date(" d ", strtotime($sceiDate)) . $month_title : date(" l F jS ", strtotime($sceiDate))); ?></h3>
	<ul class="sched-list">
	<?php 
	foreach ($dailySchedules as $dailySchedule) {
		if ($dailySchedule->fldInternational == '1') {
			if (ICL_LANGUAGE_CODE == "ga") {
				$schTitle = 'Domhanda';
			} else {
				$schTitle = 'Worldwide';
			}
		} elseif ($dailySchedule->fldLive == '1') {
			if (ICL_LANGUAGE_CODE == "ga") {
				$schTitle = 'Éirinn Amháin';
			} else {
				$schTitle = 'Ireland Only';
			}
		} else {
			if (ICL_LANGUAGE_CODE == "ga") {
				$schTitle = 'Gan Cearta';
			} else {
				$schTitle = 'Not Available';
			}
		}
	?>
		<li>
			<div class="sched-item-wrap">
				<div class="sched-time MH-sched-item"><span><?php echo tg_alterTime($dailySchedule->fldTime); ?></span></div>
				<!-- <div class="sched-item-img MH-sched-item"><img src="<?php //echo "https://d2njdm9lid19nh.cloudfront.net/" . trim(utf8_encode(utf8_decode($dailySchedule->fldImage))); ?>" alt="<?php //echo $dailySchedule->fldTitle; ?>"></div> -->
				<div class="sched-item-img MH-sched-item"><img src="<?php echo "https://res.cloudinary.com/tg4/image/upload/w_198,h_111,f_auto,q_auto/" . ($dailySchedule->fldProgramCode ? $dailySchedule->fldProgramCode : $dailySchedule->fldSeriesCode); ?>.jpg" alt="<?php echo $dailySchedule->fldTitle; ?>"></div>
				<div class="sched-item-content">
					<h4 class="sched-item-title"><?php echo $dailySchedule->fldTitle; ?><span><?php if ($dailySchedule->fldSeries) { echo (ICL_LANGUAGE_CODE == "ga" ? 'Sraith ' : 'Series '); echo $dailySchedule->fldSeries . ", "; }  if ($dailySchedule->fldEpisode) { echo (ICL_LANGUAGE_CODE == "ga" ? 'Eipeasóid ' : 'Episode '); echo $dailySchedule->fldEpisode; } ?></span><span class="<?php echo tg_geoIcon($dailySchedule->fldLive, $dailySchedule->fldInternational); ?>" title="<?php echo $schTitle; ?>"></span></h4>
					<h5 class="sched-item-sub"><?php echo (ICL_LANGUAGE_CODE == "ga" ? $dailySchedule->fldEpGaeTitle : $dailySchedule->fldEpEngTitle); ?></h5>
					<p class="sched-item-desc"><?php echo (ICL_LANGUAGE_CODE == "ga" ? $dailySchedule->fldGaeText : $dailySchedule->fldEngText); ?></p>
				</div>
			</div>
		</li>
	<?php } ?>
	</ul>
<?php } ?>