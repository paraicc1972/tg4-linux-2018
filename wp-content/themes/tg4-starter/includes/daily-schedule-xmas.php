<?php
error_reporting(E_ERROR);

//echo stripos($_SERVER['REQUEST_URI'],"ga");

/**
 * Daily Schedule - alter times greater than 24 hours
 * POC - 31/09/15
 */
if ($_GET["date"]) {
	$sceiDate = strval($_GET['date']);
} else {
	$sceiDate = date("2018-12-22");
}

if (stripos($_SERVER['REQUEST_URI'],"ga")) {
    switch (date("l", strtotime($sceiDate))) {
        case "Saturday":
            $day_title= "D&eacute; Sathairn";
            break;
        case "Sunday":
            $day_title= "D&eacute; Domhnaigh";
            break;
        case "Monday":
            $day_title= "D&eacute; Luain";
            break;
        case "Tuesday":
            $day_title= "D&eacute; M&aacute;irt";
            break;
        case "Wednesday":
            $day_title= "D&eacute; C&eacute;adaoin";
            break;
        case "Thursday":
            $day_title= "D&eacute;ardaoin";
            break;
        case "Friday":
            $day_title= "D&eacute; hAoine";
            break;
    }

    switch (date("F", strtotime($sceiDate))) {
        case "January":
            $month_title= "Ean&aacute;ir";
            break;
        case "February":
            $month_title= "Feabhra";
            break;
        case "March":
            $month_title= "M&aacute;rta";
            break;
        case "April":
            $month_title= "Aibre&aacute;n";
            break;
        case "May":
            $month_title= "Bealtaine";
            break;
        case "June":
            $month_title= "Meitheamh";
            break;
        case "July":
            $month_title= "I&uacute;il";
            break;
        case "August":
            $month_title= "L&uacute;nasa";
            break;
        case "September":
            $month_title= "Me&aacute;n F&oacute;mhair";
            break;
        case "October":
            $month_title= "Deireadh F&oacute;mhair";
            break;
        case "November":
            $month_title= "Samhain";
            break;
        case "December":
            $month_title= "Nollaig";
            break;
    }
}

$con = mysqli_connect('tg4-linux-cluster1-cluster.cluster-ro-c1hcdfsgywwv.eu-west-1.rds.amazonaws.com','root','Ga1wayman','TG4site-2015');
if (!$con) {
    die('Could not connect: ' . mysqli_error($con));
}

mysqli_select_db($con,"ajax_demo");
$sql="SELECT * FROM tg4_site_schedule WHERE fldDate = '". $sceiDate ."' ORDER by fldTime ASC";
$result = mysqli_query($con,$sql); ?>

<h3 class="sched-list-day"><?php echo (ICL_LANGUAGE_CODE == "ga" ? $day_title . date(" d ", strtotime($sceiDate)) . $month_title : date(" l F jS ", strtotime($sceiDate))); ?></h3>
<ul class="sched-list">

<?php while($row = mysqli_fetch_array($result)) { 

    if ($row['fldInternational'] == '1') {
        if (ICL_LANGUAGE_CODE == "ga") {
            $schTitle = 'Domhanda';
        } else {
            $schTitle = 'Worldwide';
        }
    } elseif ($row['fldLive'] == '1') {
        if (ICL_LANGUAGE_CODE == "ga") {
            $schTitle = 'Éirinn Amháin';
        } else {
            $schTitle = 'Ireland Only';
        }
    } else {
        if (ICL_LANGUAGE_CODE == "ga") {
            $schTitle = 'Gan Cearta';
        } else {
            $schTitle = 'Not Available';
        }
    }
?>
	<li>
		<div class="sched-item-wrap">
			<div class="sched-time MH-sched-item"><span><?php echo tg_alterTime($row['fldTime']); ?></span></div>
			<!-- <div class="sched-item-img MH-sched-item" style="background-image: url(https://placehold.it/197x111);"><img src="<?php //echo "https://d2njdm9lid19nh.cloudfront.net/" . $row['fldImage']; ?>" alt="<?php //echo $row['fldTitle']; ?>"></div> -->
			<div class="sched-item-img MH-sched-item"><img src="<?php echo "https://res.cloudinary.com/tg4/image/upload/w_198,h_111,f_auto,q_auto/" . ($row['fldProgramCode'] ? $row['fldProgramCode'] : $row['fldSeriesCode']); ?>.jpg" alt="<?php echo $row['fldTitle']; ?>"></div>
            <div class="sched-item-content">
				<h4 class="sched-item-title"><?php echo utf8_encode($row['fldTitle']); ?><span><?php if ($row['fldSeries']) { echo (ICL_LANGUAGE_CODE == "ga" ? 'Sraith ' : 'Series '); echo $row['fldSeries'] . ", "; }  if ($row['fldEpisode']) { echo (ICL_LANGUAGE_CODE == "ga" ? 'Eipeasóid ' : 'Episode '); echo $row['fldEpisode']; } ?></span><span class="<?php echo tg_geoIcon($row['fldLive'], $row['fldInternational']); ?>" title="<?php echo $schTitle; ?>"></span></h4>
				<!-- <h5 class="sched-item-sub">Programme subtitle</h5> -->
				<p class="sched-item-desc"><?php echo (ICL_LANGUAGE_CODE == "ga" ? utf8_encode($row['fldGaeText']) : utf8_encode($row['fldEngText'])); ?></p>
			</div>
		</div>
	</li>
<?php }
echo "</ul>";
mysqli_close($con); ?>