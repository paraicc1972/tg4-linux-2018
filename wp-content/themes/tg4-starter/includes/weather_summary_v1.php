<?php
if (ICL_LANGUAGE_CODE == "ga") {
    $YAMS_ID = "ie";
} else {
    $YAMS_ID = "en";
}

$YAMS_ID = isset($YAMS_ID) ? $YAMS_ID: 'ie'; // LIVE: [$YAMS_ID] passed in for lang versions in SNIPPET
//$YAMS_ID = "ie"; // TESTING: [$YAMS_ID] hard coded for local testing

$nowDate = date("Y-m-d"); // get todays date
$numDays = 4; // number of tabs to show

$comVars = array(
    'Today' => array('en'=>'Today', 'ie'=>'Inniu')
    ,'Tomorrow' => array('en'=>'Tomorrow', 'ie'=>'Am&aacute;rach')
    ,'4DayForecast' => array('en'=>'Four day weather forecast', 'ie'=>'R&eacute;amhaisn&eacute;is aimsire cheithre l&aacute;')
    ,'SelectRegion' => array('en'=>'Select your region', 'ie'=>'Roghnaigh do cheantar')
    ,'ForecastFor' => array('en'=>'Weather forecast for', 'ie'=>'R&eacute;amhaisn&eacute;is aimsire i gcomhair')
    ,'SummForecast' => array('en'=>'Summary forecast', 'ie'=>'Achoimre aimsire')
    ,'HighTemp' => array('en'=>'Highest daily temperature', 'ie'=>'Teocht laeth&uacute;il is airde')
    ,'LowTemp' => array('en'=>'Lowest daily temperature', 'ie'=>'Teocht laeth&uacute;il is &iacute;sle')
    ,'HighTag' => array('en'=>'High', 'ie'=>'Ard')
    ,'LowTag' => array('en'=>'Low', 'ie'=>'&Iacute;seal')
    ,'SrchDtls' => array('en'=>'Search by city and area or use a comma to search &lt;<b>city, area</b>&gt;.', 'ie'=>'Cuardaigh de r&eacute;ir baile n&oacute; ceantair, n&oacute; &uacute;s&aacute;id cam&oacute;g le &lt;<b>baile, ceantar</b>&gt; a chuardach.')
);

// the order of weekday in array matches day-of-week PHP éad
$weekDays = array(
    array('en'=>'Sunday', 'ie'=>'D&eacute; Domhnaigh')
    ,array('en'=>'Monday', 'ie'=>'D&eacute; Luain')
    ,array('en'=>'Tuesday', 'ie'=>'D&eacute; M&aacute;irt')
    ,array('en'=>'Wednesday', 'ie'=>'D&eacute; C&eacute;adaoin')
    ,array('en'=>'Thursday', 'ie'=>'D&eacute;ardaoin')
    ,array('en'=>'Friday', 'ie'=>'D&eacute; hAoine')
    ,array('en'=>'Saturday', 'ie'=>'D&eacute; Sathairn')
);

$iconsFolder = "https://d1og0s8nlbd0hm.cloudfront.net/images/Aimsir/weather-icons/"; //LIVE

$icons = array(
    'Cloud'=>array('img'=>'Cloud-1.png', 'en'=>'Cloudy', 'ie'=>'Scamallach')
    ,'Drizzle'=>array('img'=>'Drizzle-1.png', 'en'=>'Drizzle', 'ie'=>'Ceobhr&aacute;n')
    ,'FewShowers'=>array('img'=>'FewShowers-1.png', 'en'=>'Few Showers', 'ie'=>'Be&aacute;gan muir na b&aacute;ist&iacute;')
    ,'Fine'=>array('img'=>'Fine-1.png', 'en'=>'Fine', 'ie'=>'Grianmhar')
    ,'Fog'=>array('img'=>'Fog-1.png', 'en'=>'Fog', 'ie'=>'Ceo')
    ,'Hail'=>array('img'=>'Hail-1.png', 'en'=>'Hail', 'ie'=>'Cloichshneachta')
    ,'PartCloudy'=>array('img'=>'PartCloudy-1.png', 'en'=>'Part Cloudy', 'ie'=>'Cuid Scamallach')
    ,'Rain'=>array('img'=>'Rain-1.png', 'en'=>'Rain', 'ie'=>'B&aacute;isteach')
    ,'Showers'=>array('img'=>'Showers-1.png', 'en'=>'Showers', 'ie'=>'Muir na B&aacute;ist&iacute;')
    ,'Snow'=>array('img'=>'Snow-1.png', 'en'=>'Snow', 'ie'=>'Sneachta')
    ,'Thunder'=>array('img'=>'Thunder-1.png', 'en'=>'Thunder', 'ie'=>'Toirneach')
    ,'Wind'=>array('img'=>'Wind-1.png', 'en'=>'Wind', 'ie'=>'Gaoth')
);

/*
 * If no cookie set (no previous search by user) then randomly select an area for the
 * weather summary from following array
 * (this could be moved to a database table lookup)
 */
$defaultRegions = array();
$defaultRegions[0] = 187; // Lettermore, Galway
$defaultRegions[1] = 96; // Carraroe, Galway
$defaultRegions[2] = 99; // Gaoth Dobhair

// START - Prepare URL for Google Analytics in Client browser
//$wUrl  = 'http://localhost:8000/tg4-restricted/'; // TESTING
$wUrl  = 'https://www.tg4.ie/'; // LIVE
if ($YAMS_ID != "") { // we need to add the YAMS_ID
	$site = 'https://www.tg4.ie/'; // TESTING
	$wUrl = str_Replace($site, ($site . $YAMS_ID . '/'), $wUrl);
}
$wUrl .= "?"; // Add the '?' to prepare to add querystring below for each individual 'tab'
// END - Prepare URL for Google Analytics in Client browser

$cityID = isset($_COOKIE["dpCityID"])? $_COOKIE["dpCityID"]: null; //check if user had previously searched for a city, it is then saved to a cookie
if ($cityID === null) {
    $cityID = $defaultRegions[array_rand($defaultRegions)]; // randomly select from [$defaultRegions]
}
$output = '';

$UseDB = isset($ldb) ? 'LIVE': 'TEST'; // check for version/location of Database to be used for Datapoints look-up

// output for the 'livesearch form'
$output .= '<div id ="dpSrch_container">';
//$output .= '<h1>' . $comVars['SelectRegion'][$YAMS_ID] . '</h1>';
$output .= '<form onsubmit="return false;">';
//if ($UseDB === 'LIVE') {
	$output .= '<input type="hidden" id="ldb"/>';
//}
$output .= '<input type="hidden" id="wLang" value="' . $YAMS_ID . '"/>';
$output .= '<input type="hidden" id="wURL" value="' . $wUrl  . '"/>';
$output .= '<span class="searchText">' . $comVars['SelectRegion'][$YAMS_ID] . '</span><input id="wSrchTxt" type="text"  maxlength="80"/>';
$output .= '</form>';
$output .= '<div id="wSrchList"></div>';
$output .= '<div id="wSrchDtls">' . $comVars['SrchDtls'][$YAMS_ID] . '</div>';
$output .= '</div> ';// get/set area name details <h1> tag
//if ($UseDB === 'LIVE'){
    $mysqli = new mysqli("tg4-linux-cluster1-cluster.cluster-ro-c1hcdfsgywwv.eu-west-1.rds.amazonaws.com", "root", "Ga1wayman", "tg4"); // LIVE: www.tg4.ie
    //$mysqli = new mysqli("localhost", "root", "ga1way", "tg4"); // TEST
    if ($mysqli->connect_errno) { // test DB connection
        return null; // quit, connection failed
    }
    /* Get relevant datapoint name */
    if ($result = $mysqli->query('SELECT c_name_html,a_name_html FROM dp_qry_datapoints_' . $YAMS_ID . ' WHERE id=' . $cityID)) {    
            if ($result->num_rows > 0) {
                    $row = $result->fetch_assoc();
                    $cityText = $row['c_name_html'] . ', ' . $row['a_name_html'];
                    $cityTitleText = str_replace('"', '&quot;', $cityText); // Should never be quotes in city/are name but checking just in case
                    $output .= '<h1 class="weather-summary"  title="' . $cityTitleText . '. &#13;' . $comVars['4DayForecast'][$YAMS_ID] . '.">' 
                                    . $cityText . '</h1>';
            } else {
                $cityID = $defaultRegions[array_rand($defaultRegions)]; // randomly select from [$defaultRegions]
                if ($result = $mysqli->query('SELECT c_name_html,a_name_html FROM dp_qry_datapoints_' . $YAMS_ID . ' WHERE id=' . $cityID)) {    
                    if ($result->num_rows > 0) {
                            $row = $result->fetch_assoc();
                            $cityText = $row['c_name_html'] . ', ' . $row['a_name_html'];
                            $cityTitleText = str_replace('"', '&quot;', $cityText); // Should never be quotes in city/are name but checking just in case
                            $output .= '<h1 class="weather-summary"  title="' . $cityTitleText . '. &#13;' . $comVars['4DayForecast'][$YAMS_ID] . '.">' 
                                            . $cityText . '</h1>';
                    } else {
                            return null; //quit, problem with DB query
                    }
                }
            }
            /* free result set */
            $result->free();
    } else {
            return null; //quit, problem with DB query
    }
    $mysqli->close();
//}

$site_url = "http://77.75.98.42/"; //LIVE
//$site_url = "http://localhost:8000/tg4-restricted/"; //TESTING
// JSON call to get the weather data summaries.
$call = $site_url . 'assets/snippets/tg4/dp.aimsir.summaries.Json.php?id=' . $cityID . '&ldb'; //LIVE
//$call = $site_url . 'wp-content/themes/tg4-starter/assets/php/dp.aimsir.summaries.Json.php?id=' . $cityID; //TESTING
if ($UseDB === 'LIVE') {
	$call .= '&ldb';
}
//echo $call . "<br /><br />";
$data[] = json_decode(file_get_contents($call, true));

/*
 * Format of $data[]:
Array
(
    [0] => stdClass Object
        (
            [days] => Array
                (
                    [0] => stdClass Object
                        (
                            [date] => 2013-08-26
                            [icon] => Rain
                            [mxTmp] => 16
                            [mnTmp] => 12
                            [wndKmhr] => 7
                            [wndCom] => SW
                            [wndDeg] => 210
                        )
                       .... ... ..
                )
        )
)
 * To get 'icon' value of 1st day: $data[0]->days[0]->icon
 */
if($data[0] === null) {
    return null; // should not happen, no data for that id
}

//output the daily summaries
$output .= '<div id="weather-summary">';

for ($indexTab = 1; $indexTab <= $numDays; $indexTab++){
    $weekDayNum = date('w', strtotime($nowDate));
    $weekDay = $weekDays[$weekDayNum][$YAMS_ID];
    $dayCSS = '';
    $dayTitleMid = '';
    if ($indexTab === 1) {
        $dayCSS = 'first';
        $dayTitle = $comVars['Today'][$YAMS_ID];
       //$dayTitleMid = $dayTitle . ', ';
    } elseif ($indexTab === 2) {
        $dayCSS = 'second';
        $dayTitle = $comVars['Tomorrow'][$YAMS_ID];  
        //$dayTitleMid = $dayTitle . ', ';  
    } elseif ($indexTab === 3)  {
        $dayCSS = 'third';
        $dayTitle = $weekDay; 
    } elseif ($indexTab === 4)  {
        $dayCSS = 'fourth';
        $dayTitle = $weekDay; 
    } else {
        $dayTitle = $weekDay; 
    }
    $output .= '<div class="weather-day ' . $dayCSS . '" title="' . $cityTitleText . '. &#13;' . $comVars['ForecastFor'][$YAMS_ID] . ' ' . $dayTitle . ', ' . date("d/m/y", strtotime($nowDate)) . '.">';
    $output .= '    <div class="day-title">' . $dayTitle . '</div>';
    $output .= '    <div class="day-summary" title ="' . $comVars['SummForecast'][$YAMS_ID] . ': ' . $icons[($data[0]->days[$indexTab-1]->icon)][$YAMS_ID] . '"><img src="' . $iconsFolder . $icons[($data[0]->days[$indexTab-1]->icon)]['img'] . '"/></div>';
    $output .= '    <div class="temp-high" title="' . $comVars['HighTemp'][$YAMS_ID] . ': ' . ($data[0]->days[$indexTab-1]->mxTmp) . '&deg; celcius"><span class="highTempLabel">' . $comVars['HighTag'][$YAMS_ID] . '</span><br />';
    $output .= '        ' . ($data[0]->days[$indexTab-1]->mxTmp) . '&deg;C';
    $output .= '    </div>';
    $output .= '    <div class="temp-low" title="' . $comVars['LowTemp'][$YAMS_ID] . ': ' . ($data[0]->days[$indexTab-1]->mnTmp) . '&deg; celcius"><span class="lowTempLabel">' . $comVars['LowTag'][$YAMS_ID] . '</span><br />';
    $output .= '        ' . ($data[0]->days[$indexTab-1]->mnTmp) . '&deg;C';
    $output .= '    </div>';
    $output .= '</div>';
    
    $nowDate = date("Y-m-d", strtotime("+1 day", strtotime($nowDate))); // increment date by one day for next iteration of the loop
}
$output .= '</div><script language="JavaScript" type="text/javascript">wSumm_init();</script><div class="clearfix"></div> ';

echo $output;
?>