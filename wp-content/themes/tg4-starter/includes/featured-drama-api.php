<div class="featv-wrap">
    <!-- minimum of 4 featured videos required -->
    <slick ng-if="pDrama" init-onload="false" slides-to-show="4" data="pDrama" dots="false" settings="slickConfig1" ng-if="slickConfig1Loaded">
        <section class="prog-module" ng-repeat="p in pDrama">
            <div class="prog-slide-wrap">
                <?php
                if (ICL_LANGUAGE_CODE =="ga") {
                    $strLink = site_url() . "/ga/player/baile/?pid=";
                } else {
                    $strLink = site_url() . "/en/player/home/?pid=";
                }
                ?>
                <a href="<?php echo $strLink; ?>{{p.id+'&teideal='+p.custom_fields.title+'&series='+p.custom_fields.seriestitle+'&dlft='+diffDate((p.schedule.ends_at | date : 'yyyy-MM-dd HH:mm:ss'))+''}}" class="prog-panel">
                    <h3 class="prog-title" ng-bind="p.custom_fields.seriestitle"></h3>
                    <!--img ng-cloak ng-src="{{p.images.poster.sources[0].src}}" alt="{{p.custom_fields.seriestitle}}" class="prog-img"-->
                    <img ng-src="{{p.custom_fields.seriesimgurl.length>0?p.custom_fields.seriesimgurl:p.images.poster.sources[0].src}}" alt="{{p.custom_fields.seriestitle}}" class="prog-img">     
                    <div class="prog-footer">
                        <h4 class="prog-episode-title" ng-bind="p.custom_fields.title!=p.custom_fields.seriestitle?p.custom_fields.title:''"></h4>
                        <div class="prog-episode"><span ng-bind="p.custom_fields.date"></span></div>
                        <div class="prog-season">S<span ng-bind="p.custom_fields.series"></span></div>
                        <div class="prog-episode">E<span ng-bind="p.custom_fields.episode"></span></div>
                        <div class="arrow-box"></div>
                    </div>
                </a>
            </div>
        </section>
    </slick>
</div>

<!-- <button type="button" data-role="none" class="featv-prev" aria-label="previous">Previous</button>
<button type="button" data-role="none" class="featv-next" aria-label="next">Next</button> -->