<div class="featv-wrap" ng-app="tg4App" ng-controller="tg4Ctrl">
    <!-- minimum of 4 featured videos required -->
    <slick ng-if="pAll" init-onload="false" slides-to-show="4" data="pAll" dots="false" settings="slickConfig1" ng-if="slickConfig1Loaded">
        <section class="prog-module" ng-repeat="p in pAll">
            <div class="prog-slide-wrap">
                <?php
                if (ICL_LANGUAGE_CODE =="ga") {
                    $strLink = site_url() . "/ga/player/baile/?pid=";
                } else {
                    $strLink = site_url() . "/en/player/home/?pid=";
                }
                ?>
                <a href="<?php echo $strLink; ?>{{p.id+'&teideal='+p.custom_fields.title+'&series='+p.custom_fields.seriestitle+'&dlft='+diffDate((p.schedule.ends_at | date : 'yyyy-MM-dd HH:mm:ss'))+''}}" class="prog-panel">
                    <h3 class="prog-title" ng-bind="p.custom_fields.seriestitle"></h3>
                    <div ng-if="p.custom_fields.p_prodcode.length>0">
                        <img ng-src="https://res.cloudinary.com/tg4/image/upload/w_700,h_395,f_auto,q_auto/{{p.custom_fields.p_prodcode}}.jpg" alt="{{p.custom_fields.seriestitle}}" class="prog-img">
                    </div>
                    <div ng-if="p.custom_fields.s_prodcode.length>0 && !p.custom_fields.p_prodcode.length>0">
                        <img ng-src="https://res.cloudinary.com/tg4/image/upload/w_700,h_395,f_auto,q_auto/{{p.custom_fields.s_prodcode}}.jpg" alt="{{p.custom_fields.seriestitle}}" class="prog-img">
                    </div>
                    <div ng-if="!p.custom_fields.s_prodcode.length>0 && !p.custom_fields.p_prodcode.length>0">
                        <img ng-src="{{p.images.poster.sources[0].src}}" alt="{{p.custom_fields.seriestitle}}" class="prog-img">
                    </div>
                    <!-- <img ng-cloak ng-src="{{p.images.poster.sources[0].src}}" alt="{{p.custom_fields.seriestitle}}" class="prog-img">
                    <img ng-src="{{p.custom_fields.seriesimgurl.length>0?p.custom_fields.seriesimgurl:p.images.poster.sources[0].src}}" alt="{{p.custom_fields.seriestitle}}" class="prog-img"> -->
                    <div class="prog-footer">
                        <h4 class="prog-episode-title" ng-bind="p.custom_fields.title!=p.custom_fields.seriestitle?p.custom_fields.title:''"></h4>
                        <div class="prog-episode"><span ng-bind="p.custom_fields.date"></span></div>
                        <div class="prog-season">S<span ng-bind="p.custom_fields.series"></span></div>
                        <div class="prog-episode">E<span ng-bind="p.custom_fields.episode"></span></div>
                        <div class="arrow-box"></div>
                    </div>
                </a>
            </div>
        </section>
    </slick>
</div>

<script src='https://d1og0s8nlbd0hm.cloudfront.net/js/angular-slick.js'></script>
<script>
'use strict';

var tg4App=angular.module('tg4App',['slickCarousel']);
tg4App.controller('tg4Ctrl', function($scope,$http) {
    
    $scope.current_Lang='<?php echo ICL_LANGUAGE_CODE ?>';

    $scope.diffDate = function(date1) {
      var dateOut1 = new Date(Date.parse(date1, "yyyy-MM-dd HH:mm:ss")); // It will work if date1 is in ISO format
      var dateOut2 = new Date(); // Today's date
      var timeDiff = Math.abs(dateOut1.getTime() - dateOut2.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
      return diffDays;
    };

    //* Check querystring parameters for Sort, Page Count....
    var params={};
    decodeURIComponent(location.search).replace(/[?&]+([^=&]+)=([^&]*)/gi,function(m,k,v){ params[k]=v });

    //====================================
    // Slick 1
    //====================================
    $http.post("<?php echo get_template_directory_uri(); ?>/assets/php/tg4-proxy.php").then(function(response) {
        $scope.tAuth = response.data;
        //console.log($scope.tAuth);
        if ($scope.tAuth) {
            var headerParam = { headers: {
                "Authorization": "Bearer " + $scope.tAuth,
                "Accept": "application/json;odata=verbose"
                }
            };
            <?php if ((($seriesTag) && ($referenceID)) || ($seriesTag)) { ?>
                // Videos by Series
                var endPoint1 = '/videos?q=%2Dseriestitle:"TG4-Beo"+%2Bseriestitle:"<?php echo urlencode($seriesTag); ?>"+-category_c%3ABlaisín+%2Bplayable%3Atrue&limit=12&offset=0&sort=-schedule_starts_at';
                $http.get('https://cms.api.brightcove.com/v1/accounts/1555966122001' + endPoint1, headerParam).then(function(d) {
                    $scope.pAll = d.data;
                    $scope.pCntAll = d.data.length;
                    console.log($scope.pAll, $scope.pCntAll);
                });
            <?php } else if ($categoryTag) { ?>
                // Videos by Category
                var endPoint1 = '/videos?q=%2Dseriestitle:"TG4-Beo"+%2Bcategory_c:"<?php echo urlencode($categoryTag); ?>"+-category_c%3ABlaisín+%2Bplayable%3Atrue&limit=12&offset=0&sort=-schedule_starts_at';
                $http.get('https://cms.api.brightcove.com/v1/accounts/1555966122001' + endPoint1, headerParam).then(function(d) {
                    $scope.pAll = d.data;
                    $scope.pCntAll = d.data.length;
                    console.log($scope.pAll, $scope.pCntAll);
                });
            <?php } else if ($videoTag) { ?>
                // Videos by Tags
                var endPoint1 = '/videos?q=%2D"TG4-Beo"%2Btags:"<?php echo urlencode($videoTag); ?>"+-category_c%3ABlaisín+%2Bplayable%3Atrue&limit=12&offset=0&sort=-schedule_starts_at';
                $http.get('https://cms.api.brightcove.com/v1/accounts/1555966122001' + endPoint1, headerParam).then(function(d) {
                    $scope.pAll = d.data;
                    $scope.pCntAll = d.data.length;
                    console.log($scope.pAll, $scope.pCntAll);
                });
            <?php } else { ?>
                // Featured Videos
                var endPoint1 = '/videos?q=%2Dseriestitle:"TG4-Beo"+%2Btags:"xfeature"+-category_c%3ABlaisín+%2Bplayable%3Atrue&limit=8&offset=0&sort=-schedule_starts_at';
                $http.get('https://cms.api.brightcove.com/v1/accounts/1555966122001' + endPoint1, headerParam).then(function(d) {
                    $scope.pAll = d.data;
                    $scope.pCntAll = d.data.length;
                    console.log($scope.pAll, $scope.pCntAll);
                });
            <?php } ?>
        }
    });

    $scope.slickConfig1Loaded = true;
    $scope.slickConfig1 = {
        method: {},
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
        {
            breakpoint: 1100,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: false
            }
        },
        {
            breakpoint: 938,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 622,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        ]
    };
});
</script>

<!-- <button type="button" data-role="none" class="featv-prev" aria-label="previous">Previous</button>
<button type="button" data-role="none" class="featv-next" aria-label="next">Next</button> -->