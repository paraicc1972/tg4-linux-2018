<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8,IE=9,IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="dns-prefetch" href="https://www.google-analytics.com">
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
        <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png">

        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/main-min.css">
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="https://d1og0s8nlbd0hm.cloudfront.net/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="https://d1og0s8nlbd0hm.cloudfront.net/js/min/main-min.js"></script>

        <?php
        if (is_tree(30206) || is_tree(30397) || is_tree(72457) || is_tree(72866)) { ?>
            <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery-tools/1.2.7/jquery.tools.min.js'></script>
            <script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/js/arconix-shortcodes.min.js?ver=2.1.2'></script>
        <?php } ?>

        <!-- Google Tag Manager -->
        <script>
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-TSCVD6V');
        </script>
        <!-- End Google Tag Manager -->

        <?php 
        /* Main Slider on Homepage and Foghlaim */
        if (is_home() || is_page('foghlaim') || is_page(79666) || is_page(79668)) { ?>
            <script src="https://d1og0s8nlbd0hm.cloudfront.net/js/vendor/jquery.royalslider.min.js"></script>
        <?php }
        /* Main Slider on Homepage and Foghlaim */

        /* Contact Us */
        //if (is_tree(721) || is_tree(724)) { ?>
            <!-- <style>
               #map {
                height: 300px;
                width: 100%;
               }
            </style>
            <script>
              function initMap() {
                var uluru = {lat: 53.237350, lng: -9.494163};
                var map = new google.maps.Map(document.getElementById('map'), {
                  zoom: 10,
                  center: uluru
                });
                var marker = new google.maps.Marker({
                  position: uluru,
                  map: map
                });
              }
            </script>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB03b-ZSTT03h_ZgohM8aPHyZNVQjEZOMU&callback=initMap"></script> -->
        <?php //}
        /* Contact Us */

        /* Vóta2016 */
        if (is_tree(6445) || is_tree(6446)) { ?>
            <script src="https://d1og0s8nlbd0hm.cloudfront.net/js/jquery.vticker.js"></script>
        <?php }

        if (is_page('programmes/vota-2016/national-share/') || is_page('clair/vota-2016/national-share/')) { ?>
            <script async src="https://d1og0s8nlbd0hm.cloudfront.net/js/chart-3.js"></script>
        <?php } 

        if (is_page('programmes/vota-2016/changes-from-2011/') || is_page('clair/vota-2016/changes-from-2011/')) { ?>
            <script async src="https://d1og0s8nlbd0hm.cloudfront.net/js/chart-2.js"></script>
        <?php }
        /* Vóta2016 */

        /* Registration for TG4 Archive */
        if (is_page('membership-registration')) { ?>
            <script async type='text/javascript' src='https://d1og0s8nlbd0hm.cloudfront.net/plugins/swpm-form-builder/js/jquery.validate.min.js?ver=1.9.0'></script>
            <script async type='text/javascript' src='https://d1og0s8nlbd0hm.cloudfront.net/plugins/swpm-form-builder/js/swpm-validation.js?ver=20140412'></script>
            <script async type='text/javascript' src='https://d1og0s8nlbd0hm.cloudfront.net/plugins/swpm-form-builder/js/jquery.metadata.js?ver=2.0'></script>
            <script async type='text/javascript' src='https://d1og0s8nlbd0hm.cloudfront.net/plugins/swpm-form-builder/js/i18n/validate/messages-en_US.js?ver=1.9.0'></script>
        <?php }
        /* Registration for TG4 Archive */
        
        if (is_tree(22890) || is_tree(22893)) { ?>
            <script async type='text/javascript' src='https://d1og0s8nlbd0hm.cloudfront.net/plugins/wonderplugin-slider/engine/wonderpluginsliderskins.js'></script>
            <script async type='text/javascript' src='https://d1og0s8nlbd0hm.cloudfront.net/plugins/wonderplugin-slider/engine/wonderpluginslider.js'></script>
        <?php }
        
        /* Foghlaim */
        if (is_tree(30204)) { ?>        
            <script async type='text/javascript' src='https://d1og0s8nlbd0hm.cloudfront.net/plugins/wonderplugin-audio/engine/wonderpluginaudioskins.js?ver=4.7C'></script>
            <script async type='text/javascript' src='https://d1og0s8nlbd0hm.cloudfront.net/plugins/wonderplugin-audio/engine/wonderpluginaudio.js?ver=4.7C'></script>
            <script type='text/javascript' src='https://d1og0s8nlbd0hm.cloudfront.net/plugins/wonderplugin-tabs/engine/wonderplugin-tabs-engine.js?ver=3.6C'></script>
        <?php }
        /* Foghlaim - Ceachtanna & Dánta */
        //if (is_tree(30206) || is_tree(30397) || is_tree(72457) || is_tree(72866)) { ?>        
            <!-- script type='text/javascript' src='https://d1og0s8nlbd0hm.cloudfront.net/js/jquery/jquery-migrate.min.js'></script> -->
        <?php //}
        /* Foghlaim */

        if (is_page('live/home') ||  is_page('beo/baile') || is_page('live/corn-riada-live') || is_page('beo/corn-riada-beo')) { ?>
            <script async language="JavaScript" type="text/javascript">
                jQuery(document).ready(function(){
                    jQuery(window).resize(function(){
                        playerSize();
                    });
                });
            </script>
        <?php }
        wp_head();
        $homepageGA = "/ga/";
        $homepageEN = "/en/";
        $currentpage = $_SERVER['REQUEST_URI'];

        /* Check for user Geo details, if not set */
        /* if (!isset($geoCode)) {
            tg_geoLoc();
        } */

        if(($homepageGA==$currentpage) || ($homepageEN==$currentpage)) {
            echo "<title>Irish Television Channel, Súil Eile | TG4</title>";
        } else {
            $ancestors = get_post_ancestors($post->ID);
            echo "<title>";
            echo empty($post->post_parent) ? get_the_title($post->ID) : get_the_title($post->ID) . ' | ' . get_the_title($post->post_parent);
            echo empty($ancestors[1]) ? '' : get_the_title($ancestors[1]);
            echo empty($ancestors[2]) ? '' : get_the_title($ancestors[2]);
            echo " | Irish Television Channel, Súil Eile | TG4</title>";
        } ?>

        <!-- DFP Declare Publisher Tags -->
        <script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>

        <script>
            var googletag = googletag || {};
            googletag.cmd = googletag.cmd || [];
          
            googletag.cmd.push(function() {
                googletag.defineSlot('/172054193/StanLead//1234', [728, 90], 'div-gpt-ad-1520424645901-0').addService(googletag.pubads());
                googletag.defineSlot('/172054193/MPU//Side1234', [300, 250], 'div-gpt-ad-1484321672116-0').addService(googletag.pubads());
                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
            });
        </script>

        <!-- Google Search Console Verification -->
        <meta name="google-site-verification" content="SgwAy4fECTlR6eVmPVGTktbR_S0IsqHxG8CIM-TbVhs" />
        
        <!-- Google Analytics Tracking Code -->
        <script async language="JavaScript" type="text/javascript">
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-4024457-9', 'auto');
            ga('send', 'pageview');
        </script>
    </head>
    <body <?php body_class(); ?>>
        <!-- Google Tag Manager (noscript) -->
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TSCVD6V" height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <!-- End Google Tag Manager (noscript) -->

        <a href="#maincontent" class="skip-to-main">Skip to main content</a> 
        <div class="wrapper">
            <header class="header-nav">
                <div class="header-wrapper">
                    <div class="top-bar">
                        <a href="<?php echo (ICL_LANGUAGE_CODE == "ga" ? '/ga/' : '/en/'); ?>" class="logo-home"><img src="https://d1og0s8nlbd0hm.cloudfront.net/images/TG4.png" alt="TG4 Logo" class="logo" height="550" width="201"></a>
                        <div class="header-banner" aria-hidden="true">
                            <!-- /172054193/StanLead//1234 -->
                            <div id='div-gpt-ad-1520424645901-0' style='height:90px; width:728px;'>
                              <script>
                                googletag.cmd.push(function() { 
                                    googletag.display('div-gpt-ad-1520424645901-0');
                                });
                              </script>
                            </div>
                        </div>
                    </div>
                    <div class="languages">
                        <?php icl_post_languages() ?>
                    </div>
                    <a href="" class="search-btn-toggle"></a>
                    <div class="search-wrapper">
                        <div class="search">
                            <form method="get" id="searchform" class="search-form" action="<?php bloginfo('home'); ?>/" autocomplete="off">
                                <input type="search" class="search-input" name="s" id="s" value="" placeholder="<?php echo (ICL_LANGUAGE_CODE == "ga" ? 'Cuardaigh anseo...' : 'Enter your search here...'); ?>">
                                <button type="submit" id="searchsubmit" value="Search" class="search-btn">Search</button>
                            </form>
                        </div>
                    </div>
                    <div class="nav-cover"></div>
                    <a href="#main-menu" class="burger-menu">Menu</a>
                </div>
                <?php include('includes/main-menu.php'); ?>
            </header>
            <main id="maincontent">